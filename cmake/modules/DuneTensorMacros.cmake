include(CheckCXXSymbolExists)
check_cxx_symbol_exists(
  "std::atomic_ref<int>"
  "atomic"
  DUNE_HAVE_CXX_ATOMIC_REF
  )