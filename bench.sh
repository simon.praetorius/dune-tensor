#!/usr/bin/bash

if [ "$#" -ne 1 ]; then
    echo "usage: $(basename "$0") <benchmark_name>"
    exit 1
fi

dir=$(dirname "$0")
benchmark="$1"

configs=("build-clang15" "build-clang18" "build-cxx20-gcc12")
executables_str=""

# check that the benchmarks are compiled
for build in "${configs[@]}"; do
    if [ -d "$dir/$build" ]; then
        if cmake --build "$dir/$build" --target $benchmark; then
            executables_str="${executables_str} $dir/$build/src/$benchmark"
        fi
    fi
done

sudo cpupower frequency-set --governor performance &> /dev/null

# run the benchmarks with all compiler configs
executables=(${executables_str})
for exe in "${executables[@]}"; do
    exe_dir=$(dirname "$exe")
    echo "$exe --benchmark_out_format=json --benchmark_out=$exe.json"
    $exe --benchmark_out_format=json --benchmark_out=$exe.json
done

sudo cpupower frequency-set --governor powersave &> /dev/null