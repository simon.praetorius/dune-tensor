#pragma once

#include <type_traits>
#include <dune/common/dynmatrix.hh>

namespace Dune {

// matrix-matrix product
template <class F>
auto mat_mult1 (DynamicMatrix<F> const& a, DynamicMatrix<F> const& b)
{
  DynamicMatrix<F> c(a.N(),b.M(),0.0);
  for (int i = 0; i < a.N(); ++i)
    for (int j = 0; j < b.M(); ++j)
      for (int k = 0; k < b.N(); ++k)
        c[i][j] += a[i][k] * b[k][j];
  return c;
}

// matrix-matrix product
template <class F>
auto mat_mult2 (DynamicMatrix<F> const& a, DynamicMatrix<F> const& b)
{
  DynamicMatrix<F> c(a.N(),b.M(),0.0);
  for (int j = 0; j < b.M(); ++j)
    for (int i = 0; i < a.N(); ++i)
      for (int k = 0; k < b.N(); ++k)
        c[i][j] += a[i][k] * b[k][j];
  return c;
}

// matrix-matrix product
template <class F>
auto mat_mult3 (DynamicMatrix<F> const& a, DynamicMatrix<F> const& b)
{
  DynamicMatrix<F> c(a.N(),b.M(),0.0);
  for (int k = 0; k < b.N(); ++k)
    for (int i = 0; i < a.N(); ++i)
      for (int j = 0; j < b.M(); ++j)
        c[i][j] += a[i][k] * b[k][j];
  return c;
}

// matrix-matrix product
template <class F>
auto mat_mult4 (DynamicMatrix<F> const& a, DynamicMatrix<F> const& b)
{
  DynamicMatrix<F> c(a.N(),b.M(),0.0);
  for (int k = 0; k < b.N(); ++k)
    for (int j = 0; j < b.M(); ++j)
      for (int i = 0; i < a.N(); ++i)
        c[i][j] += a[i][k] * b[k][j];
  return c;
}

// matrix-matrix product
template <class F>
auto mat_mult5 (DynamicMatrix<F> const& a, DynamicMatrix<F> const& b)
{
  DynamicMatrix<F> c(a.N(),b.M(),0.0);
  for (int i = 0; i < a.N(); ++i)
    for (int k = 0; k < b.N(); ++k)
      for (int j = 0; j < b.M(); ++j)
        c[i][j] += a[i][k] * b[k][j];
  return c;
}

// matrix-matrix product
template <class F>
auto mat_mult6 (DynamicMatrix<F> const& a, DynamicMatrix<F> const& b)
{
  DynamicMatrix<F> c(a.N(),b.M(),0.0);
  for (int j = 0; j < a.N(); ++j)
    for (int k = 0; k < b.N(); ++k)
      for (int i = 0; i < b.M(); ++i)
        c[i][j] += a[i][k] * b[k][j];
  return c;
}



} // end namespace Dune
