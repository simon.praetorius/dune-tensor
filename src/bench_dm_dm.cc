#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include <iostream>
#include <dune/common/dynmatrix.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/test/testsuite.hh>

#include <benchmark/benchmark.h>

#include "matmult.hh"

using namespace Dune;

static constexpr std::size_t N = 4;


template <class Matrix>
static void fillMatrix (Matrix& mat)
{
  for (int i = 0; i < mat.N(); ++i) {
    mat[i][i] = 2.0;
    if (i > 0) {
      mat[i][i-1] = -1.0;
      mat[i-1][i] = -1.0;
    }
    if (i < mat.N()-1) {
      mat[i][i+1] = -1.0;
      mat[i+1][i] = -1.0;
    }
  }
}


static void BM_mat_mult_ijk(benchmark::State& state) {
  Dune::DynamicMatrix<double> mat1(N,N,0.0), mat2(N,N,0.0);
  fillMatrix(mat1);
  fillMatrix(mat2);
  for (auto _ : state) {
    auto result = mat_mult1(mat1,mat2);
    benchmark::DoNotOptimize(result);
  }
}
BENCHMARK(BM_mat_mult_ijk);


static void BM_mat_mult_jik(benchmark::State& state) {
  Dune::DynamicMatrix<double> mat1(N,N,0.0), mat2(N,N,0.0);
  fillMatrix(mat1);
  fillMatrix(mat2);
  for (auto _ : state) {
    auto result = mat_mult2(mat1,mat2);
    benchmark::DoNotOptimize(result);
  }
}
BENCHMARK(BM_mat_mult_jik);


static void BM_mat_mult_kij(benchmark::State& state) {
  Dune::DynamicMatrix<double> mat1(N,N,0.0), mat2(N,N,0.0);
  fillMatrix(mat1);
  fillMatrix(mat2);
  for (auto _ : state) {
    auto result = mat_mult3(mat1,mat2);
    benchmark::DoNotOptimize(result);
  }
}
BENCHMARK(BM_mat_mult_kij);


static void BM_mat_mult_kji(benchmark::State& state) {
  Dune::DynamicMatrix<double> mat1(N,N,0.0), mat2(N,N,0.0);
  fillMatrix(mat1);
  fillMatrix(mat2);
  for (auto _ : state) {
    auto result = mat_mult4(mat1,mat2);
    benchmark::DoNotOptimize(result);
  }
}
BENCHMARK(BM_mat_mult_kji);


static void BM_mat_mult_ikj(benchmark::State& state) {
  Dune::DynamicMatrix<double> mat1(N,N,0.0), mat2(N,N,0.0);
  fillMatrix(mat1);
  fillMatrix(mat2);
  for (auto _ : state) {
    auto result = mat_mult5(mat1,mat2);
    benchmark::DoNotOptimize(result);
  }
}
BENCHMARK(BM_mat_mult_ikj);


static void BM_mat_mult_jki(benchmark::State& state) {
  Dune::DynamicMatrix<double> mat1(N,N,0.0), mat2(N,N,0.0);
  fillMatrix(mat1);
  fillMatrix(mat2);
  for (auto _ : state) {
    auto result = mat_mult5(mat1,mat2);
    benchmark::DoNotOptimize(result);
  }
}
BENCHMARK(BM_mat_mult_jki);

BENCHMARK_MAIN();
