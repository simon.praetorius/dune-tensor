#pragma once

#include <dune/tensor/operations/sparsetensordot.hh>

namespace Dune::Tensor::Impl {

// matrix-vector product
template <class V1, class S1, std::size_t m1, std::size_t m2, class L1, class C1, class T1,
          class V2, int m, class Tensor3>
void sparsetensordot (SparseTensor<V1,Std::extents<S1,m1,m2>,L1,C1,T1> const& a,
                      FieldTensor<V2,m> const& b, Tensor3& c, Index<1>)
{
  for (auto&& [a_i, i] : Dune::sparseRange(major_of(a)))
    for (auto&& [a_ij, j] : Dune::sparseRange(a_i))
      c(i) += a_ij * b(j);
}

// tensor-vector product
template <class V1, class S1, std::size_t m0, std::size_t m1, std::size_t m2, class L1, class C1, class T1,
          class V2, int m, class Tensor3>
void sparsetensordot (SparseTensor<V1,Std::extents<S1,m0,m1,m2>,L1,C1,T1> const& a,
                      FieldTensor<V2,m> const& b, Tensor3& c, Index<1>)
{
  for (auto&& [a_i, i] : Dune::sparseRange(major_of(a)))
    for (auto&& [a_ij, j] : Dune::sparseRange(a_i))
      for (auto&& [a_ijk, k] : Dune::sparseRange(a_ij))
        c(i,j) += a_ijk * b(k);
}


}