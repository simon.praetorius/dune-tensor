#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include <iostream>
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/test/testsuite.hh>

#include <dune/tensor/csftensor.hh>
#include <dune/tensor/ftensor.hh>
#include <dune/tensor/sparsetensor.hh>
#include <dune/tensor/test/matrixoperations.hh>

#include <benchmark/benchmark.h>

#include "sparsetensordot.hh"
#include "tensordot.hh"

using namespace Dune;
using namespace Dune::Tensor;

static constexpr std::size_t N = 32;
using result_t = double;

template <class Tensor1, class Tensor2>
static void fillTensors (Tensor1& mat1, Tensor2& mat2)
{
  auto ins1 = mat1.inserter();
  auto ins2 = mat2.inserter();
  for (std::size_t i = 0; i < N; ++i) {
    ins1(i,i) = 2.0;
    ins2(i) = 3.0;
  }
}

template <class Matrix, class Vector>
static void fillMatVec (Matrix& mat, Vector& vec)
{
  for (std::size_t i = 0; i < N; ++i) {
    mat[i][i] = 2.0;
    vec[i] = 3.0;
  }
}


static void BM_tensordot_ft_ft(benchmark::State& state) {
  FieldTensor<result_t,N,N> mat;
  FieldTensor<result_t,N> vec;
  fillTensors(mat,vec);
  for (auto _ : state) {
    auto result = tensordot<1>(mat,vec);
    benchmark::DoNotOptimize(result.container_data());
  }
}
BENCHMARK(BM_tensordot_ft_ft);


static void BM_tensordot_fm_fv(benchmark::State& state) {
  Dune::FieldMatrix<result_t,N,N> mat;
  Dune::FieldVector<result_t,N> vec;
  fillMatVec(mat,vec);
  for (auto _ : state) {
    Dune::FieldVector<result_t,N> result;
    mat.mv(vec, result);
    benchmark::DoNotOptimize(result.data());
  }
}
BENCHMARK(BM_tensordot_fm_fv);


static void BM_tensordot_out_ft_ft(benchmark::State& state) {
  FieldTensor<result_t,N,N> mat;
  FieldTensor<result_t,N> vec;
  fillTensors(mat,vec);
  for (auto _ : state) {
    FieldTensor<result_t,N> result;
    Dune::Tensor::Impl::tensordot(mat,vec,result,Index<1>{});
    benchmark::DoNotOptimize(result.container_data());
  }
}
BENCHMARK(BM_tensordot_out_ft_ft);


static void BM_sparsetensordot_st_ft(benchmark::State& state) {
  CSFTensor<result_t,Std::extents<int,N,N>,N> mat;
  FieldTensor<result_t,N> vec;
  fillTensors(mat,vec);
  for (auto _ : state) {
    auto result = sparsetensordot<1>(mat,vec);
    benchmark::DoNotOptimize(result.container_data());
  }
}
BENCHMARK(BM_sparsetensordot_st_ft);


static void BM_sparsetensordot_out_st_ft(benchmark::State& state) {
  CSFTensor<result_t,Std::extents<int,N,N>,N> mat;
  FieldTensor<result_t,N> vec;
  fillTensors(mat,vec);
  for (auto _ : state) {
    FieldTensor<result_t,N> result;
    Dune::Tensor::Impl::sparsetensordot(mat,vec,result,Index<1>{});
    benchmark::DoNotOptimize(result.container_data());
  }
}
BENCHMARK(BM_sparsetensordot_out_st_ft);


static void BM_sparsetensordot_ft_ft(benchmark::State& state) {
  FieldTensor<result_t,N,N> mat;
  FieldTensor<result_t,N> vec;
  fillTensors(mat,vec);
  for (auto _ : state) {
    auto result = sparsetensordot<1>(mat,vec);
    benchmark::DoNotOptimize(result.container_data());
  }
}
BENCHMARK(BM_sparsetensordot_ft_ft);


BENCHMARK_MAIN();
