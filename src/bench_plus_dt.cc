#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include <functional>
#include <iostream>

#include <dune/common/fmatrix.hh>
#include <dune/common/dynmatrix.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/test/testsuite.hh>

#include <dune/tensor/dyntensor.hh>
#include <dune/tensor/ftensor.hh>
#include <dune/tensor/utility/algorithm.hh>

#include <benchmark/benchmark.h>

#include "filltensor.hh"

using namespace Dune;
using namespace Dune::Tensor;

static constexpr std::size_t N = 3;

static void BM_foreach_left_dt(benchmark::State& state) {
  DynamicTensor<double,3> ten1{N,N,N},ten2{N,N,N};
  fillTensor(ten1);
  fillTensor(ten2);
  for (auto _ : state) {
    DynamicTensor<double,3> ten_out{N,N,N};
    tensorTransform(ten1,ten2,ten_out,std::plus<>{});
    benchmark::DoNotOptimize(ten_out.container_data());
  }
}
BENCHMARK(BM_foreach_left_dt);

static void BM_foreach_right_dt(benchmark::State& state) {
  DynamicTensor<double,3> ten1{N,N,N},ten2{N,N,N};
  fillTensor(ten1);
  fillTensor(ten2);
  for (auto _ : state) {
    DynamicTensor<double,3> ten_out{N,N,N};
    tensorTransform(ten1,ten2,ten_out,std::plus<>{},Std::identity{},Std::identity{},tag::right);
    benchmark::DoNotOptimize(ten_out.container_data());
  }
}
BENCHMARK(BM_foreach_right_dt);

static void BM_span_dt(benchmark::State& state) {
  DynamicTensor<double,3> ten1{N,N,N},ten2{N,N,N};
  fillTensor(ten1);
  fillTensor(ten2);
  for (auto _ : state) {
    DynamicTensor<double,3> ten_out{N,N,N};
    auto values1 = ten1.values();
    auto values2 = ten2.values();
    auto values_out = ten_out.values();
    for (std::size_t i = 0; i < values1.size(); ++i)
      values_out[i] = values1[i] + values2[i];
    benchmark::DoNotOptimize(ten_out.container_data());
  }
}
BENCHMARK(BM_span_dt);

static void BM_span2_dt(benchmark::State& state) {
  DynamicTensor<double,3> ten1{N,N,N},ten2{N,N,N};
  fillTensor(ten1);
  fillTensor(ten2);
  for (auto _ : state) {
    DynamicTensor<double,3> ten_out(ten1);
    auto values2 = ten2.values();
    auto values_out = ten_out.values();
    for (std::size_t i = 0; i < values2.size(); ++i)
      values_out[i] += values2[i];
    benchmark::DoNotOptimize(ten_out.container_data());
  }
}
BENCHMARK(BM_span2_dt);

static void BM_nested_loop_dt(benchmark::State& state) {
  DynamicTensor<double,3> ten1{N,N,N},ten2{N,N,N};
  fillTensor(ten1);
  fillTensor(ten2);
  for (auto _ : state) {
    DynamicTensor<double,3> ten_out{N,N,N};
    for (int i = 0; i < N; ++i)
      for (int j = 0; j < N; ++j)
        for (int k = 0; k < N; ++k)
        ten_out(i,j,k) = ten1(i,j,k) + ten2(i,j,k);

    benchmark::DoNotOptimize(ten_out.container_data());
  }
}
BENCHMARK(BM_nested_loop_dt);

static void BM_flat_loop_dt(benchmark::State& state) {
  DynamicTensor<double,3> ten1{N,N,N},ten2{N,N,N};
  fillTensor(ten1);
  fillTensor(ten2);
  for (auto _ : state) {
    DynamicTensor<double,3> ten_out{N,N,N};
    auto ten1_data = ten1.container_data();
    auto ten2_data = ten2.container_data();
    auto ten_out_data = ten_out.container_data();
    for (int i = 0; i < N*N*N; ++i)
      ten_out_data[i] = ten1_data[i] + ten2_data[i];

    benchmark::DoNotOptimize(ten_out_data);
  }
}
BENCHMARK(BM_flat_loop_dt);

// static void BM_plus_dune_dm(benchmark::State& state) {
//   Dune::DynamicMatrix<double> ten1{N,N},ten2{N,N};
//   fillDuneMatrix(ten1);
//   fillDuneMatrix(ten2);
//   for (auto _ : state) {
//     Dune::DynamicMatrix<double> ten_out = ten1;
//     ten_out += ten2;
//     benchmark::DoNotOptimize(&ten_out[0]);
//   }
// }
// BENCHMARK(BM_plus_dune_dm);

BENCHMARK_MAIN();
