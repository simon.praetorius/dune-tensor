#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include <iostream>
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/test/testsuite.hh>

#include <dune/tensor/ftensor.hh>
#include <dune/tensor/dyntensor.hh>

#include <benchmark/benchmark.h>


using namespace Dune;
using namespace Dune::Tensor;

static constexpr std::size_t N = 4;
using result_t = double;

using VectorType = DynamicTensor<result_t,1>;
using MatrixType = DynamicTensor<result_t,2>;
using TensorType = DynamicTensor<result_t,3>;

template <class Tensor>
void _fillTensor1 (Tensor& ten)
{
  static_assert(Tensor::rank() == 3);
  assert(ten.extent(0) == ten.extent(1));
  assert(ten.extent(0) == ten.extent(2));
  for (typename Tensor::index_type i = 0; i < ten.extent(0); ++i) {
    ten(i,i,i) = 2.0;
    if (i > 0) {
      ten(i,i,i-1) = -1.0;
      ten(i,i-1,i) = -1.0;
      ten(i-1,i,i) = -1.0;
    }
    if (i < ten.extent(0)-1) {
      ten(i,i,i+1) = -1.0;
      ten(i,i+1,i) = -1.0;
      ten(i+1,i,i) = -1.0;
    }
  }
}
template <class Tensor>
void _fillTensor2 (Tensor& ten)
{
  static_assert(Tensor::rank() == 3);
  assert(ten.extent(0) == ten.extent(1));
  assert(ten.extent(0) == ten.extent(2));
  for (typename Tensor::index_type i = 0; i < ten.extent(0); ++i) {
    ten[i][i][i] = 2.0;
    if (i > 0) {
      ten[i][i][i-1] = -1.0;
      ten[i][i-1][i] = -1.0;
      ten[i-1][i][i] = -1.0;
    }
    if (i < ten.extent(0)-1) {
      ten[i][i][i+1] = -1.0;
      ten[i][i+1][i] = -1.0;
      ten[i+1][i][i] = -1.0;
    }
  }
}
template <class Tensor>
void _fillTensor3 (Tensor& ten)
{
  static_assert(Tensor::rank() == 3);
  assert(ten.extent(0) == ten.extent(1));
  assert(ten.extent(0) == ten.extent(2));
  for (typename Tensor::index_type i = 0; i < ten.extent(0); ++i) {
    ten[std::array{i,i,i}] = 2.0;
    if (i > 0) {
      ten[std::array{i,i,i-1}] = -1.0;
      ten[std::array{i,i-1,i}] = -1.0;
      ten[std::array{i-1,i,i}] = -1.0;
    }
    if (i < ten.extent(0)-1) {
      ten[std::array{i,i,i+1}] = -1.0;
      ten[std::array{i,i+1,i}] = -1.0;
      ten[std::array{i+1,i,i}] = -1.0;
    }
  }
}

template <class Matrix>
void _fillMatrix1 (Matrix& mat)
{
  static_assert(Matrix::rank() == 2);
  assert(mat.extent(0) == mat.extent(1));
  for (typename Matrix::index_type i = 0; i < mat.extent(0); ++i) {
    mat(i,i) = 2.0;
    if (i > 0) {
      mat(i,i-1) = -1.0;
      mat(i-1,i) = -1.0;
    }
    if (i < mat.extent(0)-1) {
      mat(i,i+1) = -1.0;
      mat(i+1,i) = -1.0;
    }
  }
}

template <class Matrix>
void _fillMatrix2 (Matrix& mat)
{
  static_assert(Matrix::rank() == 2);
  assert(mat.extent(0) == mat.extent(1));
  for (typename Matrix::index_type i = 0; i < mat.extent(0); ++i) {
    mat[i][i] = 2.0;
    if (i > 0) {
      mat[i][i-1] = -1.0;
      mat[i-1][i] = -1.0;
    }
    if (i < mat.extent(0)-1) {
      mat[i][i+1] = -1.0;
      mat[i+1][i] = -1.0;
    }
  }
}

template <class Matrix>
void _fillMatrix3 (Matrix& mat)
{
  static_assert(Matrix::rank() == 2);
  assert(mat.extent(0) == mat.extent(1));
  for (typename Matrix::index_type i = 0; i < mat.extent(0); ++i) {
    mat[std::array{i,i}] = 2.0;
    if (i > 0) {
      mat[std::array{i,i-1}] = -1.0;
      mat[std::array{i-1,i}] = -1.0;
    }
    if (i < mat.extent(0)-1) {
      mat[std::array{i,i+1}] = -1.0;
      mat[std::array{i+1,i}] = -1.0;
    }
  }
}

template <class Vector>
void _fillVector1 (Vector& vec)
{
  static_assert(Vector::rank() == 1);
  for (typename Vector::index_type i = 0; i < vec.extent(0); ++i) {
    vec(i) = i+1.0;
  }
}

template <class Vector>
void _fillVector2 (Vector& vec)
{
  static_assert(Vector::rank() == 1);
  for (typename Vector::index_type i = 0; i < vec.extent(0); ++i) {
    vec[i] = i+1.0;
  }
}

template <class Vector>
void _fillVector3 (Vector& vec)
{
  static_assert(Vector::rank() == 1);
  for (typename Vector::index_type i = 0; i < vec.extent(0); ++i) {
    vec[std::array{i}] = i+1.0;
  }
}


static void BM_access_round_brackets(benchmark::State& state) {
  VectorType vec{typename VectorType::extents_type{N}};
  MatrixType mat{typename MatrixType::extents_type{N,N}};
  TensorType ten{typename TensorType::extents_type{N,N,N}};
  for (auto _ : state) {
    _fillVector1(vec);
    benchmark::DoNotOptimize(vec.container_data());
    _fillMatrix1(mat);
    benchmark::DoNotOptimize(mat.container_data());
    _fillTensor1(ten);
    benchmark::DoNotOptimize(ten.container_data());
  }
}
BENCHMARK(BM_access_round_brackets);


static void BM_access_square_brackets(benchmark::State& state) {
  VectorType vec{typename VectorType::extents_type{N}};
  MatrixType mat{typename MatrixType::extents_type{N,N}};
  TensorType ten{typename TensorType::extents_type{N,N,N}};
  for (auto _ : state) {
    _fillVector2(vec);
    benchmark::DoNotOptimize(vec.container_data());
    _fillMatrix2(mat);
    benchmark::DoNotOptimize(mat.container_data());
    _fillTensor2(ten);
    benchmark::DoNotOptimize(ten.container_data());
  }
}
BENCHMARK(BM_access_square_brackets);


static void BM_access_array(benchmark::State& state) {
  VectorType vec{typename VectorType::extents_type{N}};
  MatrixType mat{typename MatrixType::extents_type{N,N}};
  TensorType ten{typename TensorType::extents_type{N,N,N}};
  for (auto _ : state) {
    _fillVector3(vec);
    benchmark::DoNotOptimize(vec.container_data());
    _fillMatrix3(mat);
    benchmark::DoNotOptimize(mat.container_data());
    _fillTensor3(ten);
    benchmark::DoNotOptimize(ten.container_data());
  }
}
BENCHMARK(BM_access_array);


BENCHMARK_MAIN();
