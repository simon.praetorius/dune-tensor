#pragma once

#include <cassert>
#include <cstddef>

template <class Tensor>
static void fillTensor5 (Tensor& ten)
{
  static_assert(Tensor::rank() == 5);
  assert(ten.extent(0) == ten.extent(1));
  assert(ten.extent(0) == ten.extent(2));
  assert(ten.extent(0) == ten.extent(3));
  assert(ten.extent(0) == ten.extent(4));
  auto ins = ten.inserter();
  for (typename Tensor::index_type i = 0; i < ten.extent(0); ++i) {
    ins(i,i,i,i,i) = 2.0;
    if (i > 0) {
      ins(i,i,i,i,i-1) = -1.0;
      ins(i,i,i,i-1,i) = -1.0;
      ins(i,i,i-1,i,i) = -1.0;
      ins(i,i-1,i,i,i) = -1.0;
      ins(i-1,i,i,i,i) = -1.0;
    }
    if (i < ten.extent(0)-1) {
      ins(i,i,i,i,i+1) = -1.0;
      ins(i,i,i,i+1,i) = -1.0;
      ins(i,i,i+1,i,i) = -1.0;
      ins(i,i+1,i,i,i) = -1.0;
      ins(i+1,i,i,i,i) = -1.0;
    }
  }
}

template <class Tensor>
static void fillTensor4 (Tensor& ten)
{
  static_assert(Tensor::rank() == 4);
  assert(ten.extent(0) == ten.extent(1));
  assert(ten.extent(0) == ten.extent(2));
  assert(ten.extent(0) == ten.extent(3));
  auto ins = ten.inserter();
  for (typename Tensor::index_type i = 0; i < ten.extent(0); ++i) {
    ins(i,i,i,i) = 2.0;
    if (i > 0) {
      ins(i,i,i,i-1) = -1.0;
      ins(i,i,i-1,i) = -1.0;
      ins(i,i-1,i,i) = -1.0;
      ins(i-1,i,i,i) = -1.0;
    }
    if (i < ten.extent(0)-1) {
      ins(i,i,i,i+1) = -1.0;
      ins(i,i,i+1,i) = -1.0;
      ins(i,i+1,i,i) = -1.0;
      ins(i+1,i,i,i) = -1.0;
    }
  }
}

template <class Tensor>
static void fillTensor3 (Tensor& ten)
{
  static_assert(Tensor::rank() == 3);
  assert(ten.extent(0) == ten.extent(1));
  assert(ten.extent(0) == ten.extent(2));
  auto ins = ten.inserter();
  for (typename Tensor::index_type i = 0; i < ten.extent(0); ++i) {
    ins(i,i,i) = 2.0;
    if (i > 0) {
      ins(i,i,i-1) = -1.0;
      ins(i,i-1,i) = -1.0;
      ins(i-1,i,i) = -1.0;
    }
    if (i < ten.extent(0)-1) {
      ins(i,i,i+1) = -1.0;
      ins(i,i+1,i) = -1.0;
      ins(i+1,i,i) = -1.0;
    }
  }
}

template <class Tensor>
static void fillTensor2 (Tensor& ten)
{
  static_assert(Tensor::rank() == 2);
  assert(ten.extent(0) == ten.extent(1));
  auto ins = ten.inserter();
  for (typename Tensor::index_type i = 0; i < ten.extent(0); ++i) {
    ins(i,i) = 2.0;
    if (i > 0) {
      ins(i,i-1) = -1.0;
      ins(i-1,i) = -1.0;
    }
    if (i < ten.extent(0)-1) {
      ins(i,i+1) = -1.0;
      ins(i+1,i) = -1.0;
    }
  }
}

template <class Tensor>
static void fillDuneMatrix (Tensor& ten)
{
  assert(ten.N() == ten.M());
  for (std::size_t i = 0; i < ten.N(); ++i) {
    ten[i][i] = 2.0;
    if (i > 0) {
      ten[i][i-1] = -1.0;
      ten[i-1][i] = -1.0;
    }
    if (i < ten.N()-1) {
      ten[i][i+1] = -1.0;
      ten[i+1][i] = -1.0;
    }
  }
}

template <class Tensor>
static void fillTensor1 (Tensor& ten)
{
  static_assert(Tensor::rank() == 1);
  auto ins = ten.inserter();
  for (typename Tensor::index_type i = 0; i < ten.extent(0); ++i) {
    ins(i) = i+1.0;
  }
}


template <class Tensor>
static void fillTensor (Tensor& ten)
{
  if constexpr(Tensor::rank() == 1)
    fillTensor1(ten);
  else if constexpr(Tensor::rank() == 2)
    fillTensor2(ten);
  else if constexpr(Tensor::rank() == 3)
    fillTensor3(ten);
  else if constexpr(Tensor::rank() == 4)
    fillTensor4(ten);
  else if constexpr(Tensor::rank() == 5)
    fillTensor5(ten);
  else {
    assert(false && "Don't know how to fill the tenstor!");
  }
}