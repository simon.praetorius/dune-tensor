#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include <iostream>
#include <dune/common/diagonalmatrix.hh>
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/test/testsuite.hh>

#include <dune/tensor/csftensor.hh>
#include <dune/tensor/diagonaltensor.hh>
#include <dune/tensor/dyntensor.hh>
#include <dune/tensor/ftensor.hh>
#include <dune/tensor/sparsetensor.hh>
#include <dune/tensor/test/matrixoperations.hh>

#include <benchmark/benchmark.h>

#include "sparsetensordot.hh"
#include "tensordot.hh"

using namespace Dune;
using namespace Dune::Tensor;

static constexpr std::size_t N = 4;
using result_t = double;

template <class Tensor1, class Tensor2>
static void fillTensors (Tensor1& mat1, Tensor2& mat2)
{
  auto ins1 = mat1.inserter();
  auto ins2 = mat2.inserter();
  for (std::size_t i = 0; i < N; ++i) {
    ins1(i,i) = 2.0;
    ins2(i,i) = 3.0;
  }
}

template <class Matrix1, class Matrix2>
static void fillMatMat (Matrix1& mat1, Matrix2& mat2)
{
  for (std::size_t i = 0; i < N; ++i) {
    mat1[i][i] = 2.0;
    mat2[i][i] = 3.0;
  }
}

static void BM_tensordot_ft_ft(benchmark::State& state) {
  Dune::Tensor::FieldTensor<result_t,N,N> mat1, mat2;
  fillTensors(mat1,mat2);
  for (auto _ : state) {
    Dune::Tensor::FieldTensor<result_t,N,N> result = tensordot<1>(mat1,mat2);
    benchmark::DoNotOptimize(result.container_data());
  }
}
BENCHMARK(BM_tensordot_ft_ft);


static void BM_tensordot_ft_dt(benchmark::State& state) {
  Dune::Tensor::FieldTensor<result_t,N,N> mat1;
  Dune::Tensor::DiagonalMatrix<result_t,N> mat2;
  fillTensors(mat1,mat2);
  for (auto _ : state) {
    Dune::Tensor::FieldTensor<result_t,N,N> result;
    Dune::Tensor::Impl::tensordot(mat1,mat2,result,Index<1>{});
    benchmark::DoNotOptimize(result.container_data());
  }
}
BENCHMARK(BM_tensordot_ft_dt);

static void BM_sparsetensordot_ft_ft(benchmark::State& state) {
  Dune::Tensor::FieldTensor<result_t,N,N> mat1, mat2;
  fillTensors(mat1,mat2);
  for (auto _ : state) {
    FieldTensor<result_t,N,N> result = sparsetensordot<1>(mat1,mat2);
    benchmark::DoNotOptimize(result.container_data());
  }
}
BENCHMARK(BM_sparsetensordot_ft_ft);

static void BM_sparsetensordot_ft_dt(benchmark::State& state) {
  Dune::Tensor::FieldTensor<result_t,N,N> mat1;
  Dune::Tensor::DiagonalMatrix<result_t,N> mat2;
  fillTensors(mat1,mat2);
  for (auto _ : state) {
    Dune::Tensor::FieldTensor<result_t,N,N> result = sparsetensordot<1>(mat1,mat2);
    benchmark::DoNotOptimize(result.container_data());
  }
}
BENCHMARK(BM_sparsetensordot_ft_dt);



static void BM_mult_fm_fm(benchmark::State& state) {
  Dune::FieldMatrix<result_t,N,N> mat1, mat2;
  fillMatMat(mat1,mat2);
  for (auto _ : state) {
    Dune::FieldMatrix<result_t,N,N> result = mat1 * mat2;
    benchmark::DoNotOptimize(&result[0]);
  }
}
BENCHMARK(BM_mult_fm_fm);

static void BM_mult_fm_dm(benchmark::State& state) {
  Dune::FieldMatrix<result_t,N,N> mat1;
  Dune::DiagonalMatrix<result_t,N> mat2;
  fillMatMat(mat1,mat2);
  for (auto _ : state) {
    Dune::FieldMatrix<result_t,N,N> result = mat1 * mat2;
    benchmark::DoNotOptimize(&result[0]);
  }
}
BENCHMARK(BM_mult_fm_dm);


static void BM_tensordot_dt_dt(benchmark::State& state) {
  DynamicTensor<result_t,2> mat1(N,N), mat2(N,N);
  fillTensors(mat1,mat2);
  for (auto _ : state) {
    auto result = tensordot<1>(mat1,mat2);
    benchmark::DoNotOptimize(result.container_data());
  }
}
BENCHMARK(BM_tensordot_dt_dt);


static void BM_sparsetensordot_st_st(benchmark::State& state) {
  CSFTensor<result_t,Std::extents<int,N,N>,N> mat1, mat2;
  fillTensors(mat1,mat2);
  for (auto _ : state) {
    FieldTensor<result_t,N,N> result = sparsetensordot<1>(mat1,mat2);
    benchmark::DoNotOptimize(result.container_data());
  }
}
BENCHMARK(BM_sparsetensordot_st_st);


BENCHMARK_MAIN();
