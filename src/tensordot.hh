#pragma once

#include <dune/tensor/operations/tensordot.hh>

namespace Dune::Tensor::Impl {

// general tensor dot c += a * b
template <class T1, class T2, class T3, std::size_t N>
void tensordot (T1 const& a, T2 const& b, T3& c, Index<N>)
{
  RecursiveLoop<0,T1::rank()-N,0,T2::rank()-N,0,N>::apply(MultAdd{},a,b,c);
}

// outer vector product
template <class F1, int m, class F2, int n, class F3>
void tensordot (FieldTensor<F1,m> const& a, FieldTensor<F2,n> const& b, FieldTensor<F3,m,n>& c, Index<0>)
{
  for (int i = 0; i < m; ++i)
    for (int j = 0; j < n; ++j)
      c(i,j) += a(i) * b(j);
}

// inner vector product
template <class F1, int n, class F2, class F3>
void tensordot (FieldTensor<F1,n> const& a, FieldTensor<F2,n> const& b, FieldTensor<F3>& c, Index<1>)
{
  for (int i = 0; i < n; ++i)
    c() += a(i) * b(i);
}

// outer matrix product
template <class F1, int m1, int m2, class F2, int n1, int n2, class F3>
void tensordot (FieldTensor<F1,m1,m2> const& a, FieldTensor<F2,n1,n2> const& b, FieldTensor<F3,m1,m2,n1,n2>& c, Index<0>)
{
  for (int i1 = 0; i1 < m1; ++i1)
    for (int i2 = 0; i2 < m2; ++i2)
      for (int j1 = 0; j1 < n1; ++j1)
        for (int j2 = 0; j2 < n2; ++j2)
          c(i1,i2,j1,j2) += a(i1,i2) * b(j1,j2);
}

// matrix-matrix product
template <class F1, int m1, int m2, class F2, int n2, class F3>
void tensordot (FieldTensor<F1,m1,m2> const& a, FieldTensor<F2,m2,n2> const& b, FieldTensor<F3,m1,n2>& c, Index<1>)
{
  for (int i1 = 0; i1 < m1; ++i1)
    for (int j2 = 0; j2 < n2; ++j2)
      for (int k = 0; k < m2; ++k)
        c(i1,j2) += a(i1,k) * b(k,j2);
}

// inner matrix product
template <class F1, int m1, int m2, class F2, class F3>
void tensordot (FieldTensor<F1,m1,m2> const& a, FieldTensor<F2,m1,m2> const& b, FieldTensor<F3>& c, Index<2>)
{
  for (int i1 = 0; i1 < m1; ++i1)
    for (int i2 = 0; i2 < m2; ++i2)
      c() += a(i1,i2) * b(i1,i2);
}


// outer matrix-vector product
template <class F1, int m1, int m2, class F2, int n, class F3>
void tensordot (FieldTensor<F1,m1,m2> const& a, FieldTensor<F2,n> const& b, FieldTensor<F3,m1,m2,n>& c, Index<0>)
{
  for (int i1 = 0; i1 < m1; ++i1)
    for (int i2 = 0; i2 < m2; ++i2)
      for (int j = 0; j < n; ++j)
        c(i1,i2,j) += a(i1,i2) * b(j);
}

// outer vector-matrix product
template <class F1, int m, class F2, int n1, int n2, class F3>
void tensordot (FieldTensor<F1,m> const& a, FieldTensor<F2,n1,n2> const& b, FieldTensor<F3,m,n1,n2>& c, Index<0>)
{
  for (int i = 0; i < m; ++i)
    for (int j1 = 0; j1 < n1; ++j1)
      for (int j2 = 0; j2 < n2; ++j2)
        c(i,j1,j2) += a(i) * b(j1,j2);
}

// matrix-vector product
template <class F1, int m1, int m2, class F2, class F3>
void tensordot (FieldTensor<F1,m1,m2> const& a, FieldTensor<F2,m2> const& b, FieldTensor<F3,m1>& c, Index<1>)
{
  for (int i1 = 0; i1 < m1; ++i1)
    for (int k = 0; k < m2; ++k)
      c(i1) += a(i1,k) * b(k);
}

// vector-matrix product
template <class F1, int m1, class F2, int n2, class F3>
void tensordot (FieldTensor<F1,m1> const& a, FieldTensor<F2,m1,n2> const& b, FieldTensor<F3,n2>& c, Index<1>)
{
  for (int k = 0; k < m1; ++k)
    for (int j2 = 0; j2 < n2; ++j2)
      c(j2) += a(k) * b(k,j2);
}

} // end namespace Dune::Tensor::Impl
