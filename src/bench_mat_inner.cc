#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include <iostream>
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/std/extents.hh>
#include <dune/common/test/testsuite.hh>

#include <dune/tensor/csftensor.hh>
#include <dune/tensor/diagonaltensor.hh>
#include <dune/tensor/dyntensor.hh>
#include <dune/tensor/ftensor.hh>
#include <dune/tensor/sparsetensor.hh>
#include <dune/tensor/test/matrixoperations.hh>

#include <benchmark/benchmark.h>

#include "sparsetensordot.hh"
#include "tensordot.hh"

using namespace Dune;
using namespace Dune::Tensor;

static constexpr std::size_t N = 8;
using result_t = double;

template <class Tensor1, class Tensor2>
static void fillTensors (Tensor1& mat1, Tensor2& mat2)
{
  auto ins1 = mat1.inserter();
  auto ins2 = mat2.inserter();
  for (std::size_t i = 0; i < N; ++i) {
    ins1(i,i) = 2.0;
    ins2(i,i) = 3.0;
  }
}

template <class Matrix1, class Matrix2>
static void fillMatMat (Matrix1& mat1, Matrix2& mat2)
{
  for (std::size_t i = 0; i < N; ++i) {
    mat1[i][i] = 2.0;
    mat2[i][i] = 3.0;
  }
}


static void BM_tensordot_ft_ft(benchmark::State& state) {
  FieldTensor<result_t,N,N> mat1, mat2;
  fillTensors(mat1,mat2);
  for (auto _ : state) {
    result_t result = tensordot<2>(mat1,mat2);
    benchmark::DoNotOptimize(result);
  }
}
BENCHMARK(BM_tensordot_ft_ft);


static void BM_inner_ft_ft(benchmark::State& state) {
  FieldTensor<result_t,N,N> mat1, mat2;
  fillTensors(mat1,mat2);
  for (auto _ : state) {
    result_t result = inner(mat1,mat2);
    benchmark::DoNotOptimize(result);
  }
}
BENCHMARK(BM_inner_ft_ft);


static void BM_inner_fm_fm(benchmark::State& state) {
  Dune::FieldMatrix<result_t,N,N> mat1, mat2;
  fillMatMat(mat1,mat2);
  for (auto _ : state) {
    result_t result = 0;
    for (std::size_t i = 0; i < N; ++i)
      for (std::size_t j = 0; j < N; ++j)
        result += mat1[i][j] * mat2[i][j];
    benchmark::DoNotOptimize(result);
  }
}
BENCHMARK(BM_inner_fm_fm);


static void BM_tensordot_dt_dt(benchmark::State& state) {
  DynamicTensor<result_t,2> mat1{N,N}, mat2{N,N};
  fillTensors(mat1,mat2);
  for (auto _ : state) {
    result_t result = tensordot<2>(mat1,mat2);
    benchmark::DoNotOptimize(result);
  }
}
BENCHMARK(BM_tensordot_dt_dt);


static void BM_inner_dt_dt(benchmark::State& state) {
  DynamicTensor<result_t,2> mat1{N,N}, mat2{N,N};
  fillTensors(mat1,mat2);
  for (auto _ : state) {
    result_t result = inner(mat1,mat2);
    benchmark::DoNotOptimize(result);
  }
}
BENCHMARK(BM_inner_dt_dt);


static void BM_sparsetensordot_st_st(benchmark::State& state) {
  CSFTensor<result_t,Std::extents<int,N,N>,N> mat1, mat2;
  fillTensors(mat1,mat2);
  for (auto _ : state) {
    result_t result = sparsetensordot<2>(mat1,mat2);
    benchmark::DoNotOptimize(result);
  }
}
BENCHMARK(BM_sparsetensordot_st_st);


static void BM_inner_st_st(benchmark::State& state) {
  CSFTensor<result_t,Std::extents<int,N,N>,N> mat1, mat2;
  fillTensors(mat1,mat2);
  for (auto _ : state) {
    result_t result = inner(mat1,mat2);
    benchmark::DoNotOptimize(result);
  }
}
BENCHMARK(BM_inner_st_st);


static void BM_inner_diag_diag(benchmark::State& state) {
  DiagonalTensor<result_t,Std::extents<int,N,N>> mat1, mat2;
  fillTensors(mat1,mat2);
  for (auto _ : state) {
    result_t result = inner(mat1,mat2);
    benchmark::DoNotOptimize(result);
  }
}
BENCHMARK(BM_inner_diag_diag);

BENCHMARK_MAIN();
