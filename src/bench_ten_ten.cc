#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include <iostream>
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/test/testsuite.hh>

#include <dune/tensor/dyntensor.hh>
#include <dune/tensor/ftensor.hh>

#include <benchmark/benchmark.h>

#include "filltensor.hh"

using namespace Dune;
using namespace Dune::Tensor;

static constexpr int N = 4;

static void BM_FieldTensor_FieldTensor(benchmark::State& state) {
  FieldTensor<double,N,N,N,N,N> ten1;
  FieldTensor<double,N,N,N,N> ten2;
  fillTensor(ten1);
  fillTensor(ten2);
  for (auto _ : state) {
    auto result = tensordot<3>(ten1,ten2);
    benchmark::DoNotOptimize(result.container_data());
  }
}
BENCHMARK(BM_FieldTensor_FieldTensor);

static void BM_DynamicTensor_DynamicTensor(benchmark::State& state) {
  DynamicTensor<double,5> ten1(N,N,N,N,N);
  DynamicTensor<double,4> ten2(N,N,N,N);
  fillTensor(ten1);
  fillTensor(ten2);
  for (auto _ : state) {
    auto result = tensordot<3>(ten1,ten2);
    benchmark::DoNotOptimize(result.container_data());
  }
}
BENCHMARK(BM_DynamicTensor_DynamicTensor);

BENCHMARK_MAIN();
