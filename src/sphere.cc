#include <config.h>

#include <dune/common/std/layout_left.hh>
#include <dune/common/std/layout_right.hh>

#include <dune/tensor/densetensor.hh>
#include <dune/tensor/derivativetraits.hh>
#include <dune/tensor/ftensor.hh>
#include <dune/tensor/extents/tensortype.hh>
#include <dune/tensor/operations/operations.hh>
#include <dune/tensor/utility/abs.hh>

using namespace Dune::Tensor;

template <class T>
void printType (T const&)
{
  std::cout << __PRETTY_FUNCTION__ << std::endl;
}

template <class Value, int... types>
using FixedSizeTensor = DenseTensor<Value, TensorType<int,types...>, Dune::Std::layout_right, FixedSizeContainer>;

template <class V, int... m, int n>
struct Dune::Tensor::DerivativeTraits<FixedSizeTensor<V,m...>(FixedSizeTensor<V,n>)>
{
  static_assert(n >= 0);
  using Range = FixedSizeTensor<V,m...,-n>;
};

template <class Tensor, int i = 0, int j = 1>
struct TransposedTraits;

template <class V, int m, int n>
struct TransposedTraits<FixedSizeTensor<V,m,n>,0,1>
{
  using type = FixedSizeTensor<V,n,m>;
};

template <int m, int n>
FixedSizeTensor<double,m,n> delta ()
{
  static_assert(Impl::abs(m) == Impl::abs(n));
  FixedSizeTensor<double,m,n> eye;
  for (int i = 0; i < eye.extent(0); ++i)
    eye(i,i) = 1;
  return eye;
}

template <int m, int n>
double det (FixedSizeTensor<double,m,n> const& a)
{
  static_assert(Impl::abs(m) == Impl::abs(n));
  static_assert(Impl::abs(m) == 2);
  return a(0,0)*a(1,1) - a(0,1)*a(1,0);
}

template <class Tensor>
struct InverseTraits;

template <class V, int n>
struct InverseTraits<FixedSizeTensor<V,n,n>>
{
  using type = FixedSizeTensor<V,-n,-n>;
};

template <int m, int n>
FixedSizeTensor<double,-n,-m> inverse (FixedSizeTensor<double,m,n> const& a)
{
  static_assert(Impl::abs(m) == Impl::abs(n));
  static_assert(Impl::abs(m) == 2);
  auto aInv = FixedSizeTensor<double,-n,-m>({{a(1,1), -a(0,1)}, {-a(1,0),  a(0,0)}});
  return aInv/det(a);
}

template <int i, int m, int n>
auto raise (FixedSizeTensor<double,m,n> a)
{
  if constexpr(i == 0)
    return FixedSizeTensor<double,Impl::abs(m),n>(std::move(a));
  else if constexpr(i == 1)
    return FixedSizeTensor<double,m,Impl::abs(n)>(std::move(a));
  else
    return a;
}

template <int i, int m, int n>
auto lower (FixedSizeTensor<double,m,n> a)
{
  if constexpr(i == 0)
    return FixedSizeTensor<double,-Impl::abs(m),n>(std::move(a));
  else if constexpr(i == 1)
    return FixedSizeTensor<double,m,-Impl::abs(n)>(std::move(a));
  else
    return a;
}

struct SphereGeometry
{
  using LocalCoordinate = FixedSizeTensor<double, 2>;
  using GlobalCoordinate = FixedSizeTensor<double, 3>;
  using Jacobian = typename DerivativeTraits<GlobalCoordinate(LocalCoordinate)>::Range;
  using JacobianTransposed = typename TransposedTraits<Jacobian>::type;

  using TangentVector = FixedSizeTensor<double, 3>;
  using MetricTensor = FixedSizeTensor<double,-2,-2>;
  using MetricTensorInv = typename InverseTraits<MetricTensor>::type;

  GlobalCoordinate global (LocalCoordinate const& x) const
  {
    using std::sin; using std::cos;
    return GlobalCoordinate({cos(x[0]), sin(x[0])*cos(x[1]), sin(x[0])*sin(x[1])});
  }

  LocalCoordinate local (GlobalCoordinate const& y) const
  {
    return LocalCoordinate({std::acos(y[0]), std::atan2(y[2],y[1])});
  }

  Jacobian jacobian (LocalCoordinate const& x) const
  {
    using std::sin; using std::cos;
    return Jacobian({
      {-sin(x[0]), 0.0},
      { cos(x[0])*cos(x[1]), -sin(x[0])*sin(x[1])},
      { cos(x[0])*sin(x[1]),  sin(x[0])*cos(x[1])},
    });
  }

  JacobianTransposed jacobianTransposed (LocalCoordinate const& x) const
  {
    // NOTE: this should be implemented with a library function
    using std::sin; using std::cos;
    return JacobianTransposed({
       {-sin(x[0]), cos(x[0])*cos(x[1]), cos(x[0])*sin(x[1])},
       {0.0,       -sin(x[0])*sin(x[1]), sin(x[0])*cos(x[1])}
    });
  }

  auto g (LocalCoordinate const& x) const
  {
    // The product with additional index lowering and transposed should be implemented directly
    return dot(jacobianTransposed(x), lower<0>(jacobian(x)));
  }

  auto gInv (LocalCoordinate const& x) const
  {
    return inverse(g(x));
  }

  double inner (LocalCoordinate const& x, TangentVector const& u, TangentVector const& v) const
  {
    return g(x)(u,v);
  }

  double integrationElement (LocalCoordinate const& x) const
  {
    return std::sqrt(det(g(x)));
  }
};



int main()
{
  SphereGeometry sphere;

  SphereGeometry::LocalCoordinate x{0.5,0.5};
  std::cout << "x = " << x << std::endl;

  auto y = sphere.global(x);
  std::cout << "y = " << y << std::endl;
  std::cout << "|y| = " << two_norm(y) << std::endl;

  auto g = sphere.g(x);
  std::cout << "g = " << g << std::endl;
  printType(g);
  std::cout << "det(g) = " << det(g) << std::endl;
  auto gInv = sphere.gInv(x);
  std::cout << "gInv = " << gInv << std::endl;
  printType(gInv);
  std::cout << "inv(g) = " << inverse(g) << std::endl;
  std::cout << "g*gInv = " << dot(g,gInv) << std::endl;
}
