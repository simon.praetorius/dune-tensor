/* begin dune-tensor
   put the definitions for config.h specific to
   your project here. Everything above will be
   overwritten
*/

/* begin private */
/* Name of package */
#define PACKAGE "@DUNE_MOD_NAME@"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "@DUNE_MAINTAINER@"

/* Define to the full name of this package. */
#define PACKAGE_NAME "@DUNE_MOD_NAME@"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "@DUNE_MOD_NAME@ @DUNE_MOD_VERSION@"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "@DUNE_MOD_NAME@"

/* Define to the home page for this package. */
#define PACKAGE_URL "@DUNE_MOD_URL@"

/* Define to the version of this package. */
#define PACKAGE_VERSION "@DUNE_MOD_VERSION@"

/* end private */

/* Define to the version of dune-tensor */
#define DUNE_TENSOR_VERSION "@DUNE_TENSOR_VERSION@"

/* Define to the major version of dune-tensor */
#define DUNE_TENSOR_VERSION_MAJOR @DUNE_TENSOR_VERSION_MAJOR@

/* Define to the minor version of dune-tensor */
#define DUNE_TENSOR_VERSION_MINOR @DUNE_TENSOR_VERSION_MINOR@

/* Define to the revision of dune-tensor */
#define DUNE_TENSOR_VERSION_REVISION @DUNE_TENSOR_VERSION_REVISION@

/* Enable DUNE_CONCEPTS only of the compiler supports concepts */
#if HAVE_DUNE_CONCEPTS && DUNE_HAVE_CXX_UNEVALUATED_CONTEXT_LAMBDA
  #if __has_include(<version>) && __has_include(<concepts>)
    #include <version>
    #if  __cpp_concepts >= 201907L && __cpp_lib_concepts >= 202002L
      #define ENABLE_DUNE_CONCEPTS 1
    #endif
  #endif
#endif

/* does the standard library provide atomic_ref() ? */
#cmakedefine DUNE_HAVE_CXX_ATOMIC_REF 1


/* end dune-tensor
   Everything below here will be overwritten
*/
