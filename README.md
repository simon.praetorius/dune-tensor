# Tensor Data structures

This modules defines flexible dense and sparse tensor data structures. The generic
classes `DenseTensor` and `SparseTensor` allow to configure the extents, index layout,
and storage types. Several specializations are provided, introduced in the following.

The implementation is based on the proposals [mdarray](https://www.open-std.org/jtc1/sc22/wg21/docs/papers/2022/p1684r3.html)
and [mdspan](https://www.open-std.org/jtc1/sc22/wg21/docs/papers/2022/p0009r18.html) but
adds vector-space operations and tensor operations for the numerics.

## FieldTensor
A fixed-size tensor inspired by the dune-common data structures `FieldVector` and
`FieldMatrix`. This tensor type has fixed extents, given as template parameters:

```c++
template <class Value, int... exts>
class FieldTensor;
```

## DynamicTensor
A dynamic-size tensor inspired by the dune-common data structures `DynamicVector`
and `DynamicMatrix`. This tensor has all dynamic extents, but a static rank:

```c++
template <class Value, int rank>
class DynamicMatrix;
```

## CSFTensor
A sparse tensor with CSF storage and an upper bound for the number of nonzeros.

```c++
template <class Value, class Extents, int capacity = tag::dynamic>
class CSFTensor;
```

The `capacity` parameter is an integral constant that can be set to `tag::dynamic` if the expected number of non-zeros it not known at compiletime.

## COOTensor
A sparse tensor with COO storage and an upper bound for the number of nonzeros.

```c++
template <class Value, class Extents, int capacity = tag::dynamic>
class COOTensor;
```

## DenseTensor and SparseTensor

The general interface classes are parametrized over the value type, the extents, and a container and
maybe an index-access layout type.

```c++
template <class Value, class Extents, class Layout, class Container>
class DenseTensor;

template <class Value, class Extents, class Layout, class Container, class Traits>
class SparseTensor;
```

The extents are thereby given by the class `Extents` that provides hybrid static and dynamic extent specification.

```c++
template <class SizeType, int... exts>
class Extents;
```

The layout is (currently) either `LayoutLeft` or `LayoutRight` defining an index mapping `SxSx...xS -> S`
for `S` a `SizeType`. The Container of a dense tensor provides a (flat) container that is indexed by the
result of the layout mapping, i.e., typically a single flat index. Compatible layout and container could
also use a multi-index access and multi-dimensional storage.

# Tensor products
The product of two tensors `a` and `b` can be done in multiple forms. We follow the notation of `numpy` and others and
call the general product `c = tensordot(a,b,axes)` where `axes` defines over which indices to sum up. In general, the result
is defined, using Ricci index calculus:

```c++
c[i1,...in,j1,...jm] = a[i1,...,in,k1,...,kN] * b[k1,...,kN,j1,...,jm]
```

Thus, we sum over the last `N` indices of `a` and the first `N` indices of `b`.

In c++ the operation allows to pass either type of tensor (sparse/dense) with fixed-size or dynamic-size storage.
The `axes` are currently given only as an integer (template parameter) and not like in numpy as an array of indices
for `a` and `b` that get folded. This is an outlook for future implementation.

```c++
// <TensorDot.hpp>

template <std::size_t N, class Updater = MultAdd, class Tensor1, class Tensor2>
  requires (Tensor1::rank() >= N && Tensor2::rank() >= N)
auto tensordot (Tensor1 const& a, Tensor2 const& b, Index<N> axes = {}, Updater updater = {});

template <std::size_t N, class Updater = MultAdd, class Tensor1, class Tensor2, class OutTensor>
    requires (Tensor1::rank() >= N && Tensor2::rank() >= N)
    requires (OutTensor::rank() == Tensor1::rank()-N+Tensor2::rank()-N)
void tensordot (Tensor1 const& a, Tensor2 const& b, OutTensor& out, Index<N> axes = {}, Updater updater = {})
```

with `Index<N> = std::integral_constant<std::size_t,N>` and `MultAdd` represents a functor

```c++
// <Updater.hpp>

struct MultAdd
{
  template <class A, class B, class C>
  constexpr void operator() (A const& a, B const& b, C& c) const noexcept
  {
    c += a * b;
  }
};
```

The flexibility in the update operation allows to, e.g., perform a conjugation of the first or second argument, or to use
optimized functions for the update like `std::fma`.

## Matrix-Matrix products
Let `a` and `b` be 2-tensors, i.e., matrices, the we can specify the axes as

- `N=0`: the outer matrix-matrix product giving a 4-tensor,
- `N=1`: the (classical) matrix-matrix product summing the column indices of `a` with the row-indices of `b`,
- `N=2`: the frobenius inner-product of the two matrices, summing up all indices.

## Matrix-Vector products
For `a` a matrix and `b` a vector (or vice versa), a matrix-vector product can be implemented:

- `N=0`: the outer matrix-vector product giving a 3-tensor,
- `N=1`: the (classical) matrix-vector product

## Vector-Vector products
When `a` and `b` are both vectors, we have the outer and inner product

- `N=0`: the outer vector-vector product giving a 2-tensor,
- `N=1`: the frobenius inner-product of the two vectors, summing up all indices.

## Outer and inner products
Some special cases of a general tensordot are given a separate name:

- `dot(a,b)`: Equivalent to `tensordot<1>(a,b)`
- `outer(a,b)`: Equivalent to `Tensordot<0>(a,b)`
- `inner(a,b)`: Equivalent to `tensordot<N>(a,b)` for `N`-tensors `a` and `b`

The outer and inner products also are specialized for complex field-types to have a correct conjugation
on the second argument coefficients.


# Sparse tensor products
For sparse tensors, the default `tensordot` implementation is very inefficient, since 1. all tensor entries
are visited, even those that are zero and not stored, and 2. the access is an index-access that is slow in
general for sparse matrices.

That is why there is a special implementation using sparse index ranges to traverse the entries and to search
for overlapping indices in the contraction index range. It requires that the sparse tensor provides a major-dimension
iterator by default, i.e., the `[begin(), end())` range just iterate over one index and only the non-zeros
in that dimension. It is required that the major index is the `0`th index. (This is juts one option for the CSF format,
like we could implement sparse matrices with CSR or CSC ordering. In case of 2-tensors the requirement means CSR format)

```c++
// <SparseTensorDot.hpp>

template <std::size_t N, class Updater = MultAdd, class Tensor1, class Tensor2,
  requires (Tensor1::rank() >= N && Tensor2::rank() >= N)
auto sparsetensordot (Tensor1 const& a, Tensor2 const& b, Index<N> axes = {}, Updater updater = {});

template <std::size_t N, class Updater = MultAdd, class Tensor1, class Tensor2, class OutTensor>
    requires (Tensor1::rank() >= N && Tensor2::rank() >= N)
    requires (OutTensor::rank() == Tensor1::rank()-N+Tensor2::rank()-N)
void sparsetensordot (Tensor1 const& a, Tensor2 const& b, OutTensor& out, Index<N> axes = {}, Updater updater = {});
```

## Example
A sparse 3-tensor times a (dense) vector product (with summing over `N=1` dimension), would correspond to

```c++
// tensor-vector product
template <class SparseTensor, class Vector, class OutTensor>
void sparsetensordot (SparseTensor const& a, Vector const& b, OutTensor& c, Index<1>)
{
  for (auto&& [a_i, i] : Dune::sparseRange(a))
    for (auto&& [a_ij, j] : Dune::sparseRange(a_i))
      for (auto&& [a_ijk, k] : Dune::sparseRange(a_ij))
        c(i,j) += a_ijk * b(k);
}
```


# Tensors as multilinear maps
An `n`-tensor can be seen as a map taking `n` vectors and map these on a scalar. This is implemented
as `multidot(a, v1,...,vn)` or as `a(v1,...,vn)`. Thereby the `i`th vector is contracted with the `i`th
index of the tensor.

```c++
// <MultiDot.hpp>

template <class Tensor, class... Vectors,
  requires (Tensor::rank() == sizeof...(Vectors))
  requires (... && (Vectors::rank() == 1))
auto multidot (Tensor const& tensor, Vectors const&... vectors);
```

Note, the result of this operation is a 0-tensor, i.e., a scalar.


# (Tensor)-Span
A span is a light-weight view onto the data of a tensor. It can be constructed from a raw pointer
plus the information about extents and index mapping. Also, it can be constructed from a tensor
directly.

The one-dimensional spans are implemented in the class `Span` and follow the c++-20 `std::span`
proposal. The multi-dimensional spans are implemented in the class `TensorSpan` and follow the
proposal for [std::mdspan](https://www.open-std.org/jtc1/sc22/wg21/docs/papers/2022/p0009r18.html).

Each tensor type has a method `.values()` that returns a `Span` over the nonzero entries of the tensor
in the order stored in the data-structure. This allows to perform element-wise operations with one
or more tensors of the same shape by iterating over the corresponding span(s).

A `TensorSpan` behaves like a dense tensor, although it is not required to be contiguous. It can
implement a strided access to a subspace of the indices. The `TensorSpan` can be used on all the
dot operations introduced above, also in mixed combinations with "real" tensors.
