#pragma once

#include <array>
#include <tuple>
#include <type_traits>

#include <dune/common/ftraits.hh>
#include <dune/common/indices.hh>
#include <dune/common/std/default_accessor.hh>
#include <dune/common/std/layout_right.hh>
#include <dune/common/std/span.hh>
#include <dune/tensor/containers/traits.hh>
#include <dune/tensor/operations/multidot.hh>
#include <dune/tensor/ranges/rangeexpression.hh>
#include <dune/tensor/utility/bracketproxy.hh>

namespace Dune::Tensor {

// forward declaration
template <class Op, class... Ts>
class TensorExpression;

template <class Element, class Extents, class Layout, class Accessor>
class TensorSpan;

template <class Element, class Extents, class Layout, class IteratorPolicy>
struct TensorTraits
{
  using element_type = Element;
  using extents_type = Extents;
  using layout_type = Layout;
  using iterators_type = IteratorPolicy;

  static constexpr std::size_t static_size () noexcept
  {
    return static_size_or_dynamic_extent<extents_type,layout_type>();
  }
};


/**
 * \brief Interface for arithmetic operation on tensors
 * \ingroup Tensors
 * \nosubgrouping
 *
 * \tparam Traits Type containing the type aliases of a tensor
 **/
template <class Derived, class Traits>
class TensorInterface
{
  using derived_type = Derived;
  using iterators_type = typename Traits::iterators_type;

public:
  using element_type = typename Traits::element_type;
  using value_type = std::remove_cv_t<element_type>;
  using field_type = typename Dune::FieldTraits<value_type>::field_type;
  using extents_type = typename Traits::extents_type;
  using size_type = typename extents_type::size_type;
  using rank_type = typename extents_type::rank_type;
  using index_type = typename extents_type::index_type;
  using layout_type = typename Traits::layout_type;
  using reference = element_type&;
  using const_reference = const element_type&;

private:

  derived_type const& asDerived () const
  {
    return static_cast<derived_type const&>(*this);
  }

  derived_type& asDerived ()
  {
    return static_cast<derived_type&>(*this);
  }

public:

  /// \name Multi index access
  /// @{

  // -----------------------------------------
  // Access by call operator, tensor(i0,i1,i2)

  /// \brief Access specified element at position i0,i1,...
  template <class... Indices,
    std::enable_if_t<(sizeof...(Indices) == extents_type::rank()), int> = 0,
    std::enable_if_t<(... && std::is_convertible_v<Indices, index_type>), int> = 0>
  constexpr decltype(auto) operator() (Indices... indices)
  {
    return asDerived()[std::array<index_type,sizeof...(Indices)>{index_type(indices)...}];
  }

  /// \brief Access specified element at position i0,i1,...
  template <class... Indices,
    std::enable_if_t<(sizeof...(Indices) == extents_type::rank()), int> = 0,
    std::enable_if_t<(... && std::is_convertible_v<Indices, index_type>), int> = 0>
  constexpr decltype(auto) operator() (Indices... indices) const
  {
    return asDerived()[std::array<index_type,sizeof...(Indices)>{index_type(indices)...}];
  }

  // ----------------------------------------------------------------------
  // Access by subscript operator returning sub-tensors, tensor[i1][i2][i3]

  // ............................................
  // Specialization for mdarray with layout_right

  template <class Index, class E = extents_type, class L = layout_type, class D = derived_type,
    std::enable_if_t<std::is_convertible_v<Index,index_type>, int> = 0,
    std::enable_if_t<(E::rank() >= 2), int> = 0,
    std::enable_if_t<std::is_same_v<L, Std::layout_right>, int> = 0,
    decltype(std::declval<D>().container_data(), bool{}) = true
    >
  constexpr auto operator[] (Index index) noexcept
  {
    return [&]<std::size_t e0, std::size_t... ee, std::size_t i0, std::size_t... ii>
      (const Std::extents<int,e0,ee...>& ext, std::integer_sequence<std::size_t,i0,ii...>) {
      return TensorSpan<value_type, Std::extents<int,ee...>, Std::layout_right, Std::default_accessor<value_type>>(
        asDerived().container_data() + asDerived().mapping().stride(0)*index, ext.extent(ii)...);
    }(asDerived().extents(),std::make_integer_sequence<std::size_t,extents_type::rank()>{});
  }

  template <class Index, class E = extents_type, class L = layout_type, class D = derived_type,
    std::enable_if_t<std::is_convertible_v<Index,index_type>, int> = 0,
    std::enable_if_t<(E::rank() >= 2), int> = 0,
    std::enable_if_t<std::is_same_v<L, Std::layout_right>, int> = 0,
    decltype(std::declval<D>().container_data(), bool{}) = true
    >
  constexpr auto operator[] (Index index) const noexcept
  {
    return [&]<std::size_t e0, std::size_t... ee, std::size_t i0, std::size_t... ii>
      (const Std::extents<int,e0,ee...>& ext, std::integer_sequence<std::size_t,i0,ii...>) {
      return TensorSpan<const value_type, Std::extents<int,ee...>, Std::layout_right, Std::default_accessor<const value_type>>(asDerived().container_data() + asDerived().mapping().stride(0)*index, ext.extent(ii)...);
    }(asDerived().extents(),std::make_integer_sequence<std::size_t,extents_type::rank()>{});
  }

  // ...........................................
  // Specialization for mdspan with layout_right

  template <class Index, class E = extents_type, class L = layout_type, class D = derived_type,
    std::enable_if_t<std::is_convertible_v<Index,index_type>, int> = 0,
    std::enable_if_t<(E::rank() >= 2), int> = 0,
    std::enable_if_t<std::is_same_v<L, Std::layout_right>, int> = 0,
    decltype(std::declval<D>().accessor(), bool{}) = true,
    decltype(std::declval<D>().data_handle(), bool{}) = true
    >
  constexpr auto operator[] (Index index) noexcept
  {
    return [&]<std::size_t e0, std::size_t... ee, std::size_t i0, std::size_t... ii>
      (const Std::extents<int,e0,ee...>& ext, std::integer_sequence<std::size_t,i0,ii...>) {
      using accessor_type = typename std::decay_t<decltype(asDerived().accessor())>::offset_policy;
      using element_type = typename accessor_type::element_type;
      return TensorSpan<element_type, Std::extents<int,ee...>, Std::layout_right, accessor_type>(
        asDerived().accessor().offset(asDerived().data_handle(), asDerived().mapping().stride(0)*index), ext.extent(ii)...);
    }(asDerived().extents(),std::make_integer_sequence<std::size_t,extents_type::rank()>{});
  }

  template <class Index, class E = extents_type, class L = layout_type, class D = derived_type,
    std::enable_if_t<std::is_convertible_v<Index,index_type>, int> = 0,
    std::enable_if_t<(E::rank() >= 2), int> = 0,
    std::enable_if_t<std::is_same_v<L, Std::layout_right>, int> = 0,
    decltype(std::declval<D>().accessor(), bool{}) = true,
    decltype(std::declval<D>().data_handle(), bool{}) = true
    >
  constexpr auto operator[] (Index index) const noexcept
  {
    return [&]<std::size_t e0, std::size_t... ee, std::size_t i0, std::size_t... ii>
      (const Std::extents<int,e0,ee...>& ext, std::integer_sequence<std::size_t,i0,ii...>) {
      using accessor_type = typename std::decay_t<decltype(asDerived().accessor())>::offset_policy;
      using element_type = typename accessor_type::element_type;
      return TensorSpan<element_type, Std::extents<int,ee...>, Std::layout_right, accessor_type>(
        asDerived().accessor().offset(asDerived().data_handle(), asDerived().mapping().stride(0)*index), ext.extent(ii)...);
    }(asDerived().extents(),std::make_integer_sequence<std::size_t,extents_type::rank()>{});
  }

  // ............................................
  // Fallback for other layouts than layout_right

  /// \brief Access specified element at position [i0][i1]...
  template <class Index, class E = extents_type, class L = layout_type,
    std::enable_if_t<std::is_convertible_v<Index,index_type>, int> = 0,
    std::enable_if_t<(E::rank() >= 2), int> = 0,
    std::enable_if_t<not std::is_same_v<L, Std::layout_right>, int> = 0>
  constexpr auto operator[] (Index index) noexcept
  {
    return BracketProxy<derived_type,2,E::rank()>{asDerived(), index};
  }

  /// \brief Access specified element at position [i0][i1]...
  template <class Index, class E = extents_type, class L = layout_type,
    std::enable_if_t<std::is_convertible_v<Index,index_type>, int> = 0,
    std::enable_if_t<(E::rank() >= 2), int> = 0,
    std::enable_if_t<not std::is_same_v<L, Std::layout_right>, int> = 0>
  constexpr auto operator[] (Index index) const noexcept
  {
    return BracketProxy<const derived_type,2,E::rank()>{asDerived(), index};
  }

  /// @}


  /// \name Conversion to the underlying value if rank is zero
  // @{

  template <class ScalarType, class E = extents_type,
    std::enable_if_t<std::is_convertible_v<value_type,ScalarType>, int> = 0,
    std::enable_if_t<(E::rank() == 0), int> = 0 >
  constexpr operator ScalarType () const noexcept
  {
    return ScalarType(*values().begin());
  }

  /// @}


  /// \name Assignment operators
  /// @{

  /// \brief Assign the given value to all elements of the tensor
  constexpr derived_type& operator= (const value_type& value) noexcept
  {
    for (auto& el : values())
      el = value;
    return asDerived();
  }

  /// \brief Unpack an expression into this tensor
  template <class Op, class... T>
  constexpr derived_type& operator= (const TensorExpression<Op,T...>& other) noexcept
  {
    assert(asDerived().extents() == other.extents());
    assert(asDerived().container_size() >= other.values().size());
    auto other_values = other.values();
    std::copy(other_values.begin(), other_values.end(), asDerived().container_data());
    return asDerived();
  }

  /// \brief Unpack an expression into this tensor
  template <class V, class E, class L, class A>
  constexpr derived_type& operator= (const TensorSpan<V,E,L,A>& other) noexcept
  {
    assert(asDerived().extents() == other.extents());
    assert(asDerived().container_size() >= other.size());
    auto other_values = other.values();
    std::copy(other_values.begin(), other_values.end(), asDerived().container_data());
    return asDerived();
  }

  /// @}


  /// \name Direct access to the data
  /// @{

  /// \brief Return a span of the values stored in the tensor
  template <class D = derived_type,
    decltype(std::declval<D>().container_data(), bool{}) = true>
  constexpr auto values () noexcept
  {
    using span_type = Std::span<value_type,Traits::static_size()>;
    return span_type(asDerived().container_data(), asDerived().size());
  }

  /// \brief Return a const span of the values stored in the tensor
  template <class D = derived_type,
    decltype(std::declval<D>().container_data(), bool{}) = true>
  constexpr auto values () const noexcept
  {
    using span_type = Std::span<value_type const,Traits::static_size()>;
    return span_type(asDerived().container_data(), asDerived().size());
  }

  /// \brief Return a range of the values stored in the tensor-span
  template <class D = derived_type,
    decltype(std::declval<D>().data_handle(), bool{}) = true,
    decltype(std::declval<D>().accessor(), bool{}) = true>
  constexpr auto values () noexcept
  {
    auto& accessor = asDerived().accessor();
    auto data_handle = asDerived().data_handle();
    return RangeExpression{[data_handle,&accessor](std::ptrdiff_t i) -> decltype(auto) {
        return accessor.access(data_handle, i);
      }, Dune::range<std::ptrdiff_t>(asDerived().size())};
  }

  /// \brief Return a const range of the values stored in the tensor-span
  template <class D = derived_type,
    decltype(std::declval<D>().data_handle(), bool{}) = true,
    decltype(std::declval<D>().accessor(), bool{}) = true>
  constexpr auto values () const noexcept
  {
    auto const& accessor = asDerived().accessor();
    auto data_handle = asDerived().data_handle();
    return RangeExpression{[data_handle,&accessor](std::ptrdiff_t i) -> decltype(auto) {
        return accessor.access(data_handle, i);
      }, Dune::range<std::ptrdiff_t>(asDerived().size())};
  }

  /// @}


  /// \name Iterators
  /// @{

  /// \brief Iterator over the values or axis of a tensor
  template <class Tag>
  auto begin (Tag tag) const noexcept
  {
    if constexpr(extents_type::rank() > 0)
      return iterators_type::begin(tag, asDerived());
    else
      return values().begin();
  }

  /// \brief End-Iterator over the values or axis of a tensor
  template <class Tag>
  auto end (Tag tag) const noexcept
  {
    if constexpr(extents_type::rank() > 0)
      return iterators_type::end(tag, asDerived());
    else
      return values().end();
  }

  /// @}


  /**
   * \name Arithmetic operations
   **/
  /// @{

  /// \brief Applies compound assignment operator to each element of the tensor
  template <class D, class T>
  constexpr derived_type& operator+= (const TensorInterface<D,T>& rhs) noexcept
  {
    auto lhs_values = this->values();
    auto rhs_values = rhs.values();
    assert(lhs_values.size() == rhs_values.size());
    for (std::size_t i = 0; i < rhs_values.size(); ++i)
      lhs_values[i] += rhs_values[i];
    return asDerived();
  }

  /// \brief Assign and add the given value to all elements of the tensor
  template <class E = extents_type, std::enable_if_t<(E::rank() == 0), int> = 0>
  constexpr derived_type& operator+= (const value_type& value) noexcept
  {
    *values().begin() += value;
    return asDerived();
  }

  /// \brief Applies compound assignment operator to each element of the tensor
  template <class D, class T>
  constexpr derived_type& operator-= (const TensorInterface<D,T>& rhs) noexcept
  {
    auto lhs_values = this->values();
    auto rhs_values = rhs.values();
    assert(lhs_values.size() == rhs_values.size());
    for (std::size_t i = 0; i < rhs_values.size(); ++i)
      lhs_values[i] -= rhs_values[i];
    return asDerived();
  }

  /// \brief Assign and subtract the given value from all elements of the tensor
  template <class E = extents_type, std::enable_if_t<(E::rank() == 0), int> = 0>
  constexpr derived_type& operator-= (const value_type& value) noexcept
  {
    *values().begin() -= value;
    return asDerived();
  }

  /// \brief Applies compound assignment operator to each element of the tensor
  constexpr derived_type& operator*= (field_type factor) noexcept
  {
    for (auto& el : values())
      el *= factor;
    return asDerived();
  }

  /// \brief Applies compound assignment operator to each element of the tensor
  constexpr derived_type& operator/= (field_type factor) noexcept
  {
    for (auto& el : values())
      el /= factor;
    return asDerived();
  }

  /// \brief Applies axpy operation `y = alpha*x + y` to each element of the tensor `y`
  template <class D, class T>
  constexpr derived_type& axpy (field_type alpha, const TensorInterface<D,T>& x) noexcept
  {
    auto lhs_values = this->values();
    auto x_values = x.values();
    assert(lhs_values.size() == x_values.size());
    for (std::size_t i = 0; i < x_values.size(); ++i)
      lhs_values[i] += alpha * x_values[i];
    return asDerived();
  }

  /// \brief Applies aypx operation `y = alpha*y + x` to each element of the tensor `y`
  template <class D, class T>
  constexpr derived_type& aypx (field_type alpha, const TensorInterface<D,T>& x) noexcept
  {
    auto lhs_values = this->values();
    auto x_values = x.values();
    assert(lhs_values.size() == x_values.size());
    for (std::size_t i = 0; i < x_values.size(); ++i)
      lhs_values[i] = alpha * lhs_values[i] + x_values[i];
    return asDerived();
  }

  /// \brief Applies binary operators to each element of two tensors, or a tensor and a value
  friend constexpr derived_type operator+ (const TensorInterface& lhs, const TensorInterface& rhs) noexcept
  {
    derived_type tmp(lhs.asDerived());
    tmp += rhs;
    return tmp;
  }

  /// \brief Applies binary operators to each element of two tensors, or a tensor and a value
  friend constexpr derived_type operator- (const TensorInterface& lhs, const TensorInterface& rhs) noexcept
  {
    derived_type tmp(lhs.asDerived());
    tmp -= rhs;
    return tmp;
  }

  /// \brief Applies binary operators to each element of two tensors, or a tensor and a value
  friend constexpr derived_type operator* (const TensorInterface& lhs, field_type const factor) noexcept
  {
    derived_type tmp(lhs.asDerived());
    tmp *= factor;
    return tmp;
  }

  /// \brief Applies binary operators to each element of two tensors, or a tensor and a value
  friend constexpr derived_type operator* (field_type factor, const TensorInterface& rhs) noexcept
  {
    derived_type tmp(rhs.asDerived());
    for (auto& v_i : tmp.values())
      v_i = factor * v_i;
    return tmp;
  }

  /// \brief Multiplication for rank-0 tensors
  template <class D, class T, class E = typename T::extents_type,
    std::enable_if_t<(E::rank() == 0 && E::rank() == extents_type::rank()), int> = 0>
  constexpr derived_type operator* (const TensorInterface<D,T>& rhs) noexcept
  {
    return derived_type{asDerived()} *= rhs.values()[0];
  }

  /// \brief Applies binary operators to each element of two tensors, or a tensor and a value
  friend constexpr derived_type operator/ (const TensorInterface& lhs, field_type factor) noexcept
  {
    derived_type tmp(lhs.asDerived());
    tmp /= factor;
    return tmp;
  }

  /// \brief Applies a unary arithmetic operator to each element of the tensor
  friend constexpr derived_type operator- (const TensorInterface& t) noexcept
  {
    derived_type tmp(t.asDerived());
    auto tmp_values = tmp.values();
    for (index_type i = 0; i < tmp.size(); ++i)
      tmp_values[i] = -tmp_values[i];
    return tmp;
  }

  /// \brief Tensor as multilinear map
  template <class... Vectors,
    class E = extents_type,
    std::enable_if_t<(E::rank() > 0), int> = 0,
    std::enable_if_t<(E::rank() == sizeof...(Vectors)), int> = 0,
    std::enable_if_t<(... && (Vectors::rank() == 1)), int> = 0>
  constexpr auto operator() (const Vectors&... vectors) const
  {
    return Dune::Tensor::multidot(this->asDerived(),vectors...);
  }

  /// @}
};


/// \relates DenseTensor
template <class D, class T>
std::ostream& operator<< (std::ostream& out, const TensorInterface<D,T>& tensorInterface)
{
  const D& tensor = static_cast<D const&>(tensorInterface);
  using index_type = typename D::index_type;
  using extents_type = typename D::extents_type;
  if constexpr(extents_type::rank() == 0) {
    out << tensor();
  } else if constexpr(extents_type::rank() == 1) {
    out << "[";
    for (index_type i = 0; i < tensor.extent(0); ++i)
      out << tensor(i) << (i < tensor.extent(0)-1 ? ", " : "]");
  } else if constexpr(extents_type::rank() == 2) {
    out << "[\n";
    for (index_type i = 0; i < tensor.extent(0); ++i) {
      out << "  [";
      for (index_type j = 0; j < tensor.extent(1); ++j)
        out << tensor(i,j) << (j < tensor.extent(1)-1 ? ", " : "]");
      out << (i < tensor.extent(0)-1 ? ",\n" : "\n");
    }
    out << ']';
  } else if constexpr(extents_type::rank() == 3) {
    out << "[\n";
    for (index_type i = 0; i < tensor.extent(0); ++i) {
      out << "  [\n";
      for (index_type j = 0; j < tensor.extent(1); ++j) {
        out << "    [";
        for (index_type k = 0; k < tensor.extent(2); ++k)
          out << tensor(i,j,k) << (k < tensor.extent(2)-1 ? ", " : "]");
        out << (j < tensor.extent(1)-1 ? ",\n" : "\n");
      }
      out << "  ]";
      out << (i < tensor.extent(0)-1 ? ",\n" : "\n");
    }
    out << ']';
  } else {
    out << "Tensor<" << extents_type::rank() << ">";
  }
  return out;
}

} // end namespace Dune::Tensor
