#pragma once

#include <iterator>
#include <type_traits>
#include <vector>

#include <dune/tensor/tags.hh>
#include <dune/tensor/containers/fixedsizecontainer.hh>

namespace Dune::Tensor {

/**
 * \brief Container that can be constructed from a dynamic size information
 * \ingroup Container
 **/
struct DynamicSizeContainer
{
  template <class Value, std::size_t /*static_size*/>
  class Storage
      : public std::vector<Value>
  {
  private:
    using base_type = std::vector<Value>;

  public:
    using value_type = typename base_type::value_type;
    using size_type = typename base_type::size_type;

    explicit constexpr Storage (size_type size)
      : base_type(size)
    {}

    constexpr Storage (size_type size, const value_type& value)
      : base_type(size, value)
    {}

    template <class... V,
      std::enable_if_t<(... && std::is_convertible_v<V,value_type>), int> = 0>
    explicit constexpr Storage (tag::from_values_t, const V&... values)
      : base_type{values...}
    {}

    template <class InputIt,
      std::enable_if_t<std::is_convertible_v<typename std::iterator_traits<InputIt>::value_type, value_type>, int> = 0>
    constexpr Storage (tag::from_range_t, InputIt first, InputIt last)
      : base_type(first, last)
    {}

    template <class Range,
      decltype(std::declval<Range>().begin(), std::declval<Range>().end(), bool{}) = true>
    constexpr Storage (tag::from_range_t, const Range& range)
      : base_type(range.begin(), range.end())
    {}
  };

  // specialization for the case of scalar data
  template <class Value>
  class Storage<Value,1>
      : public FixedSizeContainer::Storage<Value,1>
  {
    using base_type = FixedSizeContainer::Storage<Value,1>;
  public:
    using base_type::base_type;

    template <class... Args>
    void resize (Args&&... /*args*/) {}
  };
};

} // end namespace Dune::Tensor
