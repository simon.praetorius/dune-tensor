#pragma once

#include <cstdlib>
#include <memory>
#include <type_traits>

namespace Dune::Tensor {

template <class T, std::size_t alignment>
class AlignedAllocator
{
public:
  using value_type = T;
  using reference = T&;
  using const_reference = const T&;
  using pointer = T*;
  using const_pointer = const T*;
  using size_type = std::size_t;
  using difference_type = std::ptrdiff_t;
  using propagate_on_container_move_assignment = std::true_type;

  [[nodiscard]] pointer allocate (size_type n) const
  {
      pointer ptr = reinterpret_cast<pointer>( std::aligned_alloc(alignment, sizeof(T)*n) );
      if (ptr == nullptr)
        throw std::bad_alloc{};
      return ptr;
  }

  void deallocate (pointer p, size_type /*n*/) const
  {
    if (p != nullptr)
      std::free(p);
  }
};

} // end namespace Dune::Tensor
