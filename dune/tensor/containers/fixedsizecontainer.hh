#pragma once

#include <algorithm>
#include <array>
#include <cassert>
#include <iterator>
#include <type_traits>

#include <dune/common/filledarray.hh>
#include <dune/tensor/tags.hh>

namespace Dune::Tensor {

/**
 * \defgroup Container  Storage backends for tensors
 **/

/**
 * \brief Container that requires a static size information
 * \ingroup Container
 **/
struct FixedSizeContainer
{
  template <class Value, std::size_t static_size>
  class Storage
      : public std::array<Value,static_size>
  {
  private:
    using base_type = std::array<Value,static_size>;

  public:
    using value_type = typename base_type::value_type;
    using size_type = typename base_type::size_type;

    constexpr Storage () noexcept = default;

    explicit constexpr Storage (size_type size) noexcept
      : base_type{}
    {
      assert(size == static_size);
    }

    explicit constexpr Storage (size_type size, const value_type& value) noexcept
      : base_type{Dune::filledArray<static_size>(value)}
    {
      assert(size == static_size);
    }

    template <class Size, Size n,
      std::enable_if_t<std::is_convertible_v<Size, size_type>, int> = 0,
      std::enable_if_t<(int(n) == static_size), int> = 0>
    explicit constexpr Storage (std::integral_constant<Size,n>, const value_type& value) noexcept
      : base_type{Dune::filledArray<n>(value)}
    {}

    template <class... V,
      std::enable_if_t<(... && std::is_convertible_v<V,value_type>), int> = 0,
      std::enable_if_t<(sizeof...(V) == static_size), int> = 0>
    explicit constexpr Storage (tag::from_values_t, V const&... values) noexcept
      : base_type{values...}
    {}

    template <class InputIt,
      std::enable_if_t<std::is_convertible_v<typename std::iterator_traits<InputIt>::value_type, value_type>, int> = 0>
    constexpr Storage (tag::from_range_t, InputIt first, InputIt last) noexcept
    {
      std::copy(first, last, base_type::begin());
    }

    template <class Range,
      decltype(std::declval<Range>().begin(), std::declval<Range>().end(), bool{}) = true>
    constexpr Storage (tag::from_range_t, const Range& range) noexcept
    {
      std::copy(range.begin(), range.end(), base_type::begin());
    }
  };
};

} // end namespace Dune::Tensor
