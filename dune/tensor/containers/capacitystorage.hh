#pragma once

#include <vector>

#include <dune/common/reservedvector.hh>
#include <dune/common/std/extents.hh>
#include <dune/common/std/span.hh>
#include <dune/tensor/tags.hh>
#include <dune/tensor/layouts/concepts.hh>

namespace Dune::Tensor {
namespace Impl {

/// Defines a storage type of given static or dynamic capacity.
template <class T, std::size_t capacity>
struct CapacityStorage
{
  using type = Dune::ReservedVector<T,capacity>;
};

#ifndef DOXYGEN
template <class T>
struct CapacityStorage<T,Std::dynamic_extent>
{
  using type = std::vector<T>;
};
#endif

} // end namespace  Impl


/**
 * \brief A container type that provides a static or dynamic resizable storage backend.
 * \ingroup Container
 *
 * The storage backend is chose by checking whether the `capacity` parameter is
 * `tag::dynamic` (then a dynamic storage is selected) or a non-negative value
 * (then a static storage is selected).
 *
 * The storage backend is, in both cases, a resizable container with an interface
 * similar to `Std::vector`.
 *
 * \tparam capacity  The maximal capacity to provide for the container. Can be `tag::dynamic`
 *                   to indicate that a fully dynamic container should be used.
 */
template <std::size_t capacity = Std::dynamic_extent>
struct CapacityContainer
{
  template <class Value, std::size_t /*static_size*/>
  class Storage
      : public Impl::CapacityStorage<Value,capacity>::type
  {
  private:
    using base_type = typename Impl::CapacityStorage<Value,capacity>::type;

  public:
    using value_type = typename base_type::value_type;
    using size_type = typename base_type::size_type;

    explicit constexpr Storage (size_type size)
      : base_type(size)
    {}

    constexpr Storage (size_type size, const value_type& value)
      : base_type(size, value)
    {}

    template <class... V,
      std::enable_if_t<(... && std::is_convertible_v<V,value_type>), int> = 0>
    explicit constexpr Storage (tag::from_values_t, const V&... values)
      : base_type{values...}
    {}

    template <class InputIt,
      class ValueType = typename std::iterator_traits<InputIt>::value_type,
      std::enable_if_t<std::is_convertible_v<ValueType,value_type>, int> = 0>
    constexpr Storage (tag::from_range_t, InputIt first, InputIt last)
      : base_type(first, last)
    {}

    /// \brief Construct the storage and reserve memory in the size given by the mapper
    template <class M,
      std::enable_if_t<Dune::Concept::Mapping<M>, int> = 0>
    explicit constexpr Storage (const M& mapper)
      : Storage{mapper.size()}
    {
      if constexpr(capacity == Std::dynamic_extent)
        base_type::reserve(mapper.size());
    }

    /// \brief Construct the storage and reserve memory in the size given by the mapper
    template <class M,
      std::enable_if_t<Dune::Concept::Mapping<M>, int> = 0>
    explicit constexpr Storage (const M& mapper, const value_type& value)
      : Storage{mapper.size(), value}
    {
      if constexpr(capacity == Std::dynamic_extent)
        base_type::reserve(mapper.size(), value);
    }
  };
};

} // end namespace Dune::Tensor
