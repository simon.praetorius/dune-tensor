#pragma once

#include <dune/common/typeutilities.hh>
#include <dune/common/std/span.hh>
#include <dune/tensor/tags.hh>
#include <dune/tensor/containers/dynamicsizecontainer.hh>
#include <dune/tensor/containers/fixedsizecontainer.hh>
#include <dune/tensor/extents/traits.hh>

namespace Dune::Tensor {
namespace Impl {

template <class Extents, class Layout,
  class Mapping = typename Layout::template mapping<Extents>,
  decltype(Mapping::static_size(), bool{}) = true>
inline constexpr std::size_t static_size_or_dynamic_extent (Dune::PriorityTag<2>)
{
  return Mapping::static_size();
}

template <class Extents, class Layout>
inline constexpr std::size_t static_size_or_dynamic_extent (Dune::PriorityTag<1>)
{
  return static_extents_product<Extents>();
}

} // end namespace impl


template <class Extents, class Layout>
inline constexpr std::size_t static_size_or_dynamic_extent ()
{
  return Impl::static_size_or_dynamic_extent<Extents,Layout>(Dune::PriorityTag<4>{});
}

template <class Value, class Extents, class Layout, class ContainerPolicy>
using Container_t = typename ContainerPolicy::template Storage<Value,
  static_size_or_dynamic_extent<Extents,Layout>()>;


/**
 * \brief Defines a common container storage type, based on the extents of the tensors.
 *
 * Defines a storage that all tensors `Tensors...` could be parametrized
 * with. If any of the tensors has any dynamic extent, the container is a
 * `DynamicSizeContainer`, otherwise (all extents of all tensors are static)
 * the storage container is a `FixedSizeContainer`.
 **/
template <class... Tensors>
using CommonContainer = std::conditional_t<isAnyDynamic_v<typename Tensors::extents_type...>,
  DynamicSizeContainer, FixedSizeContainer>;


/**
 * \brief Defines a container type depending on whether `ext` is static or dynamic.
*/
template <std::size_t ext>
using DynamicOrFixedSizeContainer = std::conditional_t<(ext == Std::dynamic_extent),
  DynamicSizeContainer, FixedSizeContainer>;

} // end namespace Dune::Tensor
