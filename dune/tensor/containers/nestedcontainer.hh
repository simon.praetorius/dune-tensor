#pragma once

#include <algorithm>
#include <array>
#include <iterator>
#include <type_traits>

#include <dune/tensor/tags.hh>
#include <dune/tensor/containers/fixedsizecontainer.hh>

namespace Dune::Tensor {

namespace Impl {

template <class Value, int... extents>
struct NestedArray;

template <class Value>
struct NestedArray<Value>
{
  using type = Value;
};

template <class Value, int e0, int... extents>
struct NestedArray<Value,e0,extents...>
{
  using type = std::array<typename NestedArray<Value, extents...>::type, e0>;
};

} // end namespace Impl


/**
 * \brief Storage as nested `std::array<std::array<..., e1>, e0>`.
 * \ingroup Container
 *
 * The nested storage allows to get sub-structures by using nested `operator[]`s, e.g.
 * in a matrix the bracket operator returns a `std::array` representing a row.
 *
 * The implementation makes an assumption on the internal storage of the `std::array`
 * type, i.e., nested field vectors must have contiguous data storage.
 **/
template <int... extents>
struct NestedContainer
{
  template <class Value, int static_size = tag::dynamic>
  class Storage
  {
    static_assert(static_size == (1 * ... * extents));
    using base_type = typename Impl::NestedArray<Value, extents...>::type;

  public:
    using value_type = Value;
    using size_type = typename base_type::size_type;
    using pointer = value_type*;
    using const_pointer = const value_type*;
    using reference = value_type&;
    using const_reference = const value_type&;

    constexpr Storage () noexcept = default;

    constexpr Storage (size_type size) noexcept
      : base_{}
    {
      assert(size == static_size);
    }

    constexpr Storage (size_type size, const value_type& value) noexcept
      : Storage{size}
    {
      std::fill(begin(), end(), value);
    }

    template <class Size, Size n,
      std::enable_if_t<std::is_convertible_v<Size, size_type>, int> = 0,
      std::enable_if_t<(int(n) == static_size), int> = 0>
    constexpr Storage (std::integral_constant<Size,n>, const value_type& value) noexcept
      : Storage{static_size,value}
    {}

    template <class InputIt,
      std::enable_if_t<std::is_convertible_v<typename std::iterator_traits<InputIt>::value_type, value_type>, int> = 0>
    constexpr Storage (tag::from_range_t, InputIt first, InputIt last) noexcept
    {
      std::copy(first, last, begin());
    }

    constexpr pointer data ()
    {
      // NOTE: It is not guaranteed that a nested std::array stores contiguous data
      //       This additional static_assert tries to check whether any padding is added.
      static_assert(sizeof(base_type) == sizeof(value_type)*static_size);
      return std::addressof(front(base_, std::integer_sequence<int,extents...>{}));
    }

    constexpr const_pointer data () const
    {
      // NOTE: It is not guaranteed that a nested std::array stores contiguous data
      //       This additional static_assert tries to check whether any padding is added.
      static_assert(sizeof(base_type) == sizeof(value_type)*static_size);
      return std::addressof(front(base_, std::integer_sequence<int,extents...>{}));
    }

    constexpr pointer begin () { return data(); }
    constexpr pointer end () { return data() + static_size; }

    constexpr const_pointer begin () const { return data(); }
    constexpr const_pointer end () const { return data() + static_size; }

    constexpr size_type size () const { return static_size; }

    template <class SizeType,
      std::enable_if_t<std::is_convertible_v<SizeType, size_type>, int> = 0>
    constexpr auto& operator[] (SizeType i0)
    {
      return base_[i0];
    }

    template <class SizeType,
      std::enable_if_t<std::is_convertible_v<SizeType, size_type>, int> = 0>
    constexpr const auto& operator[] (SizeType i0) const
    {
      return base_[i0];
    }

    template <class SizeType, std::size_t n>
    constexpr auto& operator[] (const std::array<SizeType,n>& indices)
    {
      return std::apply([&](auto... ii) -> auto& {
        return this->at(base_, std::integer_sequence<int,extents...>{}, ii...); }, indices);
    }

    template <class SizeType, std::size_t n>
    constexpr const auto& operator[] (const std::array<SizeType,n>& indices) const
    {
      return std::apply([&](auto... ii) -> const auto& {
        return this->at(base_, std::integer_sequence<int,extents...>{}, ii...); }, indices);
    }

  private:
    template <class FV>
    constexpr auto& front(FV&& fv, std::integer_sequence<int>) const
    {
      return fv;
    }

    template <class FV, int size0, int... sizes>
    constexpr auto& front(FV&& fv, std::integer_sequence<int,size0,sizes...>) const
    {
      return front(fv[0], std::integer_sequence<int,sizes...>{});
    }

    template <class FV>
    constexpr auto& back(FV&& fv, std::integer_sequence<int>) const
    {
      return fv;
    }

    template <class FV, int size0, int... sizes>
    constexpr auto& back(FV&& fv, std::integer_sequence<int,size0,sizes...>) const
    {
      return front(fv[size0-1], std::integer_sequence<int,sizes...>{});
    }

    template <class FV, int... sizes>
    constexpr auto& at(FV&& fv, std::integer_sequence<int,sizes...>) const
    {
      return fv;
    }

    template <class FV, int size0, int... sizes, class I0, class... II>
    constexpr auto& at(FV&& fv, std::integer_sequence<int,size0,sizes...>, I0 i0, II... ii) const
    {
      assert(i0 < size0);
      return at(fv[i0], std::integer_sequence<int,sizes...>{}, ii...);
    }

  private:
    base_type base_;
  };
};

template <>
struct NestedContainer<>
  : FixedSizeContainer
{};

} // end namespace Dune::Tensor
