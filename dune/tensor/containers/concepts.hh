#pragma once

#include <tuple>
#include <type_traits>
#include <utility>

#include <dune/common/std/span.hh>
#include <dune/tensor/tags.hh>

#if ENABLE_DUNE_CONCEPTS
#include <dune/concepts/utilities.hh>
#include <dune/concepts/std/concepts.hh>
#include <dune/concepts/std/iterator.hh>

namespace Dune::Concept {

template <class S>
concept Storage = requires(const S& storage, typename S::size_type size, typename S::value_type value)
{
  // type requirements
  typename S::value_type;
  typename S::size_type;
  typename S::pointer;
  typename S::reference;
  typename S::const_pointer;
  typename S::const_reference;

  // constructor requirements
  S{size};
  S{size, value};

  // element access
  { storage[size] } -> Std::convertible_to<typename S::const_reference>;
  { const_cast<S&>(storage)[size] } -> Std::convertible_to<typename S::reference>;

  // iterators
  { storage.begin() } -> Std::forward_iterator;
  { storage.end() }   -> Std::sentinel_for<std::decay_t<decltype(storage.begin())>>;

  // information
  { storage.size() } -> Std::convertible_to<typename S::size_type>;
  { storage.data() } -> Std::convertible_to<typename S::const_pointer>;
  { const_cast<S&>(storage).data() } -> Std::convertible_to<typename S::pointer>;
};

template <class C, class V, std::size_t size = Std::dynamic_extent>
concept Container = Storage<typename C::template Storage<V,size>>;


template <class S>
concept SparseStorage = requires(const S& storage, typename S::extents_type extents, typename S::indices_type indices)
{
  // type requirements
  typename S::value_type;
  typename S::size_type;
  typename S::pointer;
  typename S::reference;
  typename S::const_pointer;
  typename S::const_reference;

  // constructor requirements
  S{extents};

  // element access
  { storage.entry(indices) } -> Std::convertible_to<typename S::const_pointer>;
  { const_cast<S&>(storage).entry(indices) } -> Std::convertible_to<typename S::pointer>;

  // iterators
  { storage.begin(Tensor::tag::nz) } -> Std::forward_iterator;
  { storage.end(Tensor::tag::nz) }   -> Std::sentinel_for<std::decay_t<decltype(storage.begin(Tensor::tag::nz))>>;

  // information
  { storage.size() } -> Std::convertible_to<typename S::size_type>;
  { storage.data() } -> Std::convertible_to<typename S::const_pointer>;
  { const_cast<S&>(storage).data() } -> Std::convertible_to<typename S::pointer>;
};

template <class C, class V, class E>
concept SparseContainer = SparseStorage<typename C::template Storage<V,E>>;


} // end namespace Dune::Concept

#else // ENABLE_DUNE_CONCEPTS

// fallback implementation
#include <dune/common/concept.hh>

namespace Dune::Concept {

struct StorageConcept
{
  template <class S>
  auto require(const S& storage, typename S::size_type size = {}, typename S::value_type value = {})
    -> decltype
  (
    // type requirements
    requireType<typename S::value_type>(),
    requireType<typename S::size_type>(),
    requireType<typename S::pointer>(),
    requireType<typename S::reference>(),
    requireType<typename S::const_pointer>(),
    requireType<typename S::const_reference>(),

    // constructor requirements
    S{size},
    S{size, value},

    // element access
    requireConvertible<typename S::value_type>(storage[size]),

    // iterators
    // requireConcept<ForwardIterator>(storage.begin()),
    // requireConcept<SentinelFor<std::decay_t<decltype(storage.begin())>>>(
    //   storage.end()),

    // information
    requireConvertible<typename S::size_type>(storage.size()),
    requireConvertible<typename S::const_pointer>(storage.data()),
    requireConvertible<typename S::pointer>(const_cast<S&>(storage).data()),
  0);
};

template <class S>
inline constexpr bool Storage = models<StorageConcept,S>();


template <class V, std::size_t size = Std::dynamic_extent>
struct ContainerConcept
{
  template <class C>
  auto require(const C&) -> decltype
  (
    requireConcept<StorageConcept, typename C::template Storage<V,size>>()
  );
};

template <class C, class V, std::size_t size = Std::dynamic_extent>
inline constexpr bool Container = models<ContainerConcept<V,size>,C>();


struct SparseStorageConcept
{
  template <class S, class E>
  auto require(const S& storage, const E& extents, typename S::indices_type indices = {})
    -> decltype
  (
    // type requirements
    requireType<typename S::value_type>(),
    requireType<typename S::size_type>(),
    requireType<typename S::pointer>(),
    requireType<typename S::reference>(),
    requireType<typename S::const_pointer>(),
    requireType<typename S::const_reference>(),

    // constructor requirements
    S{extents},

    // element access
    requireConvertible<typename S::const_pointer>(storage.entry(indices)),
    requireConvertible<typename S::pointer>(const_cast<S&>(storage).entry(indices)),

    // iterators
    // requireConcept<ForwardIterator>(storage.begin(Tensor::tag::nz)),
    // requireConcept<SentinelFor<std::decay_t<decltype(storage.begin(Tensor::tag::nz))>>>(
    //   storage.end()),

    // information
    requireConvertible<typename S::size_type>(storage.size()),
    requireConvertible<typename S::const_pointer>(storage.data()),
    requireConvertible<typename S::pointer>(const_cast<S&>(storage).data()),
  0);
};

template <class S, class E = typename S::extents_type>
inline constexpr bool SparseStorage = models<SparseStorageConcept,S,E>();


template <class V, class E>
struct SparseContainerConcept
{
  template <class C>
  auto require(const C&) -> decltype
  (
    requireConcept<SparseStorageConcept, typename C::template Storage<V,E>, E>()
  );
};

template <class C, class V, class E>
inline constexpr bool SparseContainer = models<SparseContainerConcept<V,E>,C>();

} // end namespace Dune::Concept

#endif // ENABLE_DUNE_CONCEPTS
