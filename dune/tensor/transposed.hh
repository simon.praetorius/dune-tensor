#pragma once

#include <array>
#include <type_traits>

#include <dune/common/std/default_accessor.hh>
#include <dune/common/std/type_traits.hh>

#include <dune/tensor/tensorspan.hh>
#include <dune/tensor/accessors/transformedaccessor.hh>
#include <dune/tensor/extents/transposed.hh>
#include <dune/tensor/layouts/transposed.hh>
#include <dune/tensor/utility/conj.hh>

namespace Dune::Tensor {

/// \brief Permutation that interchanges the index `i` and `j`.
template <int i, int j>
struct Transposition
{
  static constexpr int first () { return i; }
  static constexpr int second () { return j; }

  template <int k>
  static constexpr int get () { return k == i ? j : k == j ? i : k; }
};

namespace Impl {

template <class Tensor, class Transposition, class Conjugation>
struct TransposedSpanTraits
{
  using tensor_type = std::remove_const_t<Tensor>;
  using element_type = std::remove_reference_t<decltype(*std::cbegin(std::declval<Tensor>().values()))>;

  using transposition_type = Transposition;

  using tensor_extents_type = typename tensor_type::extents_type;
  using extents_type = TransposedExtents<tensor_extents_type, transposition_type>;

  using tensor_layout_type = typename tensor_type::layout_type;
  using layout_type = TransposedLayout<tensor_layout_type, tensor_extents_type, transposition_type>;

  template <class T>
  using Accessor = typename T::accessor_type;
  using accessor_type = TransformedAccessor<
    Std::detected_or_t<Std::default_accessor<element_type>, Accessor, tensor_type>,
    Conjugation>;

  // type of the tensor span with transposed extents, layout, and accessor
  using span_type = TensorSpan<element_type, extents_type, layout_type, accessor_type>;
};

} // end namespace Impl


/**
 * \brief A transposed multi-dimensional span of values.
 *
 * This span with transposed the index-access in a tensor.
 *
 * \tparam TensorSpan   The original md-span with tensor interface
 * \tparam Trans        A transposition type, see \ref Transposition
 * \tparam Conjugation  A functor type representing the conjugate of a value
 *
 * NOTE: A TransposedSpan can also be represented as a PermutedSpan using `Transposition` as
 * a permutation type.
 */
template <class TensorSpan, class Trans, class Conjugation>
using TransposedSpan = typename Impl::TransposedSpanTraits<TensorSpan, Trans, Conjugation>::span_type;


template <class Trans = Transposition<0,1>,
          class Conjugation = Identity,
          class V,class E,class L,class A>
TransposedSpan<TensorSpan<V,E,L,A>,Trans,Conjugation> transposed (TensorSpan<V,E,L,A> const& span)
{
  using transposed_span = TransposedSpan<TensorSpan<V,E,L,A>,Trans,Conjugation>;
  using data_handle_type = typename transposed_span::data_handle_type;
  using mapping_type = typename transposed_span::mapping_type;
  using accessor_type = typename transposed_span::accessor_type;
  return transposed_span{data_handle_type(span.data_handle()),
                         mapping_type(span.mapping()),
                         accessor_type(span.accessor())};
}

template <class Trans = Transposition<0,1>,
          class Conjugation = Identity,
          class Tensor,
  decltype(std::declval<Tensor>().container(), bool{}) = true>
auto transposed (Tensor&& tensor)
{
  return transposed<Trans,Conjugation>(TensorSpan{tensor.container_data(),tensor.mapping()});
}

} // end namespace Dune::Tensor
