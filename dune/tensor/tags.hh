#pragma once

#include <cstddef>
#include <limits>

namespace Dune::Tensor {
namespace tag {

inline constexpr int dynamic = std::numeric_limits<int>::min();
inline constexpr int invalid = std::numeric_limits<int>::min();

struct from_range_t {};
static constexpr from_range_t from_range = {};

struct from_values_t {};
static constexpr from_values_t from_values = {};

struct major_t {};
static constexpr major_t major = {};

struct left_t {};
static constexpr left_t left = {};

struct right_t {};
static constexpr right_t right = {};

struct nz_t {};
static constexpr nz_t nz = {};

struct all_t {};
static constexpr all_t all = {};

struct unknown_t {};
static constexpr unknown_t unknown = {};

} // end namespace tag
} // end namespace Dune::Tensor
