#pragma once

#include <array>

#include <dune/common/std/extents.hh>
#include <dune/tensor/extents/traits.hh>
#include <dune/tensor/utility/abs.hh>

namespace Dune::Tensor {

/**
 * \brief Multidimensional index space with co and contra variant indices.
 * \ingroup Extents
 *
 * This class template represents a multidimensional index space of rank equal to `sizeof...(exts)`.
 * Each extent might be specified as a template parameter or as a dynamic parameter in the
 * constructor.
 *
 * \tparam SizeType  A signed integral type other than `bool`
 * \tparam exts...   Each element of exts is either `tag::dynamic` or a representable value of
 *                   type `SizeType`. Positive extents represent contravariant components and
 *                   negative extents covariant components.
 **/
template <class IndexType, int... exts>
class TensorType
    : public Std::extents<IndexType, Impl::abs_or_dynamic_extent(exts)...>
{
  using base_type = Std::extents<IndexType, Impl::abs_or_dynamic_extent(exts)...>;
  using array_type = std::array<int,sizeof...(exts)>;

  static_assert(std::is_signed_v<IndexType>);

  template <class S, std::size_t rd>
  static constexpr auto to_extents_array (std::array<int, rd> const& types)
  {
    std::array<typename base_type::index_type, rs> extents;
    for (std::size_t i; i < rd; ++i)
      extents[i] = Impl::abs(types[i]);
    return extents;
  }

public:
  using rank_type = typename base_type::rank_type;
  using size_type = typename base_type::size_type;
  using index_type = typename base_type::index_type;
  using extents_type = base_type;

  /// \brief The default constructor requires that all exts are not `Std::dynamic_extent`.
  template <class E = extents,
    std::enable_if_t<(E::rank_dynamic() == 0),int> = 0>
  constexpr TensorType () noexcept
  {}

  /// \brief Set all extents by the given integral sequence
  /// [[pre: all static extents correspond to the given value e]]
  template <class... IndexTypes,
    std::enable_if_t<(... && std::is_convertible_v<IndexTypes,int>), int> = 0,
    std::enable_if_t<(sizeof...(IndexTypes) == rank() || sizeof...(IndexTypes) == rank_dynamic()), int> = 0>
  constexpr TensorType (const IndexTypes&... e) noexcept
    : TensorType{std::array<int,sizeof...(e)>{int(e)...}}
  {}

  /// \brief Set all dynamic extents by the given integral array
  /// [[pre: all static extents correspond to the given values in e]]
  template <class S, std::size_t n,
    std::enable_if_t<std::is_convertible_v<S, int>, int> = 0,
    std::enable_if_t<(n == rank()  || n == rank_dynamic()), int> = 0>
  explicit constexpr TensorType (const std::array<S,n>& types) noexcept
    : base_type{to_extents_array(types)}
  {
    if constexpr(n == rank_dynamic())
      dynamic_types_ = types;
    else {
      for (rank_type i = 0; i < rank_dynamic(); ++i)
        dynamic_types_[i] = types[dynamic_index_inv(i)];
    }
  }

  template <class S, int... e>
  constexpr TensorType (const TensorType<S,e...>& other) noexcept
    : TensorType{as_array<int>(other)}
  {}

  /// \brief Return the static extent of dimension `i` with sign or `tag::dynamic`
  static constexpr int static_type (rank_type i) noexcept
  {
    return base_type::rank() == 0 ? 1 : array_type{exts...}[i];
  }

  /// \brief Return the extent of dimension `i` with sign
  constexpr int type (rank_type i) const noexcept
  {
    if constexpr(base_type::rank() == 0)
      return 1;
    else if (int e = static_type(i); e != tag::dynamic)
      return int(e);
    else
      return dynamic_types_[base_type::dynamic_index(i)];
  }

  const extents_type& to_extents () const
  {
    return static_cast<const base_type&>(*this);
  }

private:
  std::array<int, base_type::rank_dynamic()> dynamic_types_;
};


template <class SizeType, int... exts>
struct IsMixedTensor<TensorType<SizeType,exts...>>
    : std::true_type {};

} // end namespace Dune::Tensor
