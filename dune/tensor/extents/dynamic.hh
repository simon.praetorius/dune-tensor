#pragma once

#include <array>
#include <type_traits>

#include <dune/common/filledarray.hh>
#include <dune/common/std/span.hh>

namespace Dune::Tensor {

/**
 * \brief Multidimensional index space with purely dynamic extents.
 * \ingroup Extents
 *
 * \tparam IndexType An integral type other than `bool` representing the size and indices
 * \tparam R         The rank (number of dimensions) of the index space
 **/
template <class IndexType, std::size_t R>
class DynamicExtents
{
public:
  using rank_type = int;
  using index_type = IndexType;
  using size_type = std::make_unsigned_t<index_type>;

  template <class Size, Size N,
    std::enable_if_t<std::is_convertible_v<Size,std::size_t>, int> = 0,
    std::enable_if_t<(N == R), int> = 0>
  constexpr DynamicExtents (std::integral_constant<Size,N>, index_type ext) noexcept
    : exts_{Dune::filledArray<N>(ext)}
  {}

  template <class... IndexTypes,
    std::enable_if_t<(... && std::is_convertible_v<IndexTypes,index_type>), int> = 0,
    std::enable_if_t<(sizeof...(IndexTypes) == R), int> = 0>
  constexpr DynamicExtents (const IndexTypes&... exts) noexcept
    : exts_{exts...}
  {}

  template <class I, std::size_t N,
    std::enable_if_t<std::is_convertible_v<I, index_type>, int> = 0,
    std::enable_if_t<(N == R), int> = 0>
  explicit constexpr DynamicExtents (const std::array<I,N>& exts) noexcept
    : exts_{exts}
  {}

  static constexpr rank_type rank () noexcept { return R; }

  static constexpr std::size_t static_extent (rank_type /*r*/) noexcept
  {
    return rank() == 0 ? 1 : Std::dynamic_extent;
  }

  constexpr index_type extent (rank_type r) const noexcept
  {
    return rank() == 0 ? 1 : exts_[r];
  }

  static constexpr std::size_t static_product () noexcept { return Std::dynamic_extent; }

  constexpr size_type product () const noexcept
  {
    size_type prod = 1;
    for (rank_type i = 0; i < rank(); ++i)
      prod *= exts_[i];
    return prod;
  }

  template <class OtherIndexType, std::size_t otherRank>
  friend constexpr bool operator== (const DynamicExtents& a, const DynamicExtents<OtherIndexType, otherRank>& b) noexcept
  {
    return a.rank() == b.rank() && a.exts_ == b.exts_;
  }

private:
  std::array<index_type,R> exts_;
};

} // end namespace Dune::Tensor
