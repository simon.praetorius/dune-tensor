#pragma once

#include <algorithm>
#include <type_traits>

#include <dune/common/std/extents.hh>
#include <dune/tensor/utility/apply.hh>

namespace Dune::Tensor {

// forward declarations
template <class SizeType, int r>
class DynamicExtents;

template <class SizeType, int... exts>
class StaticExtents;

/// Type traits that defines true if extents can be positive and negative
template <class Extents>
struct IsMixedTensor
    : std::false_type {};

namespace Impl {

// Extents of a tensor that is the result of an expression involving multiple
// tensors of potentially different extents. We assume that all tensors have
// the same shape, but it might be that some extents are given only as dynamic
// value and others as static value. Prefer the static extents over dynamic
// extents.
template <class... EE>
struct ExtentsTraits;

template <class E0>
struct ExtentsTraits<E0> { using type = E0; };

template <class S0, std::size_t... e0, class S1, std::size_t... e1>
struct ExtentsTraits<Std::extents<S0,e0...>,Std::extents<S1,e1...>>
{
  using S = std::common_type_t<S0,S1>;
  using type = Std::extents<S, std::max(e0,e1)...>;
};

template <class S0, std::size_t... e0, class S1, std::size_t... e1>
struct ExtentsTraits<Dune::Tensor::StaticExtents<S0,e0...>,Std::extents<S1,e1...>>
{
  using S = std::common_type_t<S0,S1>;
  using type = Std::extents<S, std::max(e0,e1)...>;
};

template <class S0, std::size_t... e0, class S1, std::size_t... e1>
struct ExtentsTraits<Std::extents<S0,e0...>,Dune::Tensor::StaticExtents<S1,e1...>>
{
  using S = std::common_type_t<S0,S1>;
  using type = Std::extents<S, std::max(e0,e1)...>;
};

template <class S0, std::size_t... e, class S1>
struct ExtentsTraits<Dune::Tensor::StaticExtents<S0,e...>,Dune::Tensor::StaticExtents<S1,e...>>
{
  using S = std::common_type_t<S0,S1>;
  using type = Std::extents<S, e...>;
};

template <class S0, std::size_t n0, class S1, std::size_t... e1>
struct ExtentsTraits<Dune::Tensor::DynamicExtents<S0,n0>,Std::extents<S1,e1...>>
{
  using S = std::common_type_t<S0,S1>;
  using type = Std::extents<S, e1...>;
};

template <class S0, std::size_t n0, class S1, std::size_t... e1>
struct ExtentsTraits<Dune::Tensor::DynamicExtents<S0,n0>,Dune::Tensor::StaticExtents<S1,e1...>>
{
  using S = std::common_type_t<S0,S1>;
  using type = Std::extents<S, e1...>;
};

template <class S0, std::size_t... e0, class S1, std::size_t n1>
struct ExtentsTraits<Std::extents<S0,e0...>,Dune::Tensor::DynamicExtents<S1,n1>>
{
  using S = std::common_type_t<S0,S1>;
  using type = Std::extents<S, e0...>;
};

template <class S0, std::size_t... e0, class S1, std::size_t n1>
struct ExtentsTraits<Dune::Tensor::StaticExtents<S0,e0...>,Dune::Tensor::DynamicExtents<S1,n1>>
{
  using S = std::common_type_t<S0,S1>;
  using type = Std::extents<S, e0...>;
};

template <class S0, std::size_t n, class S1>
struct ExtentsTraits<Dune::Tensor::DynamicExtents<S0,n>,Dune::Tensor::DynamicExtents<S1,n>>
{
  using S = std::common_type_t<S0,S1>;
  using type = Std::dextents<S, n>;
};


template <class E0, class E1, class... EE>
struct ExtentsTraits<E0,E1,EE...>
{
  using type = typename ExtentsTraits<
    typename ExtentsTraits<E0,E1>::type, typename ExtentsTraits<E0,EE>::type...>::type;
};

} // end namespace Impl


namespace Impl {

template <class Seq, class... E>
struct IsAnyDynamicImpl;

template <std::size_t... II, class... E>
struct IsAnyDynamicImpl<std::index_sequence<II...>, E...>
{
  template <std::size_t I>
  static constexpr bool value_i = (... || (E::static_extent(I) == Std::dynamic_extent));
  static constexpr bool value = (... || value_i<II>);
};

} // end namespace Impl

/// \brief Traits type that is true if any of the given extents has any dynamic extent
template <class E0, class... Es>
using IsAnyDynamic = Impl::IsAnyDynamicImpl<std::make_index_sequence<E0::rank()>, E0, Es...>;

/// \brief Traits constant that is true if any of the given extents has any dynamic extent
template <class E0, class... Es>
inline constexpr bool isAnyDynamic_v = IsAnyDynamic<E0,Es...>::value;


namespace Impl {

template <class Seq, class... E>
struct MaxExtentImpl;

template <std::size_t... II, class... E>
struct MaxExtentImpl<std::index_sequence<II...>, E...>
{
  using index_type = std::common_type_t<typename E::index_type...>;

  template <std::size_t I>
  static constexpr index_type value_i = std::max({E::static_extent(I)...});
  static constexpr index_type value = std::max({value_i<II>...});
};

} // end namespace Impl

/// \brief Integral constant representing the maximal extent of all given extents
template <class E0, class... Es>
using MaxExtent = Impl::MaxExtentImpl<std::make_index_sequence<E0::rank()>, E0, Es...>;

/// \brief Integral value representing the maximal extent of all given extents
template <class E0, class... Es>
inline constexpr auto maxExtent_v = MaxExtent<E0,Es...>::value;

/// \brief A representation of all extents as an array
template <class I, std::size_t... exts>
constexpr std::array<I,sizeof...(exts)> as_array (Std::extents<I,exts...> const& e) noexcept
{
  return Dune::Tensor::applyIndex<sizeof...(exts)>([&](auto... ii) {
    return std::array<I,sizeof...(exts)>{I(e.extent(ii))...}; });
}


/// \brief The product of all static extents or `Std::dynamic_extent` if any extent is dynamic
template <class Extents>
constexpr std::size_t static_extents_product () noexcept
{
  if constexpr(Extents::rank_dynamic() > 0)
    return Std::dynamic_extent;
  else {
    std::size_t s = 1;
    for (std::size_t r = 0; r < Extents::rank(); ++r)
      s *= Extents::static_extent(r);
    return s;
  }
}

/// \brief The product of all dynamic extents
template <class Extents>
constexpr typename Extents::index_type extents_product (Extents const& extents) noexcept
{
  typename Extents::index_type s = 1;
  for (std::size_t r = 0; r < Extents::rank(); ++r)
    s *= extents.extent(r);
  return s;
}




} // end namespace Dune::Tensor
