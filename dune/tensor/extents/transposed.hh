#pragma once

#include <algorithm>
#include <type_traits>

#include <dune/common/std/extents.hh>
#include <dune/tensor/utility/apply.hh>

namespace Dune::Tensor {
namespace Impl {

template <class Extents, class Transposition>
struct TransposedExtentsImpl
{
private:
  using original_extents_type = Extents;

public:
  using index_type = typename original_extents_type::index_type;
  using size_type = typename original_extents_type::size_type;
  using rank_type = typename original_extents_type::rank_type;
  using transposition_type = Transposition;

  constexpr static std::size_t static_extent (rank_type r)
  {
    return r == rank_type(transposition_type::first())
                ? original_extents_type::static_extent(transposition_type::second()) :
           r == rank_type(transposition_type::second())
                ? original_extents_type::static_extent(transposition_type::first()) :
                  original_extents_type::static_extent(r);
  }

  template <class Seq>
  struct extents_type_impl;

  template <rank_type... r>
  struct extents_type_impl<std::integer_sequence<rank_type,r...>>
  {
    using type = Std::extents<index_type,static_extent(r)...>;
  };

  using type = typename extents_type_impl<std::make_integer_sequence<rank_type,original_extents_type::rank()>>::type;
};

} // end namespace Impl

/// \brief Type of the transposed extents
template <class Extents, class Transposition>
using TransposedExtents = typename Impl::TransposedExtentsImpl<Extents,Transposition>::type;


/**
 * \brief Transposition of the extents is of type \ref TransposedExtents
 *
 * The transposition of the extents is given by a transposition parameter `Trans` that
 * provides static `first()` and `second()` method of the extents components that should be
 * switched. See \ref Transposition for an example.
 **/
template <class Trans, class Extents>
constexpr TransposedExtents<Extents,Trans> transposedExtents (const Extents& extents)
{
  using extents_type = TransposedExtents<Extents,Trans>;
  return applyIndex<Extents::rank()>([&](auto... r) -> extents_type
  {
    return extents_type{
      (r.value == Trans::first() ? extents.extent(Trans::second()) :
        r.value == Trans::second() ? extents.extent(Trans::first()) :
        extents.extent(r.value))...
    };
  });
}

} // end namespace Dune::Tensor
