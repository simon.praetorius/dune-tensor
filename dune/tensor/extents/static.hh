#pragma once

#include <array>
#include <type_traits>

namespace Dune::Tensor {

/**
 * \brief Multidimensional index space with purely static extents.
 * \ingroup Extents
 *
 * \tparam IndexType  An integral type other than `bool` representing the size and indices
 * \tparam exts...   Each extent in `exts` must be representable in the type `IndexType`.
 **/
template <class IndexType, std::size_t... exts>
class StaticExtents
{
  using array_type = std::array<std::size_t, sizeof...(exts)>;

public:
  using rank_type = std::size_t;
  using index_type = IndexType;
  using size_type = std::make_unsigned_t<index_type>;

  constexpr StaticExtents () noexcept = default;

  template <class I, std::size_t n,
    std::enable_if_t<std::is_convertible_v<I, index_type>, int> = 0,
    std::enable_if_t<(n == sizeof...(exts)), int> = 0>
  explicit constexpr StaticExtents (std::array<I,n>) noexcept {}

  static constexpr rank_type rank () noexcept { return sizeof...(exts); }

  static constexpr std::size_t static_extent (rank_type r) noexcept
  {
    return rank() == 0 ? 1 : array_type{exts...}[r];
  }

  constexpr index_type extent (rank_type r) const noexcept
  {
    return static_extent(r);
  }

  static constexpr std::size_t static_product () noexcept { return (1u * ... * exts); }
  constexpr size_type product () const noexcept { return (1u * ... * exts); }

  template <class OtherIndexType, std::size_t... otherExts>
  friend constexpr bool operator== (const StaticExtents& a, const StaticExtents<OtherIndexType, otherExts...>& b) noexcept
  {
    if constexpr(sizeof...(exts) == sizeof...(otherExts))
      return (... && (exts == otherExts));
    else
      return false;
  }
};

} // end namespace Dune::Tensor
