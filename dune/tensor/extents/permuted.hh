#pragma once

#include <algorithm>
#include <type_traits>

#include <dune/common/std/extents.hh>
#include <dune/tensor/utility/apply.hh>

namespace Dune::Tensor {
namespace Impl {

template <class Extents, class Perm>
struct PermutedExtentsImpl
{
private:
  using original_extents_type = Extents;

public:
  using index_type = typename original_extents_type::index_type;
  using rank_type = typename original_extents_type::rank_type;
  using iota = std::make_integer_sequence<rank_type,original_extents_type::rank()>;
  using perm_type = Perm;

  template <class Seq>
  struct extents_type_impl;

  template <rank_type... r>
  struct extents_type_impl<std::integer_sequence<rank_type,r...>>
  {
    using type = Std::extents<index_type,perm_type::template get<r>()...>;
  };

  using type = typename extents_type_impl<iota>::type;
};

} // end namespace Impl

/// \brief Type of the permuted extents
template <class Extents, class Perm>
using PermutedExtents = typename Impl::PermutedExtentsImpl<Extents,Perm>::type;


/**
 * \brief Permutation of the extents is of type \ref PermutedExtends
 *
 * The permutation of the extents is given by a permutation parameter `Perm` that
 * provides a static `get<index>()` method returning the permuted index in the extents
 * tuple. See \ref Permutation for an example.
 **/
template <class Perm, class Extents>
constexpr PermutedExtents<Extents,Perm> permutedExtents (const Extents& extents)
{
  using extents_type = PermutedExtents<Extents,Perm>;
  return applyIndex<Extents::rank()>([&](auto... r) -> extents_type
  {
    return extents_type{extents.extent(Perm::template get<decltype(r)::value>())...};
  });
}

} // end namespace Dune::Tensor
