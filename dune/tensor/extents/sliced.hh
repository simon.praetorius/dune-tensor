#pragma once

#include <array>
#include <cassert>
#include <type_traits>

#include <dune/common/std/extents.hh>
#include <dune/common/std/impl/applyindex.hh>
#include <dune/tensor/slices.hh>
#include <dune/tensor/tags.hh>
#include <dune/tensor/extents/extents.hh>
#include <dune/tensor/utility/apply.hh>

namespace Dune::Tensor {
namespace Impl {

template <std::size_t...> struct Exts {};

template <class S, class E0, class E1, class... Slices>
struct SlicedExtents;


template <class Extent>
std::size_t slicedExtent (Extent extent, tag::all_t slice)
{
  return extent;
}

template <class Extent, class Begin, class End>
std::size_t slicedExtent (Extent extent, IndexRange<Begin,End> slice)
{
  assert(slice.end - slice.begin <= extent);
  return slice.end - slice.begin;
}

template <class Extent, class Begin, class Length, class Stride>
std::size_t slicedExtent (Extent extent, StridedIndexRange<Begin,Length,Stride> slice)
{
  assert(slice.extent <= extent);
  return slice.extent;
}

template <class Extent, class Index,
  std::enable_if_t<std::is_integral_v<Index>, int> = 0>
std::size_t slicedExtent (Extent extent, Index slice)
{
  return tag::invalid;
}

template <class Extent, class Index, Index index,
  std::enable_if_t<std::is_integral_v<Index>, int> = 0>
std::size_t slicedExtent (Extent extent, std::integral_constant<Index,index> slice)
{
  return tag::invalid;
}

} // end namespace Impl


/// \brief Type of the extents given as a subset of `E` subject to the provided slice specifiers `S`
template <class E, class... S>
struct SlicedExtents;

template <class E, class... S>
using SlicedExtents_t = typename SlicedExtents<E,S...>::type;

template <class S, std::size_t... exts, class... Slices>
struct SlicedExtents<Std::extents<S,exts...>, Slices...>
{
  using type = typename Impl::SlicedExtents<S,Impl::Exts<>,Impl::Exts<exts...>,Slices...>::type;
};


/**
 * \brief A sub-set of the given extents subject to the provided slice specifiers.
 */
template <class S, std::size_t... exts, class... Slices,
  std::enable_if_t<(sizeof...(exts) == sizeof...(Slices)), int> = 0>
auto slicedExtents (const Std::extents<S,exts...>& extents, Slices... slices)
{
  using AllSlicedExtents = std::array<S,sizeof...(exts)>;
  auto allSlicedExtents = Std::Impl::applyIndex<sizeof...(exts)>([&](auto... i) {
    return AllSlicedExtents{S(Impl::slicedExtent(extents.extent(i), slices))...};
  });

  using SlicedExtents = SlicedExtents_t<Std::extents<S,exts...>, Slices...>;
  std::array<S,SlicedExtents::rank()> slicedExtents;
  std::size_t r = 0;
  for (std::size_t i = 0; i < allSlicedExtents.size(); ++i) {
    if (allSlicedExtents[i] != tag::invalid)
      slicedExtents[r++] = allSlicedExtents[i];
  }

  return SlicedExtents{slicedExtents};
}


namespace Impl {

// if all original extents and slices are processed, define the final type
template <class S, std::size_t... e>
struct SlicedExtents<S, Exts<e...>, Exts<>>
{
  using type = Std::extents<S,e...>;
};

// specialization for full extents
template <class S, std::size_t... e, std::size_t f0, std::size_t... ff, class... Slices>
struct SlicedExtents<S, Exts<e...>, Exts<f0,ff...>, tag::all_t, Slices...>
{
  using type = typename SlicedExtents<S, Exts<e...,f0>, Exts<ff...>, Slices...>::type;
};

// specialization for scalar extents
template <class S, std::size_t... e, std::size_t f0, std::size_t... ff,
          class Slice0, class... Slices>
struct SlicedExtents<S, Exts<e...>, Exts<f0,ff...>, Slice0, Slices...>
{
  static_assert(std::is_convertible_v<Slice0, std::size_t>);
  using type = typename SlicedExtents<S, Exts<e...>, Exts<ff...>, Slices...>::type;
};


template <class A, class B>
struct DifferenceValue {
  // if the index-range has dynamic bounds, the extents are dynamic
  static constexpr std::size_t value = Std::dynamic_extent;
};

template <class A, A a, class B, B b>
struct DifferenceValue<std::integral_constant<A,a>, std::integral_constant<B,b>> {
  // if the index-range has static bounds, the extents are static
  static constexpr std::size_t value = a - b;
};

// specialization for sub-range extents
template <class S, std::size_t... e, std::size_t f0, std::size_t... ff,
          class Begin, class End, class... Slices>
struct SlicedExtents<S, Exts<e...>, Exts<f0,ff...>, IndexRange<Begin,End>, Slices...>
{
  using extent_type = DifferenceValue<End,Begin>;
  using type = typename SlicedExtents<S, Exts<e...,extent_type::value>, Exts<ff...>, Slices...>::type;
};


template <class E>
struct ExtentValue {
  // if the extent is dynamic value, the result is `Std::dynamic_extent`
  static constexpr std::size_t value = Std::dynamic_extent;
};

template <class A, A a>
struct ExtentValue<std::integral_constant<A,a>> {
  // if the extent is static value, the result is that value
  static constexpr std::size_t value = a;
};

// specialization for strided extents
template <class S, std::size_t... e, std::size_t f0, std::size_t... ff,
          class Begin, class Extent, class Stride, class... Slices>
struct SlicedExtents<S, Exts<e...>, Exts<f0,ff...>, StridedIndexRange<Begin,Extent,Stride>, Slices...>
{
  using extent_type = ExtentValue<Extent>;
  using type = typename SlicedExtents<S, Exts<e...,extent_type::value>, Exts<ff...>, Slices...>::type;
};

} // end namespace Impl
} // end namespace Dune::Tensor
