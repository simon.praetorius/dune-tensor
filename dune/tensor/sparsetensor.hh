#pragma once

#include <array>
#include <tuple>
#include <type_traits>

#include <dune/common/ftraits.hh>
#include <dune/common/std/span.hh>
#include <dune/tensor/derivativetraits.hh>
#include <dune/tensor/containers/traits.hh>
#include <dune/tensor/utility/bracketproxy.hh>

namespace Dune::Tensor {

/**
 * \brief A sparse tensor of arbitrary rank and individual dimension extents.
 * \ingroup Tensors
 * \nosubgrouping
 *
 * \tparam Value    The element type stored in the tensor.
 * \tparam Extents  The type representing the tensor extents. Must be compatible
 *                  to `Dune::Tensor::Extents`.
 * \tparam Layout   A class providing the index mapping from the multi-dimensional
 *                  index space to a single index usable in the `Container`. See
 *                  the `Dune::Concept::Layout` concept.
 * \tparam ContainerPolicy  A storage backend with single index access a compatible
 *                    initialization constructors. See the `Dune::Concept::Container`
 *                    concept.
 * \tparam Traits   Provider for inserter and iterators templates.
 **/
template <class Value, class Extents, class Layout, class ContainerPolicy, class Traits>
class SparseTensor
{
  template <class,class,class,class,class> friend class SparseTensor;

public:
  using value_type = Value;
  using field_type = typename Dune::FieldTraits<value_type>::field_type;
  using extents_type = Extents;
  using layout_type = Layout;
  using mapping_type = typename layout_type::template mapping<extents_type>;
  using size_type = typename extents_type::size_type;
  using rank_type = typename extents_type::rank_type;
  using index_type = typename extents_type::index_type;

  using storage_type = Container_t<Value,Extents,Layout,ContainerPolicy>;

  using pointer = typename storage_type::pointer;
  using reference = typename storage_type::reference;
  using const_pointer = typename storage_type::const_pointer;
  using const_reference = typename storage_type::const_reference;

  using inserter_type = typename Traits::template Inserter<SparseTensor>;
  using iterators_type = typename Traits::Iterators;

public:
  /// \name SparseTensor constructors
  /// @{

  template <class E = extents_type,
    std::enable_if_t<std::is_default_constructible_v<E>, int> = 0>
  SparseTensor () noexcept
    : SparseTensor{extents_type{}}
  {}

  explicit SparseTensor (const extents_type& e) noexcept
    : SparseTensor{mapping_type{e}}
  {}

  explicit SparseTensor (const mapping_type& m) noexcept
    : mapping_{m}
    , storage_(mapping_.required_span_size())
  {}

  /// \brief Construct from the layout of the tensor
  explicit SparseTensor (mapping_type&& m) noexcept
    : mapping_{std::move(m)}
    , storage_(mapping_.required_span_size())
  {}


  // ----------------------------
  // constructors from storage

  /// \brief Construct from storage
  template <class E = extents_type,
    std::enable_if_t<std::is_default_constructible_v<E>, int> = 0>
  constexpr SparseTensor (const storage_type& storage) noexcept
    : SparseTensor{storage, extents_type{}}
  {}

  /// \brief Construct from storage
  template <class E = extents_type,
    std::enable_if_t<std::is_default_constructible_v<E>, int> = 0>
  constexpr SparseTensor (storage_type&& storage) noexcept
    : SparseTensor{std::move(storage), extents_type{}}
  {}

  /// \brief Construct from storage and individual extents
  template <class R, class... SizeTypes,
    std::enable_if_t<(... && std::is_convertible_v<SizeTypes,size_type>), int> = 0,
    std::enable_if_t<std::is_constructible_v<extents_type,SizeTypes...>, int> = 0>
  constexpr SparseTensor (const storage_type& storage, SizeTypes... exts) noexcept
    : SparseTensor{storage, extents_type{exts...}}
  {}

  /// \brief Construct from storage and individual extents
  template <class R, class... SizeTypes,
    std::enable_if_t<(... && std::is_convertible_v<SizeTypes,size_type>), int> = 0,
    std::enable_if_t<std::is_constructible_v<extents_type,SizeTypes...>, int> = 0>
  constexpr SparseTensor (storage_type&& storage, SizeTypes... exts) noexcept
    : SparseTensor{std::move(storage), extents_type{exts...}}
  {}

  /// \brief Construct from storage and extents
  constexpr SparseTensor (const storage_type& storage, const extents_type& e) noexcept
    : mapping_{e}
    , storage_{storage}
  {}

  /// \brief Construct from storage and extents
  constexpr SparseTensor (storage_type&& storage, const extents_type& e) noexcept
    : mapping_{e}
    , storage_{std::move(storage)}
  {}

  /// \brief Construct from mapping and storage
  constexpr SparseTensor (const mapping_type& m, const storage_type& s) noexcept
    : mapping_{m}
    , storage_{s}
  {}

  /// \brief Construct from mapping and storage
  constexpr SparseTensor (const mapping_type& m, storage_type&& s) noexcept
    : mapping_{m}
    , storage_{std::move(s)}
  {}

  /// \brief Construct from mapping and storage
  constexpr SparseTensor (mapping_type&& m, const storage_type& s) noexcept
    : mapping_{std::move(m)}
    , storage_{s}
  {}

  /// \brief Construct from mapping and storage
  constexpr SparseTensor (mapping_type&& m, storage_type&& s) noexcept
    : mapping_{std::move(m)}
    , storage_{std::move(s)}
  {}


  /// \brief Converting copy constructor
  template <class V, class E, class L, class S, class T>
  SparseTensor (const SparseTensor<V,E,L,S,T>& tensor) noexcept
    : mapping_{tensor.mapping_}
    , storage_{tensor.storage_}
  {}

  /// \brief Converting move constructor
  template <class V, class E, class L, class S, class T>
  SparseTensor (SparseTensor<V,E,L,S,T>&& tensor) noexcept
    : mapping_{std::move(tensor.mapping_)}
    , storage_{std::move(tensor.storage_)}
  {}

  /// @}


  /// \name Assignment operators
  /// @{

  /// \brief Assign the given value to all elements of the tensor
  SparseTensor& operator= (const value_type& value) noexcept
  {
    for (auto& el : values())
      el = value;
    return *this;
  }

  /// @}


  inserter_type inserter () noexcept
  {
    return inserter_type{this};
  }


  /// \name Multi index access
  /// @{

  /**
   * \brief Access specified element at position i0,i1,...
   * \throws std::bad_optional_access
  */
  template <class... Indices,
    std::enable_if_t<(... && std::is_convertible_v<Indices,index_type>), int> = 0>
  reference operator() (Indices... indices)
  {
    return storage_[mapping_(indices...).value()];
  }

  /// \brief Access specified element at position i0,i1,...
  template <class... Indices,
    std::enable_if_t<(... && std::is_convertible_v<Indices,index_type>), int> = 0>
  value_type operator() (Indices... indices) const noexcept
  {
    const auto index = mapping_(indices...);
    return index.has_value() ? storage_[*index] : value_type(0);
  }


  /**
   * \brief Access specified element at position [{i0,i1,...}]
   * \throws std::bad_optional_access
  */
  template <class Index,
    std::enable_if_t<std::is_convertible_v<Index,index_type>, int> = 0>
  reference operator[] (const std::array<Index,extents_type::rank()>& indices)
  {
    return std::apply([&](auto... ii) -> reference {
      return storage_[mapping_(ii...).value()]; }, indices);
  }

  /// \brief Access specified element at position [{i0,i1,...}]
  template <class Index,
    std::enable_if_t<std::is_convertible_v<Index,index_type>, int> = 0>
  value_type operator[] (const std::array<Index,extents_type::rank()>& indices) const noexcept
  {
    return std::apply([&](auto... ii) -> value_type {
      const auto index = mapping_(ii...);
      return index.has_value() ? storage_[*index] : value_type(0); }, indices);
  }


  /// \brief Access specified element at position [i0][i1]...
  /**
   * \throws std::bad_optional_access
  */
#ifndef DOXYGEN
  template <class E = extents_type,
    std::enable_if_t<(E::rank() >= 2), int> = 0>
#endif
  auto operator[] (index_type i0)
  {
    return BracketProxy<SparseTensor,2,E::rank()>{this, i0};
  }

#ifndef DOXYGEN
  /**
   * \brief Access specified element at position [i0]
   * \throws std::bad_optional_access
  */
  template <class E = extents_type,
    std::enable_if_t<(E::rank() == 1), int> = 0>
  reference operator[] (index_type i0)
  {
    return storage_[mapping_(i0).value()];
  }
#endif

  /// \brief Access specified element at position [i0][i1]...
#ifndef DOXYGEN
  template <class E = extents_type,
    std::enable_if_t<(E::rank() >= 2), int> = 0>
#endif
  auto operator[] (index_type i0) const noexcept
  {
    return BracketProxy<const SparseTensor,2,E::rank()>{this, i0};
  }

#ifndef DOXYGEN
  /// \brief Access specified element at position [i0]
  template <class E = extents_type,
    std::enable_if_t<(E::rank() == 1), int> = 0>
  value_type operator[] (index_type i0) const noexcept
  {
    const auto index = mapping_(i0);
    return index.has_value() ? storage_[*index] : value_type(0);
  }
#endif

  /// @}


  /// \brief Check whether position i0,i1,... exists
  template <class... Indices,
    std::enable_if_t<(... && std::is_convertible_v<Indices,index_type>), int> = 0>
  bool exists (Indices... indices) const noexcept
  {
    return mapping_(indices...).has_value();
  }

  /// \brief Index mapping of a layout policy
  constexpr mapping_type& mapping () noexcept { return mapping_; }

  /// \brief Index mapping of a layout policy
  constexpr const mapping_type& mapping () const noexcept { return mapping_; }

  /// \brief The underlying storage
  constexpr storage_type& storage () noexcept { return storage_; }

  /// \brief The underlying storage
  constexpr const storage_type& storage () const noexcept { return storage_; }


  /// \name Size information
  /// @{

  /// \brief Number of dimensions of the tensor
  static constexpr rank_type rank () noexcept { return extents_type::rank(); }

  /// \brief Number of elements in the r'th dimension of the tensor
  static constexpr std::size_t static_extent (rank_type r) noexcept
  {
    return extents_type::static_extent(r);
  }

  /// \brief Number of elements in the r'th dimension of the tensor
  constexpr index_type extent (rank_type r) const noexcept
  {
    return extents().extent(r);
  }

  /// \brief Total number of elements in the tensor
  constexpr size_type size () const noexcept { return storage_.size(); }

  /// \brief Number of elements in all dimensions of the tensor, \related Extents
  constexpr const extents_type& extents () const noexcept
  {
    return mapping().extents();
  }

  /// @}


  static constexpr bool is_always_dense () noexcept
  {
    return false;
  }


  /// \name Iterators
  /// @{

  /// \brief Iterator over the major axis of the tensor
  template <class Tag, class E = extents_type, std::enable_if_t<(E::rank() > 0), int> = 0>
  auto begin (Tag tag) const noexcept
  {
    return iterators_type::begin(tag, mapping_, storage_);
  }

  /// \brief Iterator over the major axis of the 0-tensor
  template <class Tag, class E = extents_type, std::enable_if_t<(E::rank() == 0), int> = 0>
  auto begin (Tag /*tag*/) const noexcept
  {
    return storage_.begin();
  }

  /// \brief End-Iterator over the major axis of the tensor
  template <class Tag, class E = extents_type, std::enable_if_t<(E::rank() > 0), int> = 0>
  auto end (Tag tag) const noexcept
  {
    return iterators_type::end(tag, mapping_, storage_);
  }

  /// \brief End-Iterator over the major axis of the tensor
  template <class Tag, class E = extents_type, std::enable_if_t<(E::rank() == 0), int> = 0>
  auto end (Tag /*tag*/) const noexcept
  {
    return storage_.end();
  }

  /// @}


  /// \name Direct access to the data
  /// @{

  /// \brief Return a span of the (nonzero) values stored in the tensor
  constexpr Std::span<value_type,Std::dynamic_extent> values () noexcept
  {
    using span_type = Std::span<value_type,Std::dynamic_extent>;
    return span_type(storage_.data(), storage_.size());
  }

  /// \brief Return a const span of the (nonzero) values stored in the tensor
  constexpr Std::span<value_type const,Std::dynamic_extent> values () const noexcept
  {
    using span_type = Std::span<value_type const,Std::dynamic_extent>;
    return span_type(storage_.data(), storage_.size());
  }

  /// @}

protected:
  mapping_type mapping_;
  storage_type storage_;
};

} // end namespace Dune::Tensor


template <class V, class E, class L, class C, class T>
struct Dune::FieldTraits< Dune::Tensor::SparseTensor<V,E,L,C,T> >
{
  using field_type = typename FieldTraits<V>::field_type;
  using real_type = typename FieldTraits<V>::real_type;
};
