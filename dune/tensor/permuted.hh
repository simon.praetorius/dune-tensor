#pragma once

#include <array>
#include <type_traits>

#include <dune/common/std/type_traits.hh>
#include <dune/common/std/default_accessor.hh>

#include <dune/tensor/tensorspan.hh>
#include <dune/tensor/accessors/transformedaccessor.hh>
#include <dune/tensor/extents/permuted.hh>
#include <dune/tensor/layouts/permuted.hh>
#include <dune/tensor/utility/conj.hh>

namespace Dune::Tensor {

/**
 * \brief Permutation of indices.
 *
 * The index `j` is mapped to index `{i...}[j]`. Note, The sequence `{i...}`
 * must be a shuffle of the consecutive range `{0,1,2...,n-1}` with `n == sizeof...(i)`.
 */
template <int... i>
struct Permutation
{
  template <int j>
  static constexpr int get () { return std::array{i...}[j]; }
};

/// \brief The IdentityPermutation maps the index `j -> j`.
struct IdentityPermutation
{
  template <int j>
  static constexpr int get () { return j; }
};


namespace Impl {

// construct the type of a permuted tensor span by
// - permutation of the extents, using PermutedExtents.
// - permutation of the layout (mapping), using PermutedLayout.
// - wrapping the accessor using `TransformedAccessor with the `Conjugation` functor.
template <class Tensor, class Permutation, class Conjugation>
struct PermutedSpanTraits
{
  using tensor_type = std::remove_const_t<Tensor>;
  using element_type = std::remove_reference_t<decltype(*std::cbegin(std::declval<Tensor>().values()))>;

  using perm_type = Permutation;

  using tensor_extents_type = typename tensor_type::extents_type;
  using extents_type = PermutedExtents<tensor_extents_type, perm_type>;

  using tensor_layout_type = typename tensor_type::layout_type;
  using layout_type = PermutedLayout<tensor_layout_type, tensor_extents_type, perm_type>;

  template <class T>
  using Accessor = typename T::accessor_type;
  using accessor_type = TransformedAccessor<
    Std::detected_or_t<Std::default_accessor<element_type>, Accessor, tensor_type>,
    Conjugation>;

  // the type of the tensor span with permuted extents, layout, and accessor
  using span_type = TensorSpan<element_type, extents_type, layout_type, accessor_type>;
};

} // end namespace Impl


/**
 * \brief A permuted multi-dimensional span of values.
 *
 * This span permutes the index-access in a tensor.
 *
 * \tparam TensorSpan   The original md-span with tensor interface
 * \tparam Perm         A permutation type, see \ref Permutation
 * \tparam Conjugation  A functor type representing the conjugate of a value
 */
template <class TensorSpan,
          class Perm = IdentityPermutation,
          class Conjugation = Identity>
using PermutedSpan = typename Impl::PermutedSpanTraits<TensorSpan, Perm, Conjugation>::span_type;

template <class Perm = IdentityPermutation,
          class Conjugation = Identity,
          class V,class E,class L,class A>
PermutedSpan<TensorSpan<V,E,L,A>,Perm,Conjugation> permuted (TensorSpan<V,E,L,A> const& span)
{
  using transposed_span = PermutedSpan<TensorSpan<V,E,L,A>,Perm,Conjugation>;
  using data_handle_type = typename transposed_span::data_handle_type;
  using mapping_type = typename transposed_span::mapping_type;
  using accessor_type = typename transposed_span::accessor_type;
  return transposed_span{data_handle_type(span.data_handle()),
                         mapping_type(span.mapping()),
                         accessor_type(span.accessor())};
}

template <class Perm = IdentityPermutation,
          class Conjugation = Identity,
          class Tensor,
  decltype(std::declval<Tensor>().container(), bool{}) = true>
auto permuted (Tensor&& tensor)
{
  return permuted<Perm,Conjugation>(TensorSpan{tensor.container_data(),tensor.mapping()});
}

} // end namespace Dune::Tensor
