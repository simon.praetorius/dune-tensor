#pragma once

#include <array>
#include <tuple>
#include <type_traits>

#include <dune/common/ftraits.hh>
#include <dune/common/std/default_accessor.hh>
#include <dune/common/std/layout_right.hh>
#include <dune/common/std/mdarray.hh>
#include <dune/common/std/span.hh>
#include <dune/common/std/impl/containerconstructiontraits.hh>
#include <dune/tensor/tags.hh>
#include <dune/tensor/tensorinterface.hh>
#include <dune/tensor/tensorspan.hh>
#include <dune/tensor/containers/traits.hh>
#include <dune/tensor/inserters/denseinserter.hh>
#include <dune/tensor/iterators/denseiterators.hh>
#include <dune/tensor/ranges/traits.hh>
#include <dune/tensor/utility/array.hh>
#include <dune/tensor/utility/bracketproxy.hh>
#include <dune/tensor/utility/indices.hh>
#include <dune/tensor/utility/initializerlist.hh>

namespace Dune::Tensor {

// forward declaration
template <class Op, class... Ts>
class TensorExpression;

/**
 * \brief A tensor of arbitrary rank and individual dimension extents storing values
 * in a given container backend.
 * \ingroup Tensors
 * \nosubgrouping
 *
 * The implementation is inspired by the mdarray c++ standard proposals.
 *
 * \tparam Value    The element type stored in the tensor.
 * \tparam Extents  The type representing the tensor extents. Must be compatible
 *                  to `Dune::Tensor::Extents`.
 * \tparam Layout   A class providing the index mapping from the multi-dimensional
 *                  index space to a single index usable in the `Container`. See
 *                  the `Dune::Concept::Layout` concept.
 * \tparam ContainerPolicy  A storage backend with single index access a compatible
 *                    initialization constructors. See the `Dune::Concept::Container`
 *                    concept.
 **/
template <class Element, class Extents, class LayoutPolicy, class ContainerPolicy>
class DenseTensor
    : public Std::mdarray<Element,Extents,LayoutPolicy,Container_t<Element,Extents,LayoutPolicy,ContainerPolicy>>
    , public TensorInterface<DenseTensor<Element,Extents,LayoutPolicy,ContainerPolicy>,
        TensorTraits<Element,Extents,LayoutPolicy,DenseIterators>>
{
  using self_type = DenseTensor;
  using base_type = Std::mdarray<Element,Extents,LayoutPolicy,Container_t<Element,Extents,LayoutPolicy,ContainerPolicy>>;
  using interface_type = TensorInterface<self_type, TensorTraits<Element,Extents,LayoutPolicy,DenseIterators>>;

  template <class,class,class,class> friend class DenseTensor;

public:
  using element_type = Element;
  using value_type = element_type;
  using field_type = typename Dune::FieldTraits<value_type>::field_type;
  using extents_type = Extents;
  using index_type = typename extents_type::index_type;
  using size_type = typename extents_type::size_type;
  using rank_type = typename extents_type::rank_type;
  using layout_type = LayoutPolicy;
  using mapping_type = typename base_type::mapping_type;
  using container_type = typename base_type::container_type;

  using tensor_span_type = TensorSpan<element_type,extents_type,layout_type>;
  using const_tensor_span_type = TensorSpan<const element_type,extents_type,layout_type>;

  using pointer = typename base_type::pointer;
  using reference = typename base_type::reference;
  using const_pointer = typename base_type::const_pointer;
  using const_reference = typename base_type::const_reference;

  using inserter_type = DenseInserter<DenseTensor>;
  using iterators_type = DenseIterators;

private:
  // helper function to construct the container
  template <class C, class... Args>
  static constexpr auto construct_container (Args&&... args)
    -> decltype(Std::Impl::ContainerConstructionTraits<C>::construct(std::forward<Args>(args)...))
  {
    return Std::Impl::ContainerConstructionTraits<C>::construct(std::forward<Args>(args)...);
  }

public:
  /// \name DenseTensor constructors
  /// @{

  using base_type::base_type;

  // ---------------------------------------
  // constructors with a given initial value

  /// \brief Construct from extents and initial value
  template <class C = container_type,
    decltype(construct_container<C>(std::declval<std::size_t>(),std::declval<const value_type&>()), bool{}) = true>
  constexpr DenseTensor (const extents_type& e, const value_type& v)
    : DenseTensor{mapping_type(e), v}
  {}

  /// \brief Construct from layout mapping and initial value
  template <class C = container_type,
    decltype(construct_container<C>(std::declval<std::size_t>(),std::declval<const value_type&>()), bool{}) = true>
  constexpr DenseTensor (const mapping_type& m, const value_type& v)
    : base_type{m, construct_container<C>(m.required_span_size(), v)}
  {}


  // -------------------------------------
  // constructors from an initializer list

  /// \brief Constructor from a brace-init list of values
  template <class C = container_type,
    std::enable_if_t<std::is_default_constructible_v<C>, int> = 0>
  constexpr DenseTensor (const extents_type& e, NestedInitializerList_t<value_type,extents_type::rank()> init) noexcept
    : base_type{mapping_type{e}}
  {
    auto it = this->container_data();
    InitializerList<value_type,extents_type>::apply(init,this->extents(),
      [&it](value_type value) { *it++ = value; });
  }

  /// \brief Converting copy constructor
  template <class V, class E, class L, class C>
  constexpr DenseTensor (const DenseTensor<V,E,L,C>& tensor) noexcept
    : base_type{tensor}
  {}

  /// \brief Converting move constructor
  template <class V, class E, class L, class C>
  constexpr DenseTensor (DenseTensor<V,E,L,C>&& tensor) noexcept
    : base_type{std::move(tensor)}
  {}

  /// \brief base copy constructor
  constexpr DenseTensor (const base_type& tensor) noexcept
    : base_type{tensor}
  {}

  /// \brief base move constructor
  constexpr DenseTensor (base_type&& tensor) noexcept
    : base_type{std::move(tensor)}
  {}

  /// \brief Construct the tensor from an expression
  template <class Op, class... T>
  constexpr DenseTensor (const TensorExpression<Op,T...>& expr) noexcept
    : base_type{extents_type{expr.extents()}, container_type(tag::from_range, expr.values())}
  {}

  /// @}


  /// \name Assignment operators
  /// @{

  using interface_type::operator=;

  /// @}

  /// \name Multi index access
  /// @{

  using base_type::operator[];
  using interface_type::operator();
  using interface_type::operator[];

  /// @}


  static constexpr bool is_always_dense () noexcept { return true; }


  /// \name Iterators
  /// @{

  /// \brief Iterator over the values or axis of a tensor
  template <class Tag>
  auto begin (Tag tag) const noexcept
  {
    if constexpr(extents_type::rank() > 0)
      return iterators_type::begin(tag, *this);
    else
      return interface_type::values().begin();
  }

  /// \brief End-Iterator over the values or axis of a tensor
  template <class Tag>
  auto end (Tag tag) const noexcept
  {
    if constexpr(extents_type::rank() > 0)
      return iterators_type::end(tag, *this);
    else
      return interface_type::values().end();
  }

  /// @}


  /// \brief An inserter object to fill the tensor
  constexpr inserter_type inserter () noexcept { return inserter_type{this}; }


  /// \name Conversion into TensorSpan
  /// @{

  /// \brief Conversion operator to TensorSpan
  template <class V, class E, class L, class A,
    std::enable_if_t<std::is_assignable_v<TensorSpan<V,E,L,A>, tensor_span_type>, int> = 0>
  constexpr operator TensorSpan<V,E,L,A> ()
  {
    return tensor_span_type(this->container_data(), this->mapping());
  }

  /// \brief Conversion operator to TensorSpan
  template <class V, class E, class L, class A,
    std::enable_if_t<std::is_assignable_v<TensorSpan<V,E,L,A>, const_tensor_span_type>, int> = 0>
  constexpr operator TensorSpan<V,E,L,A> () const
  {
    return const_tensor_span_type(this->container_data(), this->mapping());
  }

  template <class AccessorPolicy = Std::default_accessor<element_type>>
  constexpr TensorSpan<element_type,extents_type,layout_type,AccessorPolicy> toTensorSpan (const AccessorPolicy& a = AccessorPolicy{})
  {
    return TensorSpan<element_type,extents_type,layout_type,AccessorPolicy>(this->container_data(), this->mapping(), a);
  }

  template <class AccessorPolicy = Std::default_accessor<const element_type>>
  constexpr TensorSpan<const element_type,extents_type,layout_type,AccessorPolicy> toTensorSpan (const AccessorPolicy& a = AccessorPolicy{}) const
  {
    return TensorSpan<const element_type,extents_type,layout_type,AccessorPolicy>(this->container_data(), this->mapping(), a);
  }

  /// @}


  friend constexpr bool operator== (const self_type& lhs, const self_type& rhs) noexcept
  {
    return static_cast<const base_type&>(lhs) == static_cast<const base_type&>(rhs);
  }
};

template <class V, class E, class L, class C>
TensorSpan (const DenseTensor<V,E,L,C>&)
  -> TensorSpan<V const,E,L>;

template <class V, class E, class L, class C>
TensorSpan (DenseTensor<V,E,L,C>&)
  -> TensorSpan<V,E,L>;

/**
 * \name Comparison operators
 * \relates DenseTensor
 **/
/// @{

template <class V, class E, class L, class C,
  std::enable_if_t<(E::rank() == 0), int> = 0>
constexpr bool operator== (const DenseTensor<V,E,L,C>& lhs, const V& rhs) noexcept
{
  return lhs.values().front() == rhs;
}

template <class V, class E, class L, class C,
  std::enable_if_t<(E::rank() == 0), int> = 0>
constexpr bool operator== (const V& lhs, const DenseTensor<V,E,L,C>& rhs) noexcept
{
  return lhs == rhs.values().front();
}

/// @}


/// \relates DenseTensor
template <class F, class E, class L, class C>
std::ostream& operator<< (std::ostream& out, const DenseTensor<F,E,L,C>& tensor)
{
  using index_type = typename DenseTensor<F,E,L,C>::index_type;
  if constexpr(E::rank() == 0) {
    out << tensor();
  } else if constexpr(E::rank() == 1) {
    out << "[";
    for (index_type i = 0; i < tensor.extent(0); ++i)
      out << tensor(i) << (i < tensor.extent(0)-1 ? ", " : "]");
  } else if constexpr(E::rank() == 2) {
    out << "[\n";
    for (index_type i = 0; i < tensor.extent(0); ++i) {
      out << "  [";
      for (index_type j = 0; j < tensor.extent(1); ++j)
        out << tensor(i,j) << (j < tensor.extent(1)-1 ? ", " : "]");
      out << (i < tensor.extent(0)-1 ? ",\n" : "\n");
    }
    out << ']';
  } else if constexpr(E::rank() == 3) {
    out << "[\n";
    for (index_type i = 0; i < tensor.extent(0); ++i) {
      out << "  [\n";
      for (index_type j = 0; j < tensor.extent(1); ++j) {
        out << "    [";
        for (index_type k = 0; k < tensor.extent(2); ++k)
          out << tensor(i,j,k) << (k < tensor.extent(2)-1 ? ", " : "]");
        out << (j < tensor.extent(1)-1 ? ",\n" : "\n");
      }
      out << "  ]";
      out << (i < tensor.extent(0)-1 ? ",\n" : "\n");
    }
    out << ']';
  } else {
    out << "DenseTensor<" << E::rank() << ">";
  }
  return out;
}


// deduction guides
template <class Op, class... T,
  class Expr = TensorExpression<Op,T...>,
  class V = typename Expr::value_type,
  class E = typename Expr::extents_type>
DenseTensor (const TensorExpression<Op,T...>&)
  -> DenseTensor<V,E,Std::layout_right,CommonContainer<Expr>>;


} // end namespace Dune::Tensor


template <class V, class E, class L, class C>
struct Dune::FieldTraits< Dune::Tensor::DenseTensor<V,E,L,C> >
{
  using field_type = typename FieldTraits<V>::field_type;
  using real_type = typename FieldTraits<V>::real_type;
};
