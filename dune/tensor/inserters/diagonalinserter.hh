#pragma once

#include <array>
#include <type_traits>

#include <dune/tensor/inserters/denseinserter.hh>

namespace Dune::Tensor {

/// \brief A dense inserter with an additional method to insert into the diagonal
template <class Tensor>
struct DiagonalInserter
    : public DenseInserter<Tensor>
{
  using base_type = DenseInserter<Tensor>;
  using tensor_type = Tensor;
  using extents_type = typename tensor_type::extents_type;
  using size_type = typename extents_type::size_type;

  using base_type::base_type;

  /// \brief Access specified element at diagonal position i
  constexpr auto& diagonal (size_type i)
  {
    return (*this)[Dune::filledArray<extents_type::rank()>(i)];
  }
};

} // end namespace Dune::Tensor
