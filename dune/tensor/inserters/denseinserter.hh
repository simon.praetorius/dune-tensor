#pragma once

#include <array>
#include <type_traits>

namespace Dune::Tensor {

/// \brief A dense inserter directly inserts the entries into a tensor.
template <class Tensor>
struct DenseInserter
{
  using tensor_type = Tensor;
  using extents_type = typename tensor_type::extents_type;
  using rank_type = typename extents_type::rank_type;
  using size_type = typename extents_type::size_type;
  using index_type = typename extents_type::index_type;

  /// \brief Constructor that stores a pointer to the tensor that should be filled
  explicit constexpr DenseInserter (tensor_type* tensor) noexcept
    : tensor_{tensor}
  {}

  /// \brief Access specified element at position i0,i1,...
  template <class... Indices,
    std::enable_if_t<(sizeof...(Indices) == extents_type::rank()), int> = 0,
    std::enable_if_t<(... && std::is_convertible_v<Indices,index_type>), int> = 0>
  constexpr auto& operator() (Indices... indices)
  {
    return (*tensor_)(index_type(std::move(indices))...);
  }

  /// \brief Access specified element at position [i0,i1,...]
  template <class Index,
    std::enable_if_t<std::is_convertible_v<Index,index_type>, int> = 0>
  constexpr auto& operator[] (const std::array<Index,extents_type::rank()>& indices)
  {
    return (*tensor_)[indices];
  }

private:
  tensor_type* tensor_;
};

} // end namespace Dune::Tensor
