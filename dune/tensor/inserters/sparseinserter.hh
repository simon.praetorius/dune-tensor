#pragma once

#include <array>
#include <cassert>
#include <map>
#include <type_traits>
#include <utility>

#if HAVE_LIBS_ETL
#include <etl/flat_map.h>
#endif

#include <dune/tensor/extents/traits.hh>
#include <dune/tensor/ranges/rangeexpression.hh>
#include <dune/tensor/utility/typetraits.hh>

namespace Dune::Tensor {

template <class Tensor, std::size_t capacity = Std::dynamic_extent>
struct SparseInserter
{
  using tensor_type = Tensor;

public:
  using value_type = typename tensor_type::value_type;
  using extents_type = typename tensor_type::extents_type;
  using mapping_type = typename tensor_type::mapping_type;
  using size_type = typename extents_type::size_type;
  using index_type = typename extents_type::index_type;
  using storage_type = typename tensor_type::storage_type;

private:
  template <class C>
  using map_t = std::map<std::array<index_type,extents_type::rank()>, value_type>;
#if HAVE_LIBS_ETL
  template <class C>
  using flat_map_t = etl::flat_map<std::array<index_type,extents_type::rank()>, value_type, C::value>;
#else
  template <class C>
  using flat_map_t = map_t<C>;
#endif

public:
  using map_type = ConditionalDelay_t<(capacity == Std::dynamic_extent), std::integral_constant<std::size_t,capacity>, map_t, flat_map_t>;
  using key_type = typename map_type::key_type;

  struct modifier_type
  {
    modifier_type& operator= (const value_type& value) noexcept
    {
      return assign(value, [](auto& el, const auto& v) { el = v; });
    }

    modifier_type& operator+= (const value_type& value) noexcept
    {
      return assign(value, [](auto& el, const auto& v) { el += v; });
    }

    template <class Assigner>
    modifier_type& assign (const value_type& value, const Assigner& assigner) noexcept
    {
      auto [it,inserted] = map_->emplace(key_, value);
      if (!inserted)
        assigner(it->second, value);
      return *this;
    }

    map_type* map_;
    key_type key_;
  };

  /// \brief Constructor stores a pointer to the mapping and storage of the passed tensor
  explicit SparseInserter (tensor_type* tensor) noexcept
    : mapping_{&tensor->mapping()}
    , storage_{&tensor->storage()}
  {}

  /**
   * \brief Actual insertion happens at destruction.
   *
   * Fill the index-mapping of the tensor by passing all inserted indices.
   * Then fill the values into the tensor storage in the same order as the indices.
   **/
  ~SparseInserter () noexcept
  {
    mapping_->clear();
    storage_->clear();

    auto index_range = RangeExpression{[](auto& el) { return el.first; }, map_};
    mapping_->build(index_range.begin(), index_range.end());

    reserve(storage_, map_.size());
    for (auto const& [index,value] : map_)
      storage_->push_back(value);
  }

  /**
   * \brief Access specified element at position i0,i1,...
   * \return A proxy type that can be used for (compound) assignment of values at
   *         the position given by the index-tuple.
   **/
  template <class... Indices,
    std::enable_if_t<(... && std::is_convertible_v<Indices,index_type>), int> = 0>
  modifier_type operator() (Indices... indices) noexcept
  {
    assert(( key_type{index_type(indices)...} < as_array(mapping_->extents()) ));
    return modifier_type{&map_, key_type{index_type(indices)...}};
  }

  /**
   * \brief Access specified element at position [i0,i1,...]
   * \return A proxy type that can be used for (compound) assignment of values at
   *         the position given by the index-tuple.
   **/
  template <class Index,
    std::enable_if_t<std::is_convertible_v<Index,index_type>, int> = 0>
  modifier_type operator[] (const std::array<Index, extents_type::rank()>& indices) noexcept
  {
    assert(( indices < as_array(mapping_->extents()) ));
    return modifier_type{&map_, key_type{indices}};
  }

private:
  mapping_type* mapping_;
  storage_type* storage_;
  map_type map_{};
};

} // end namespace Dune::Tensor
