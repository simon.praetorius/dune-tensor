#pragma once

#include <type_traits>
#include <dune/common/std/default_accessor.hh>
#include <dune/common/std/extents.hh>
#include <dune/common/std/layout_right.hh>
#include <dune/tensor/tags.hh>
#include <dune/tensor/iterators/denseiterators.hh>

namespace Dune::Tensor {

/**
 * \brief A multi-dimensional span with tensor interface.
 * \ingroup Tensors
 * \nosubgrouping
 *
 * The implementation is inspired by the mdspan c++ standard proposals.
 *
 * \tparam Element  The element type stored in the tensor.
 * \tparam Extents  The type representing the tensor extents. Must be compatible
 *                  to `Dune::Tensor::Extents`.
 * \tparam Layout   A class providing the index mapping from the multi-dimensional
 *                  index space to a single index usable in the `Container`. See
 *                  the `Dune::Concept::Layout` concept.
 * \tparam Accessor A class that defines types and operations to create a reference
 *                  to a single element stored by a data pointer and index.
 **/
template <class Element, class Extents, class LayoutPolicy = Std::layout_right,
          class AccessorPolicy = Std::default_accessor<Element>>
class TensorSpan
    : public Std::mdspan<Element,Extents,LayoutPolicy,AccessorPolicy>
    , public TensorInterface<TensorSpan<Element,Extents,LayoutPolicy,AccessorPolicy>,
        TensorTraits<Element,Extents,LayoutPolicy,DenseIterators>>
{
  using self_type = TensorSpan;
  using base_type = Std::mdspan<Element,Extents,LayoutPolicy,AccessorPolicy>;
  using interface_type = TensorInterface<self_type,TensorTraits<Element,Extents,LayoutPolicy,DenseIterators>>;

public:
  using element_type =	Element;
  using extents_type = Extents;
  using layout_type = LayoutPolicy;
  using accessor_type = AccessorPolicy;

  using value_type = typename base_type::value_type;
  using mapping_type = typename base_type::mapping_type;
  using index_type = typename base_type::index_type;
  using size_type = typename base_type::size_type;
  using rank_type = typename base_type::rank_type;
  using data_handle_type = typename base_type::data_handle_type;
  using reference = typename base_type::reference;

public:
  /// \name TensorSpan constructors
  /// @{

    using base_type::base_type;

  /// \brief Converting constructor
  template <class V, class E, class L, class A,
    class MappingType = typename L::template mapping<E>,
    std::enable_if_t<std::is_constructible_v<mapping_type, const MappingType&>, int> = 0,
    std::enable_if_t<std::is_constructible_v<accessor_type, const A&>, int> = 0>
  constexpr TensorSpan (const TensorSpan<V,E,L,A>& other)
    : base_type{other}
  {}

  /// \brief Converting move constructor
  template <class V, class E, class L, class A,
    class MappingType = typename L::template mapping<E>,
    std::enable_if_t<std::is_constructible_v<mapping_type, const MappingType&>, int> = 0,
    std::enable_if_t<std::is_constructible_v<accessor_type, const A&>, int> = 0>
  constexpr TensorSpan (TensorSpan<V,E,L,A>&& tensor)
    : base_type{std::move(tensor)}
  {}

  /// \brief base copy constructor
  constexpr TensorSpan (const base_type& tensor)
    : base_type{tensor}
  {}

  /// \brief base move constructor
  constexpr TensorSpan (base_type&& tensor)
    : base_type{std::move(tensor)}
  {}

  /// @}


  /// \name Multi index access
  /// @{

  using base_type::operator[];
  using interface_type::operator();
  using interface_type::operator[];

  /// @}
};

// deduction guides
// @{

template <class CArray,
  std::enable_if_t<std::is_array_v<CArray>, int> = 0,
  std::enable_if_t<(std::rank_v<CArray> == 1), int> = 0>
TensorSpan (CArray&)
  -> TensorSpan<std::remove_all_extents_t<CArray>, Std::extents<std::size_t, std::extent_v<CArray,0>>>;

template <class Pointer,
  std::enable_if_t<std::is_pointer_v<std::remove_reference_t<Pointer>>, int> = 0>
TensorSpan (Pointer&&)
  -> TensorSpan<std::remove_pointer_t<std::remove_reference_t<Pointer>>, Std::extents<std::size_t>>;

template <class ElementType, class... II,
  std::enable_if_t<(... && std::is_convertible_v<II,std::size_t>), int> = 0,
  std::enable_if_t<(sizeof...(II) > 0), int> = 0>
TensorSpan (ElementType*, II...)
  -> TensorSpan<ElementType, Std::dextents<std::size_t, sizeof...(II)>>;

template <class ElementType, class SizeType, std::size_t N>
TensorSpan (ElementType*, Std::span<SizeType,N>&)
  -> TensorSpan<ElementType, Std::dextents<std::size_t, N>>;

template <class ElementType, class SizeType, std::size_t N>
TensorSpan (ElementType*, const std::array<SizeType,N>&)
  -> TensorSpan<ElementType, Std::dextents<std::size_t, N>>;

template <class ElementType, class IndexType, std::size_t... exts>
TensorSpan (ElementType*, const Std::extents<IndexType,exts...>&)
  -> TensorSpan<ElementType, Std::extents<IndexType,exts...>>;

template <class ElementType, class Mapping,
  class Extents = typename Mapping::extents_type,
  class Layout = typename Mapping::layout_type>
TensorSpan (ElementType*, const Mapping&)
  -> TensorSpan<ElementType, Extents, Layout>;

template <class Mapping, class Accessor,
  class DataHandle = typename Accessor::data_handle_type,
  class Element = typename Accessor::element_type,
  class Extents = typename Mapping::extents_type,
  class Layout = typename Mapping::layout_type>
TensorSpan (const DataHandle&, const Mapping&, const Accessor&)
  -> TensorSpan<Element, Extents, Layout, Accessor>;

template <class V, class E, class L, class A>
TensorSpan (Std::mdspan<V,E,L,A>)
  -> TensorSpan<V,E,L,A>;

/// @}

} // end namespace Dune::Tensor
