#pragma once

#include <atomic>
#include <type_traits>

#if ! DUNE_HAVE_CXX_ATOMIC_REF
  #error "For the AtomicAccessor we require std::atomic_ref to be implemented in the standard library"
#endif

namespace Dune::Tensor {

template <class Element>
class AtomicAccessor
{
  static_assert(std::is_trivially_copyable_v<Element>);

public:
  using element_type = Element;
  using data_handle_type = element_type*;
  using reference = std::atomic_ref<element_type>;
  using offset_policy = AtomicAccessor;

public:
  /// \brief Default constructor
  constexpr AtomicAccessor () noexcept = default;

  /// \brief Converting constructor from an accessor with different element type
  template <class OtherElement,
    std::enable_if_t<std::is_convertible_v<OtherElement(*)[], Element(*)[]>, int> = 0>
  constexpr AtomicAccessor (AtomicAccessor<OtherElement>) noexcept {}

  /// \brief Return an atomic reference to the i'th element in the data range starting at `p`
  constexpr reference access (data_handle_type p, std::ptrdiff_t i) const noexcept
  {
    return std::atomic_ref{ p[i] };
  }

  /// \brief Return a pointer to the i'th element in the data range starting at `p`
  constexpr data_handle_type offset (data_handle_type p, std::ptrdiff_t i) const noexcept
  {
    return p + i;
  }
};

} // end namespace Dune::Tensor
