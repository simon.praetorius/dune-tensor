#pragma once

#include <memory>
#include <type_traits>

#include <dune/common/std/default_accessor.hh>

// attribute that specifies that the values a pointer points are aligned by `alignment`
#if defined(__ICC) || defined(__clang__)
  #define DUNE_ALIGN_VALUE_ATTRIBUTE( ALIGNMENT ) __attribute__((align_value( ALIGNMENT )));
#else
  #define DUNE_ALIGN_VALUE_ATTRIBUTE( ALIGNMENT )
#endif

namespace Dune::Tensor {

template <class Element, std::size_t alignment>
class AlignedAccessor
{
  static_assert(alignment >= alignof(Element));

public:
  using element_type = Element;
  using data_handle_type = element_type* DUNE_ALIGN_VALUE_ATTRIBUTE( alignment );
  using reference = element_type&;
  using offset_policy = Std::default_accessor<element_type>;

public:
  /// \brief Default constructor
  constexpr AlignedAccessor () noexcept = default;

  /// \brief Converting constructor from an accessor with different element type
  template <class OtherElement, std::size_t otherAlignment,
    std::enable_if_t<std::is_convertible_v<OtherElement(*)[], Element(*)[]>, int> = 0>
  constexpr AlignedAccessor (AlignedAccessor<OtherElement,otherAlignment>) noexcept {}

  /// \brief Return a reference to aligned data
  constexpr reference access (data_handle_type p, std::ptrdiff_t i) const noexcept
  {
#if __cpp_lib_assume_aligned >=	201811L
    return std::assume_aligned<alignment>(p)[i];
#else
    return p[i];
#endif
  }

  /// \brief Return a pointer to the i'th element in the data range starting at `p`
  constexpr typename offset_policy::data_handle_type offset (data_handle_type p, std::ptrdiff_t i) const noexcept
  {
    return p + i;
  }
};

} // end namespace Dune::Tensor
