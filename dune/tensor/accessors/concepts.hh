#pragma once

#include <type_traits>

#if ENABLE_DUNE_CONCEPTS
#include <dune/concepts/std/concepts.hh>

namespace Dune::Concept {

template <class A>
concept Accessor = requires(A const& accessor, typename A::data_handle_type p, std::ptrdiff_t i)
{
  // type requirements
  typename A::data_handle_type;
  typename A::reference;
  typename A::offset_policy;

  // information
  { accessor.access(p,i) } -> Std::same_as<typename A::reference>;
  { accessor.offset(p,i) } -> Std::same_as<typename A::offset_policy::data_handle_type>;
};

} // end namespace Dune::Concept

#else // ENABLE_DUNE_CONCEPTS

// fallback implementation
#include <dune/common/concept.hh>

namespace Dune::Concept {

struct AccessorConcept
{
  template <class A>
  static auto require(A const& accessor, typename A::data_handle_type p = {}, std::ptrdiff_t i = {}) -> decltype
  (
    // type requirements
    requireType<typename A::data_handle_type>(),
    requireType<typename A::reference>(),
    requireType<typename A::offset_policy>(),

    // information
    requireSameType<typename A::reference, decltype(accessor.access(p,i))>(),
    requireSameType<typename A::offset_policy::data_handle_type, decltype(accessor.offset(p,i))>(),
  0);
};

template <class A>
inline constexpr bool Accessor = models<AccessorConcept,A>();

} // end namespace Dune::Concept

#endif // ENABLE_DUNE_CONCEPTS
