#pragma once

#include <type_traits>

namespace Dune::Tensor {

template <class Accessor, class Transform>
class TransformedAccessor
{
public:
  using element_type = typename Accessor::element_type;
  using data_handle_type = typename Accessor::data_handle_type;
  using reference = std::invoke_result_t<Transform, typename Accessor::reference>;
  using offset_policy = typename Accessor::offset_policy;

public:
  /// \brief Default constructor
  template <class A = Accessor, class T = Transform,
    std::enable_if_t<std::is_default_constructible_v<A>, int> = 0,
    std::enable_if_t<std::is_default_constructible_v<T>, int> = 0>
  constexpr TransformedAccessor () noexcept
    : accessor_{}
    , transform_{}
  {}

  /// \brief Constructor that stores the transformation by value
  template <class A = Accessor,
    std::enable_if_t<std::is_default_constructible_v<A>, int> = 0>
  explicit constexpr TransformedAccessor (const Transform& transform) noexcept
    : accessor_{}
    , transform_(transform)
  {}

  /// \brief Constructor that stores the accessor by value
  template <class T = Transform,
    std::enable_if_t<std::is_default_constructible_v<T>, int> = 0>
  explicit constexpr TransformedAccessor (const Accessor& accessor) noexcept
    : accessor_(accessor)
  {}

  /// \brief Constructor that stores the accessor and transformation by value
  constexpr TransformedAccessor (const Accessor& accessor, const Transform& transform) noexcept
    : accessor_(accessor)
    , transform_(transform)
  {}

  /// \brief Return a reference to the i'th element in the data range starting at `p`
  constexpr reference access (data_handle_type p, std::ptrdiff_t i) const noexcept
  {
    return transform_( accessor_.access(p,i) );
  }

  /// \brief Return a pointer to the i'th element in the data range starting at `p`
  constexpr typename offset_policy::data_handle_type offset (data_handle_type p, std::ptrdiff_t i) const noexcept
  {
    return accessor_.offset(p,i);
  }

private:
  Accessor accessor_;
  Transform transform_;
};

} // end namespace Dune::Tensor
