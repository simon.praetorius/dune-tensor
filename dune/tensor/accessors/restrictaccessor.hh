#pragma once

#include <type_traits>

#include <dune/common/std/default_accessor.hh>

namespace Dune::Tensor {

template <class Element>
class RestrictAccessor
{
public:
  using element_type = Element;
  using data_handle_type = element_type* __restrict;
  using reference = element_type&;
  using offset_policy = Std::default_accessor<element_type>;

public:
  /// \brief Default constructor
  constexpr RestrictAccessor () noexcept = default;

  /// \brief Converting constructor from an accessor with different element type
  template <class OtherElement,
    std::enable_if_t<std::is_convertible_v<OtherElement(*)[], Element(*)[]>, int> = 0>
  constexpr RestrictAccessor (RestrictAccessor<OtherElement>) noexcept {}

  /// \brief Return a reference to the i'th element in the data range starting at `p`
  constexpr reference access (data_handle_type p, std::ptrdiff_t i) const noexcept
  {
    return p[i];
  }

  /// \brief Return a pointer to the i'th element in the data range starting at `p`
  constexpr typename offset_policy::data_handle_type offset (data_handle_type p, std::ptrdiff_t i) const noexcept
  {
    return p + i;
  }
};

} // end namespace Dune::Tensor
