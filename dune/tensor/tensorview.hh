#pragma once

#include <array>
#include <tuple>
#include <type_traits>

#include <dune/common/ftraits.hh>
#include <dune/tensor/tensorspan.hh>
#include <dune/tensor/utility/bracketproxy.hh>

namespace Dune::Tensor {

/**
 * \brief A view on the entries of a tensor
 * \ingroup Tensors
 * \nosubgrouping
 **/
template <class Tensor, class View>
class TensorView
{
public:
  using tensor_type = Tensor;
  using view_type = View;
  using extents_type = typename tensor_type::extents_type;
  using layout_type = typename tensor_type::layout_type;
  using mapping_type = typename tensor_type::mapping_type;
  using size_type = typename tensor_type::size_type;
  using rank_type = typename tensor_type::rank_type;
  using index_type = typename tensor_type::rank_type;

  using span_type = TensorSpan<const typename tensor_type::value_type, extents_type, layout_type>;
  using reference = std::invoke_result_t<view_type,typename tensor_type::reference>;
  using pointer = std::add_pointer_t<std::remove_reference_t<reference>>;
  using value_type = std::decay_t<reference>;
  using field_type = typename Dune::FieldTraits<value_type>::field_type;

  /// \name TensorView constructors
  /// @{

  /// \brief Constructor stores the tensor in a span and stores the view functor by value
  constexpr TensorView (const Tensor& tensor, View view) noexcept
    : tensor_{tensor}
    , view_{std::move(view)}
  {}

  /// @}

  /// \name Multi index access
  /// @{

  /// \brief Access specified element at position i0,i1,...
  template <class... Indices,
    std::enable_if_t<(... && std::is_convertible_v<Indices,index_type>), int> = 0>
  constexpr reference operator() (const Indices&... indices) const noexcept
  {
    return view_(tensor_(indices...));
  }

  /// \brief Access specified element at position [i0,i1,...]
  template <class IndexType, class E = extents_type,
    std::enable_if_t<std::is_convertible_v<IndexType,index_type>, int> = 0>
  constexpr reference operator[] (const std::array<IndexType, E::rank()>& indices) const noexcept
  {
    return view_(tensor_[indices]);
  }

  /// \brief Access specified element at position [i0][i1]...
#ifndef DOXYGEN
  template <class E = extents_type,
    std::enable_if_t<(E::rank() >= 2), int> = 0>
#endif
  constexpr auto operator[] (index_type i0) const noexcept
  {
    return BracketProxy<TensorView const,2,E::rank()>{this, i0};
  }

#ifndef DOXYGEN
  /// \brief Access specified element at position [i0]
  template <class E = extents_type,
    std::enable_if_t<(E::rank() == 1), int> = 0>
  constexpr reference operator[] (index_type i0) const noexcept
  {
    return view_(tensor_[i0]);
  }
#endif

  /// @}


  /// \brief Index mapping of a layout policy
  constexpr const mapping_type& mapping () const noexcept { return tensor_.mapping(); }

  /// \brief Value mapping
  constexpr const view_type& view () const noexcept { return view_; }


  /// \name Size information
  /// @{

  /// \brief Number of dimensions of the tensor
  static constexpr rank_type rank () noexcept { return extents_type::rank(); }

  /// \brief Number of elements in the r'th dimension of the tensor
  static constexpr std::size_t static_extent (rank_type r) noexcept
  {
    return extents_type::static_extent(r);
  }

  /// \brief Number of elements in the r'th dimension of the tensor
  constexpr index_type extent (rank_type r) const noexcept { return tensor_.extent(r); }

  /// \brief Total number of elements in the tensor
  constexpr size_type size () const noexcept { return tensor_.size(); }

  /// \brief Number of elements in all dimensions of the tensor, \related Impl::Extents
  constexpr const extents_type& extents () const noexcept { return tensor_.extents(); }

  /// @}

private:
  span_type tensor_;
  view_type view_;
};

} // end namespace Dune::Tensor


template <class T, class V>
struct Dune::FieldTraits< Dune::Tensor::TensorView<T,V> >
{
  using field_type = typename FieldTraits<T>::field_type;
  using real_type = typename FieldTraits<T>::real_type;
};
