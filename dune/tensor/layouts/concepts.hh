#pragma once

#include <tuple>
#include <type_traits>
#include <utility>

#include <dune/tensor/tags.hh>

#if ENABLE_DUNE_CONCEPTS
#include <dune/concepts/utilities.hh>
#include <dune/concepts/std/concepts.hh>

namespace Dune::Concept {
namespace Impl
{
  template <class S, class M, class... I>
  concept Mapping = requires(M map, I... ii)
  {
    { map(ii...) } -> Std::convertible_to<S>;
  };

  template <class E>
  inline constexpr RepeatedType_t<E::rank(),typename E::index_type> expandIndices() { return {}; }

  template <class M, class... I>
  inline constexpr void checkMapping(M const& mapping, std::tuple<I...> const& indices)
    requires Mapping<typename M::index_type,M,I...> {};

} // end namespace Impl

template <class M>
concept Mapping = requires(M const& mapping)
{
  // type requirements
  typename M::extents_type;
  typename M::size_type;
  typename M::index_type;

  // information
  { mapping.required_span_size() }   -> Std::convertible_to<std::size_t>;

  // mapping of indices
  Impl::checkMapping(mapping, Impl::expandIndices<typename M::extents_type>());
};

template <class L, class Extents>
concept Layout = Mapping<typename L::template mapping<Extents>>;

} // end namespace Dune::Concept

#else // ENABLE_DUNE_CONCEPTS

// fallback implementation
#include <dune/common/concept.hh>

namespace Dune::Concept {
namespace Impl {

/**
 * \brief Generate a tuple by repeating the type `T` `r`-times.
 **/
template <std::size_t r, class T>
struct RepeatedType
{
  template <std::size_t... i>
  static auto unpack(std::index_sequence<i...>)
    -> std::tuple<decltype((i,std::declval<T>()))...>;

  using type = decltype(unpack(std::make_index_sequence<r>{}));
};

template <std::size_t r, class T>
using RepeatedType_t = typename RepeatedType<r,T>::type;

} // end namespace Impl

struct MappingConcept
{
  template <class S, class M, class... I>
  auto requireMapImpl(M const& map, std::tuple<I...> const&) -> decltype
  (
    requireConvertible<S>(map(I(0)...))
  );

  template <class M, class E,
    class Indices = Impl::RepeatedType_t<E::rank(),typename E::index_type>>
  auto requireMap(M const& map, E const& extents) -> decltype
  (
    requireMapImpl<typename M::index_type>(map, std::declval<Indices>())
  );

  template <class M>
  auto require(M const& map) -> decltype
  (
    // type requirements
    requireType<typename M::extents_type>(),
    requireType<typename M::size_type>(),
    requireType<typename M::index_type>(),

    // information
    requireConvertible<std::size_t>(map.required_span_size()),

    // mapping of indices
    requireMap(map, map.extents()),
  0);
};

template <class M>
inline constexpr bool Mapping = models<MappingConcept,M>();


template <class Extents>
struct LayoutConcept
{
  template <class L>
  auto require(L const&) -> decltype
  (
    requireConcept<MappingConcept, typename L::template mapping<Extents>>()
  );
};

template <class L, class Extents>
inline constexpr bool Layout = models<LayoutConcept<Extents>,L>();

} // end namespace Dune::Concept

#endif // ENABLE_DUNE_CONCEPTS
