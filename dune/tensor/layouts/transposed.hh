#pragma once

#include <type_traits>
#include <dune/common/std/type_traits.hh>
#include <dune/tensor/extents/transposed.hh>

namespace Dune::Tensor {

/**
 * \brief A Layout that swaps indices on access
 * \ingroup Layout
 **/
template <class OriginalLayout, class OriginalExtents, class Transposition>
struct TransposedLayout
{
  using original_layout_type = OriginalLayout;
  using original_extents_type = OriginalExtents;
  using original_mapping_type = typename original_layout_type::template mapping<original_extents_type>;

  using transposition_type = Transposition;
  using transposed_extents_type = TransposedExtents<original_extents_type, transposition_type>;

  template <class Extents>
  class mapping
  {
    template <class> friend class mapping;

    static_assert(std::is_same_v<Extents, transposed_extents_type>);

  public:
    using extents_type = Extents;
    using size_type = typename extents_type::size_type;
    using index_type = typename extents_type::index_type;

    constexpr mapping (const original_mapping_type& original_mapping) noexcept
      : extents_(transposedExtents<transposition_type>(original_mapping.extents()))
      , original_mapping_(original_mapping)
    {}

    constexpr const extents_type& extents () const noexcept { return extents_; }

    constexpr std::size_t required_span_size () const noexcept
    {
      return original_mapping_.required_span_size();
    }
    template <class OM = original_mapping_type>
    static constexpr auto static_size () noexcept
      -> decltype(OM::static_size())
    {
      return original_mapping_type::static_size();
    }

    template <class... Indices,
      std::enable_if_t<(sizeof...(Indices) == extents_type::rank()), int> = 0>
    constexpr index_type operator() (Indices... ii) const noexcept
    {
      std::array<index_type, sizeof...(Indices)> indices{index_type(ii)...};
      std::swap(indices[transposition_type::first()], indices[transposition_type::second()]);
      return std::apply([&](auto... jj) -> index_type {
        return original_mapping_(jj...);
      }, indices);
    }

    template <class OtherExtents>
    friend constexpr bool operator== (mapping const& a, mapping<OtherExtents> const& b) noexcept
    {
      return a.extents_ == b.extents_;
    }

  private:
    extents_type extents_;
    original_mapping_type original_mapping_;
  };
};

} // end namespace Dune::Tensor
