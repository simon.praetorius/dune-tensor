#pragma once

#include <array>
#include <type_traits>

#include <dune/common/indices.hh>
#include <dune/tensor/containers/traits.hh>

namespace Dune::Tensor {

/**
 * \brief An identity mapping, returning the indices tuple as array of indices
 * \ingroup Layout
 **/
struct Identity
{
  template <class Extents>
  class mapping
  {
    template <class> friend class mapping;

  public:
    using extents_type = Extents;
    using size_type = typename extents_type::size_type;
    using rank_type = typename extents_type::rank_type;
    using index_type = typename extents_type::index_type;
    using result_type
      = std::conditional_t<(extents_type::rank() == 0), index_type, std::array<index_type, extents_type::rank()>>;
    using layout_type = Identity;

    constexpr mapping () noexcept = default;
    constexpr mapping (extents_type const& e) noexcept
      : extents_{e}
    {}

    template <class E>
    constexpr mapping (mapping<E> const& m) noexcept
      : extents_{m.extents_}
    {}

    template <class E>
    constexpr mapping (mapping<E>&& m) noexcept
      : extents_{std::move(m.extents_)}
    {}

    constexpr extents_type const& extents () const noexcept { return extents_; }
    constexpr std::size_t required_span_size () const noexcept { return extents_.product();  }

    template <class... Indices,
      std::enable_if_t<(sizeof...(Indices) == extents_type::rank()), int> = 0>
    constexpr result_type operator() (Indices... ii) const noexcept
    {
      return result_type{index_type(ii)...};
    }

    constexpr size_type operator() () const noexcept
    {
      return 0;
    }

    template <class OtherExtents>
    friend constexpr bool operator== (mapping const& a, mapping<OtherExtents> const& b) noexcept
    {
      return a.extents_ == b.extents_;
    }

  private:
    extents_type extents_;
  };
};


} // end namespace Dune::Tensor
