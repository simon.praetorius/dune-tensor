#pragma once

#include <algorithm>
#include <array>
#include <iterator>
#include <optional>
#include <tuple>
#include <type_traits>

#include <dune/common/exceptions.hh>
#include <dune/tensor/tags.hh>
#include <dune/tensor/containers/capacitystorage.hh>
#include <dune/tensor/utility/typetraits.hh>

namespace Dune::Tensor {

/**
 * \brief Sparse container layout with COO (Coordinate) internal representation
 * \ingroup Layout
 **/
template <std::size_t capacity = Std::dynamic_extent>
struct COOLayout
{
  template <class Extents>
  class mapping
  {
  public:
    using extents_type = Extents;
    using size_type = typename extents_type::size_type;
    using rank_type = typename extents_type::rank_type;
    using index_type = typename extents_type::index_type;
    using result_type = std::optional<size_type>;
    using layout_type = COOLayout;

    using indices_type = std::array<index_type,extents_type::rank()>;
    using index_storage_type = typename Impl::CapacityStorage<indices_type,capacity>::type;

  public:
    mapping () noexcept = default;
    mapping (extents_type const& e) noexcept
      : extents_{e}
    {}

    template <class E>
    mapping (mapping<E> const& m) noexcept
      : extents_{m.extents_}
    {}

    template <class E>
    mapping (mapping<E>&& m) noexcept
      : extents_{std::move(m.extents_)}
    {}

    /// \brief Copy the given index range into a local index-vector
    template <class InputIt,
      class IndicesType = typename std::iterator_traits<InputIt>::value_type,
      std::enable_if_t<std::is_convertible_v<IndicesType,indices_type>, int> = 0>
    void build (InputIt first, InputIt last)
    {
      indices_.insert(indices_.end(), first, last);
    }

    /// \brief Reset all internal storage. NOTE: This does not perform any reallocation.
    void clear () noexcept
    {
      indices_.clear();
    }

    const extents_type& extents () const noexcept { return extents_; }
    std::size_t required_span_size () const noexcept { return indices_.size(); }
    constexpr static std::size_t static_size () noexcept { return Std::dynamic_extent; }

    /**
     * \brief Compute the position in the internal index storage of the given index-tuple
     *
     * \param ii... The index tuple {ii...} to search the flat index for.
     * \return  An `optional<Index>` representing the flat index associated with the index-tuple
     *          or nullopt, if the corresponding entry is not found.
     **/
    template <class... Indices,
      std::enable_if_t<(sizeof...(Indices) == extents_type::rank()), int> = 0>
    result_type operator() (Indices... ii) const noexcept
    {
      if (auto [pos,found] = find(indices_type{ii...}); found)
        return pos;
      return std::nullopt;
    }

  private:
    // Return the position in the indices array and wether the entry is found
    std::pair<index_type,bool> find (const indices_type& index) const noexcept
    {
      auto it = std::lower_bound(indices_.begin(), indices_.end(), index);
      bool found = !(it == indices_.end() || *it != index);
      return found ? std::pair<index_type,bool>{std::distance(indices_.begin(), it), true}
                   : std::pair<index_type,bool>{0, false};
    }

  private:
    extents_type extents_{};
    index_storage_type indices_{};
  };
};

} // end namespace Dune::Tensor
