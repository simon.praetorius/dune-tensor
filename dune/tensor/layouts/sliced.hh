#pragma once

#include <array>
#include <type_traits>

#include <dune/common/hybridutilities.hh>
#include <dune/tensor/slices.hh>
#include <dune/tensor/extents/sliced.hh>

namespace Dune::Tensor {

/**
 * \brief Mapping with sliced dimensions
 * \ingroup Layout
 *
 * \tparam OriginalMapping  The mapping that is sliced
 * \tparam Slices...        Slices in all dimensions.
 **/
template <class OriginalMapping, class... Slices>
struct SlicedLayout
{
  using original_mapping_type = OriginalMapping;

  template <class Extents>
  class mapping
  {
    template <class> friend class mapping;

  public:
    using extents_type = Extents;
    using size_type = typename extents_type::size_type;
    using rank_type = typename extents_type::rank_type;
    using index_type = typename extents_type::index_type;
    using result_type = index_type;
    using layout_type = SlicedLayout;
    using slices_type = std::tuple<Slices...>;

    /// \brief Default construct the sliced mapping
    constexpr mapping () noexcept = default;

    /// \brief Construct the sliced mapping, from a given sub-extents and default-construct
    /// the original mapping and the slices
    constexpr mapping (extents_type const& e) noexcept
      : extents_{e}
    {}

    /// \brief Construct the sliced mapping by building sub-extents
    constexpr mapping (original_mapping_type const& m, Slices... slices) noexcept
      : extents_{slicedExtents(m.extents(), slices...)}
      , original_mapping_{m}
      , slices_{slices...}
    {}

    /// \brief Subset of the extents incorporating the slices.
    constexpr extents_type const& extents () const noexcept { return extents_; }

    /// \brief Number of elements in the sliced representation
    constexpr std::size_t required_span_size () const noexcept { return extents_.product(); }

    /// \brief Transform the indices by inversion of the slicing and then original mapping
    template <class... Indices,
      std::enable_if_t<(sizeof...(Indices) == extents_type::rank()), int> = 0>
    constexpr index_type operator() (Indices... ii) const noexcept
    {
      std::array<index_type, sizeof...(Indices)> indices{ii...};
      std::array<index_type, sizeof...(Slices)> newIndices;
      auto indexIt = std::begin(indices);
      Dune::Hybrid::forEach(std::index_sequence_for<Slices...>{}, [&](auto jj) {
        newIndices[jj] = nextIndex(indexIt, std::get<jj>(slices_));
      });

      return std::apply([&](auto... jj) -> index_type {
        return original_mapping_(jj...); }, newIndices);
    }

    template <class OtherExtents>
    friend constexpr bool operator== (mapping const& a, mapping<OtherExtents> const& b) noexcept
    {
      return a.extents_ == b.extents_ && a.original_mapping_ == b.original_mapping_;
    }

  private:
    #ifdef DOXYGEN
    /**
    * \brief Compute the next index in a dimension restricted by a slice specifier.
    *
    * The next index is either
    * - The given slice index, if slice is a single integer/integral_constant
    * - The relative index within the slice index-set/range
    *
    * If a relative index is computed, the index iterator represents the number of that
    * index within the sub-index-set/range. It is incremented after computing the
    * relative index within the slice.
    *
    * \return The next index in the original dimension range of type
    * `typename IndexIter::value_type`.
    **/
    template <class IndexIter, class Slice>
    static constexpr index_type nextIndex (IndexIter& it, Slice slice);
    #else

    template <class IndexIter>
    static constexpr auto nextIndex (IndexIter& it, tag::all_t /*slice*/)
    {
      return *it++;
    }

    template <class IndexIter, class Begin, class End>
    static constexpr auto nextIndex (IndexIter& it, IndexRange<Begin,End> slice)
    {
      assert(*it < slice.end - slice.begin);
      return slice.begin + (*it++);
    }

    template <class IndexIter, class Begin, class Length, class Stride>
    static constexpr auto nextIndex (IndexIter& it, StridedIndexRange<Begin,Length,Stride> slice)
    {
      assert(*it < slice.extent);
      return slice.begin + (*it++)*slice.stride;
    }

    template <class IndexIter, class Index,
      std::enable_if_t<std::is_integral_v<Index>, int> = 0>
    static constexpr auto nextIndex (IndexIter /*it*/, Index index)
    {
      return index;
    }

    template <class IndexIter, class Index, Index index,
      std::enable_if_t<std::is_integral_v<Index>, int> = 0>
    static constexpr auto nextIndex (IndexIter /*it*/, std::integral_constant<Index,index>)
    {
      return index;
    }

    #endif // DOXYGEN

  private:
    extents_type extents_;
    original_mapping_type original_mapping_;
    slices_type slices_;
  };
};


/// \brief Build a sliced mapping by constructing a sliced layout and sub extents
template <class M, class... Slices>
auto slicedMapping (const M& mapping, Slices... slices)
{
  using SL = SlicedLayout<M,Slices...>;
  using SE = SlicedExtents_t<typename M::extents_type, Slices...>;
  using SM = typename SL::template mapping<SE>;
  return SM{mapping, slices...};
}

} // end namespace Dune::Tensor
