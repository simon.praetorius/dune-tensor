#pragma once

#include <algorithm>
#include <iterator>
#include <optional>
#include <tuple>
#include <type_traits>

#include <dune/tensor/tags.hh>
#include <dune/tensor/containers/capacitystorage.hh>
#include <dune/tensor/utility/typetraits.hh>

namespace Dune::Tensor {

/**
 * \brief Sparse container storage backend with CSF (Compressed sparse fibers) internal representation
 * \ingroup Container
 **/
template <std::size_t capacity = Std::dynamic_extent>
struct CSFLayout
{
  template <class T>
  using CapacityStorage_t = typename Impl::CapacityStorage<T,capacity>::type;

  template <class Extents>
  class mapping
  {
    friend struct CSFIterators;

  public:
    using extents_type = Extents;
    using rank_type = typename extents_type::rank_type;
    using size_type = typename extents_type::size_type;
    using index_type = typename extents_type::index_type;
    using result_type = std::optional<size_type>;
    using indices_type = std::array<index_type,extents_type::rank()>;
    using offset_storage_type
      = std::array<CapacityStorage_t<size_type>,extents_type::rank()-1>;
    using index_storage_type
      = std::array<CapacityStorage_t<index_type>,extents_type::rank()>;

  public:
    mapping () noexcept = default;
    mapping (extents_type const& e) noexcept
      : extents_{e}
      , offsets_{}
      , indices_{}
    {}

    template <class E>
    mapping (mapping<E> const& m) noexcept
      : mapping{m.extents_}
    {}

    template <class E>
    mapping (mapping<E>&& m) noexcept
      : mapping{std::move(m.extents_)}
    {}

    /**
     * \brief Build the internal representation in terms of an offset and index array.
     *
     * \param [first,last)  Range of index-tuples to be inserted
     *
     * The given iterators must be forward iterators.
     */
    template <class InputIt,
      class IndicesType = typename std::iterator_traits<InputIt>::value_type,
      std::enable_if_t<std::is_convertible_v<IndicesType,indices_type>, int> = 0>
    void build (InputIt first, InputIt last)
    {
      if (first == last)
        return;

      // insert the first entry
      const auto& index = *first;
      size_ = 1;
      for (rank_type r = 0; r < extents_type::rank(); ++r) {
        if (r < extents_type::rank()-1) {
          offsets_[r].push_back(0);
          offsets_[r].push_back(1);
        }
        indices_[r].push_back(index[r]);
      }

      // insert the remaining entries
      for (auto it = ++first; it != last; ++it,++size_) {
        const auto& index = *it;

        for (rank_type r = 0; r < extents_type::rank(); ++r)
        {
          if (index[r] != indices_[r].back()) {
            if (r > 0)
              offsets_[r-1].back()++;
            for (rank_type s = r; s < extents_type::rank(); ++s) {
              indices_[s].push_back(index[s]);
              if (s > r)
                offsets_[s-1].push_back(offsets_[s-1].back()+1);
            }
            break;
          }
        }
      }

    }

    /// \brief Reset the internal storage. NOTE: This does not perform any reallocation.
    void clear () noexcept
    {
      for (auto& offset : offsets_)
        offset.clear();
      for (auto& index : indices_)
        index.clear();
      size_ = 0;
    }

    const extents_type& extents () const noexcept { return extents_; }
    std::size_t required_span_size () const noexcept { return size_; }
    constexpr static std::size_t static_size () noexcept { return Std::dynamic_extent; }

    /// \brief Return an index to the value associated to the given multi-index `ii...` or
    /// nullopt if not found
    template <class... Indices,
      std::enable_if_t<(sizeof...(Indices) == extents_type::rank()), int> = 0>
    result_type operator() (Indices... ii) const noexcept
    {
      if (auto [pos,found] = find(indices_type{ii...}); found)
        return pos;
      return std::nullopt;
    }

  private:
    // Return the position in the values array and wether the entry is found
    std::pair<index_type,bool> find (const indices_type& index) const noexcept
    {
      auto it = std::lower_bound(indices_[0].begin(), indices_[0].end(), index[0]);
      bool found = (it != indices_[0].end()) && (*it == index[0]);
      index_type pos = std::distance(indices_[0].begin(), it),first = 0,last = 0;
      for (rank_type r = 1; found && r < extents_type::rank(); ++r) {
        first = offsets_[r-1][pos];
        last = offsets_[r-1][pos+1];
        auto it = std::lower_bound(indices_[r].begin()+first, indices_[r].begin()+last, index[r]);
        found = found && ((it != indices_[r].begin()+last) && (*it == index[r]));
        pos = std::distance(indices_[r].begin()+first, it);
      }
      return {first+pos,found};
    }

  private:
    extents_type extents_{};
    offset_storage_type offsets_{};
    index_storage_type indices_{};
    size_type size_{0};
  };
};

} // end namespace Dune::Tensor
