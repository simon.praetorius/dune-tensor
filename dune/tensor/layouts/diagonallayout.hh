#pragma once

#include <algorithm>
#include <array>
#include <iterator>
#include <optional>
#include <tuple>
#include <type_traits>

#include <dune/tensor/tags.hh>


namespace Dune::Tensor {

/**
 * \brief Container to store only diagonal elements of a tensor.
 * \ingroup Container
 **/
struct DiagonalLayout
{
  template <class Extents>
  class mapping
  {
    template <class E, class Seq = std::make_integer_sequence<std::size_t,E::rank()>>
    struct equal_extents;

    template <class E, std::size_t... i>
    struct equal_extents<E,std::integer_sequence<std::size_t,i...>>
    {
      static constexpr bool value = (true && ... && (E::static_extent(i) == E::static_extent(0)));
      static constexpr bool eval (const E& extents)
      {
        return (true && ... && (extents.extent(i) == extents.extent(0)));
      }
    };

    // require that all extents are equal
    static_assert(equal_extents<Extents>::value);

  public:
    using extents_type = Extents;
    using rank_type = typename extents_type::rank_type;
    using size_type = typename extents_type::size_type;
    using index_type = typename extents_type::index_type;
    using result_type = std::optional<size_type>;
    using indices_type = std::array<index_type,extents_type::rank()>;

  public:
    constexpr explicit mapping (const extents_type& extents) noexcept
      : extents_(extents)
    {
      assert(equal_extents<extents_type>::eval(extents));
    }

    template <class E>
    constexpr explicit mapping (mapping<E> const& m) noexcept
      : mapping{m.extents_}
    {}

    template <class E>
    constexpr explicit mapping (mapping<E>&& m) noexcept
      : mapping{std::move(m.extents_)}
    {}

    constexpr extents_type const& extents () const noexcept { return extents_; }
    constexpr std::size_t required_span_size () const noexcept { return extents_.extent(0); }
    static constexpr std::size_t static_size () noexcept { return extents_type::static_extent(0); }

    /// \brief Return an index to the value associated to the given multi-index `ii...` or
    /// nullopt if not found
    template <class... Indices,
      std::enable_if_t<(sizeof...(Indices) == extents_type::rank()), int> = 0>
    result_type operator() (Indices... ii) const noexcept
    {
      if (auto [pos,found] = find(indices_type{index_type(ii)...}); found)
        return pos;
      return std::nullopt;
    }

  private:
    // Return the position in the values array and wether the entry is found
    constexpr std::pair<index_type,bool> find (const indices_type& index) const noexcept
    {
      if constexpr(extents_type::rank() == 0)
        return {0,true};
      else {
        bool found = true;
        for (std::size_t i = 1; found && i < index.size(); ++i)
          found = index[i] == index[0];
        return {index[0],found};
      }
    }

  private:
    [[no_unique_address]] extents_type extents_{};
  };
};

} // end namespace Dune::Tensor
