#pragma once

#include <type_traits>
#include <dune/common/std/type_traits.hh>
#include <dune/common/std/layout_left.hh>
#include <dune/common/std/layout_right.hh>

namespace Dune::Tensor {

template <class Tensor>
struct IsDenseLayout : std::false_type {};

template <>
struct IsDenseLayout<Std::layout_left> : std::true_type {};

template <>
struct IsDenseLayout<Std::layout_right> : std::true_type {};

/**
 * \brief Extract the Layout from the tensors or use `Std::layout_right`
 * \ingroup Layout
 **/
template <class Tensor1, class Tensor2>
struct LayoutTraits
{
  template <class Tensor>
  using LayoutType = typename Tensor::layout_type;

  using L1 = Dune::Std::detected_or_t<Std::layout_right, LayoutType, Tensor1>;
  using L2 = Dune::Std::detected_or_t<Std::layout_right, LayoutType, Tensor2>;

  using Layout1 = std::conditional_t<IsDenseLayout<L1>::value, L1, Std::layout_right>;
  using Layout2 = std::conditional_t<IsDenseLayout<L2>::value, L2, Std::layout_right>;

  static_assert(std::is_same_v<Layout1, Layout2>);
  using type = Layout1;
};


} // end namespace Dune::Tensor
