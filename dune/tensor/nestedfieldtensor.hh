#pragma once

#include <type_traits>

#include <dune/common/std/extents.hh>
#include <dune/common/std/layout_right.hh>

#include <dune/tensor/densetensor.hh>
#include <dune/tensor/extents/extents.hh>
#include <dune/tensor/containers/nestedcontainer.hh>
#include <dune/tensor/layouts/identity.hh>

namespace Dune::Tensor {

/**
 * \brief A tensor stored as nested `std::array<std::array<...,e1>, e0>` with static extents.
 * \ingroup Tensors
 * \nosubgrouping
 *
 * The tensor provides access to the sub-tensors with fixed first indices, by using nested
 * `operator[](size_type)`.
 **/
template <class Value, int... exts>
class NestedFieldTensor
    : public DenseTensor<Value, Extents<int, std::size_t(exts)...>, Identity, NestedContainer<exts...>>
{
  using base_type = DenseTensor<Value, Extents<int, std::size_t(exts)...>, Identity, NestedContainer<exts...>>;

public:
  using value_type = typename base_type::value_type;

  /// \name Additional FieldTensor constructors
  /// @{

  constexpr NestedFieldTensor (const value_type& value = 0) noexcept
    : base_type{value}
  {}

  constexpr NestedFieldTensor (const base_type& base) noexcept
    : base_type{base}
  {}

  constexpr NestedFieldTensor (base_type&& base) noexcept
    : base_type{std::move(base)}
  {}

  /// @}

  using base_type::base_type;
  using base_type::operator=;

  /// \brief Access sub-tensor with first index i0
  constexpr auto& operator[] (typename base_type::index_type i0)
  {
    return base_type::storage()[i0];
  }

  /// \brief Access sub-tensor with first index i0
  constexpr const auto& operator[] (typename base_type::index_type i0) const
  {
    return base_type::storage()[i0];
  }

  using base_type::operator[];
};

// Deduction guides
// @{
template <class IndexType, std::size_t... exts, class Value>
NestedFieldTensor(Std::extents<IndexType,exts...>, Value)
  -> NestedFieldTensor<Value, int(exts)...>;

template <class IndexType, std::size_t... exts, class Value>
NestedFieldTensor(typename Std::layout_right::template mapping<Std::extents<IndexType,exts...>>, Value)
  -> NestedFieldTensor<Value, int(exts)...>;
// @}

} // end namespace Dune::Tensor


template <class V, int... exts>
struct Dune::FieldTraits< Dune::Tensor::NestedFieldTensor<V,exts...> >
{
  using field_type = typename FieldTraits<V>::field_type;
  using real_type = typename FieldTraits<V>::real_type;
};
