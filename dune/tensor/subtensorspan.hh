#pragma once

#include <dune/common/std/mdspan.hh>

#if !DUNE_HAVE_CXX_STD_MDSPAN
#error "Need <mdspan> for submdspan()"
#else

#include <dune/tensor/tensorspan.hh>

namespace Dune::Tensor {

/**
 * \brief Create a sub-tensor span by slicing a given tensor-span.
 *
 * A slice can be either
 * - A single index (integer or integral_constant)
 * - An index-range: \ref IndexRange
 * - A strided index-range: \ref std::strided_slice
 * - no slice or the full range: \ref std::full_extent
 *
 * With these slices a subset of dimensions and index-ranges can be
 * specified.
 *
 * \param span       The source tensor that should be sliced.
 * \param slices...  For each dimension a slice specifier.
 *
 * \par Requirements
 * - `sizeof...(Slices) == E::rank()`
 */
template <class T, class E, class L, class A,
          class... Slices>
auto subTensorSpan (const TensorSpan<T,E,L,A>& span, Slices... slices)
{
  return TensorSpan(std::submdspan(span,slices...));
}


/// \brief The span representing a single row of the tensor
template <class T, class E, class L, class A,
  std::enable_if_t<(E::rank() == 2), int> = 0>
auto row (const TensorSpan<T,E,L,A>& span, std::size_t r)
{
  return subTensorSpan(span, r, std::full_extent);
}


/// \brief The span representing a single column of the tensor
template <class T, class E, class L, class A,
  std::enable_if_t<(E::rank() == 2), int> = 0>
auto column (const TensorSpan<T,E,L,A>& span, std::size_t c)
{
  return subTensorSpan(span, std::full_extent, c);
}

} // end namespace Dune::Tensor

#endif // !DUNE_HAVE_CXX_STD_MDSPAN
