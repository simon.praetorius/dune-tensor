#pragma once

#include <array>
#include <type_traits>

#include <dune/common/filledarray.hh>
#include <dune/common/std/extents.hh>
#include <dune/common/std/layout_right.hh>
#include <dune/common/std/mdarray.hh>

#include <dune/tensor/derivativetraits.hh>
#include <dune/tensor/tensorinterface.hh>
#include <dune/tensor/tensorspan.hh>
#include <dune/tensor/inserters/denseinserter.hh>
#include <dune/tensor/iterators/denseiterators.hh>
#include <dune/tensor/utility/initializerlist.hh>

namespace Dune::Tensor {

// forward declaration
template <class Value, int... exts>
class FieldTensor;

namespace Impl {

template <class Value, int... exts>
struct FieldTensorTraits
{
  using element_type = Value;
  using extents_type = Std::extents<int, std::size_t(exts)...>;
  using layout_type = Std::layout_right;
  using iterators_type = DenseIterators;
  static constexpr std::size_t static_size () noexcept { return (exts * ... * 1); }
};

template <class Value, int... exts>
struct FieldTensorBase
{
  using Traits = FieldTensorTraits<Value,exts...>;
  using Container = std::array<Value,Traits::static_size()>;
  using type = Std::mdarray<Value,typename Traits::extents_type,typename Traits::layout_type,Container>;
};

template <class Value, int... exts>
struct FieldTensorInterface
{
  using Self = FieldTensor<Value,exts...>;
  using Traits = FieldTensorTraits<Value,exts...>;
  using type = TensorInterface<Self,Traits>;
};

} // end namespace Impl


/**
 * \brief A tensor of static extents
 * \ingroup Tensors
 * \nosubgrouping
 *
 * This class is a specialization of `DenseTensor` for purely static extents
 * with fixed right-first layout and a corresponding container of static capacity.
 *
 * \tparam Value    The element type stored in the tensor.
 * \tparam exts...  The individual extents of the dimensions of the tensor.
 **/
template <class Value, int... exts>
class FieldTensor
    : public Impl::FieldTensorBase<Value,exts...>::type
    , public Impl::FieldTensorInterface<Value,exts...>::type
{
  using base_type = typename Impl::FieldTensorBase<Value,exts...>::type;
  using interface_type = typename Impl::FieldTensorInterface<Value,exts...>::type;

  static constexpr std::size_t rank_ = base_type::rank();

public:
  using value_type = Value;
  using extents_type = typename base_type::extents_type;
  using rank_type = typename base_type::rank_type;
  using index_type = typename base_type::index_type;
  using size_type = typename base_type::size_type;
  using mapping_type = typename base_type::mapping_type;
  using layout_type = typename base_type::layout_type;
  using reference = typename base_type::reference;
  using const_reference = typename base_type::const_reference;
  using container_type = typename base_type::container_type;
  using inserter_type = DenseInserter<FieldTensor>;

  /// \name FieldTensor constructors
  /// @{

  /// \brief Default constructor that sets initializes the tensor to zero
  constexpr FieldTensor () noexcept
    : FieldTensor{extents_type{}}
  {}

  /// \brief Constructor from extents; sets all entries to zero
  constexpr FieldTensor (const extents_type& e) noexcept
    : base_type{mapping_type{e}, container_type{}}
  {}

  /// \brief Constructor that sets all entries to the given `value`
  template <class V,
    std::enable_if_t<std::is_convertible_v<V,value_type>, int> = 0>
  explicit constexpr FieldTensor (const V& value) noexcept
    : base_type{extents_type{}, value_type(value)}
  {}

  /// \brief Constructor from extents and initial value
  template <class V,
    std::enable_if_t<std::is_convertible_v<V,value_type>, int> = 0>
  explicit constexpr FieldTensor (const extents_type& e, const V& value) noexcept
    : base_type{e, value_type(value)}
  {}

  /// \brief Constructor from a brace-init list of values
  constexpr FieldTensor (NestedInitializerList_t<value_type,rank_> init)
    : FieldTensor{extents_type{}, init}
  {}

  /// \brief Constructor from extents and brace-init list of values
  constexpr FieldTensor (const extents_type& e, NestedInitializerList_t<value_type,rank_> init)
    : base_type{mapping_type{e}}
  {
    auto it = this->container_data();
    InitializerList<value_type,extents_type>::apply(init,this->extents(),
      [&it](value_type value) { *it++ = value; });
  }

  /// \brief Converting constructor from mdarray
  constexpr FieldTensor (const base_type& base) noexcept
    : base_type{base}
  {}

  /// \brief Converting constructor from mdarray
  constexpr FieldTensor (base_type&& base) noexcept
    : base_type{std::move(base)}
  {}

  /// @}


  /// \name Assignment operators
  /// @{

  /// \brief Copy assignment operator
  constexpr FieldTensor& operator= (const FieldTensor&) = default;

  /// \brief Copy assignment from a different mdarray
  template <class V, class E, class L, class C,
    std::enable_if_t<std::is_constructible_v<base_type, Std::mdarray<V,E,L,C>>, int> = 0>
  constexpr FieldTensor& operator= (const Std::mdarray<V,E,L,C>& other)
  {
    return *this = base_type(other);
  }

  /// \brief Assignment of a scalar value
  using interface_type::operator=;

  /// @}

  /// TODO: remove
  static constexpr bool is_always_dense () noexcept { return true; }

  /// \brief An inserter object to fill the tensor
  constexpr inserter_type inserter () noexcept { return inserter_type{this}; }

  /// \name Multi index access
  /// @{

  /**
   * \brief Nested subscript operator to access either a subdimension or an element.
   *
   * The bracket operator can be nested to successively access more dimensions.
   * Each subscript operator returns an mdspan over the sub-tensor with fixed
   * first index.
   *
   * \b Examples:
   * \code{c++}
     FieldTensor<double,3,3> matrix;
     auto row = matrix[0]; // 0th row
     row[1] = 56;          // the row is an mdspan
     matrix[0][1] = 42;    // element at (0,1)
     matrix[0,1] = 7.0;    // only with c++23
     \endcode
   **/
  using base_type::operator[];
  using interface_type::operator[];

  /**
   * \brief Access an element of the tensor using a variadic list of indices.
   * \b Examples:
   * \code{c++}
     FieldTensor<double,3,3,3> tensor;
     tensor(0,1,2) = 42.0;
     \endcode
   **/
  using interface_type::operator();

  /// @}

  friend constexpr bool operator== (const FieldTensor& lhs, const FieldTensor& rhs) noexcept
  {
    return static_cast<const base_type&>(lhs) == static_cast<const base_type&>(rhs);
  }
};

template <class V>
constexpr bool operator== (const FieldTensor<V>& lhs, const V& rhs) noexcept
{
  return lhs.values().front() == rhs;
}

template <class V>
constexpr bool operator== (const V& lhs, const FieldTensor<V>& rhs) noexcept
{
  return lhs == rhs.values().front();
}

/// \brief A FieldVector is a FieldTensor of rank 1
template <class Value, int n>
using FieldVector = FieldTensor<Value,n>;

/// \brief A FieldMatrix is a FieldTensor of rank 2
template <class Value, int m, int n>
using FieldMatrix = FieldTensor<Value,m,n>;


/// Specialization of DerivativeTraits for Tensor valued maps
/// @{

template <class Value, int... exts, class K, int dim>
struct DerivativeTraits<FieldTensor<Value,exts...>(Dune::FieldVector<K,dim>)>
{
  using Range = FieldTensor<Value,exts...,dim>;
};

template <class Value, int... exts, class K, int dim>
struct DerivativeTraits<FieldTensor<Value,exts...>(FieldTensor<K,dim>)>
{
  using Range = FieldTensor<Value,exts...,dim>;
};

/// @}

} // end namespace Dune::Tensor


template <class V, int... exts>
struct Dune::FieldTraits< Dune::Tensor::FieldTensor<V,exts...> >
{
  using field_type = typename FieldTraits<V>::field_type;
  using real_type = typename FieldTraits<V>::real_type;
};
