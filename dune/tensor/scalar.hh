#pragma once

#include <type_traits>

namespace Dune::Tensor {

/**
 * \brief A CRTP base class for containers behaving like a scalar
 * \nosubgrouping
 **/
template <class Derived, class Value>
class ScalarValueBase
{
public:
  using derived_type = Derived;
  using value_type = Value;

public: // assignment operators

  /// \brief Value assignment operator
  derived_type& operator= (value_type const& value) noexcept
  {
    get() = value;
    return asDerived();
  }

  /// \brief Value add-assignment operator
  derived_type& operator+= (value_type const& value) noexcept
  {
    get() += value;
    return asDerived();
  }

  /// \brief Value subtract-assignment operator
  derived_type& operator-= (value_type const& value) noexcept
  {
    get() -= value;
    return asDerived();
  }
  /// \brief Value multiply-assignment operator
  derived_type& operator*= (value_type const& value) noexcept
  {
    get() *= value;
    return asDerived();
  }
  /// \brief Value divide-assignment operator
  derived_type& operator/= (value_type const& value) noexcept
  {
    get() /= value;
    return asDerived();
  }

public: // conversion operators

  /// \brief conversion operators
  template <class ScalarType,
    std::enable_if_t<std::is_convertible_v<value_type,ScalarType>, int> = 0 >
  constexpr operator ScalarType () const noexcept
  {
    return ScalarType(get());
  }

private:

  value_type& get ()
  {
    return asDerived().storage().front();
  }

  value_type const& get () const
  {
    return asDerived().storage().front();
  }

  derived_type& asDerived ()
  {
    return static_cast<derived_type&>(*this);
  }

  derived_type const& asDerived () const
  {
    return static_cast<derived_type const&>(*this);
  }
};

} // end namespace Dune::Tensor
