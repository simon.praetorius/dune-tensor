#pragma once

#include <functional>
#include <iterator>
#include <tuple>
#include <type_traits>

#include <dune/common/indices.hh>
#include <dune/tensor/ranges/concepts.hh>
#include <dune/tensor/ranges/traits.hh>
#include <dune/tensor/utility/apply.hh>
#include <dune/tensor/utility/pointer.hh>

namespace Dune::Tensor {

/**
 * \brief An expression-template involving multiple iterators
 * \ingroup Ranges
 **/
template <class Operation, class... Iters>
class IteratorExpression
{
  using iter_tuple_type = std::tuple<Iters...>;

public:
  using op_type = Operation;
  using iterator_category = CommonIteratorCategory_t<Iters...>;
  using difference_type = std::common_type_t<
    typename std::iterator_traits<Iters>::difference_type...>;
  using reference = std::invoke_result_t<op_type,
    typename std::iterator_traits<Iters>::reference...>;
  using value_type = std::decay_t<reference>;
  using pointer = Pointer<reference>;

private:
  using op_storage_type = std::function<reference(typename std::iterator_traits<Iters>::reference...)>;

public:
  /// Constructors
  /// @{
  IteratorExpression () = default;

  IteratorExpression (op_type op, Iters... iters) noexcept
    : op_{std::move(op)}
    , iters_{std::move(iters)...}
  {}
  /// @}

  /// Access to the data
  /// @{
  reference operator* () const noexcept
  {
    return std::apply([&](auto&&... iters) -> reference {
      return op_((*iters)...); }, iters_);
  }

  pointer operator-> () const noexcept
  {
    return pointer{*(*this)};
  }

  template <class C = iterator_category,
    std::enable_if_t<Concept::RandomAccessIteratorCategory<C>, int> = 0>
  reference operator[] (difference_type n) const noexcept
  {
    return *((*this) + n);
  }
  /// @}

  /// Increment and decrement operators
  /// @{
  IteratorExpression& operator++ () noexcept
  {
    std::apply([](auto&&... iters) { ((++iters),...); }, iters_);
    return *this;
  }

  IteratorExpression operator++ (int) noexcept
  {
    IteratorExpression tmp{*this};
    ++(*this);
    return tmp;
  }

  template <class C = iterator_category,
    std::enable_if_t<Concept::BidirectionalIteratorCategory<C>, int> = 0>
  IteratorExpression& operator-- () noexcept
  {
    std::apply([](auto&&... iters) { ((--iters),...); }, iters_);
    return *this;
  }

  template <class C = iterator_category,
    std::enable_if_t<Concept::BidirectionalIteratorCategory<C>, int> = 0>
  IteratorExpression operator-- (int) noexcept
  {
    IteratorExpression tmp{*this};
    --(*this);
    return tmp;
  }

  template <class C = iterator_category,
    std::enable_if_t<Concept::RandomAccessIteratorCategory<C>, int> = 0>
  IteratorExpression& operator+= (difference_type n) noexcept
  {
    std::apply([n](auto&&... iters) { ((iters+=n),...); }, iters_);
    return *this;
  }

  template <class C = iterator_category,
    std::enable_if_t<Concept::RandomAccessIteratorCategory<C>, int> = 0>
  IteratorExpression& operator-= (difference_type n) noexcept
  {
    std::apply([n](auto&&... iters) { ((iters-=n),...); }, iters_);
    return *this;
  }

  const iter_tuple_type& iterators () const
  {
    return iters_;
  }

  template <std::size_t I>
  const auto& iterator (std::integral_constant<std::size_t,I>) const
  {
    return std::get<I>(iters_);
  }

protected:
  op_storage_type op_;
  iter_tuple_type iters_ = {};
};


/// Increment and decrement operators
/// @{
template <class O, class... I, class C = CommonIteratorCategory_t<I...>,
  std::enable_if_t<Concept::RandomAccessIteratorCategory<C>, int> = 0>
IteratorExpression<O,I...>
operator+ (IteratorExpression<O,I...> lhs,
           typename IteratorExpression<O,I...>::difference_type n) noexcept
{
  return lhs += n;
}

template <class O, class... I, class C = CommonIteratorCategory_t<I...>,
  std::enable_if_t<Concept::RandomAccessIteratorCategory<C>, int> = 0>
IteratorExpression<O,I...>
operator- (IteratorExpression<O,I...> lhs,
           typename IteratorExpression<O,I...>::difference_type n) noexcept
{
  return lhs -= n;
}

template <class O, class... I, class C = CommonIteratorCategory_t<I...>,
  std::enable_if_t<Concept::RandomAccessIteratorCategory<C>, int> = 0>
IteratorExpression<O,I...>
operator+ (typename IteratorExpression<O,I...>::difference_type n,
           IteratorExpression<O,I...> rhs) noexcept
{
  return rhs += n;
}

template <class O, class... I, class C = CommonIteratorCategory_t<I...>,
  std::enable_if_t<Concept::RandomAccessIteratorCategory<C>, int> = 0>
typename IteratorExpression<O,I...>::difference_type
operator- (const IteratorExpression<O,I...>& lhs, const IteratorExpression<O,I...>& rhs) noexcept
{
  if constexpr(sizeof...(I) > 0) {
    assert(( applyIndex<sizeof...(I)>([&](auto... ii) {
      return std::min({(lhs.iterator(ii) - rhs.iterator(ii))...}) ==
             std::max({(lhs.iterator(ii) - rhs.iterator(ii))...});
      })
    ));
    return lhs.iterator(Indices::_0) - rhs.iterator(Indices::_0);
  }
  else {
    return 0;
  }
}
/// @}

/// Comparison operators
/// @{
template <class O, class... I, class C = CommonIteratorCategory_t<I...>,
  std::enable_if_t<Concept::ForwardIteratorCategory<C>, int> = 0>
bool operator== (const IteratorExpression<O,I...>& lhs, const IteratorExpression<O,I...>& rhs) noexcept
{
  return lhs.iterators() == rhs.iterators();
}

template <class O, class... I, class C = CommonIteratorCategory_t<I...>,
  std::enable_if_t<Concept::ForwardIteratorCategory<C>, int> = 0>
bool operator!= (const IteratorExpression<O,I...>& lhs, const IteratorExpression<O,I...>& rhs) noexcept
{
  return !(lhs == rhs);
}

template <class O, class... I, class C = CommonIteratorCategory_t<I...>,
  std::enable_if_t<Concept::RandomAccessIteratorCategory<C>, int> = 0>
bool operator< (const IteratorExpression<O,I...>& lhs, const IteratorExpression<O,I...>& rhs) noexcept
{
  return lhs.iterators() < rhs.iterators();
}

template <class O, class... I, class C = CommonIteratorCategory_t<I...>,
  std::enable_if_t<Concept::RandomAccessIteratorCategory<C>, int> = 0>
bool operator<= (const IteratorExpression<O,I...>& lhs, const IteratorExpression<O,I...>& rhs) noexcept
{
  return !(lhs > rhs);
}

template <class O, class... I, class C = CommonIteratorCategory_t<I...>,
  std::enable_if_t<Concept::RandomAccessIteratorCategory<C>, int> = 0>
bool operator> (const IteratorExpression<O,I...>& lhs, const IteratorExpression<O,I...>& rhs) noexcept
{
  return rhs < lhs;
}

template <class O, class... I, class C = CommonIteratorCategory_t<I...>,
  std::enable_if_t<Concept::RandomAccessIteratorCategory<C>, int> = 0>
bool operator>= (const IteratorExpression<O,I...>& lhs, const IteratorExpression<O,I...>& rhs) noexcept
{
  return !(lhs < rhs);
}
/// @}


} // end namespace Dune::Tensor

