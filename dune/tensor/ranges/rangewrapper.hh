#pragma once

#include <dune/tensor/tags.hh>

namespace Dune::Tensor {

/**
 * \brief Range with tagged begin and end iterators
 * \ingroup Ranges
 **/
template <class Tag, class Storage>
class RangeWrapper
{
public:
  constexpr auto begin () noexcept
  {
    return storage_->begin(Tag{});
  }

  constexpr auto begin () const noexcept
  {
    return storage_->begin(Tag{});
  }

  constexpr auto end () noexcept
  {
    return storage_->end(Tag{});
  }

  constexpr auto end () const noexcept
  {
    return storage_->end(Tag{});
  }

  Storage* storage_;
};


template <class Storage>
auto major_of (Storage& storage)
{
  return RangeWrapper<tag::major_t,Storage>{&storage};
}

template <class Storage>
auto left_of (Storage& storage)
{
  return RangeWrapper<tag::left_t,Storage>{&storage};
}

template <class Storage>
auto right_of (Storage& storage)
{
  return RangeWrapper<tag::right_t,Storage>{&storage};
}

template <class Storage>
auto nz_of (Storage& storage)
{
  return RangeWrapper<tag::nz_t,Storage>{&storage};
}

template <class Storage>
auto all_of (Storage& storage)
{
  return RangeWrapper<tag::all_t,Storage>{&storage};
}

} // end namespace Dune::Tensor
