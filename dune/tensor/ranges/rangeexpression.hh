#pragma once

#include <array>
#include <cassert>
#include <tuple>
#include <type_traits>

#include <dune/common/ftraits.hh>
#include <dune/common/typeutilities.hh>
#include <dune/tensor/ranges/iteratorexpression.hh>
#include <dune/tensor/ranges/iteratorrange.hh>

namespace Dune::Tensor {

/**
 * \defgroup Ranges  Transformations of ranges and iterators
 **/

/**
 * \brief An expression-template involving multiple ranges
 * \ingroup Ranges
 **/
template <class Operation, class Range0, class... Ranges>
class RangeExpression
{
  template<class R>
  using iter_t = decltype(std::cbegin(std::declval<R&>()));

  template<class R>
  using reference_t = decltype(*std::declval<iter_t<R>&>());

  template <class T>
  using range_t = IteratorRange<iter_t<T>>;
  using range_tuple_type = std::tuple<range_t<Range0>,range_t<Ranges>...>;

public:
  using op_type = Operation;
  using reference = std::invoke_result_t<op_type, reference_t<Range0>, reference_t<Ranges>...>;
  using pointer = std::add_pointer_t<reference>;
  using value_type = std::decay_t<reference>;
  using iterator = IteratorExpression<op_type, iter_t<Range0>, iter_t<Ranges>...>;

public:
  /// RangeExpression constructors
  /// @{

  /// \brief Constructor stores the operation by value and all and ranges in a span
  constexpr RangeExpression (op_type op, const Range0& range0, const Ranges&... ranges) noexcept
    : op_{std::move(op)}
    , ranges_{
        range_t<Range0>{range0.begin(),range0.end()},
        range_t<Ranges>{ranges.begin(),ranges.end()}...}
  {
#ifndef NDEBUG
    assertSize(Dune::PriorityTag<2>{}, range0, ranges...);
#endif
  }

  /// @}

  /// \brief Iterator over the expression applied to the non-zeros of the ranges
  iterator begin () const noexcept
  {
    return std::apply([&](auto&&... ranges) {
      return iterator{op_, ranges.begin()...}; }, ranges_);
  }

  /// \brief End-Iterator over the expression applied to the non-zeros of the ranges
  iterator end () const noexcept
  {
    return std::apply([&](auto&&... ranges) {
      return iterator{op_, ranges.end()...}; }, ranges_);
  }

  /// Information
  /// @{

  /// \brief Value mapping
  constexpr const op_type& op () const noexcept { return op_; }

  /// @}

private:
  template <class R0, class... R>
  static auto assertSize(Dune::PriorityTag<1>, const R0& range0, const R&... ranges)
    -> std::void_t<decltype(range0.size()),decltype(ranges.size())...>
  {
    assert((... && (range0.size() == ranges.size())));
  }

  template <class... R>
  static void assertSize(Dune::PriorityTag<0>, const R&...) {}

private:
  op_type op_;
  range_tuple_type ranges_;
};

} // end namespace Dune::Tensor
