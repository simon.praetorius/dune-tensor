#pragma once

#include <utility>

namespace Dune::Tensor {

/**
 * \brief Defines a range as pair of iterators.
 * \ingroup Ranges
 **/
template <class Iterator>
class IteratorRange
{
public:
  constexpr IteratorRange (Iterator begin, Iterator end) noexcept
    : begin_{std::move(begin)}
    , end_{std::move(end)}
  {}

  constexpr auto begin () const noexcept
  {
    return begin_;
  }

  constexpr auto end () const noexcept
  {
    return end_;
  }

  Iterator begin_;
  Iterator end_;
};


} // end namespace Dune::Tensor
