#pragma once

#include <tuple>

#include <dune/tensor/ranges/rangeexpression.hh>

namespace Dune::Tensor {

/**
 * \brief Zip two ranges into a single range that returns a pair of values on access
 * \ingroup Ranges
 **/
template <class Range0, class Range1>
auto zip (const Range0& range0, const Range1& range1)
{
  return RangeExpression{[](const auto& lhs, const auto& rhs) { return std::pair{lhs,rhs}; }, range0, range1};
}

} // end namespace Dune::Tensor
