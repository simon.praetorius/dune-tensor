#pragma once

#include <dune/tensor/ranges/traits.hh>

namespace Dune::Concept {

template <class I>
inline constexpr bool InputIteratorCategory
  = (Dune::Tensor::Impl::IteratorCategoryToInt<I>::value >= 0);

template <class I>
inline constexpr bool ForwardIteratorCategory
  = (Dune::Tensor::Impl::IteratorCategoryToInt<I>::value >= 1);

template <class I>
inline constexpr bool BidirectionalIteratorCategory
  = (Dune::Tensor::Impl::IteratorCategoryToInt<I>::value >= 2);

template <class I>
inline constexpr bool RandomAccessIteratorCategory
  = (Dune::Tensor::Impl::IteratorCategoryToInt<I>::value >= 3);

template <class I>
inline constexpr bool ContiguousIteratorCategory
  = (Dune::Tensor::Impl::IteratorCategoryToInt<I>::value >= 4);


} // end namespace Dune::Concept
