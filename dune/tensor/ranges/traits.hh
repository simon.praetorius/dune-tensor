#pragma once

#include <algorithm>
#include <iterator>
#include <type_traits>

namespace Dune::Tensor {
namespace Impl {

template <class R, class E, class Enable = std::void_t<>>
struct IsRange
  : std::false_type
{};

template <class R, class E>
struct IsRange<R,E,std::void_t<decltype(std::declval<R>().begin(), std::declval<R>().end())>>
  : std::is_convertible<decltype(*std::declval<R>().begin()), E>
{};

} // end namespace Impl

template <class R, class E>
using IsRange = Impl::IsRange<R,E>;

template <class R, class E>
inline constexpr bool IsRange_v = Impl::IsRange<R,E>::value;

// Iterator traits

namespace Impl {

template <class Cat>
struct IteratorCategoryToInt : std::integral_constant<int,-1> {};
template <>
struct IteratorCategoryToInt<std::input_iterator_tag> : std::integral_constant<int,0> {};
template <>
struct IteratorCategoryToInt<std::forward_iterator_tag> : std::integral_constant<int,1> {};
template <>
struct IteratorCategoryToInt<std::bidirectional_iterator_tag> : std::integral_constant<int,2> {};
template <>
struct IteratorCategoryToInt<std::random_access_iterator_tag> : std::integral_constant<int,3> {};
#if __cplusplus > 201709L
template <>
struct IteratorCategoryToInt<std::contiguous_iterator_tag> : std::integral_constant<int,4> {};
#endif

template <int id>
struct IteratorCategoryFromInt;
template <>
struct IteratorCategoryFromInt<0> { using type = std::input_iterator_tag; };
template <>
struct IteratorCategoryFromInt<1> { using type = std::forward_iterator_tag; };
template <>
struct IteratorCategoryFromInt<2> { using type = std::bidirectional_iterator_tag; };
template <>
struct IteratorCategoryFromInt<3> { using type = std::random_access_iterator_tag; };
#if __cplusplus > 201709L
template <>
struct IteratorCategoryFromInt<4> { using type = std::contiguous_iterator_tag; };
#endif

} // end namespace Impl


/// \brief The iterator category that is the minimal common category
template <class... Categories>
struct CommonIteratorCategory
{
  using type = typename Impl::IteratorCategoryFromInt<std::min({Impl::IteratorCategoryToInt<Categories>::value...})>::type;
};

/// \relates CommonIteratorCategory
template <class... Iter>
using CommonIteratorCategory_t = typename CommonIteratorCategory<
  typename std::iterator_traits<Iter>::iterator_category...>::type;

} // end namepsace Dune::Tensor
