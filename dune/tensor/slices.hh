#pragma once

namespace Dune::Tensor {

/// \brief A slice specifier representing a bounded index range [begin, end)
template <class Begin, class End = Begin>
struct IndexRange
{
  using begin_type = Begin;
  using end_type = End;

  begin_type begin{};
  end_type end{};
};

/**
 * \brief A slice specifier representing a bounded index set in the range
 * [begin, end) with a stride, where `end` is defined as `begin + extent*stride`.
 *
 * The index set contains the indices `begin, begin + stride, begin + 2*stride...`.
 */
template <class Begin, class Extent, class Stride>
struct StridedIndexRange
{
  using begin_type = Begin;
  using extent_type = Extent;
  using stride_type = Stride;

  begin_type begin{};
  extent_type extent{};
  stride_type stride{};
};

} // end namespace Dune::Tensor
