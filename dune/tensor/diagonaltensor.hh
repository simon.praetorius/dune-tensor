#pragma once

#include <type_traits>

#include <dune/common/filledarray.hh>
#include <dune/common/std/extents.hh>

#include <dune/tensor/derivativetraits.hh>
#include <dune/tensor/sparsetensor.hh>
#include <dune/tensor/extents/traits.hh>
#include <dune/tensor/containers/traits.hh>
#include <dune/tensor/inserters/diagonalinserter.hh>
#include <dune/tensor/iterators/diagonaliterators.hh>
#include <dune/tensor/layouts/diagonallayout.hh>

namespace Dune::Tensor {

struct DiagonalTensorTraits
{
  template <class T>
  using Inserter = DiagonalInserter<T>;

  using Iterators = DiagonalIterators;
};

/**
 * \brief A diagonal tensor of arbitrary rank and all equal extents.
 * \ingroup Tensors
 * \nosubgrouping
 *
 * Stores only the diagonal values. Note, this is a sparse tensor. The
 * extents can be `Std::dynamic_extent` or static. If a dynamic extent is given, the
 * constructor with size and (optionally) value must be used, in order to
 * set this dynamic extent. In case all extents are static, also a default
 * constructor is enabled.
 *
 * \tparam Value    The element type stored in the tensor diagonal
 * \tparam Extents  The type representing the tensor extents. Must be compatible
 *                  to `Dune::Tensor::Extents`. If static extents are given, they
 *                  must be all equal.
 * \tparam Container  A storage backend with single index access and compatible
 *                    initialization constructors. See the `Dune::Concept::Container`
 *                    concept. It is a FixedSizeContainer if any extent is static,
 *                    otherwise DynamicSizeContainer.
 **/
template <class Value, class Extents,
          class Container = DynamicOrFixedSizeContainer<maxExtent_v<Extents>>>
class DiagonalTensor
    : public SparseTensor<Value, Extents, DiagonalLayout, Container, DiagonalTensorTraits>
{
  using base_type = SparseTensor<Value, Extents, DiagonalLayout, Container, DiagonalTensorTraits>;

public:
  using extents_type = Extents;
  using size_type = typename base_type::size_type;
  using index_type = typename base_type::index_type;
  using storage_type = typename base_type::storage_type;
  using value_type = Value;
  using reference = typename base_type::reference;
  using const_reference = typename base_type::const_reference;

  /// \name Additional DiagonalTensor constructors
  /// @{

  /// \brief Construct the tensor from a uniform extent
  template <class E = extents_type,
    std::enable_if_t<(E::rank_dynamic() > 0), int> = 0>
  explicit constexpr DiagonalTensor (size_type ext) noexcept
    : base_type{
        storage_type{ext},
        extents_type{Dune::filledArray<extents_type::rank_dynamic()>(ext)}}
  {}

  /// \brief Construct the tensor from a uniform extent and a constant value set to all diagonal entries
  template <class E = extents_type,
    std::enable_if_t<(E::rank_dynamic() > 0), int> = 0>
  constexpr DiagonalTensor (size_type ext, const value_type& value) noexcept
    : base_type{
        storage_type{ext,value},
        extents_type{Dune::filledArray<extents_type::rank_dynamic()>(ext)}}
  {}

  /// \brief Construct the tensor from extents and a constant value set to all diagonal entries
  constexpr DiagonalTensor (extents_type e, const value_type& value) noexcept
    : base_type{storage_type{e.extent(0),value},e}
  {}

  constexpr DiagonalTensor (const base_type& base) noexcept
    : base_type{base}
  {}

  constexpr DiagonalTensor (base_type&& base) noexcept
    : base_type{std::move(base)}
  {}

  /// @}

  using base_type::base_type;
  using base_type::operator=;

  /// \brief Get a reference to the i'th diagonal entry
  reference diagonal (index_type i) noexcept
  {
    return base_type::values()[i];
  }

  /// \brief Get a const reference to the i'th diagonal entry
  const_reference diagonal (index_type i) const noexcept
  {
    return base_type::values()[i];
  }

  /// \brief Return a span over the diagonal entries
  auto diagonal () noexcept { return base_type::values(); }

  /// \brief Return a const span over the diagonal entries
  auto diagonal () const noexcept { return base_type::values(); }
};

/// \brief A diagonal matrix is a DiagonalTensor of rank 2 with all extents equal.
template <class Value, std::size_t extent = Std::dynamic_extent>
using DiagonalMatrix = DiagonalTensor<Value,Std::extents<int,extent,extent>>;


/// Specialization of DerivativeTraits for Tensor valued maps
/// @{
template <class V, class S, std::size_t... exts, class C, class K, int dim>
struct DerivativeTraits<DiagonalTensor<V,Std::extents<S,exts...>,C>(Dune::FieldVector<K,dim>),
  std::enable_if_t<(... && (exts != Std::dynamic_extent))>>
{
  using Range = FieldTensor<V,int(exts)...,dim>;
};

template <class V, class S, std::size_t... exts, class C, class K, int dim>
struct DerivativeTraits<DiagonalTensor<V,Std::extents<S,exts...>,C>(FieldTensor<K,dim>),
  std::enable_if_t<(... && (exts != Std::dynamic_extent))>>
{
  using Range = FieldTensor<V,int(exts)...,dim>;
};
/// @}

} // end namespace Dune::Tensor


template <class V, class E, class C>
struct Dune::FieldTraits< Dune::Tensor::DiagonalTensor<V,E,C> >
{
  using field_type = typename FieldTraits<V>::field_type;
  using real_type = typename FieldTraits<V>::real_type;
};
