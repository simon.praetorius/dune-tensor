#pragma once

#include <dune/common/typetraits.hh>
#include <dune/tensor/tags.hh>

namespace Dune {

template <class Value, int size>
class FieldVector;

} // end namespace Dune

namespace Dune::Tensor {

// forward declaration
template <class Value, int... exts>
class FieldTensor;

template <class Signature, class Enable = void>
struct DerivativeTraits
{
  /// \brief Range of derivative for functions with given signature
  using Range = tag::unknown_t;
};

// If return type is a number type

template <class Value, class K, int dim>
struct DerivativeTraits<Value(FieldTensor<K,dim>),
  std::enable_if_t<Dune::IsNumber<Value>::value>>
{
  using Range = FieldTensor<Value,dim>;
};

template <class Value, class K, int dim>
struct DerivativeTraits<Value(Dune::FieldVector<K,dim>),
  std::enable_if_t<Dune::IsNumber<Value>::value>>
{
  using Range = FieldTensor<Value,dim>;
};

// If return type is a legacy Dune::FieldVector

template <class Value, int ext, class K, int dim>
struct DerivativeTraits<Dune::FieldVector<Value,ext>(FieldTensor<K,dim>)>
{
  using Range = FieldTensor<Value,ext,dim>;
};

template <class Value, int ext, class K, int dim>
struct DerivativeTraits<Dune::FieldVector<Value,ext>(Dune::FieldVector<K,dim>)>
{
  using Range = FieldTensor<Value,ext,dim>;
};


} // end namespace Dune::Tensor
