#pragma once

#include <array>
#include <iterator>
#include <tuple>

#include <dune/tensor/utility/pointer.hh>

namespace Dune::Tensor {

/**
 * \brief Iterators over the entries of a sparse matrix in CSF format.
 *
 * Provide two iterators: a non-zero iterator and an iterator over the major-axis.
 */
struct CSFIterators
{
  /// \brief Type of the iterator over the non-zero entries in the matrix
  template <class Mapping, class Storage>
  struct NonZeroIterator
  {
  public:
    using mapping_type = Mapping;
    using storage_type = Storage;
    using extents_type = typename mapping_type::extents_type;
    using size_type = typename extents_type::size_type;
    using rank_type = typename extents_type::rank_type;

    using difference_type = std::ptrdiff_t;
    using iterator_category = std::forward_iterator_tag;
    using indices_type = typename mapping_type::indices_type;
    using value_type = std::pair<indices_type, typename storage_type::const_reference>;
    using reference = value_type;
    using pointer = Pointer<reference>;

  public:
    NonZeroIterator () noexcept = default;
    NonZeroIterator (const mapping_type& mapping,
                     const storage_type& storage, size_type pos = 0) noexcept
      : mapping_{&mapping}
      , storage_{&storage}
    {
      iPos_.back() = pos;
    }

    /// \brief Return a pair `<index,value>` with `index` an index-tuple
    reference operator* () const noexcept
    {
      indices_type index;
      for (rank_type r = 0; r < extents_type::rank(); ++r)
        index[r] = (mapping_->indices_)[r][iPos_[r]];
      return {index,(*storage_)[iPos_.back()]};
    }

    pointer operator-> () const noexcept
    {
      return pointer{*this};
    }

    NonZeroIterator& operator++ () noexcept
    {
      iPos_.back()++;
      for (rank_type r = 1; r < extents_type::rank(); ++r) {
        auto i = extents_type::rank()-r;
        if ((mapping_->offsets_)[i-1][oPos_[i-1]+1] == iPos_[i]) {
          iPos_[i-1]++;
          oPos_[i-1]++;
        }
      }

      return *this;
    }

    NonZeroIterator operator++ (int) noexcept
    {
      NonZeroIterator tmp{*this};
      ++(*this);
      return tmp;
    }

    friend bool operator== (const NonZeroIterator& lhs, const NonZeroIterator& rhs) noexcept
    {
      return lhs.iPos_.back() == rhs.iPos_.back();
    }

    friend bool operator!= (const NonZeroIterator& lhs, const NonZeroIterator& rhs) noexcept
    {
      return lhs.iPos_.back() != rhs.iPos_.back();
    }

  private:
    const mapping_type* mapping_ = nullptr;
    const storage_type* storage_ = nullptr;
    std::array<size_type,extents_type::rank()> iPos_{};
    std::array<size_type,extents_type::rank()> oPos_{};
  };

  /// \brief Return a non-zero begin-iterator over the given tensor
  template <class M, class S>
  static NonZeroIterator<M,S> begin (tag::nz_t, const M& mapping, const S& storage)
  {
    return NonZeroIterator<M,S>{mapping.begin(), storage.begin(), 0};
  }

  /// \brief Return a non-zero end-iterator over the given tensor
  template <class M, class S>
  static NonZeroIterator<M,S> end (tag::nz_t, const M& mapping, const S& storage)
  {
    return NonZeroIterator<M,S>{mapping.end(), storage.end(), storage.size()};
  }


  /// \brief Type of an iterator of the major-axis of a CSF tensor
  template <class Mapping, class Storage, typename Mapping::extents_type::rank_type I = 0>
  struct MajorIterator
  {
    using mapping_type = Mapping;
    using storage_type = Storage;
    using extents_type = typename mapping_type::extents_type;
    using size_type = typename extents_type::size_type;
    using rank_type = typename extents_type::rank_type;

    template <class J>
    struct MajorRange
    {
      constexpr MajorIterator<mapping_type,storage_type,J::value> begin () const noexcept
      {
        return {*mapping_, *storage_, begin_};
      }

      constexpr MajorIterator<mapping_type,storage_type,J::value> end () const noexcept
      {
        return {*mapping_, *storage_, end_};
      }

      const mapping_type* mapping_;
      const storage_type* storage_;
      size_type begin_{0};
      size_type end_{0};
    };

    template <class>
    using reference_t = typename storage_type::const_reference;

  public:
    using difference_type = std::ptrdiff_t;
    using iterator_category = std::forward_iterator_tag;
    using reference = ConditionalDelay_t<(I+1 < extents_type::rank()),
      std::integral_constant<rank_type, I+1>, MajorRange, reference_t>;
    using pointer = Pointer<reference>;
    using value_type = std::decay_t<reference>;

    MajorIterator () noexcept = default;
    MajorIterator (const mapping_type& mapping,
                   const storage_type& storage, size_type pos = 0) noexcept
      : mapping_{&mapping}
      , storage_{&storage}
      , iPos_{pos}
    {}

    /// \brief The index in the current dimension
    constexpr size_type index () const noexcept
    {
      return (mapping_->indices_)[I][iPos_];
    }

    /// \brief Return either a range over the next dimension or the final value
    constexpr decltype(auto) operator* () const noexcept
    {
      if constexpr(I+1 < extents_type::rank())
        return value_type{mapping_, storage_,
          (mapping_->offsets_)[I][oPos_], (mapping_->offsets_)[I][oPos_+1]};
      else
        return (*storage_)[iPos_];
    }

    pointer operator-> () const noexcept
    {
      return pointer{*(*this)};
    }

    constexpr MajorIterator& operator++ () noexcept
    {
      ++iPos_;
      ++oPos_;
      return *this;
    }

    constexpr MajorIterator operator++ (int) noexcept
    {
      MajorIterator tmp{*this};
      ++(*this);
      return tmp;
    }

    friend constexpr bool operator== (const MajorIterator& lhs, const MajorIterator& rhs) noexcept
    {
      return lhs.iPos_ == rhs.iPos_;
    }

    friend constexpr bool operator!= (const MajorIterator& lhs, const MajorIterator& rhs) noexcept
    {
      return lhs.iPos_ != rhs.iPos_;
    }

    const mapping_type* mapping_ = nullptr;
    const storage_type* storage_ = nullptr;
    size_type iPos_{0};
    size_type oPos_{0};
  };

  /// \brief Return a major begin-iterator to the given tensor
  template <class M, class S>
  static MajorIterator<M,S> begin (tag::major_t, const M& mapping, const S& storage)
  {
    return MajorIterator<M,S>{mapping, storage, 0};
  }

  /// \brief Return a major end-iterator to the given tensor
  template <class M, class S>
  static MajorIterator<M,S> end (tag::major_t, const M& mapping, const S& storage)
  {
    using size_type = typename M::extents_type::size_type;
    return MajorIterator<M,S>{mapping, storage, size_type(mapping.indices_[0].size())};
  }

  /// \brief Return a major begin-iterator to the given tensor (left-first)
  template <class M, class S>
  static MajorIterator<M,S> begin (tag::left_t, const M& mapping, const S& storage)
  {
    return MajorIterator<M,S>{mapping, storage, 0};
  }

  /// \brief Return a major end-iterator to the given tensor (left-first)
  template <class M, class S>
  static MajorIterator<M,S> end (tag::left_t, const M& mapping, const S& storage)
  {
    using size_type = typename M::extents_type::size_type;
    return MajorIterator<M,S>{mapping, storage, size_type(mapping.indices_[0].size())};
  }
};

} // end namespace Dune::Tensor
