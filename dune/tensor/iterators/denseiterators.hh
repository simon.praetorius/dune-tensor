#pragma once

#include <array>
#include <iterator>
#include <tuple>
#include <type_traits>

#include <dune/common/std/mdspan.hh>
#include <dune/common/std/layout_left.hh>
#include <dune/common/std/layout_right.hh>

#include <dune/tensor/utility/array.hh>
#include <dune/tensor/utility/pointer.hh>
#include <dune/tensor/utility/typetraits.hh>

namespace Dune::Tensor {

struct DenseIterators
{
  /**
   * \brief Type of the non-zero iterator.
   *
   * NOTE: This iterator does not yet implement the `NonZeroIterator` concept, i.e.,
   * it does not provide the index-tuple of the non-zero value, but just the value.
   *
   * TODO: Implement a reverse mapping of the flat index to the index-tuple.
   */

  /// \brief Return the non-zero begin-iterator to the given tensor
  template <class Tensor>
  static auto begin (tag::nz_t, Tensor& tensor)
  {
    return tensor.values().begin();
  }

  /// \brief Return the non-zero end-iterator to the given tensor
  template <class Tensor>
  static auto end (tag::nz_t, Tensor& tensor)
  {
    return tensor.values().end();
  }


  /// \brief Iterator with left-first index-tuple traversal
  template <class Tensor, std::size_t I = 0>
  struct LeftIterator
  {
    using self_type = LeftIterator;
    using tensor_type = Tensor;
    using extents_type = typename tensor_type::extents_type;
    using index_type = typename extents_type::index_type;
    using indices_type = std::array<index_type,I+1>;

    template <class J>
    struct DimensionRange
    {
      constexpr LeftIterator<tensor_type,J::value> begin () const noexcept
      {
        return {tensor_, push_back(indices_,begin_)};
      }

      constexpr LeftIterator<tensor_type,J::value> end () const noexcept
      {
        return {tensor_, push_back(indices_,end_)};
      }

      tensor_type tensor_;
      indices_type indices_;
      index_type begin_{0};
      index_type end_{0};
    };

    template <class>
    using reference_t = typename tensor_type::reference;

  public:
    using iterator_category = std::random_access_iterator_tag;
    using difference_type = std::ptrdiff_t;
    using reference = ConditionalDelay_t<(I+1 < extents_type::rank()),
      std::integral_constant<std::size_t, I+1>, DimensionRange, reference_t>;
    using pointer = Pointer<reference>;
    using value_type = std::decay_t<reference>;

    constexpr LeftIterator () noexcept = default;
    constexpr LeftIterator (const tensor_type& tensor, const indices_type& indices) noexcept
      : tensor_{tensor}
      , indices_{indices}
    {}

    /**
     * \brief Accesses the pointed-to element.
     * This either results in a range representing the sub-tensor with prefixed indices,
     * or the final tensor element if all components are traversed.
     **/
    constexpr decltype(auto) operator* () const noexcept
    {
      if constexpr(I+1 < extents_type::rank()) {
        using SubTensorRange = DimensionRange<std::integral_constant<std::size_t, I+1>>;
        return SubTensorRange{tensor_, indices_, 0, tensor_.extent(I)};
      }
      else {
        return tensor_[indices_];
      }
    }

    /// \brief Accesses an element by index.
    constexpr decltype(auto) operator[] (difference_type n) const noexcept
    {
      return *((*this)+n);
    }

    /// \brief Accesses the pointed-to element.
    constexpr pointer operator-> () const noexcept
    {
      return pointer{*(*this)};
    }

    /// \brief Return the current component-index.
    constexpr index_type index () const noexcept
    {
      return indices_.back();
    }

    /// \name Increments or decrements the iterator
    /// @{

    constexpr self_type& operator++ () noexcept { ++indices_.back(); return *this; }
    constexpr self_type  operator++ (int) noexcept { self_type tmp{*this}; ++(*this); return tmp; }
    constexpr self_type& operator-- () noexcept { --indices_.back(); return *this; }
    constexpr self_type  operator-- (int) noexcept { self_type tmp{*this}; --(*this); return tmp; }

    constexpr self_type& operator+= (difference_type n) noexcept { indices_.back() += n; return *this; }
    constexpr self_type& operator-= (difference_type n) noexcept { indices_.back() -= n; return *this; }

    friend constexpr self_type operator+ (self_type lhs, difference_type n) noexcept { return lhs += n; }
    friend constexpr self_type operator+ (difference_type n, self_type rhs) noexcept { return rhs += n; }
    friend constexpr self_type operator- (self_type lhs, difference_type n) noexcept { return lhs -= n; }

    friend constexpr difference_type operator- (const self_type& lhs, const self_type& rhs) noexcept
    {
      return lhs.index() - rhs.index();
    }

    /// @}


    /// \name Comparison operators
    /// @{

    friend constexpr bool operator== (const self_type& lhs, const self_type& rhs) { return lhs.index() == rhs.index(); }
    friend constexpr bool operator<  (const self_type& lhs, const self_type& rhs) { return lhs.index() <  rhs.index(); }
    friend constexpr bool operator<= (const self_type& lhs, const self_type& rhs) { return lhs.index() <= rhs.index(); }
    friend constexpr bool operator>  (const self_type& lhs, const self_type& rhs) { return lhs.index() >  rhs.index(); }
    friend constexpr bool operator>= (const self_type& lhs, const self_type& rhs) { return lhs.index() >= rhs.index(); }
    friend constexpr bool operator!= (const self_type& lhs, const self_type& rhs) { return lhs.index() != rhs.index(); }

    /// @}

  private:
    tensor_type tensor_;
    indices_type indices_{};
  };

  template <class V, class E, class L, class A,
    std::enable_if_t<(E::rank() > 0), int> = 0>
  static auto begin (tag::left_t, Std::mdspan<V,E,L,A> tensor)
  {
    return LeftIterator<Std::mdspan<V,E,L,A>>{tensor,
      std::array<typename E::index_type,1>{0}};
  }

  template <class Tensor,
    decltype(std::declval<Tensor&>().to_mdspan(), bool{}) = true>
  static auto begin (tag::left_t, Tensor& tensor)
  {
    return begin(tag::left, tensor.to_mdspan());
  }

  template <class V, class E, class L, class A,
    std::enable_if_t<(E::rank() > 0), int> = 0>
  static auto end (tag::left_t, Std::mdspan<V,E,L,A> tensor)
  {
    return LeftIterator<Std::mdspan<V,E,L,A>>{tensor,
      std::array<typename E::index_type,1>{tensor.extent(0)}};
  }

  template <class Tensor,
    decltype(std::declval<Tensor&>().to_mdspan(), bool{}) = true>
  static auto end (tag::left_t, Tensor& tensor)
  {
    return end(tag::left, tensor.to_mdspan());
  }


  /// \brief Iterator with right-first index-tuple traversal
  template <class Tensor, std::size_t I = 0>
  struct RightIterator
  {
    using self_type = RightIterator;
    using tensor_type = Tensor;
    using extents_type = typename tensor_type::extents_type;
    using index_type = typename extents_type::index_type;
    using indices_type = std::array<index_type,I+1>;

    template <class J>
    struct DimensionRange
    {
      constexpr RightIterator<tensor_type,J::value> begin () const noexcept
      {
        return {tensor_, push_front(indices_,begin_)};
      }

      constexpr RightIterator<tensor_type,J::value> end () const noexcept
      {
        return {tensor_, push_front(indices_,end_)};
      }

      tensor_type tensor_;
      indices_type indices_;
      index_type begin_{0};
      index_type end_{0};
    };

    template <class>
    using reference_t = typename tensor_type::reference;

  public:
    using iterator_category = std::random_access_iterator_tag;
    using difference_type = std::ptrdiff_t;
    using reference = ConditionalDelay_t<(I+1 < extents_type::rank()),
      std::integral_constant<std::size_t, I+1>, DimensionRange, reference_t>;
    using pointer = Pointer<reference>;
    using value_type = std::decay_t<reference>;

    constexpr RightIterator () noexcept = default;
    constexpr RightIterator (const tensor_type& tensor, indices_type indices) noexcept
      : tensor_{tensor}
      , indices_{indices}
    {}

    /**
     * \brief Accesses the pointed-to element.
     * This either results in a range representing the sub-tensor with prefixed indices,
     * or the final tensor element if all components are traversed.
     **/
    constexpr decltype(auto) operator* () const noexcept
    {
      if constexpr(I+1 < extents_type::rank()) {
        using SubTensorRange = DimensionRange<std::integral_constant<std::size_t, I+1>>;
        return SubTensorRange{tensor_, indices_, 0, tensor_.extent(extents_type::rank()-I-1)};
      } 
      else
        return tensor_[indices_];
    }

    /// \brief Accesses an element by index.
    constexpr decltype(auto) operator[] (difference_type n) const noexcept
    {
      return *((*this)+n);
    }

    /// \brief Accesses the pointed-to element.
    constexpr pointer operator-> () const noexcept
    {
      return pointer{*this};
    }

    /// \brief Return the current component-index.
    constexpr index_type index () const noexcept
    {
      return indices_.front();
    }

    /// \name Increments or decrements the iterator
    /// @{

    constexpr self_type& operator++ () noexcept {  ++indices_.front(); return *this; }
    constexpr self_type  operator++ (int) noexcept { self_type tmp{*this}; ++(*this); return tmp; }
    constexpr self_type& operator-- () noexcept { --indices_.front(); return *this; }
    constexpr self_type  operator-- (int) noexcept { self_type tmp{*this}; --(*this); return tmp; }

    constexpr self_type& operator+= (difference_type n) noexcept { indices_.front() += n; return *this; }
    constexpr self_type& operator-= (difference_type n) noexcept { indices_.front() -= n; return *this; }

    friend constexpr self_type operator+ (self_type lhs, difference_type n) noexcept { return lhs += n; }
    friend constexpr self_type operator+ (difference_type n, self_type rhs) noexcept { return rhs += n; }
    friend constexpr self_type operator- (self_type lhs, difference_type n) noexcept { return lhs -= n; }

    friend constexpr difference_type operator- (const self_type& lhs, const self_type& rhs) noexcept 
    { 
      return lhs.index() - rhs.index();
    }

    /// @}


    /// \name Comparison operators
    /// @{

    friend constexpr bool operator== (const self_type& lhs, const self_type& rhs) { return lhs.index() == rhs.index(); }
    friend constexpr bool operator<  (const self_type& lhs, const self_type& rhs) { return lhs.index() <  rhs.index(); }
    friend constexpr bool operator<= (const self_type& lhs, const self_type& rhs) { return lhs.index() <= rhs.index(); }
    friend constexpr bool operator>  (const self_type& lhs, const self_type& rhs) { return lhs.index() >  rhs.index(); }
    friend constexpr bool operator>= (const self_type& lhs, const self_type& rhs) { return lhs.index() >= rhs.index(); }
    friend constexpr bool operator!= (const self_type& lhs, const self_type& rhs) { return lhs.index() != rhs.index(); }
    
    /// @}

  private:
    tensor_type tensor_;
    indices_type indices_{};
  };

  template <class V, class E, class L, class A,
    std::enable_if_t<(E::rank() > 0), int> = 0>
  static auto begin (tag::right_t, Std::mdspan<V,E,L,A> tensor)
  {
    return RightIterator<Std::mdspan<V,E,L,A>>{tensor,
      std::array<typename E::index_type,1>{0}};
  }

  template <class Tensor,
    decltype(std::declval<Tensor&>().to_mdspan(), bool{}) = true>
  static auto begin (tag::right_t, Tensor& tensor)
  {
    return begin(tag::right, tensor.to_mdspan());
  }

  template <class V, class E, class L, class A,
    std::enable_if_t<(E::rank() > 0), int> = 0>
  static auto end (tag::right_t, Std::mdspan<V,E,L,A> tensor)
  {
    return RightIterator<Std::mdspan<V,E,L,A>>{tensor,
      std::array<typename E::index_type,1>{tensor.extent(E::rank()-1)}};
  }

  template <class Tensor,
    decltype(std::declval<Tensor&>().to_mdspan(), bool{}) = true>
  static auto end (tag::right_t, Tensor& tensor)
  {
    return end(tag::right, tensor.to_mdspan());
  }
};

} // end namespace Dune::Tensor
