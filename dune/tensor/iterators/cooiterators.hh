#pragma once

#include <array>
#include <iterator>
#include <tuple>

#include <dune/tensor/utility/pointer.hh>

namespace Dune::Tensor {

/**
 * \brief Iterators over the entries of a sparse matrix in COO format.
 *
 * Provide an iterator over the non-zero entries of the matrix, represented
 * as a pair `<index,value>`, where `index` is an index-tuple.
 */
class COOIterators
{
  struct MakePair
  {
    template <class L, class R>
    std::pair<L,R> operator() (const L& l, const R& r) const { return {l,r}; }
  };

public:
  /**
   * \brief Type of the non-zero iterator.
   * \tparam M  The mapping type of the tensor.
   * \tparam S  The storage type of the tensor.
   **/
  template <class M, class S>
  using NonZeroIterator = IteratorExpression<MakePair,
    typename M::index_storage_type::const_iterator,
    typename S::const_iterator>;

public:
  /// \brief Return the begin-iterator to the non-zero range.
  template <class M, class S>
  static NonZeroIterator<M,S> begin (tag::nz_t, const M& mapping, const S& storage)
  {
    return NonZeroIterator<M,S>{mapping.begin(), storage.begin()};
  }

  /// \brief Return the end-iterator to the non-zero range.
  template <class M, class S>
  static NonZeroIterator<M,S> end (tag::nz_t, const M& mapping, const S& storage)
  {
    return NonZeroIterator<M,S>{mapping.end(), storage.end()};
  }
};

} // end namespace Dune::Tensor
