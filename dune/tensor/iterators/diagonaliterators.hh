#pragma once

#include <array>
#include <iterator>
#include <tuple>
#include <type_traits>

#include <dune/tensor/utility/array.hh>
#include <dune/tensor/utility/pointer.hh>
#include <dune/tensor/utility/typetraits.hh>

namespace Dune::Tensor {

struct DiagonalIterators
{
  /// \brief Type of an iterator of the nonzero entries (diagonal entries) only
  template <class Mapping, class Storage>
  struct NonZeroIterator
  {
    using mapping_type = Mapping;
    using storage_type = Storage;
    using extents_type = typename mapping_type::extents_type;
    using size_type = typename extents_type::size_type;
    using rank_type = typename extents_type::rank_type;
    using indices_type = std::array<size_type,extents_type::rank()>;

    using iterator_category = std::forward_iterator_tag;
    using difference_type = std::ptrdiff_t;
    using reference = std::pair<indices_type const, typename storage_type::const_reference>;
    using pointer = Pointer<reference>;
    using value_type = std::decay_t<reference>;

    constexpr NonZeroIterator () noexcept = default;
    constexpr NonZeroIterator (typename storage_type::const_iterator value_it,
                               size_type index) noexcept
      : value_{value_it}
      , index_{index}
    {}

    constexpr reference operator* () const noexcept
    {
      return {Dune::filledArray<extents_type::rank()>(index_), *value_};
    }

    constexpr pointer operator-> () const noexcept
    {
      return pointer{*(*this)};
    }

    constexpr NonZeroIterator& operator++ () noexcept
    {
      ++value_;
      ++index_;
      return *this;
    }

    constexpr NonZeroIterator operator++ (int) noexcept
    {
      NonZeroIterator tmp{*this};
      ++(*this);
      return tmp;
    }

    friend constexpr bool operator== (const NonZeroIterator& lhs, const NonZeroIterator& rhs)
    {
      return lhs.value_ == rhs.value_;
    }

    friend constexpr bool operator!= (const NonZeroIterator& lhs, const NonZeroIterator& rhs)
    {
      return !(lhs == rhs);
    }

  private:
    typename storage_type::const_iterator value_;
    size_type index_;
  };

  template <class M, class S>
  static NonZeroIterator<M,S> begin (tag::nz_t, const M& mapping, const S& storage)
  {
    return NonZeroIterator<M,S>{storage.begin(),0};
  }

  template <class M, class S>
  static NonZeroIterator<M,S> end (tag::nz_t, const M& mapping, const S& storage)
  {
    return NonZeroIterator<M,S>{storage.end(),storage.size()};
  }



  /// \brief Type of an iterator of the major-axis
  template <class Extents, class Storage, typename Extents::rank_type I = 0>
  struct MajorIterator
  {
    using Self = MajorIterator;
    using extents_type = Extents;
    using storage_type = Storage;
    using size_type = typename extents_type::size_type;
    using rank_type = typename extents_type::rank_type;

    template <class>
    using reference_t = typename storage_type::const_reference;

    template <class J>
    struct SingleElementRange
    {
      struct SingleElementIterator
      {
        using Self = SingleElementIterator;

        using difference_type = std::ptrdiff_t;
        using iterator_category = std::forward_iterator_tag;
        using reference = ConditionalDelay_t<(J::value+1 < extents_type::rank()),
          std::integral_constant<rank_type, J::value+1>, SingleElementRange, reference_t>;
        using pointer = Pointer<reference>;
        using value_type = std::decay_t<reference>;

        /// \brief Return either a range over the next dimension or the final value
        constexpr decltype(auto) operator* () const noexcept
        {
          if constexpr(J::value+1 < extents_type::rank())
            return value_type{value_, index_};
          else
            return *value_;
        }

        constexpr size_type index () const noexcept { return index_; }
        pointer operator-> () const noexcept { return pointer{*(*this)}; }
        constexpr Self& operator++ () noexcept { ++index_; return *this; }
        constexpr Self operator++ (int) noexcept { Self tmp{*this}; ++(*this); return tmp; }

        friend constexpr bool operator== (const Self& lhs, const Self& rhs) noexcept
        {
          return lhs.index_ == rhs.index_;
        }

        friend constexpr bool operator!= (const Self& lhs, const Self& rhs) noexcept
        {
          return lhs.index_ != rhs.index_;
        }

        typename storage_type::const_pointer value_;
        size_type index_;
      };

      constexpr SingleElementIterator begin () const noexcept { return {value_,index_}; }
      constexpr SingleElementIterator end () const noexcept { return {nullptr,index_+1}; }
      typename storage_type::const_pointer value_;
      size_type index_;
    };


  public:
    using difference_type = std::ptrdiff_t;
    using iterator_category = std::forward_iterator_tag;
    using reference = ConditionalDelay_t<(I+1 < extents_type::rank()),
      std::integral_constant<rank_type, I+1>, SingleElementRange, reference_t>;
    using pointer = Pointer<reference>;
    using value_type = std::decay_t<reference>;

    MajorIterator () noexcept = default;
    MajorIterator (const storage_type& storage, size_type index) noexcept
      : storage_{&storage}
      , index_{index}
    {}

    /// \brief Return either a range over the next dimension or the final value
    constexpr decltype(auto) operator* () const noexcept
    {
      if constexpr(I+1 < extents_type::rank())
        return value_type{storage_->data()+index_, index_};
      else
        return storage_->data()[index_];
    }

    constexpr size_type index () const noexcept { return index_; }
    pointer operator-> () const noexcept { return pointer{*(*this)}; }
    constexpr Self& operator++ () noexcept { ++index_; return *this; }
    constexpr Self operator++ (int) noexcept { Self tmp{*this}; ++(*this); return tmp; }

    friend constexpr bool operator== (const Self& lhs, const Self& rhs) noexcept
    {
      return lhs.index_ == rhs.index_;
    }

    friend constexpr bool operator!= (const Self& lhs, const Self& rhs) noexcept
    {
      return lhs.index_ != rhs.index_;
    }

    const storage_type* storage_ = nullptr;
    size_type index_{0};
  };

  /// \brief Return a major begin-iterator to the given tensor
  template <class M, class S>
  static MajorIterator<typename M::extents_type,S> begin (tag::major_t, const M& mapping, const S& storage)
  {
    return {storage, 0};
  }

  /// \brief Return a major end-iterator to the given tensor
  template <class M, class S>
  static MajorIterator<typename M::extents_type,S> end (tag::major_t, const M& mapping, const S& storage)
  {
    using size_type = typename M::extents_type::size_type;
    return {storage, size_type(storage.size())};
  }

  /// \brief Return a major begin-iterator to the given tensor
  template <class M, class S>
  static MajorIterator<typename M::extents_type,S> begin (tag::left_t, const M& mapping, const S& storage)
  {
    return {storage, 0};
  }

  /// \brief Return a major end-iterator to the given tensor
  template <class M, class S>
  static MajorIterator<typename M::extents_type,S> end (tag::left_t, const M& mapping, const S& storage)
  {
    using size_type = typename M::extents_type::size_type;
    return {storage, size_type(storage.size())};
  }
};

} // end namespace Dune::Tensor
