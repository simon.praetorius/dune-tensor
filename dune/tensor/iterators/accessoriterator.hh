#pragma once

#include <iterator>

namespace Dune::Tensor {

template <class Accessor>
class AccessorIterator
{
  using self_type = AccessorIterator;
  using accessor_type = Accessor;
  using data_handle_type = typename accessor_type::data_handle_type;

public:
  using iterator_category = std::random_access_iterator_tag;
  using difference_type = std::ptrdiff_t;
  using reference = typename accessor_type::reference;
  using pointer = std::add_pointer_t<reference>;
  using value_type = std::decay_t<reference>;

  constexpr AccessorIterator () = default;

  constexpr AccessorIterator (data_handle_type p, const accessor_type& a, difference_type i) noexcept
    : data_handle_(p)
    , accessor_(a)
    , index_(i)
  {}

  constexpr reference operator* () const noexcept { return accessor_.access(data_handle_, index_); }
  constexpr reference operator[] (difference_type n) const noexcept { return *((*this)+n); }
  constexpr pointer   operator-> () const noexcept { return &(*(*this)); }

  constexpr self_type& operator++ () noexcept { ++index_; return *this; }
  constexpr self_type  operator++ (int) noexcept { self_type tmp{*this}; ++(*this); return tmp; }
  constexpr self_type& operator-- () noexcept { --index_; return *this; }
  constexpr self_type  operator-- (int) noexcept { self_type tmp{*this}; --(*this); return tmp; }
  constexpr self_type& operator+= (difference_type n) noexcept { index_ += n; return *this; }
  constexpr self_type& operator-= (difference_type n) noexcept { index_ -= n; return *this; }

  friend constexpr self_type operator+ (self_type lhs, difference_type n) noexcept { return lhs += n; }
  friend constexpr self_type operator+ (difference_type n, self_type rhs) noexcept { return rhs += n; }
  friend constexpr self_type operator- (self_type lhs, difference_type n) noexcept { return lhs -= n; }
  friend constexpr difference_type operator- (self_type lhs, self_type rhs) noexcept { return lhs.index_ - rhs.index_; }

  bool operator== (const self_type& other) const { return index_ == other.index_; }

private:
  data_handle_type data_handle_;
  accessor_type accessor_;
  difference_type index_ = 0;
};

} // end namespace Dune::Tensor
