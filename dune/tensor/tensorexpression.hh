#pragma once

#include <array>
#include <tuple>
#include <type_traits>

#include <dune/common/ftraits.hh>
#include <dune/common/std/layout_right.hh>

#include <dune/tensor/tensorspan.hh>
#include <dune/tensor/containers/traits.hh>
#include <dune/tensor/extents/traits.hh>
#include <dune/tensor/ranges/rangeexpression.hh>
#include <dune/tensor/utility/bracketproxy.hh>
#include <dune/tensor/utility/typetraits.hh>

namespace Dune::Tensor {

// forward declaration
template <class Operation, class... Tensors>
class TensorExpression;

template <class Element, class Extents, class Layout, class Accessor>
class TensorSpan;

template <class Tensor, class View>
class TensorView;

template <class Value, class Extents, class Layout, class Container>
class DenseTensor;


namespace Impl {

// Wrap a tensor into a span or use the expression/view/span directly
template <class T>
struct SpanOrExpr
{
  using type = TensorSpan<const typename T::value_type,
    typename T::extents_type,
    typename T::layout_type>;
};

template <class Op, class... T>
struct SpanOrExpr<TensorExpression<Op,T...>> { using type = TensorExpression<Op,T...>; };

template <class T, class V>
struct SpanOrExpr<TensorView<T,V>> { using type = TensorView<T,V>; };

template <class V, class E, class L, class A>
struct SpanOrExpr<TensorSpan<V,E,L,A>> { using type = TensorSpan<V,E,L,A>; };

} // end namespace Impl


/**
 * \brief An expression-template involving multiple tensors.
 * \ingroup Tensors
 * \nosubgrouping
 *
 * \tparam Operation  An n-aray functor applied to the values of the n tensors
 * \tparam Tensors... The n tensors to be combined by the `Operation`
 *
 * \par Requirements
 * - All tensors must have the same rank
 * - All tensor extents must be compatible, i.e., have the same value but could
 *   be different in static property
 **/
template <class Operation, class... Tensors>
class TensorExpression
{
  template <class T>
  using span_t = typename Impl::SpanOrExpr<T>::type;
  using span_tuple_type = std::tuple<span_t<Tensors>...>;

  // All tensors must have the same rank
  using Tensor0 = First_t<Tensors...>;
  static_assert((... && (Tensor0::rank() == Tensors::rank())));

public:
  using op_type = Operation;
  using extents_type = typename Impl::ExtentsTraits<
    typename Tensors::extents_type...>::type;
  using size_type = typename extents_type::size_type;
  using rank_type = typename extents_type::rank_type;
  using index_type = typename extents_type::index_type;

  using reference = std::invoke_result_t<op_type,
    typename Tensors::reference...>;
  using pointer = std::add_pointer_t<std::remove_reference_t<reference>>;
  using value_type = std::decay_t<reference>;
  using field_type = typename Dune::FieldTraits<value_type>::field_type;

public:
  /// \name TensorExpression constructors
  /// @{

  /// \brief Constructor stores the operation by value and all tensors in a span
  constexpr TensorExpression (op_type op, const Tensors&... tensors)
    : op_{std::move(op)}
    , tensors_{tensors...}
    , extents_{first(tensors...).extents()}
  {
#ifndef NDEBUG
    assertSize(tensors...);
#endif
  }

  /// @}

  /// \name Multi index access
  /// @{

  /// \brief Access specified element at position i0,i1,...
  template <class... Indices,
    std::enable_if_t<(... && std::is_convertible_v<Indices,index_type>), int> = 0>
  constexpr reference operator() (const Indices&... indices) const noexcept
  {
    auto eval = [&](auto&& tensor) { return tensor(indices...); };
    return std::apply([&](auto&&... tensors) -> reference {
      return op_(eval(tensors)...); }, tensors_);
  }

  /// \brief Access specified element at position [{i0,i1,...}]
  template <class IndexType, class E = extents_type,
    std::enable_if_t<std::is_convertible_v<IndexType,index_type>, int> = 0>
  constexpr reference operator[] (const std::array<IndexType, E::rank()>& indices) const noexcept
  {
    return std::apply([&](auto&&... tensors) -> reference {
      return op_(tensors[indices]...); }, tensors_);
  }

  /// \brief Access specified element at position [i0][i1]...
#ifndef DOXYGEN
  template <class E = extents_type,
    std::enable_if_t<(E::rank() >= 2), int> = 0>
#endif
  constexpr auto operator[] (index_type i0) const noexcept
  {
    return BracketProxy<TensorExpression const,2,E::rank()>{this, i0};
  }

#ifndef DOXYGEN
  /// \brief Access specified element at position [i0]
  template <class E = extents_type,
    std::enable_if_t<(E::rank() == 1), int> = 0>
  constexpr reference operator[] (index_type i0) const noexcept
  {
    return std::apply([&](auto&&... tensors) -> reference {
      return op_(tensors(i0)...); }, tensors_);
  }
#endif

  /// @}

  /// \brief Generate a range expressions using the value range of the tensors
  /**
   * \note Requires that all tensors have the same layout.
   */
  auto values () const noexcept
  {
    return std::apply([&](auto&&... tensors) {
      return RangeExpression{op_, tensors.values()...}; }, tensors_);
  }

  /// \brief Generate a DenseTensor from the expression
  template <class E = extents_type>
  auto store () const noexcept
  {
    using V = value_type;
    using L = Std::layout_right;
    using C = CommonContainer<TensorExpression>;
    return DenseTensor<V,E,L,C>{*this};
  }

  /// \brief Value mapping
  constexpr const op_type& op () const noexcept { return op_; }

  /// \name Size information
  /// @{

  /// \brief Number of elements in all dimensions of the tensor, \related Extents
  constexpr const extents_type& extents () const noexcept { return extents_; }

  /// \brief Number of dimensions of the tensor
  static constexpr rank_type rank () noexcept
  {
    return extents_type::rank();
  }

  /// \brief Number of elements in the r'th dimension of the tensor
  static constexpr std::size_t static_extent (rank_type r) noexcept
  {
    return extents_type::static_extent(r);
  }

  /// \brief Number of elements in the r'th dimension of the tensor
  constexpr index_type extent (rank_type r) const noexcept { return extents_.extent(r); }

  /// \brief Total number of elements in the tensor
  constexpr size_type size () const noexcept { return std::get<0>(tensors_).size(); }

  /// @}

private:
  template <class T0, class... T>
  static void assertSize(const T0& tensor0, const T&... tensors)
  {
    assert((... && (tensor0.size() == tensors.size())));
    // assert((... && (tensor0.mapping() == tensors.mapping())));
    for (rank_type r = 0; r < extents_type::rank(); ++r)
      assert((... && (tensor0.extent(r) == tensors.extent(r))));
  }

private:
  op_type op_;
  span_tuple_type tensors_;
  extents_type extents_;
};

} // end namespace Dune::Tensor


template <class Op, class... T>
struct Dune::FieldTraits< Dune::Tensor::TensorExpression<Op,T...> >
{
  using value_type = typename Dune::Tensor::TensorExpression<Op,T...>::value_type;
  using field_type = typename FieldTraits<value_type>::field_type;
  using real_type = typename FieldTraits<field_type>::real_type;
};
