#pragma once

#include <dune/tensor/densetensor.hh>
#include <dune/tensor/tags.hh>
#include <dune/tensor/extents/tensortype.hh>

namespace Dune::Tensor {

template <class Value, class IndexType, int... exts, class Layout, class Container>
class DenseTensor<Value, TensorType<IndexType,exts...>, Layout, Container>
  : public DenseTensor<Value, typename TensorType<IndexType,exts...>::extents_type, Layout, Container>
{
  using base_type = DenseTensor<Value, typename TensorType<IndexType,exts...>::extents_type, Layout, Container>;

public:
  using types_type = TensorType<IndexType,exts...>;
  using rank_type = typename types_type::rank_type;


  /// \name DenseTensor constructors
  /// @{

  /// \brief Construct from the extents of the tensor and a default value
  constexpr DenseTensor (const types_type& t) noexcept
    : DenseTensor{mapping_type{t.to_extents()}, t}
  {}

  /// \brief Construct from the mapping of the tensor and a default value
  constexpr DenseTensor (const mapping_type& m, const types_type& t) noexcept
    : base_type{m, container_type(m.required_span_size())}
    , types_{t}
  {}

  /// \brief Construct from the extents of the tensor and a default value
  constexpr DenseTensor (const types_type& t, const value_type& value) noexcept
    : DenseTensor{mapping_type{t.to_extents()}, t, value}
  {}

  /// \brief Construct from the mapping of the tensor and a default value
  constexpr DenseTensor (const mapping_type& m, const types_type& t, const value_type& value) noexcept
    : base_type{m, container_type(m.required_span_size(), value)}
    , types_{t}
  {}


  // -------------------------------------
  // constructors from an initializer list

  /// \brief Constructor from a brace-init list of values
  template <class... IndexTypes,
    std::enable_if_t<(... && std::is_convertible_v<IndexTypes,int>), int> = 0,
    std::enable_if_t<std::is_constructible_v<types_type,SizeTypes...>, int> = 0>
  constexpr DenseTensor (NestedInitializerList_t<value_type,extents_type::rank()> init,
                         SizeTypes... exts) noexcept
    : DenseTensor{init, types_type{exts...}}
  {}

  /// \brief Constructor from a brace-init list of values
  template <class C = container_type,
    std::enable_if_t<std::is_default_constructible_v<C>, int> = 0>
  constexpr DenseTensor (NestedInitializerList_t<value_type,extents_type::rank()> init,
                         const types_type& t) noexcept
    : DenseTensor{mapping_type{t.to_extents()}, t}
  {
    auto it = Super::container().begin();
    InitializerList<value_type,extents_type>::apply(init,Super::extents(),
      [&it](value_type value) { *it++ = value; });
  }


  // ----------------------------
  // constructors from a range

  /// \brief Construct from another range
  template <class R, class E = extents_type,
    std::enable_if_t<std::is_default_constructible_v<E>, int> = 0,
    std::enable_if_t<IsRange_v<R, value_type>, int> = 0>
  constexpr DenseTensor (tag::from_range_t, R&& rg) noexcept
    : DenseTensor{tag::from_range, std::forward<R>(rg), extents_type{}}
  {}

  /// \brief Construct from another range with individual extents
  template <class R, class... IndexTypes,
    std::enable_if_t<IsRange_v<R, value_type>, int> = 0,
    std::enable_if_t<(... && std::is_convertible_v<IndexTypes,int>), int> = 0,
    std::enable_if_t<std::is_constructible_v<types_type,IndexTypes...>, int> = 0>
  constexpr DenseTensor (tag::from_range_t, R&& rg, IndexTypes... exts) noexcept
    : DenseTensor{tag::from_range, std::forward<R>(rg), types_type{exts...}}
  {}

  /// \brief Constructor from another range
  template <class R,
    std::enable_if_t<IsRange_v<R, value_type>, int> = 0>
  constexpr DenseTensor (tag::from_range_t, R&& rg, const types_type& t) noexcept
    : base_type{mapping_type{t.to_extents()}, container_type{tag::from_range, std::begin(rg), std::end(rg)}}
    , types_{t}
  {}

  /// \brief Construct the tensor from an expression
  template <class Op, class... T>
  constexpr DenseTensor (const TensorExpression<Op,T...>& expr) noexcept
    : DenseTensor{tag::from_range, expr.values(), types_type{expr.extents()}}
  {}

  /// @}

  using base_type::base_type;

  /// \brief Number of elements in all dimensions of the tensor, \related Extents
  constexpr const types_type& types () const noexcept { return types_; }

  /// \brief Number of elements in the r'th dimension of the tensor
  static constexpr int static_type (rank_type r) noexcept
  {
    return types_type::static_type(r);
  }

  /// \brief Number of elements in the r'th dimension of the tensor
  constexpr int types (rank_type r) const noexcept { return types().type(r); }

private:
  types_type types_;
};

} // end namespace Dune::Tensor

