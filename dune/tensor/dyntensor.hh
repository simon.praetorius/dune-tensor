#pragma once

#include <type_traits>

#include <dune/common/std/extents.hh>
#include <dune/common/std/layout_right.hh>
#include <dune/tensor/densetensor.hh>
#include <dune/tensor/extents/traits.hh>
#include <dune/tensor/containers/dynamicsizecontainer.hh>

namespace Dune::Tensor {

/**
 * \brief A tensor of arbitrary rank and individual dynamic dimension extents
 * storing field values in a container of dynamic size.
 * \ingroup Tensors
 * \nosubgrouping
 *
 * This is a specialization of a `DenseTensor` for purely dynamic extents, a
 * layout `Std::layout_right` ordering and a storage that allocates space dynamically.
 *
 * \tparam Value  The element type stored in the tensor
 * \tparam rank   The tensorial rank, must be a non-negative integer.
 **/
template <class Value, std::size_t rank>
class DynamicTensor
    : public DenseTensor<Value, Std::dextents<int,rank>, Std::layout_right, DynamicSizeContainer>
{
  using self_type = DynamicTensor;
  using base_type = DenseTensor<Value, Std::dextents<int,rank>, Std::layout_right, DynamicSizeContainer>;

public:
  using value_type = typename base_type::value_type;
  using extents_type = typename base_type::extents_type;
  using index_type = typename base_type::index_type;
  using mapping_type = typename base_type::mapping_type;

  /// \name Additional DynamicTensor constructors
  /// @{

  constexpr DynamicTensor () = default;

  /// \brief Construct from the individual extents; sets all entries to zero
  template <class... IndexTypes,
    std::enable_if_t<(sizeof...(IndexTypes) == rank), int> = 0,
    std::enable_if_t<(... && std::is_convertible_v<IndexTypes,index_type>), int> = 0,
    std::enable_if_t<std::is_constructible_v<extents_type,IndexTypes...>, int> = 0>
  explicit constexpr DynamicTensor (IndexTypes... exts)
    : base_type{extents_type(std::move(exts)...), value_type(0)}
  {}

  /// \brief Constructor from extents; sets all entries to zero
  constexpr DynamicTensor (const extents_type& e)
    : base_type{e, value_type(0)}
  {}

  /// \brief Construct from the individual extents and an initial value
  template <class... IndexTypes, class V,
    std::enable_if_t<(sizeof...(IndexTypes) == rank), int> = 0,
    std::enable_if_t<(... && std::is_convertible_v<IndexTypes,index_type>), int> = 0,
    std::enable_if_t<std::is_constructible_v<extents_type,IndexTypes...>, int> = 0,
    std::enable_if_t<std::is_convertible_v<V,value_type>, int> = 0>
  explicit constexpr DynamicTensor (IndexTypes... exts, const V& value)
    : base_type{extents_type(std::move(exts)...), value}
  {}

  /// \brief Constructor from extents and initial value
  template <class V,
    std::enable_if_t<std::is_convertible_v<V,value_type>, int> = 0>
  explicit constexpr DynamicTensor (const extents_type& e, const V& value)
    : base_type{e, value}
  {}

  /// \brief Constructor from extents and brace-init list of values
  constexpr DynamicTensor (const extents_type& e, NestedInitializerList_t<value_type,extents_type::rank()> init)
    : base_type{e, init}
  {}

  constexpr DynamicTensor (const base_type& base)
    : base_type{base}
  {}

  constexpr DynamicTensor (base_type&& base)
    : base_type{std::move(base)}
  {}

  /// @}


  /// \name Assignment operators
  /// @{

  /// \brief Copy assignment operator
  constexpr self_type& operator= (const self_type&) = default;

  /// \brief Copy assignment from a different DenseTensor
  template <class V, class E, class L, class C,
    std::enable_if_t<std::is_constructible_v<base_type, DenseTensor<V,E,L,C>>, int> = 0>
  constexpr self_type& operator= (const DenseTensor<V,E,L,C>& other)
  {
    return *this = base_type(other);
  }

  using base_type::operator=;

  /// @}


  /// \name Multi index access
  /// @{

  /**
   * \brief Nested subscript operator to access either a subdimension or an element.
   *
   * The bracket operator can be nested to successively access more dimensions.
   * Each subscript operator returns an mdspan over the sub-tensor with fixed
   * first index.
   *
   * \b Examples:
   * \code{c++}
     DynamicTensor<double,2> matrix(3,3);
     auto row = matrix[0]; // 0th row
     row[1] = 56;          // the row is an mdspan
     matrix[0][1] = 42;    // element at (0,1)
     matrix[0,1] = 7.0;    // only with c++23
     \endcode
   **/
  using base_type::operator[];

  /**
   * \brief Access an element of the tensor using a variadic list of indices.
   * \b Examples:
   * \code{c++}
     DynamicTensor<double,3> tensor(3,3,3);
     tensor(0,1,2) = 42.0;
     \endcode
   **/
  using base_type::operator();

  /// @}


  /// \name Modifiers
  /// @{

  /// \brief Change the extents of the tensor and resize the underlying container with given default value
  template <class V,
    std::enable_if_t<std::is_convertible_v<V,value_type>, int> = 0>
  void resize (const extents_type& e, const V& v)
  {
    *this = [](self_type&& self, const mapping_type& m, const value_type& value) {
      auto container = std::move(self).extract_container();
      container.resize(m.required_span_size(), value);
      return base_type{m, std::move(container)};
    }(std::move(*this), mapping_type(e), v);
  }

  /// \brief Change the extents of the tensor and resize the underlying container
  void resize (const extents_type& e)
  {
    resize(e, value_type(0));
  }

  /// \brief Change the extents of the tensor by the given individual extents
  template <class... IndexTypes, class V,
    std::enable_if_t<(sizeof...(IndexTypes) == rank), int> = 0,
    std::enable_if_t<(... && std::is_convertible_v<IndexTypes,index_type>), int> = 0,
    std::enable_if_t<std::is_constructible_v<extents_type,IndexTypes...>, int> = 0,
    std::enable_if_t<std::is_convertible_v<V,value_type>, int> = 0>
  void resize (IndexTypes... exts, const V& v)
  {
    resize(extents_type{exts...}, v);
  }

  /// \brief Change the extents of the tensor by the given individual extents
  template <class... IndexTypes,
    std::enable_if_t<(sizeof...(IndexTypes) == rank), int> = 0,
    std::enable_if_t<(... && std::is_convertible_v<IndexTypes,index_type>), int> = 0,
    std::enable_if_t<std::is_constructible_v<extents_type,IndexTypes...>, int> = 0>
  void resize (IndexTypes... exts)
  {
    resize(extents_type{exts...}, value_type(0));
  }

  /// @}
};

/// \brief A dynamic vector is a DynamicTensor of rank 1
template <class Value>
using DynamicVector = DynamicTensor<Value,1>;

/// \brief A dynamic matrix is a DynamicTensor of rank 2
template <class Value>
using DynamicMatrix = DynamicTensor<Value,2>;

} // end namespace Dune::Tensor


template <class V, int rank>
struct Dune::FieldTraits< Dune::Tensor::DynamicTensor<V,rank> >
{
  using field_type = typename FieldTraits<V>::field_type;
  using real_type = typename FieldTraits<V>::real_type;
};
