#pragma once

#include <dune/common/std/extents.hh>

#include <dune/tensor/sparsetensor.hh>
#include <dune/tensor/containers/capacitystorage.hh>
#include <dune/tensor/inserters/sparseinserter.hh>
#include <dune/tensor/iterators/csfiterators.hh>
#include <dune/tensor/layouts/csflayout.hh>

namespace Dune::Tensor {

struct CSFTraits
{
  template <class T>
  using Inserter = SparseInserter<T>;

  using Iterators = CSFIterators;
};


/**
 * \brief A sparse tensor with CSF storage.
 * \ingroup Tensors
 * \nosubgrouping
 *
 * \tparam Value    The element type stored in the tensor.
 * \tparam Extents  The type representing the tensor extents. Must be compatible
 *                  to `Dune::Tensor::Extents`.
 * \tparam capacity  A static bound for the number of nonzeros or `Std::dynamic_extent`.
 **/
template <class Value, class Extents, std::size_t capacity = Std::dynamic_extent>
using CSFTensor
  = SparseTensor<Value,Extents,CSFLayout<capacity>,CapacityContainer<capacity>,CSFTraits>;


/// \brief A sparse tensor with static extents
template <class Value, int... exts>
using CSFFieldTensor
  = CSFTensor<Value, Std::extents<int, std::size_t(exts)...>>;

/// \brief A sparse tensor with dynamic extents
template <class Value, std::size_t rank>
using CSFDynamicTensor
  = CSFTensor<Value, Std::dextents<int, rank>>;


/// Specialization of DerivativeTraits for SparseTensor valued maps
/// @{
template <class V, class S, std::size_t... exts, std::size_t capacity, class K, int dim>
struct DerivativeTraits<SparseTensor<V,Std::extents<S,exts...>,CSFLayout<capacity>,CapacityContainer<capacity>,CSFTraits>(Dune::FieldVector<K,dim>)>
{
  static constexpr std::size_t newCapacity = (capacity == Std::dynamic_extent ? Std::dynamic_extent : capacity*dim);
  using Range = SparseTensor<V,Std::extents<S,exts...,dim>,CSFLayout<newCapacity>,CapacityContainer<newCapacity>,CSFTraits>;
};

template <class V, class S, std::size_t... exts, std::size_t capacity, class K, int dim>
struct DerivativeTraits<SparseTensor<V,Std::extents<S,exts...>,CSFLayout<capacity>,CapacityContainer<capacity>,CSFTraits>(FieldTensor<K,dim>)>
{
  static constexpr std::size_t newCapacity = (capacity == Std::dynamic_extent ? Std::dynamic_extent : capacity*dim);
  using Range = SparseTensor<V,Std::extents<S,exts...,dim>,CSFLayout<newCapacity>,CapacityContainer<newCapacity>,CSFTraits>;
};
/// @}

} // end namespace Dune::Tensor
