#pragma once

#include <type_traits>
#include <dune/common/typeutilities.hh>

namespace Dune::Tensor {

/**
 * \brief Conditional type trait with delayed instantiation
 *
 * Type trait like `std::conditional`, but instantiates the template `T1` or `T2`
 * with `Arg` only if the condition is `true` or `false`, respectively.
 **/
template <bool /*true*/, class Arg, template<class> class T1, template<class> class T2>
struct ConditionalDelay
{
  using type = T1<Arg>;
};

#ifndef DOXYGEN
template <class Arg, template<class> class T1, template<class> class T2>
struct ConditionalDelay<false,Arg,T1,T2>
{
  using type = T2<Arg>;
};
#endif

/// \related ConditionalDelay
template <bool condition, class Arg, template<class> class T1, template<class> class T2>
using ConditionalDelay_t = typename ConditionalDelay<condition,Arg,T1,T2>::type;


template <class Derived, class Base>
struct IsDerivedFrom : std::conjunction<
  std::is_base_of<Base, Derived>,
  std::is_convertible<const volatile Derived*, const volatile Base*>> {};


template <class Derived, class Base>
inline constexpr bool DerivedFrom = IsDerivedFrom<Derived,Base>::value;

template <class... Ts>
struct First;

template <class T0, class... Ts>
struct First<T0,Ts...>
{
  using type = T0;
};

template <class... Ts>
using First_t = typename First<Ts...>::type;

template <class T0, class... Ts>
const T0& first (const T0& t0, const Ts&... /*ts*/)
{
  return t0;
}

namespace Impl {

template <class Container>
auto reserveImpl (Container& container, std::size_t size, Dune::PriorityTag<2>)
  -> std::void_t<decltype(container.reserve(size))>
{
  container.reserve(size);
}

template <class Container>
void reserveImpl (Container& container, std::size_t size, Dune::PriorityTag<1>)
{}

} // end namespace Impl

template <class Container>
void reserve (Container& container, std::size_t size)
{
  Impl::reserveImpl(container,size, Dune::PriorityTag<5>{});
}

} // end namespace Dune::Tensor
