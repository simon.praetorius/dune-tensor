#pragma once

#include <cmath>
#include <complex>

namespace Dune::Tensor {

struct Identity
{
  template <class T>
  inline __attribute__((always_inline))
  T& operator() (T& arg) const { return arg; }
};

struct Conjugate
{
  template <class T>
  inline __attribute__((always_inline))
  T operator() (const T& arg) const { using std::conj; return conj(arg); }
};

} // end namespace Dune::Tensor
