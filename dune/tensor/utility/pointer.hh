#pragma once

#include <type_traits>

namespace Dune::Tensor {

/**
 * \brief Proxy to wrap a reference in a pointer-like interface
 **/
template <class Ref>
class Pointer
{
  using raw_type = std::remove_reference_t<Ref>;

public:
  using reference = Ref;
  using pointer = raw_type*;
  using const_reference = const raw_type&;
  using const_pointer = const raw_type*;

  template <class S = raw_type,
    std::enable_if_t<not std::is_const_v<S>, int> = 0>
  pointer operator-> () { return &value_; }

  const_pointer operator-> () const { return &value_; }

  reference value_;
};

} // end namespace Dune::Tensor
