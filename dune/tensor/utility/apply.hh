#pragma once

#include <dune/common/indices.hh>

namespace Dune::Tensor {
namespace Impl {

template<class F, std::size_t... i>
inline __attribute__((always_inline))
constexpr decltype(auto) applyIndexHelper(F&& f, std::index_sequence<i...>)
{
  return f(index_constant<i>{}...);
}

} // end namespace Impl

template<std::size_t N, class F>
inline __attribute__((always_inline))
constexpr decltype(auto) applyIndex(F&& f, index_constant<N> = {})
{
  return Impl::applyIndexHelper(std::forward<F>(f),
    std::make_index_sequence<N>{});
}

} // end namespace Dune::Tensor
