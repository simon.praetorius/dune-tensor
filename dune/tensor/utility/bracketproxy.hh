#pragma once

#include <array>
#include <tuple>
#include <type_traits>

#include <dune/common/indices.hh>
#include <dune/tensor/utility/array.hh>

namespace Dune::Tensor {

/// \brief A proxy class to allow multi-dimensional array access `[i][j][k]...`
template <class Tensor, std::size_t size, std::size_t rank>
struct BracketProxy
{
  using tensor_type = std::remove_cv_t<Tensor>;
  using size_type = typename tensor_type::size_type;
  using index_type = typename tensor_type::index_type;

  BracketProxy (Tensor* tensor) noexcept
    : tensor_{tensor}
    , indices_{}
  {}

  BracketProxy (Tensor* tensor, index_type i) noexcept
    : tensor_{tensor}
    , indices_{i}
  {}

  BracketProxy (Tensor* tensor, const std::array<index_type,size-1>& indices) noexcept
    : tensor_{tensor}
    , indices_{indices}
  {}

  template <class T = Tensor, std::enable_if_t<not std::is_const_v<T>, int> = 0>
  decltype(auto) operator[] (index_type i) noexcept
  {
    auto indices = push_back(indices_,i);
    if constexpr(size == rank)
      return (*tensor_)[indices];
    else
      return BracketProxy<Tensor,size+1,rank>{tensor_,indices};
  }

  decltype(auto) operator[] (index_type i) const noexcept
  {
    auto indices = push_back(indices_,i);
    if constexpr(size == rank)
      return (*tensor_)[indices];
    else
      return BracketProxy<Tensor,size+1,rank>{tensor_,indices};
  }

  Tensor* tensor_;
  std::array<index_type,size-1> indices_;
};

#ifndef DOXYGEN
// Specialization for matrices (2-tensors)
template <class Tensor>
struct BracketProxy<Tensor,2,2>
{
  using tensor_type = std::remove_cv_t<Tensor>;
  using size_type = typename tensor_type::size_type;
  using index_type = typename tensor_type::index_type;

  BracketProxy (Tensor* tensor, index_type i0) noexcept
    : tensor_{tensor}
    , i0_{i0}
  {}

  template <class T = Tensor, std::enable_if_t<not std::is_const_v<T>, int> = 0>
  decltype(auto) operator[] (index_type i1) noexcept
  {
    return (*tensor_)(i0_, i1);
  }

  decltype(auto) operator[] (index_type i1) const noexcept
  {
    return (*tensor_)(i0_, i1);
  }

  Tensor* tensor_;
  index_type i0_;
};
#endif // DOXYGEN

} // end namespace Dune::Tensor
