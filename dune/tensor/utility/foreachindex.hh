#pragma once

#include <array>
#include <functional>
#include <type_traits>
#include <utility>

#include <dune/common/std/functional.hh>
#include <dune/tensor/tags.hh>

namespace Dune::Tensor {
namespace Impl {

template <class Extents, class Fun, class Order, class... Indices>
inline __attribute__((always_inline))
void forEachIndexImpl (const Extents& extents, Fun f, Order order, Indices... ii)
{
  constexpr typename Extents::rank_type pos = sizeof...(Indices);
  if constexpr(pos < Extents::rank()) {
    if constexpr(std::is_same_v<Order,tag::left_t>) {
      if constexpr(Extents::static_extent(pos) == Std::dynamic_extent)
        for (typename Extents::index_type i = 0; i < extents.extent(pos); ++i)
          forEachIndexImpl(extents, std::move(f), order, ii...,i);
      else
        for (std::size_t i = 0; i < Extents::static_extent(pos); ++i)
          forEachIndexImpl(extents, std::move(f), order, ii...,typename Extents::index_type(i));
    }
    else if constexpr(std::is_same_v<Order,tag::right_t>) {
      if constexpr(Extents::static_extent(pos) == Std::dynamic_extent)
        for (typename Extents::index_type i = 0; i < extents.extent(Extents::rank()-pos-1); ++i)
          forEachIndexImpl(extents, std::move(f), order, i,ii...);
      else
        for (std::size_t i = 0; i < Extents::static_extent(Extents::rank()-pos-1); ++i)
          forEachIndexImpl(extents, std::move(f), order, typename Extents::index_type(i),ii...);
    }
  }
  else {
    using I = std::array<typename Extents::index_type,Extents::rank()>;
    std::invoke(f, I{ii...});
  }
}

} // end namespace Impl


/// \brief Invoke the function `f` on all index-tuples in the multi dimensional index-space given by `extents`.
template <class Extents, class Fun, class Order = tag::left_t,
  class I = std::array<typename Extents::index_type,Extents::rank()>,
  std::enable_if_t<std::is_invocable_v<Fun,I>, int> = 0>
inline __attribute__((always_inline))
void forEachIndex (const Extents& extents, Fun f, Order order = {})
{
  static_assert(std::is_same_v<Order,tag::left_t> || std::is_same_v<Order,tag::right_t>);
  Impl::forEachIndexImpl(extents, std::move(f), order);
}

} // end namespace Dune::Tensor
