#pragma once

#include <type_traits>

#include <dune/tensor/tags.hh>

namespace Dune::Tensor {
namespace Impl {

constexpr inline __attribute__((always_inline))
std::size_t abs_or_dynamic_extent (int a) { return a < 0 && a != tag::dynamic ? -a : a; }

template <class Int,
  std::enable_if_t<std::is_integral_v<Int>, int> = 0>
constexpr inline __attribute__((always_inline))
Int abs (Int a)
{
  if constexpr(std::is_signed_v<Int>)
    return a < 0 ? -a : a;
  else
    return a;
}

} // end namespace Impl
} // end namespace Tensor
