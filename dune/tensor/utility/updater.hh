#pragma once

#include <cmath>
#include <complex>

namespace Dune::Tensor {

/// \file Functors representing the multiply-add function c += a * b
/// in various forms, e.g. compound assignment, `std::fma` and with conjugation

struct MultAdd
{
  template <class A, class B>
  using result_t = std::decay_t<decltype(std::declval<A>() * std::declval<B>())>;

  template <class A, class B, class C>
  constexpr inline __attribute__((always_inline))
  void operator() (const A& a, const B& b, C& c) const noexcept
  {
    c += a * b;
  }
};

struct FusedMultAdd
{
  template <class A, class B>
  using result_t = std::common_type_t<A,B>;

  template <class A, class B, class C>
  inline __attribute__((always_inline))
  void operator() (const A& a, const B& b, C& c) const noexcept
  {
    using std::fma;
    c = fma(a,b,c);
  }
};

struct ConjMultAdd
{
  template <class A, class B>
  using result_t = std::decay_t<decltype(std::declval<A>() * std::declval<B>())>;

  template <class A, class B, class C>
  constexpr inline __attribute__((always_inline))
  void operator() (const A& a, const B& b, C& c) const noexcept
  {
    c += a * b;
  }

  template <class A, class B, class C>
  constexpr inline __attribute__((always_inline))
  void operator() (const A& a, const std::complex<B>& b, C& c) const noexcept
  {
    using std::conj;
    c += a * conj(b);
  }
};

} // end namespace Dune::Tensor
