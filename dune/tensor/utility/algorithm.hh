#pragma once

#include <array>
#include <functional>
#include <type_traits>
#include <utility>

#include <dune/common/std/functional.hh>
#include <dune/tensor/tags.hh>
#include <dune/tensor/utility/foreachindex.hh>

namespace Dune::Tensor {

/**
 * \brief Invoke the function `f` on all elements of the tensor.
 *
 * Traverse the in the given order (`tag::left_major` or `tag::right_major`) and
 * apply the functor `f` on the (possibly projected) elements of the tensor. If
 * the functor `f` accepts an element and an index array, pass both, otherwise
 * just the value.
 *
 * \b Requirements:
 * Let `f` be a functor of type `Fun` and `p` the projection of an element of
 * the tensor with type `std::invoke_result_t<Proj,Tensor::[const_]reference>`.
 * Also let `i` be an array of type `std::array<Tensor::index_type, Tensor::rank()>`
 * representing the index-tuple of the visited element. Then:
 *
 * - Either `f(p, i)` is a valid expression,
 * - or `f(p)` is a valid expression.
 **/
template <class Tensor, class Fun, class Proj = Std::identity, class Order = tag::left_t,
  class T = std::remove_reference_t<Tensor>,
  class I = std::array<typename T::index_type,T::rank()>,
  class V = std::invoke_result_t<Proj,decltype(std::declval<Tensor>()[std::declval<I>()])>,
  std::enable_if_t<(std::is_invocable_v<Fun,V,I> || std::is_invocable_v<Fun,V>), int> = 0>
void tensorForEach (Tensor&& tensor, Fun f, Proj proj = {}, Order order = {})
{
  forEachIndex(tensor.extents(), [&](auto&& index) {
    if constexpr(std::is_invocable_v<Fun,V,I>)
      std::invoke(f, std::invoke(proj, tensor[index]), index);
    else
      std::invoke(f, std::invoke(proj, tensor[index]));
  }, order);
}

/// \brief Applies the given function `op` to the elements of a `tensor` and stores the result in another tensor
template <class Tensor, class OutTensor, class F, class Proj = Std::identity, class Order = tag::left_t,
  std::enable_if_t<(OutTensor::rank() == std::remove_reference_t<Tensor>::rank()), int> = 0,
  class I = std::array<typename OutTensor::index_type,OutTensor::rank()>,
  class V = std::invoke_result_t<Proj,decltype(std::declval<Tensor>()[std::declval<I>()])>,
  std::enable_if_t<std::is_invocable_v<F,V>, int> = 0>
void tensorTransform (Tensor&& tensor, OutTensor& result, F op, Proj proj = {}, Order order = {})
{
  assert(result.extents() == tensor.extents());
  forEachIndex(result.extents(), [&](auto&& index) {
    result[index] = std::invoke(op, std::invoke(proj, tensor[index]));
  }, order);
}

/// \brief Applies the given function `binary_op` to the elements of `tensor1` and `tensor2` and stores the result in another tensor
template <class Tensor1, class Tensor2, class OutTensor, class F,
          class Proj1 = Std::identity, class Proj2 = Std::identity, class Order = tag::left_t,
  std::enable_if_t<(OutTensor::rank() == std::remove_reference_t<Tensor1>::rank()), int> = 0,
  std::enable_if_t<(OutTensor::rank() == std::remove_reference_t<Tensor2>::rank()), int> = 0,
  class I = std::array<typename OutTensor::index_type,OutTensor::rank()>,
  class V1 = std::invoke_result_t<Proj1,decltype(std::declval<Tensor1>()[std::declval<I>()])>,
  class V2 = std::invoke_result_t<Proj2,decltype(std::declval<Tensor2>()[std::declval<I>()])>,
  std::enable_if_t<std::is_invocable_v<F,V1,V2>, int> = 0>
void tensorTransform (Tensor1&& tensor1, Tensor2&& tensor2, OutTensor& result, F binary_op,
                      Proj1 proj1 = {}, Proj2 proj2 = {}, Order order = {})
{
  assert(result.extents() == tensor1.extents());
  assert(result.extents() == tensor2.extents());
  forEachIndex(result.extents(), [&](auto&& index) {
    result[index] = std::invoke(binary_op, std::invoke(proj1, tensor1[index]), std::invoke(proj2, tensor2[index]));
  }, order);
}

} // end namespace Dune::Tensor
