#pragma once

#include <array>
#include <ostream>
#include <type_traits>
#include <utility>

#include <dune/common/reservedvector.hh>

namespace Dune::Tensor {
namespace Impl {

template<class A1, class A2, std::size_t... i, std::size_t... j>
auto concatHelper (const A1& a1, const A2& a2, std::index_sequence<i...>, std::index_sequence<j...>)
{
  using T = typename A1::value_type;
  return std::array<T, sizeof...(i)+sizeof...(j)>{a1[i]..., a2[j]...};
}

} // end namespace Impl

template <class T, std::size_t N1, std::size_t N2>
auto concat (const std::array<T,N1>& a1, const std::array<T,N2>& a2)
{
  return Impl::concatHelper(a1, a2, std::make_index_sequence<N1>{}, std::make_index_sequence<N2>{});
}

template <class T, int N1, int N2>
auto concat (ReservedVector<T,N1> a1, const ReservedVector<T,N2>& a2)
{
  assert( a1.size() + a2.size() < a1.capacity() );
  for (const auto& v : a2)
    a1.push_back(v);
  return a1;
}

template <class T, std::size_t N, class S>
auto push_front (const std::array<T,N>& a, const S& value)
{
  return concat(std::array<T,1>{T(value)}, a);
}

template <class T, int N, class S>
auto push_front (const ReservedVector<T,N>& a, const S& value)
{
  assert( a.size() + 1 < a.capacity() );
  ReservedVector<T,N> out;
  out.push_back(value);
  for (const auto& v : a)
    out.push_back(v);
  return out;
}

template <class T, std::size_t N, class S>
auto push_back (const std::array<T,N>& a, const S& value)
{
  return concat(a, std::array<T,1>{T(value)});
}

template <class T, int N, class S>
auto push_back (ReservedVector<T,N> a, const S& value)
{
  assert( a.size() + 1 < a.capacity() );
  a.push_back(value);
  return a;
}

namespace Impl {

template <class A, std::size_t i0, std::size_t... i>
auto popFrontHelper (const A& a, std::index_sequence<i0,i...>)
{
  using T = typename A::value_type;
  return std::array<T,sizeof...(i)>{a[i]...};
}

template <class A, std::size_t... i, std::size_t iN>
auto popBackHelper (const A& a, std::index_sequence<i...,iN>)
{
  using T = typename A::value_type;
  return std::array<T,sizeof...(i)>{a[i]...};
}

} // end namespace Impl

template <class T, std::size_t N>
auto pop_front (const std::array<T,N>& a)
{
  return Impl::popFrontHelper(a, std::make_index_sequence<N>{});
}

template <class T, int N>
auto pop_front (const ReservedVector<T,N>& a)
{
  assert(!a.empty());
  ReservedVector<T,N-1> out;
  for (std::size_t i = 1; i < a.size(); ++i)
    out.push_back(a[i]);
  return out;
}

template <class T, std::size_t N>
auto pop_back (const std::array<T,N>& a)
{
  return Impl::popBackHelper(a, std::make_index_sequence<N>{});
}

template <class T, int N>
auto pop_back (const ReservedVector<T,N>& a)
{
  assert(!a.empty());
  ReservedVector<T,N-1> out;
  for (std::size_t i = 0; i < a.size()-1; ++i)
    out.push_back(a[i]);
  return out;
}


template <class Index, std::size_t n,
  std::enable_if_t<std::is_integral_v<Index>, int> = 0>
std::ostream& operator<< (std::ostream& out, const std::array<Index,n>& indices)
{
  for (std::size_t i = 0; i < n; ++i)
    out << indices[i] << (i < n-1 ? "," : "");
  return out;
}

} // end namespace Dune::Tensor
