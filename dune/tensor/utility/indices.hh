#pragma once

#include <type_traits>

namespace Dune::Tensor {

#ifndef DOXYGEN
template <std::size_t I>
using Index = std::integral_constant<std::size_t,I>;

template <std::size_t... II>
using IndexSeq = std::index_sequence<II...>;

template <std::size_t N>
using MakeIndexSeq = std::make_index_sequence<N>;
#endif

} // end namespace Dune::Tensor
