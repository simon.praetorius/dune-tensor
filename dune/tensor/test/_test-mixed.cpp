#include <config.h>

#include <set>
#include <dune/common/hybridutilities.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/test/testsuite.hh>
#include <dune/tensor/densetensor.hh>
#include <dune/tensor/ftensor.hh>
#include <dune/tensor/extents/tensortype.hh>
#include <dune/tensor/operations/tensordot.hh>

using namespace Dune;
using namespace Dune::Tensor;


template <int rows, int cols>
using Matrix = DenseTensor<double, TensorType<int,rows,-cols>, LayoutRight, FixedSizeContainer>;

template <int size>
using ColumnVector = DenseTensor<double, TensorType<int,size>, LayoutRight, FixedSizeContainer>;

template <int size>
using RowVector = DenseTensor<double, TensorType<int,-size>, LayoutRight, FixedSizeContainer>;



int main(int argc, char** argv)
{
  Dune::MPIHelper::instance(argc, argv);
  Dune::TestSuite testSuite;

  { // check matrix-vector product
    std::cout << "check matrix-vector product..." << std::endl;
    Matrix<3,4> matrix(2.0);
    RowVector<3> row_vector(3.0);
    ColumnVector<4> col_vector(4.0);

    [[maybe_unused]] auto mc = dot(matrix, col_vector);
    [[maybe_unused]] auto rm = dot(row_vector, matrix);
  }

  { // check matrix-matrix products
    std::cout << "check matrix-matrix products..." << std::endl;
    Matrix<3,3> matrix2(2.0), matrix3(3.0);

    [[maybe_unused]] auto mm = dot(matrix2, matrix3);
  }

  return testSuite.exit();
}
