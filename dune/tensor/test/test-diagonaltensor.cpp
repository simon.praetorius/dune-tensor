#include <config.h>

#include <iostream>
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/hybridutilities.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/std/extents.hh>
#include <dune/common/test/testsuite.hh>

#if ENABLE_DUNE_CONCEPTS
#include <dune/concepts/common/tensor.hh>
#endif

#include <dune/tensor/diagonaltensor.hh>
// #include <dune/tensor/extents/tensortype.hh>

using namespace Dune;
using namespace Dune::Tensor;

template <class Tensor1, class Tensor2>
void checkEqual(Dune::TestSuite& testSuite, Tensor1 const& tensor1, Tensor2 const& tensor2)
{
  Dune::TestSuite subTestSuite("checkEqualValue");
  subTestSuite.require(tensor1.size() == tensor2.size());

  auto span1 = tensor1.values();
  auto span2 = tensor2.values();
  auto it1 = std::begin(span1);
  auto it2 = std::begin(span2);
  for (; it1 != std::end(span1); ++it1,++it2)
    subTestSuite.check(*it1 == *it2, std::to_string(*it1)+" == "+std::to_string(*it2));
  testSuite.subTest(subTestSuite);
}

template <class Tensor>
void checkEqualValue(Dune::TestSuite& testSuite, Tensor const& tensor, typename Tensor::value_type const& value)
{
  Dune::TestSuite subTestSuite("checkEqualValue");
  for (auto const& a : tensor.values())
    subTestSuite.check(a == value, std::to_string(a)+" == "+std::to_string(value));
  testSuite.subTest(subTestSuite);
}


template <class Tensor>
void checkConstructors(Dune::TestSuite& testSuite)
{
  Dune::TestSuite subTestSuite("checkConstructors");

  using extents_type = typename Tensor::extents_type;
  extents_type ext{Dune::filledArray<Tensor::rank()>(3)};

  // default constructor
  Tensor tensor(ext);
  checkEqualValue(subTestSuite, tensor, 0.0);

  tensor = 1.0;
  checkEqualValue(subTestSuite, tensor, 1.0);

  // constructor with a default value
  Tensor tensor1(ext);
  {
    auto ins = tensor1.inserter();
    for (int i = 0; i < ext.extent(0); ++i)
      ins.diagonal(i) = 1.0;
  }
  checkEqual(subTestSuite, tensor, tensor1);

  // copy/move constructors
  Tensor tensor2{tensor};
  checkEqual(subTestSuite, tensor,tensor2);

  Tensor tensor3 = tensor;
  checkEqual(subTestSuite, tensor,tensor3);

  Tensor tensor4{std::move(tensor2)};
  checkEqual(subTestSuite, tensor,tensor4);

  Tensor tensor5 = std::move(tensor3);
  checkEqual(subTestSuite, tensor,tensor5);

  // copy/move assignment operators
  tensor4 = tensor;
  checkEqual(subTestSuite, tensor,tensor4);

  tensor5 = std::move(tensor4);
  checkEqual(subTestSuite, tensor,tensor5);

  testSuite.subTest(subTestSuite);
}


template <class Tensor>
void checkAccess(Dune::TestSuite& testSuite)
{
  Dune::TestSuite subTestSuite("checkAccess");

  using index_type = typename Tensor::index_type;
  using extents_type = typename Tensor::extents_type;
  extents_type ext{Dune::filledArray<Tensor::rank()>(3)};

  Tensor tensor(ext);
  {
    auto ins = tensor.inserter();
    for (index_type i = 0; i < ext.extent(0); ++i)
      ins.diagonal(i) = 42.0;
  }
  checkEqualValue(subTestSuite, tensor, 42.0);

  if constexpr(Tensor::rank() == 0) {
    subTestSuite.check(tensor[std::array<index_type,0>{}] == 42.0);
    subTestSuite.check(tensor() == 42.0);
    subTestSuite.check(tensor == 42.0);
  }
  else if constexpr(Tensor::rank() == 1) {
    for (index_type i = 0; i < tensor.extent(0); ++i) {
      subTestSuite.check(tensor[std::array{i}] == 42.0);
      subTestSuite.check(tensor(i) == 42.0);
      subTestSuite.check(tensor[i] == 42.0);
    }
  }
  else if constexpr(Tensor::rank() == 2) {
    for (index_type i = 0; i < tensor.extent(0); ++i) {
      subTestSuite.check(tensor[std::array{i,i}] == 42.0);
      subTestSuite.check(tensor(i,i) == 42.0);
      subTestSuite.check(tensor[i][i] == 42.0);
    }
  }
  else if constexpr(Tensor::rank() == 3) {
    for (index_type i = 0; i < tensor.extent(0); ++i) {
      subTestSuite.check(tensor[std::array{i,i,i}] == 42.0);
      subTestSuite.check(tensor(i,i,i) == 42.0);
      subTestSuite.check(tensor[i][i][i] == 42.0);
    }
  }

  testSuite.subTest(subTestSuite);
}


int main(int argc, char** argv)
{
  MPIHelper::instance(argc, argv);

  TestSuite testSuite;

#if ENABLE_DUNE_CONCEPTS
  static_assert(Dune::Concept::Tensor<DiagonalTensor<double, Std::extents<int,3>>, 1>);
  static_assert(Dune::Concept::Tensor<DiagonalTensor<double, Std::extents<int,3,3>>, 2>);
  static_assert(Dune::Concept::Tensor<DiagonalTensor<double, Std::extents<int,3,3,3>>, 3>);
  static_assert(Dune::Concept::Tensor<DiagonalTensor<double, Std::extents<int,3,3,3,3>>, 4>);
#endif

  using Tensor1 = DiagonalTensor<double, Std::extents<int,3>>;
  using Tensor2 = DiagonalTensor<double, Std::extents<int,3,3>>;
  using Tensor3 = DiagonalTensor<double, Std::extents<int,3,3,3>>;

  checkConstructors<Tensor1>(testSuite);
  checkConstructors<Tensor2>(testSuite);
  checkConstructors<Tensor3>(testSuite);

  checkAccess<Tensor1>(testSuite);
  checkAccess<Tensor2>(testSuite);
  checkAccess<Tensor3>(testSuite);

  Tensor2 tensor2;
  for (int i = 0; i < tensor2.extent(0); ++i)
    testSuite.check(tensor2.exists(i,i), "exists(i,i)");

  for (int i = 0; i < tensor2.extent(0); ++i)
    testSuite.check(tensor2(i,i) == tensor2.diagonal(i), "diagonal(i)");

  auto diag2 = tensor2.diagonal();
  for (int i = 0; i < tensor2.extent(0); ++i)
    testSuite.check(tensor2(i,i) == diag2[i], "diagonal()[i]");

  return testSuite.exit();
}
