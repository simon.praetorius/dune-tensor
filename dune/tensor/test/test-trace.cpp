#include <config.h>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/test/testsuite.hh>
#include <dune/tensor/ftensor.hh>
#include <dune/tensor/operations/trace.hh>

int main(int argc, char** argv)
{
  using namespace Dune;
  using namespace Dune::Tensor;

  MPIHelper::instance(argc, argv);

  TestSuite testSuite;

  { // test 2d tensor
    using Tensor = FieldTensor<double,3,3>;
    Tensor tensor;
    int k = 7;
    for (int i = 0; i < 3; ++i)
      for (int j = 0; j < 3; ++j)
        tensor[i][j] = (i+j)%5 + k++;

    testSuite.check(trace(tensor) == tensor[0][0]+tensor[1][1]+tensor[2][2]);
  }


  { // test 2d tensor
    using Tensor = FieldTensor<double,3,3,3>;
    Tensor tensor;
    int l = 9;
    for (int i = 0; i < 3; ++i)
      for (int j = 0; j < 3; ++j)
        for (int k = 0; k < 3; ++k)
          tensor[i][j][k] = (i+j-2*k)%5 + l++;

    testSuite.check(trace(tensor)[0] == tensor[0][0][0]+tensor[0][1][1]+tensor[0][2][2]);
    testSuite.check(trace(tensor)[1] == tensor[1][0][0]+tensor[1][1][1]+tensor[1][2][2]);
    testSuite.check(trace(tensor)[2] == tensor[2][0][0]+tensor[2][1][1]+tensor[2][2][2]);
  }

  return testSuite.exit();
}