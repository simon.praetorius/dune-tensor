#include <config.h>

#include <iostream>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/test/testsuite.hh>

#include <dune/tensor/ftensor.hh>

using namespace Dune;
using namespace Dune::Tensor;

int main(int argc, char** argv)
{
  MPIHelper::instance(argc, argv);

  TestSuite testSuite;

  using Scalar = FieldTensor<double>;
  Scalar scalar0{};
  [[maybe_unused]] Scalar scalar1{1.0};
  [[maybe_unused]] Scalar scalar2(2.0);
  [[maybe_unused]] Scalar scalar3 = 3.0;
  [[maybe_unused]] Scalar scalar4 = {4.0};

  scalar0 = 5.0;

  double d = 6.0;
  float f = 7.0f;
  int i = 8;
  scalar0 = d;
  scalar0 = f;
  scalar0 = i;

  d = scalar0;
  f = scalar0;
  i = scalar0;

  [[maybe_unused]] double d2 = d < f ? double(scalar0) : 0.0;
  [[maybe_unused]] float f2 = d < f ? float(scalar0) : 0.0f;
  [[maybe_unused]] int i2 = d < f ? int(scalar0) : 0;

  scalar0 += 1.0;
  scalar0 += 1.0f;
  scalar0 += 1;

  return testSuite.exit();
}
