#include <config.h>

#include <set>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/std/extents.hh>
#include <dune/common/test/testsuite.hh>
#include <dune/tensor/dyntensor.hh>
#include <dune/tensor/ftensor.hh>
#include <dune/tensor/csftensor.hh>

#include "krylov.hh"

using namespace Dune;
using namespace Dune::Tensor;

template <class Matrix, class Vector, class MatExtents, class VecExtents>
void checkKrylov(Dune::TestSuite& testSuite, MatExtents mat_extents, VecExtents vec_extents)
{
  Matrix matrix(mat_extents);
  {
    auto ins = matrix.inserter();
                     ins(0,0) = 2.0; ins(0,1) = -1.0;
    ins(1,0) = -1.0; ins(1,1) = 2.0; ins(1,2) = -1.0;
    ins(2,1) = -1.0; ins(2,2) = 2.0; ins(2,3) = -1.0;
    ins(3,2) = -1.0; ins(3,3) = 2.0; ins(3,4) = -1.0;
    ins(4,3) = -1.0; ins(4,4) = 2.0; ins(4,5) = -1.0;
    ins(5,4) = -1.0; ins(5,5) = 2.0; ins(5,6) = -1.0;
    ins(6,5) = -1.0; ins(6,6) = 2.0;
  }

  // std::cout << "A = " << matrix << std::endl;

  Vector b(vec_extents, 1.0);
  std::cout << "b = " << b << std::endl;

  Vector x(vec_extents, 0.0);

  int it = Dune::Tensor::cg(matrix,x,b, 1.e-7, 10);
  std::cout << it << std::endl;
}

int main(int argc, char** argv)
{
  Dune::MPIHelper::instance(argc, argv);
  Dune::TestSuite testSuite;

  checkKrylov<DynamicTensor<double,2>,DynamicTensor<double,1>>(testSuite,
    Std::extents<int,Std::dynamic_extent,Std::dynamic_extent>{7,7},
    Std::extents<int,Std::dynamic_extent>{7});

  checkKrylov<FieldTensor<double,7,7>,FieldTensor<double,7>>(testSuite,
    Std::extents<int,7,7>{}, Std::extents<int,7>{});

  checkKrylov<CSFFieldTensor<double,7,7>,FieldTensor<double,7>>(testSuite,
    Std::extents<int,7,7>{}, Std::extents<int,7>{});
}
