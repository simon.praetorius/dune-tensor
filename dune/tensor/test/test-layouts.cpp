#include <config.h>

#include <iostream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/std/extents.hh>
#include <dune/common/std/layout_left.hh>
#include <dune/common/std/layout_right.hh>
#include <dune/common/std/layout_stride.hh>
#include <dune/common/test/testsuite.hh>
#include <dune/tensor/extents/dynamic.hh>
#include <dune/tensor/extents/static.hh>
#include <dune/tensor/layouts/concepts.hh>

int main(int argc, char** argv)
{
  using namespace Dune;
  MPIHelper::instance(argc, argv);

  TestSuite testSuite;

  static_assert(Concept::Layout<Std::layout_left, Std::extents<int,1,2,3>>);
  static_assert(Concept::Layout<Std::layout_left, Std::dextents<int,3>>);
  // static_assert(Concept::Layout<Std::layout_left, Tensor::StaticExtents<int,1,2,3>>);
  // static_assert(Concept::Layout<Std::layout_left, Tensor::DynamicExtents<int,3>>);

  static_assert(Concept::Layout<Std::layout_right, Std::extents<int,1,2,3>>);
  static_assert(Concept::Layout<Std::layout_right, Std::dextents<int,3>>);
  // static_assert(Concept::Layout<Std::layout_right, Tensor::StaticExtents<int,1,2,3>>);
  // static_assert(Concept::Layout<Std::layout_right, Tensor::DynamicExtents<int,3>>);

  static_assert(Concept::Layout<Std::layout_stride, Std::extents<int,1,2,3>>);

  return testSuite.exit();
}