#include <config.h>

#if DUNE_HAVE_CXX_STD_MDSPAN

#include <iostream>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/test/testsuite.hh>

#include <dune/tensor/ftensor.hh>
#include <dune/tensor/tensorspan.hh>
#include <dune/tensor/subtensorspan.hh>

using namespace Dune;
using namespace Dune::Tensor;

int main(int argc, char** argv)
{
  MPIHelper::instance(argc, argv);

  TestSuite testSuite;

  using Tensor2 = FieldTensor<double,3,3>;
  Tensor2 tensor2({
    {1,2,3},
    {4,5,6},
    {7,8,9}
  });

  TensorSpan tspan2{tensor2.data(), tensor2.mapping()};
  auto sub2a = subTensorSpan(tspan2,std::full_extent,std::full_extent);
  testSuite.check(sub2a(1,2) == 6, "sub2a(1,2) == 6");
  testSuite.check(sub2a(2,1) == 8, "sub2a(2,1) == 8");

  auto sub2b = subTensorSpan(tspan2,1,std::full_extent);
  testSuite.check(sub2b(0) == 4, "sub2b(0) == 4");
  testSuite.check(sub2b(1) == 5, "sub2b(1) == 5");
  testSuite.check(sub2b(2) == 6, "sub2b(2) == 6");
  // testSuite.check(sub2b == row(tspan2,1), "sub2b == row(1)");

  auto sub2c = subTensorSpan(tspan2,std::full_extent,2);
  testSuite.check(sub2c(0) == 3, "sub2c(0) == 3");
  testSuite.check(sub2c(1) == 6, "sub2c(1) == 6");
  testSuite.check(sub2c(2) == 9, "sub2c(2) == 9");
  // testSuite.check(sub2c == column(tspan2,2), "sub2c == column(2)");

  auto sub2d = subTensorSpan(tspan2,1,2);
  testSuite.check(sub2d() == 6, "sub2d() == 6");

  return testSuite.exit();
}

#else

int main() { return 77; }

#endif
