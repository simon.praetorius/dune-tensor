#pragma once

#include <type_traits>
#include <dune/tensor/ftensor.hh>

namespace Dune::Tensor {

// matrix-matrix product
template <class F1, int m1, int m2, class F2, int n2>
auto mat_mult1 (FieldTensor<F1,m1,m2> const& a, FieldTensor<F2,m2,n2> const& b)
{
  using F = std::decay_t<decltype(std::declval<F1>()*std::declval<F2>())>;
  FieldTensor<F,m1,n2> c(0.0);
  for (int i1 = 0; i1 < m1; ++i1)
    for (int j2 = 0; j2 < n2; ++j2)
      for (int k = 0; k < m2; ++k)
        c(i1,j2) += a(i1,k) * b(k,j2);
  return c;
}

// matrix-matrix product
template <class F1, int m1, int m2, class F2, int n2>
auto mat_mult2 (FieldTensor<F1,m1,m2> const& a, FieldTensor<F2,m2,n2> const& b)
{
  using F = std::decay_t<decltype(std::declval<F1>()*std::declval<F2>())>;
  FieldTensor<F,m1,n2> c;
  for (int j2 = 0; j2 < n2; ++j2)
    for (int i1 = 0; i1 < m1; ++i1)
      for (int k = 0; k < m2; ++k)
        c(i1,j2) += a(i1,k) * b(k,j2);
  return c;
}

// matrix-matrix product
template <class F1, int m1, int m2, class F2, int n2>
auto mat_mult3 (FieldTensor<F1,m1,m2> const& a, FieldTensor<F2,m2,n2> const& b)
{
  using F = std::decay_t<decltype(std::declval<F1>()*std::declval<F2>())>;
  FieldTensor<F,m1,n2> c;
  for (int k = 0; k < m2; ++k)
    for (int i1 = 0; i1 < m1; ++i1)
      for (int j2 = 0; j2 < n2; ++j2)
        c(i1,j2) += a(i1,k) * b(k,j2);
  return c;
}

// matrix-matrix product
template <class F1, int m1, int m2, class F2, int n2>
auto mat_mult4 (FieldTensor<F1,m1,m2> const& a, FieldTensor<F2,m2,n2> const& b)
{
  using F = std::decay_t<decltype(std::declval<F1>()*std::declval<F2>())>;
  FieldTensor<F,m1,n2> c;
  for (int k = 0; k < m2; ++k)
    for (int j2 = 0; j2 < n2; ++j2)
      for (int i1 = 0; i1 < m1; ++i1)
        c(i1,j2) += a(i1,k) * b(k,j2);
  return c;
}


} // end namespace Dune::Tensor
