#include <config.h>

#include <iostream>
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/hybridutilities.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/test/testsuite.hh>

#if ENABLE_DUNE_CONCEPTS
#include <dune/concepts/common/tensor.hh>
#endif

#include <dune/tensor/dyntensor.hh>
#include <dune/tensor/ftensor.hh>
#include <dune/tensor/tensorexpression.hh>
#include <dune/tensor/operations/expressions.hh>
#include <dune/tensor/operations/operations.hh>
#include <dune/tensor/operations/tensordot.hh>

using namespace Dune;
using namespace Dune::Tensor;

template <class Tensor1, class Tensor2>
void checkEqual(Dune::TestSuite& testSuite, Tensor1 const& a, Tensor2 const& b)
{
  Dune::TestSuite subTestSuite("checkEqual");
  using index_type = std::common_type_t<typename Tensor1::index_type, typename Tensor2::index_type>;
  if constexpr(Tensor1::rank() == 0) {
    subTestSuite.check(a() == b());
  }
  else if constexpr(Tensor1::rank() == 1) {
    for (index_type i = 0; i < a.extent(0); ++i)
      subTestSuite.check(a(i) == b(i));
  }
  else if constexpr(Tensor1::rank() == 2) {
    for (index_type i = 0; i < a.extent(0); ++i)
      for (index_type j = 0; j < a.extent(1); ++j)
        subTestSuite.check(a(i,j) == b(i,j));
  }
  else if constexpr(Tensor1::rank() == 3) {
    for (index_type i = 0; i < a.extent(0); ++i)
      for (index_type j = 0; j < a.extent(1); ++j)
        for (index_type k = 0; k < a.extent(2); ++k)
          subTestSuite.check(a(i,j,k) == b(i,j,k));
  }
  testSuite.subTest(subTestSuite);
}


template <class Tensor>
void checkEqualValue(Dune::TestSuite& testSuite, Tensor const& a, typename Tensor::value_type const& value)
{
  Dune::TestSuite subTestSuite("checkEqualValue");
  using index_type = typename Tensor::index_type;
  if constexpr(Tensor::rank() == 0) {
    subTestSuite.check(a() == value);
  }
  else if constexpr(Tensor::rank() == 1) {
    for (index_type i = 0; i < a.extent(0); ++i)
      subTestSuite.check(a(i) == value);
  }
  else if constexpr(Tensor::rank() == 2) {
    for (index_type i = 0; i < a.extent(0); ++i)
      for (index_type j = 0; j < a.extent(1); ++j)
        subTestSuite.check(a(i,j) == value);
  }
  else if constexpr(Tensor::rank() == 3) {
    for (index_type i = 0; i < a.extent(0); ++i)
      for (index_type j = 0; j < a.extent(1); ++j)
        for (index_type k = 0; k < a.extent(2); ++k)
          subTestSuite.check(a(i,j,k) == value);
  }
  testSuite.subTest(subTestSuite);
}

template <class Tensor>
void checkAccess(Dune::TestSuite& testSuite, Tensor tensor)
{
  Dune::TestSuite subTestSuite("checkAccess");

  checkEqualValue(subTestSuite, tensor, 42.0);

  using index_type = typename Tensor::index_type;
  if constexpr(Tensor::rank() == 0) {
    subTestSuite.check(tensor[std::array<index_type,0>{}] == 42.0);
    subTestSuite.check(tensor() == 42.0);
  }
  else if constexpr(Tensor::rank() == 1) {
    for (index_type i = 0; i < tensor.extent(0); ++i) {
      subTestSuite.check(tensor[std::array{i}] == 42.0);
      subTestSuite.check(tensor(i) == 42.0);
      subTestSuite.check(tensor[i] == 42.0);
    }
  }
  else if constexpr(Tensor::rank() == 2) {
    for (index_type i = 0; i < tensor.extent(0); ++i) {
      for (index_type j = 0; j < tensor.extent(1); ++j) {
        subTestSuite.check(tensor[std::array{i,j}] == 42.0);
        subTestSuite.check(tensor(i,j) == 42.0);
        subTestSuite.check(tensor[i][j] == 42.0);
      }
    }
  }
  else if constexpr(Tensor::rank() == 3) {
    for (index_type i = 0; i < tensor.extent(0); ++i) {
      for (index_type j = 0; j < tensor.extent(1); ++j) {
        for (index_type k = 0; k < tensor.extent(2); ++k) {
          subTestSuite.check(tensor[std::array{i,j,k}] == 42.0);
          subTestSuite.check(tensor(i,j,k) == 42.0);
          subTestSuite.check(tensor[i][j][k] == 42.0);
        }
      }
    }
  }

  testSuite.subTest(subTestSuite);
}

template <class Tensor1, class Tensor2, class Expr>
void checkTensorExpr(Dune::TestSuite& testSuite,
  Tensor1 const& tensor1, Tensor2 const& tensor2, Expr const& expr)
{
  TensorExpression texpr1{expr, tensor1, tensor2};
  auto texpr2 = tensor1 + tensor2;
  [[maybe_unused]] auto texpr3 = texpr1 + tensor2;
  [[maybe_unused]] auto texpr4 = tensor1 + texpr1;
  [[maybe_unused]] auto texpr5 = texpr1 + texpr2;

  checkAccess(testSuite, texpr1);
  checkAccess(testSuite, texpr2);

  [[maybe_unused]] auto dyadic = tensordot<0>(texpr1, texpr2);
  if constexpr(Tensor1::rank() >= 1)
    [[maybe_unused]] auto prod1 = tensordot<1>(texpr1, texpr2);
  if constexpr(Tensor1::rank() >= 2)
    [[maybe_unused]] auto prod2 = tensordot<2>(texpr1, texpr2);
  if constexpr(Tensor1::rank() >= 3)
    [[maybe_unused]] auto prod3 = tensordot<3>(texpr1, texpr2);

  DenseTensor storage1{texpr1};
  checkEqual(testSuite, storage1, texpr1);
  DenseTensor storage2{texpr2};
  checkEqual(testSuite, storage2, texpr2);
  DenseTensor storage3{texpr3};
  checkEqual(testSuite, storage3, texpr3);
  DenseTensor storage4{texpr4};
  checkEqual(testSuite, storage4, texpr4);
  DenseTensor storage5{texpr5};
  checkEqual(testSuite, storage5, texpr5);

  auto store1 = texpr1.store();
  checkEqual(testSuite, store1, texpr1);
  auto store2 = texpr2.store();
  checkEqual(testSuite, store2, texpr2);
  auto store3 = texpr3.store();
  checkEqual(testSuite, store3, texpr3);
  auto store4 = texpr4.store();
  checkEqual(testSuite, store4, texpr4);
  auto store5 = texpr5.store();
  checkEqual(testSuite, store5, texpr5);
}

int main(int argc, char** argv)
{
  MPIHelper::instance(argc, argv);

  TestSuite testSuite;

  using Tensor0 = FieldTensor<double>;
  using Tensor1 = FieldTensor<double,2>;
  using Tensor2 = FieldTensor<double,2,2>;
  using Tensor3 = FieldTensor<double,2,2,2>;

  using DTensor0 = DynamicTensor<double,0>;
  using DTensor1 = DynamicTensor<double,1>;
  using DTensor2 = DynamicTensor<double,2>;
  using DTensor3 = DynamicTensor<double,3>;

  auto expr = [](auto const& value1, auto const& value2) { return value1 + value2; };

  { // mutable tensors
    Tensor0 tensor0(21.0);
    Tensor1 tensor1(21.0);
    Tensor2 tensor2(21.0);
    Tensor3 tensor3(21.0);

    DTensor0 dtensor0(typename DTensor0::extents_type{}, 21.0);
    DTensor1 dtensor1(typename DTensor1::extents_type{2}, 21.0);
    DTensor2 dtensor2(typename DTensor2::extents_type{2,2}, 21.0);
    DTensor3 dtensor3(typename DTensor3::extents_type{2,2,2}, 21.0);

    checkTensorExpr(testSuite, tensor0, dtensor0, expr);
    checkTensorExpr(testSuite, tensor1, dtensor1, expr);
    checkTensorExpr(testSuite, tensor2, dtensor2, expr);
    checkTensorExpr(testSuite, tensor3, dtensor3, expr);
  }

  return testSuite.exit();
}
