#include <config.h>

#include <iostream>
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/hybridutilities.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/test/testsuite.hh>

#if ENABLE_DUNE_CONCEPTS
#include <dune/concepts/common/tensor.hh>
#endif

#include <dune/tensor/ftensor.hh>

using namespace Dune;
using namespace Dune::Tensor;

template <class F>
void checkEqual(Dune::TestSuite& testSuite, FieldTensor<F> const& a, FieldTensor<F> const& b)
{
  Dune::TestSuite subTestSuite("checkEqual");
  subTestSuite.check(a() == b());
  testSuite.subTest(subTestSuite);
}

template <class F, int n>
void checkEqual(Dune::TestSuite& testSuite, FieldTensor<F,n> const& a, FieldTensor<F,n> const& b)
{
  Dune::TestSuite subTestSuite("checkEqual");
  for (int i = 0; i < n; ++i)
    subTestSuite.check(a(i) == b(i));
  testSuite.subTest(subTestSuite);
}

template <class F, int m, int n>
void checkEqual(Dune::TestSuite& testSuite, FieldTensor<F,m,n> const& a, FieldTensor<F,m,n> const& b)
{
  Dune::TestSuite subTestSuite("checkEqual");
  for (int i = 0; i < m; ++i)
    for (int j = 0; j < n; ++j)
      subTestSuite.check(a(i,j) == b(i,j));
  testSuite.subTest(subTestSuite);
}

template <class F, int n1, int n2, int n3>
void checkEqual(Dune::TestSuite& testSuite, FieldTensor<F,n1,n2,n3> const& a, FieldTensor<F,n1,n2,n3> const& b)
{
  Dune::TestSuite subTestSuite("checkEqual");
  for (int i = 0; i < n1; ++i)
    for (int j = 0; j < n2; ++j)
      for (int k = 0; k < n3; ++k)
        subTestSuite.check(a(i,j,k) == b(i,j,k));
  testSuite.subTest(subTestSuite);
}

template <class Tensor>
void checkEqualValue(Dune::TestSuite& testSuite, Tensor const& a, typename Tensor::value_type const& value)
{
  Dune::TestSuite subTestSuite("checkEqualValue");
  for (auto const& a_value : a.values())
    subTestSuite.check(a_value == value, std::to_string(a_value)+" == "+std::to_string(value));
  testSuite.subTest(subTestSuite);
}


template <class Tensor>
void checkConstructors(Dune::TestSuite& testSuite)
{
  Dune::TestSuite subTestSuite("checkConstructors");

  // default constructor
  Tensor tensor;
  checkEqualValue(subTestSuite, tensor, 0.0);

  tensor = 1.0;
  checkEqualValue(subTestSuite, tensor, 1.0);

  // constructor with a default value
  Tensor tensor1(1.0);
  checkEqual(subTestSuite, tensor, tensor1);

  // copy/move constructors
  Tensor tensor2{tensor};
  checkEqual(subTestSuite, tensor,tensor2);

  Tensor tensor3 = tensor;
  checkEqual(subTestSuite, tensor,tensor3);

  Tensor tensor4{std::move(tensor2)};
  checkEqual(subTestSuite, tensor,tensor4);

  Tensor tensor5 = std::move(tensor3);
  checkEqual(subTestSuite, tensor,tensor5);

  // copy/move assignment operators
  tensor4 = tensor;
  checkEqual(subTestSuite, tensor,tensor4);

  tensor5 = std::move(tensor4);
  checkEqual(subTestSuite, tensor,tensor5);

  if constexpr(Tensor::rank() == 1) {
    Tensor tensor6{6,7};
    Tensor tensor7{6.0,7.0};
    Tensor tensor8({6.0,7.0});
    Tensor tensor9{{6.0,7.0}};
    Tensor tensor10 = {6.0,7.0};

    subTestSuite.check(tensor6(0) == 6.0);
    subTestSuite.check(tensor6(1) == 7.0);
    subTestSuite.check(tensor7(0) == 6.0);
    subTestSuite.check(tensor7(1) == 7.0);
    subTestSuite.check(tensor8(0) == 6.0);
    subTestSuite.check(tensor8(1) == 7.0);
    subTestSuite.check(tensor9(0) == 6.0);
    subTestSuite.check(tensor9(1) == 7.0);
    subTestSuite.check(tensor10(0) == 6.0);
    subTestSuite.check(tensor10(1) == 7.0);
  }
  else
  if constexpr(Tensor::rank() == 2) {
    Tensor tensor6{{6,7},{8,9}};
    Tensor tensor7{{6.0,7.0},{8.0,9.0}};
    Tensor tensor8({{6.0,7.0},{8.0,9.0}});
    Tensor tensor9{{{6.0,7.0},{8.0,9.0}}};
    Tensor tensor10 = {{6.0,7.0},{8.0,9.0}};

    subTestSuite.check(tensor6(0,0) == 6.0);
    subTestSuite.check(tensor6(0,1) == 7.0);
    subTestSuite.check(tensor6(1,0) == 8.0);
    subTestSuite.check(tensor6(1,1) == 9.0);
    subTestSuite.check(tensor7(0,0) == 6.0);
    subTestSuite.check(tensor7(0,1) == 7.0);
    subTestSuite.check(tensor7(1,0) == 8.0);
    subTestSuite.check(tensor7(1,1) == 9.0);
    subTestSuite.check(tensor8(0,0) == 6.0);
    subTestSuite.check(tensor8(0,1) == 7.0);
    subTestSuite.check(tensor8(1,0) == 8.0);
    subTestSuite.check(tensor8(1,1) == 9.0);
    subTestSuite.check(tensor9(0,0) == 6.0);
    subTestSuite.check(tensor9(0,1) == 7.0);
    subTestSuite.check(tensor9(1,0) == 8.0);
    subTestSuite.check(tensor9(1,1) == 9.0);
    subTestSuite.check(tensor10(0,0) == 6.0);
    subTestSuite.check(tensor10(0,1) == 7.0);
    subTestSuite.check(tensor10(1,0) == 8.0);
    subTestSuite.check(tensor10(1,1) == 9.0);
  }

  testSuite.subTest(subTestSuite);
}


template <class Tensor>
void checkAccess(Dune::TestSuite& testSuite)
{
  Dune::TestSuite subTestSuite("checkAccess");

  Tensor tensor(42.0);
  checkEqualValue(subTestSuite, tensor, 42.0);

  if constexpr(Tensor::rank() == 0) {
    // subTestSuite.check(tensor[std::array<int,0>{}] == 42.0);
    subTestSuite.check(tensor() == 42.0);
    subTestSuite.check(tensor == 42.0);
  }
  else if constexpr(Tensor::rank() == 1) {
    for (std::size_t i = 0; i < Tensor::static_extent(0); ++i) {
      // subTestSuite.check(tensor[std::array{i}] == 42.0);
      subTestSuite.check(tensor(i) == 42.0);
      subTestSuite.check(tensor[i] == 42.0);
    }
  }
  else if constexpr(Tensor::rank() == 2) {
    for (std::size_t i = 0; i < Tensor::static_extent(0); ++i) {
      for (std::size_t j = 0; j < Tensor::static_extent(1); ++j) {
        // subTestSuite.check(tensor[std::array{i,j}] == 42.0);
        subTestSuite.check(tensor(i,j) == 42.0);
        subTestSuite.check(tensor[i][j] == 42.0);
      }
    }
  }
  else if constexpr(Tensor::rank() == 3) {
    for (std::size_t i = 0; i < Tensor::static_extent(0); ++i) {
      for (std::size_t j = 0; j < Tensor::static_extent(1); ++j) {
        for (std::size_t k = 0; k < Tensor::static_extent(2); ++k) {
          // subTestSuite.check(tensor[std::array{i,j,k}] == 42.0);
          subTestSuite.check(tensor(i,j,k) == 42.0);
          subTestSuite.check(tensor[i][j][k] == 42.0);
        }
      }
    }
  }

  testSuite.subTest(subTestSuite);
}


template <class Tensor>
void checkArithmetic(Dune::TestSuite& testSuite)
{
  Dune::TestSuite subTestSuite("checkArithmetic");

  Tensor tensor(1.0);
  Tensor tensor2(2.0);
  checkEqualValue(subTestSuite, tensor, 1.0);
  checkEqualValue(subTestSuite, tensor2, 2.0);

  tensor *= 2.0;
  checkEqualValue(subTestSuite, tensor, 2.0);

  tensor += tensor2;
  checkEqualValue(subTestSuite, tensor, 4.0);

  tensor.axpy(4.0, tensor2); // 12
  checkEqualValue(subTestSuite, tensor, 12.0);

  tensor.aypx(4.0, tensor2); // 50
  checkEqualValue(subTestSuite, tensor, 50.0);

  tensor -= tensor2;
  checkEqualValue(subTestSuite, tensor, 48.0);

  testSuite.subTest(subTestSuite);
}



int main(int argc, char** argv)
{
  MPIHelper::instance(argc, argv);

  TestSuite testSuite;

#if ENABLE_DUNE_CONCEPTS
  static_assert(Dune::Concept::Tensor<FieldTensor<double>, 0>);
  static_assert(Dune::Concept::Tensor<FieldTensor<double, 2>, 1>);
  static_assert(Dune::Concept::Tensor<FieldTensor<double, 2,2>, 2>);
  static_assert(Dune::Concept::Tensor<FieldTensor<double, 2,2,2>, 3>);
  static_assert(Dune::Concept::Tensor<FieldTensor<double, 2,2,2,2>, 4>);
#endif

  using Tensor0 = FieldTensor<double>;
  using Tensor1 = FieldTensor<double,2>;
  using Tensor2 = FieldTensor<double,2,2>;
  using Tensor3 = FieldTensor<double,2,2,2>;

  checkConstructors<Tensor0>(testSuite);
  checkConstructors<Tensor1>(testSuite);
  checkConstructors<Tensor2>(testSuite);
  checkConstructors<Tensor3>(testSuite);

  checkAccess<Tensor0>(testSuite);
  checkAccess<Tensor1>(testSuite);
  checkAccess<Tensor2>(testSuite);
  checkAccess<Tensor3>(testSuite);

  checkArithmetic<Tensor0>(testSuite);
  checkArithmetic<Tensor1>(testSuite);
  checkArithmetic<Tensor2>(testSuite);
  checkArithmetic<Tensor3>(testSuite);

  return testSuite.exit();
}
