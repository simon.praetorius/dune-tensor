#include <array>
#include <cassert>
#include <iostream>
#include <list>
#include <vector>

#include <dune/tensor/ranges/rangeexpression.hh>

int main()
{
  std::vector v{1.0,1.0,1.0,1.0};
  std::list l{2.0,2.0,2.0,2.0};
  std::array a{3.0,3.0,3.0,3.0};

  for (auto v_i : Dune::Tensor::RangeExpression{[](const auto& x) { return 2.0*x; }, v})
    assert(v_i == 2.0);

  for (auto l_i : Dune::Tensor::RangeExpression{[](const auto& x) { return 2.0*x; }, l})
    assert(l_i == 4.0);

  for (auto a_i : Dune::Tensor::RangeExpression{[](const auto& x) { return 2.0*x; }, a})
    assert(a_i == 6.0);
}