#include <config.h>

#include <set>
#include <dune/common/hybridutilities.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/test/testsuite.hh>
#include <dune/tensor/ftensor.hh>
#include <dune/tensor/operations/tensordot.hh>
#include <dune/tensor/utility/algorithm.hh>
#include <dune/tensor/utility/indices.hh>

#include <dune/tensor/test/matrixoperations.hh>

using namespace Dune;
using namespace Dune::Tensor;

template <class Tensor1, class Tensor2>
void checkInner(Dune::TestSuite& testSuite, Tensor1& a, Tensor2& b)
{
  a = 2.0;
  b = 3.0;
  testSuite.check(inner(a,b) == 2.0*3.0*a.size());
}


int main(int argc, char** argv)
{
  Dune::MPIHelper::instance(argc, argv);
  Dune::TestSuite testSuite;

  using Tensor1 = FieldTensor<double,7>;
  Tensor1 tensor1a, tensor1b;
  checkInner(testSuite, tensor1a, tensor1b);

  using Tensor2 = FieldTensor<double,7,7>;
  Tensor2 tensor2a, tensor2b;
  checkInner(testSuite, tensor2a, tensor2b);

  using Tensor3 = FieldTensor<double,7,7,7>;
  Tensor3 tensor3a, tensor3b;
  checkInner(testSuite, tensor3a, tensor3b);

  { // perform some tensordot product

    FieldTensor<double,3> vector1(2.0), vector2(3.0);
    FieldTensor<double,3,3> matrix1(2.0), matrix2(3.0);

    [[maybe_unused]] FieldTensor<double,3,3> vec_vec_0 = tensordot<0>(vector1, vector2);
    [[maybe_unused]] FieldTensor<double,3,3,3,3> mat_mat_0 = tensordot<0>(matrix1, matrix2);
    [[maybe_unused]] FieldTensor<double,3,3,3> vec_mat_0 = tensordot<0>(vector1, matrix2);
    [[maybe_unused]] FieldTensor<double,3,3,3> mat_vec_0 = tensordot<0>(matrix1, vector2);
    [[maybe_unused]] FieldTensor<double> vec_vec_1 = tensordot<1>(vector1, vector2);
    [[maybe_unused]] FieldTensor<double,3,3> mat_mat_1 = tensordot<1>(matrix1, matrix2);
    [[maybe_unused]] FieldTensor<double,3> vec_mat_1 = tensordot<1>(vector1, matrix2);
    [[maybe_unused]] FieldTensor<double,3> mat_vec_1 = tensordot<1>(matrix1, vector2);
    [[maybe_unused]] FieldTensor<double> mat_mat_2 = tensordot<2>(matrix1, matrix2);
  }

  { // check matrix-matrix products

    Hybrid::forEach(MakeIndexSeq<5>{}, [&testSuite](auto n) {
      static constexpr int N = decltype(n)::value+1;
      FieldTensor<double,N,N> matrix1(2.0), matrix2(3.0);

      FieldTensor<double,N,N> mat_mat_1a = tensordot<1>(matrix1, matrix2);
      FieldTensor<double,N,N> mat_mat_1b = mat_mult1(matrix1, matrix2);
      FieldTensor<double,N,N> mat_mat_1c = dot(matrix1, matrix2);

      for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N; ++j) {
          testSuite.check(mat_mat_1a(i,j) == mat_mat_1b(i,j));
          testSuite.check(mat_mat_1a(i,j) == mat_mat_1c(i,j));
        }
      }
    });
  }

  { // check multilinear map

    FieldTensor<double,3> vector1(1.0), vector2(2.0), f(3.0), g(4.0);
    FieldTensor<double,3,3> matrix(2.0);
    [[maybe_unused]] double value = matrix(vector1, vector2);

    testSuite.check(outer(f,g)(vector1,vector2) == f(vector1)*g(vector2));
  }

  { // check foreach and transform algorithms

    FieldTensor<double,3,3,3,3> tensor4{};
    tensorForEach(tensor4, [](auto& tensor4_i) { tensor4_i = 4.0; });

    double sum = 0.0;
    tensorForEach(tensor4, [&sum](auto const& value) { sum += value; }, [](auto const& tensor4_i) { return 2*tensor4_i; });

    FieldTensor<double,3,3,3,3> tensor4b{2.0}, tensor4c;
    tensorTransform(tensor4, tensor4b, tensor4c, [](auto const& va, auto const& vb) { return va + vb; });
  }

  return testSuite.exit();
}
