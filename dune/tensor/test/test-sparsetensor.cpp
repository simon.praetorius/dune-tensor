#include <config.h>

#include <set>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/std/extents.hh>
#include <dune/common/test/testsuite.hh>
#include <dune/tensor/cootensor.hh>
#include <dune/tensor/csftensor.hh>
#include <dune/tensor/dyntensor.hh>
#include <dune/tensor/ftensor.hh>
#include <dune/tensor/operations/sparsetensordot.hh>
#include <dune/tensor/operations/tensordot.hh>

using namespace Dune;
using namespace Dune::Tensor;

template <class Tensor1, class Tensor2>
void checkEqual(Dune::TestSuite& testSuite, Tensor1 const& tensor1, Tensor2 const& tensor2)
{
  std::cout << "checkEqual..." << std::endl;
  testSuite.check(tensor1.extents() == tensor2.extents());
  for (int i = 0; i < tensor1.extent(0); ++i)
    for (int j = 0; j < tensor1.extent(1); ++j)
      testSuite.check(tensor1(i,j) == tensor2(i,j),
        "("+std::to_string(i)+","+std::to_string(j)+"): "+std::to_string(tensor1(i,j))+" != "+std::to_string(tensor2(i,j)));
}

void checkInner(Dune::TestSuite& testSuite)
{
  std::cout << "checkInner..." << std::endl;
  using Tensor1 = CSFTensor<double,Std::extents<int,7,7>,7>;
  Tensor1 tensor1;
  {
    auto ins = tensor1.inserter();
    ins(3,3) = 8;
    ins(6,6) = 2;
    ins(1,1) = 9;
  }

  // std::cout << "a.offsets[0] = " << tensor1.storage().offsets()[0] << std::endl;
  // std::cout << "a.indices[0] = " << tensor1.storage().indices()[0] << std::endl;
  // std::cout << "a.indices[1] = " << tensor1.storage().indices()[1] << std::endl;
  // std::cout << "a.values = " << tensor1.values() << std::endl;

  using Tensor2 = FieldTensor<double,7,7>;
  Tensor2 tensor2;
  {
    auto ins = tensor2.inserter();
    ins(3,3) = 8;
    ins(6,6) = 2;
    ins(1,1) = 9;
  }

  checkEqual(testSuite,tensor1,tensor2);

  auto result = tensordot<2>(tensor2, tensor2);
  std::cout << "dot(b,b) = " << result << std::endl;

  {
    auto tensor1_1_sparse = sparsetensordot<2>(tensor1, tensor1);
    auto tensor1_1_dense = tensordot<2>(tensor1, tensor1);
    auto tensor2_2_sparse = sparsetensordot<2>(tensor2, tensor2);
    auto tensor2_2_dense = tensordot<2>(tensor2, tensor2);

    std::cout << "sparsedot(a,a) = " << tensor1_1_sparse << std::endl;
    std::cout << "dot(a,a) = " << tensor1_1_dense << std::endl;
    std::cout << "sparsedot(b,b) = " << tensor2_2_sparse << std::endl;

    testSuite.check(tensor1_1_sparse == result);
    testSuite.check(tensor1_1_dense == result);
    testSuite.check(tensor2_2_sparse == result);
    testSuite.check(tensor2_2_dense == result);
  }

  {
    auto tensor1_2_sparse = sparsetensordot<2>(tensor1, tensor2);
    auto tensor1_2_dense = tensordot<2>(tensor1, tensor2);

    std::cout << "sparsedot(a,b) = " << tensor1_2_sparse << std::endl;
    std::cout << "dot(a,b) = " << tensor1_2_dense << std::endl;

    testSuite.check(tensor1_2_sparse == result);
    testSuite.check(tensor1_2_dense == result);
  }

  {
    auto tensor2_1_sparse = sparsetensordot<2>(tensor2, tensor1);
    auto tensor2_1_dense = tensordot<2>(tensor2, tensor1);

    std::cout << "sparsedot(b,a) = " << tensor2_1_sparse << std::endl;
    std::cout << "dot(b,a) = " << tensor2_1_dense << std::endl;

    testSuite.check(tensor2_1_sparse == result);
    testSuite.check(tensor2_1_dense == result);
  }
}

void checkOuter(Dune::TestSuite& testSuite)
{
  std::cout << "checkOuter..." << std::endl;
  using Tensor1 = CSFTensor<double,Std::extents<int,7>,5>;
  Tensor1 tensor1;
  {
    auto ins = tensor1.inserter();
    ins(3) = 8;
    ins(6) = 2;
    ins(1) = 9;
  }

  auto tensor1_1 = sparsetensordot<0>(tensor1, tensor1);
  testSuite.check(tensor1_1(3,3) == 8.0*8.0);
  testSuite.check(tensor1_1(3,6) == 8.0*2.0);
  testSuite.check(tensor1_1(3,1) == 8.0*9.0);
  testSuite.check(tensor1_1(6,3) == 2.0*8.0);
  testSuite.check(tensor1_1(6,6) == 2.0*2.0);
  testSuite.check(tensor1_1(6,1) == 2.0*9.0);
  testSuite.check(tensor1_1(1,3) == 9.0*8.0);
  testSuite.check(tensor1_1(1,6) == 9.0*2.0);
  testSuite.check(tensor1_1(1,1) == 9.0*9.0);

  std::cout << "a*a = " << tensor1_1 << std::endl;

  using Tensor2 = FieldTensor<double,7>;
  Tensor2 tensor2;
  {
    auto ins = tensor2.inserter();
    ins(3) = 8;
    ins(6) = 2;
    ins(1) = 9;
  }

  { // test the dense tensordot
    auto tensor1_2 = tensordot<0>(tensor1, tensor2);
    testSuite.check(tensor1_2(3,3) == 8.0*8.0);
    testSuite.check(tensor1_2(3,6) == 8.0*2.0);
    testSuite.check(tensor1_2(3,1) == 8.0*9.0);
    testSuite.check(tensor1_2(6,3) == 2.0*8.0);
    testSuite.check(tensor1_2(6,6) == 2.0*2.0);
    testSuite.check(tensor1_2(6,1) == 2.0*9.0);
    testSuite.check(tensor1_2(1,3) == 9.0*8.0);
    testSuite.check(tensor1_2(1,6) == 9.0*2.0);
    testSuite.check(tensor1_2(1,1) == 9.0*9.0);
    std::cout << "a*b = " << tensor1_2 << std::endl;
  }

  { // test the sparse tensordot
    auto tensor1_2 = sparsetensordot<0>(tensor1, tensor2);
    testSuite.check(tensor1_2(3,3) == 8.0*8.0);
    testSuite.check(tensor1_2(3,6) == 8.0*2.0);
    testSuite.check(tensor1_2(3,1) == 8.0*9.0);
    testSuite.check(tensor1_2(6,3) == 2.0*8.0);
    testSuite.check(tensor1_2(6,6) == 2.0*2.0);
    testSuite.check(tensor1_2(6,1) == 2.0*9.0);
    testSuite.check(tensor1_2(1,3) == 9.0*8.0);
    testSuite.check(tensor1_2(1,6) == 9.0*2.0);
    testSuite.check(tensor1_2(1,1) == 9.0*9.0);
    std::cout << "a(*)b = " << tensor1_2 << std::endl;
  }

  { // test the sparse tensordot
    auto tensor2_2 = sparsetensordot<0>(tensor2, tensor2);
    testSuite.check(tensor2_2(3,3) == 8.0*8.0);
    testSuite.check(tensor2_2(3,6) == 8.0*2.0);
    testSuite.check(tensor2_2(3,1) == 8.0*9.0);
    testSuite.check(tensor2_2(6,3) == 2.0*8.0);
    testSuite.check(tensor2_2(6,6) == 2.0*2.0);
    testSuite.check(tensor2_2(6,1) == 2.0*9.0);
    testSuite.check(tensor2_2(1,3) == 9.0*8.0);
    testSuite.check(tensor2_2(1,6) == 9.0*2.0);
    testSuite.check(tensor2_2(1,1) == 9.0*9.0);
    std::cout << "a(*)b = " << tensor2_2 << std::endl;
  }
}


int main(int argc, char** argv)
{
  Dune::MPIHelper::instance(argc, argv);
  Dune::TestSuite testSuite;


  using Tensor1 = CSFTensor<double,Std::extents<int,7>,5>;
  Tensor1 tensor1;
  {
    auto ins = tensor1.inserter();
    ins(3) = 8;
    ins(6) = 2;
    ins(1) = 9;
  }
  // std::cout << "a.indices = " << tensor1.storage().indices()[0] << std::endl;
  // std::cout << "a.values = " << tensor1.values() << std::endl;

  testSuite.check(tensor1.size() == 3);
  testSuite.check(tensor1(3) == 8.0);
  testSuite.check(tensor1(6) == 2.0);
  testSuite.check(tensor1(1) == 9.0);

  checkOuter(testSuite);
  checkInner(testSuite);

  // using Tensor2 = SparseTensor<double,StaticExtents<int,7,7>,CSFContainer<14>>;
  using Tensor2 = CSFFieldTensor<double,7,7>;
  Tensor2 tensor2;
  {
    auto ins = tensor2.inserter();
    for (int i = 0; i < 7; ++i) {
      ins(i,i) = 2.0;
      ins(i,(i+1)%7) = -1.0;
    }
  }
  // std::cout << "b.offsets[0] = " << tensor2.storage().offsets()[0] << std::endl;
  // std::cout << "b.indices[0] = " << tensor2.storage().indices()[0] << std::endl;
  // std::cout << "b.indices[1] = " << tensor2.storage().indices()[1] << std::endl;
  // std::cout << "b.values = " << tensor2.values() << std::endl;

  testSuite.check(tensor2.size() == 14);

  // for (auto [index,value] : tensor2.storage())
  //   std::cout << index << ": " << value << std::endl;

  using Tensor3a = CSFTensor<double,Std::extents<int,8,8,8>,8>;
  std::cout << sizeof(Tensor3a) << std::endl;

  using Tensor3b = FieldTensor<double,8,8,8>;
  std::cout << sizeof(Tensor3b) << std::endl;

  using Tensor3c = COOTensor<double,Std::extents<int,8,8,8>,8>;
  std::cout << sizeof(Tensor3c) << std::endl;

}
