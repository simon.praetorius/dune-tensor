#include <config.h>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/std/extents.hh>
#include <dune/common/test/testsuite.hh>
#include <dune/tensor/slices.hh>
#include <dune/tensor/extents/sliced.hh>

using namespace Dune;
using namespace Dune::Tensor;

int main(int argc, char** argv)
{
  Dune::MPIHelper::instance(argc, argv);
  Dune::TestSuite testSuite;

  using Extents = Std::extents<int,5,6,7>;
  Extents exts;

  auto subexts1 = slicedExtents(exts, tag::all, tag::all, tag::all);
  testSuite.check(subexts1.rank() == 3);
  testSuite.check(subexts1.static_extent(0) == 5);
  testSuite.check(subexts1.extent(0) == 5);
  testSuite.check(subexts1.static_extent(1) == 6);
  testSuite.check(subexts1.extent(1) == 6);
  testSuite.check(subexts1.static_extent(2) == 7);
  testSuite.check(subexts1.extent(2) == 7);

  auto subexts2 = slicedExtents(exts, 1, tag::all, tag::all);
  testSuite.check(subexts2.rank() == 2);
  testSuite.check(subexts2.static_extent(0) == 6);
  testSuite.check(subexts2.extent(0) == 6);
  testSuite.check(subexts2.static_extent(1) == 7);
  testSuite.check(subexts2.extent(1) == 7);

  auto subexts3 = slicedExtents(exts, tag::all, 2, tag::all);
  testSuite.check(subexts3.rank() == 2);
  testSuite.check(subexts3.static_extent(0) == 5);
  testSuite.check(subexts3.extent(0) == 5);
  testSuite.check(subexts3.static_extent(1) == 7);
  testSuite.check(subexts3.extent(1) == 7);

  auto indexRange4 = IndexRange<int>{2,4};
  auto subexts4 = slicedExtents(exts, indexRange4, tag::all, tag::all);
  testSuite.check(subexts4.rank() == 3);
  testSuite.check(subexts4.static_extent(0) == tag::dynamic);
  testSuite.check(subexts4.extent(0) == 2);
  testSuite.check(subexts4.static_extent(1) == 6);
  testSuite.check(subexts4.extent(1) == 6);
  testSuite.check(subexts4.static_extent(2) == 7);
  testSuite.check(subexts4.extent(2) == 7);

  auto indexRange5 = IndexRange<std::integral_constant<int,2>,std::integral_constant<int,4>>{};
  auto subexts5 = slicedExtents(exts, tag::all, tag::all, indexRange5);
  testSuite.check(subexts5.rank() == 3);
  testSuite.check(subexts5.static_extent(0) == 5);
  testSuite.check(subexts5.extent(0) == 5);
  testSuite.check(subexts5.static_extent(1) == 6);
  testSuite.check(subexts5.extent(1) == 6);
  testSuite.check(subexts5.static_extent(2) == 2);
  testSuite.check(subexts5.extent(2) == 2);
}
