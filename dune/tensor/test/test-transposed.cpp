#include <config.h>

#include <iostream>
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/hybridutilities.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/test/testsuite.hh>

#if ENABLE_DUNE_CONCEPTS
#include <dune/concepts/common/tensor.hh>
#endif

#include <dune/tensor/ftensor.hh>
#include <dune/tensor/permuted.hh>
#include <dune/tensor/transposed.hh>

using namespace Dune;
using namespace Dune::Tensor;

template <class T1, class T2>
void checkEqual(Dune::TestSuite& testSuite, const T1& a, const T2& b)
{
  Dune::TestSuite subTestSuite("checkEqual");
  if constexpr(T1::rank() == 0) {
    subTestSuite.check(a() == b());
  }
  else if constexpr(T1::rank() == 1) {
    for (int i = 0; i < a.extent(0); ++i)
      subTestSuite.check(a(i) == b(i));
  }
  else if constexpr(T1::rank() == 2) {
    for (int i = 0; i < a.extent(0); ++i)
      for (int j = 0; j < a.extent(1); ++j)
        subTestSuite.check(a(i,j) == b(i,j));
  }
  else if constexpr(T1::rank() == 3) {
    for (int i = 0; i < a.extent(0); ++i)
      for (int j = 0; j < a.extent(1); ++j)
        for (int k = 0; k < a.extent(2); ++k)
          subTestSuite.check(a(i,j,k) == b(i,j,k));
  }
  testSuite.subTest(subTestSuite);
}

template <class T1, class T2>
void checkTransposedMatrix(Dune::TestSuite& testSuite, const T1& a, const T2& b)
{
  Dune::TestSuite subTestSuite("checkTransposedMatrix");
  static_assert(T1::rank() == 2);

  subTestSuite.check(a.extent(0) == b.extent(1), "a.extent(0) == b.extent(1)");
  subTestSuite.check(a.extent(1) == b.extent(0), "a.extent(1) == b.extent(0)");
  for (int i = 0; i < a.extent(0); ++i)
    for (int j = 0; j < a.extent(1); ++j)
      subTestSuite.check(a(i,j) == b(j,i), "a(i,j) == b(j,i)");
  assert(bool(subTestSuite));
  testSuite.subTest(subTestSuite);
}

template <class T1, class T2>
void checkTransposedTensor01(Dune::TestSuite& testSuite, const T1& a, const T2& b)
{
  Dune::TestSuite subTestSuite("checkTransposedTensor01");
  static_assert(T1::rank() == 3);

  subTestSuite.check(a.extent(0) == b.extent(1), "a.extent(0) == b.extent(1)");
  subTestSuite.check(a.extent(1) == b.extent(0), "a.extent(1) == b.extent(0)");
  for (int i = 0; i < a.extent(0); ++i)
    for (int j = 0; j < a.extent(1); ++j)
      for (int k = 0; k < a.extent(2); ++k)
        subTestSuite.check(a(i,j,k) == b(j,i,k), "a(i,j,k) == b(j,i,k)");
  assert(bool(subTestSuite));
  testSuite.subTest(subTestSuite);
}

template <class T1, class T2>
void checkTransposedTensor02(Dune::TestSuite& testSuite, const T1& a, const T2& b)
{
  Dune::TestSuite subTestSuite("checkTransposedTensor02");
  static_assert(T1::rank() == 3);

  subTestSuite.check(a.extent(0) == b.extent(2), "a.extent(0) == b.extent(2)");
  subTestSuite.check(a.extent(2) == b.extent(0), "a.extent(2) == b.extent(0)");
  for (int i = 0; i < a.extent(0); ++i)
    for (int j = 0; j < a.extent(1); ++j)
      for (int k = 0; k < a.extent(2); ++k)
        subTestSuite.check(a(i,j,k) == b(k,j,i), "a(i,j,k) == b(k,j,i)");
  assert(bool(subTestSuite));
  testSuite.subTest(subTestSuite);
}

template <class T1, class T2>
void checkTransposedTensor12(Dune::TestSuite& testSuite, const T1& a, const T2& b)
{
  Dune::TestSuite subTestSuite("checkTransposedTensor12");
  static_assert(T1::rank() == 3);

  subTestSuite.check(a.extent(1) == b.extent(2), "a.extent(1) == b.extent(2)");
  subTestSuite.check(a.extent(2) == b.extent(1), "a.extent(2) == b.extent(1)");
  for (int i = 0; i < a.extent(0); ++i)
    for (int j = 0; j < a.extent(1); ++j)
      for (int k = 0; k < a.extent(2); ++k)
        subTestSuite.check(a(i,j,k) == b(i,k,j), "a(i,j,k) == b(i,k,j)");
  assert(bool(subTestSuite));
  testSuite.subTest(subTestSuite);
}

template <class T1, class T2>
void checkHermiteanTensor12(Dune::TestSuite& testSuite, const T1& a, const T2& b)
{
  Dune::TestSuite subTestSuite("checkHermiteanTensor12");
  static_assert(T1::rank() == 3);

  subTestSuite.check(a.extent(1) == b.extent(2), "a.extent(1) == b.extent(2)");
  subTestSuite.check(a.extent(2) == b.extent(1), "a.extent(2) == b.extent(1)");
  for (int i = 0; i < a.extent(0); ++i)
    for (int j = 0; j < a.extent(1); ++j)
      for (int k = 0; k < a.extent(2); ++k)
        subTestSuite.check(std::conj(a(i,j,k)) == b(i,k,j), "std::conj(a(i,j,k)) == b(i,k,j)");
  assert(bool(subTestSuite));
  testSuite.subTest(subTestSuite);
}

template <class Tensor>
void init(Tensor& tensor)
{
  auto it = tensor.values().begin();
  for (typename Tensor::size_type i = 0; i < tensor.size(); ++i)
    *it++ = i;
}

template <class Tensor>
void initComplex(Tensor& tensor)
{
  auto it = tensor.values().begin();
  for (typename Tensor::size_type i = 0; i < tensor.size(); ++i)
    *it++ = std::complex<double>(i,(i+1)%3);
}

int main(int argc, char** argv)
{
  MPIHelper::instance(argc, argv);

  TestSuite testSuite;

  using Tensor2 = FieldTensor<double,2,3>;
  using Tensor3 = FieldTensor<double,2,3,4>;

  { // mutable tensors
    Tensor2 tensor2;
    init(tensor2);

    Tensor3 tensor3;
    init(tensor3);

    auto transposed2a = transposed<Transposition<0,1>>(tensor2);
    checkTransposedMatrix(testSuite, transposed2a, tensor2);

    auto transposed2b = transposed<Transposition<1,0>>(tensor2);
    checkTransposedMatrix(testSuite, transposed2b, tensor2);

    // transposition is symmetric
    checkEqual(testSuite, transposed2a, transposed2b);

    // A = (A^t)^t
    auto ttransposed2a = transposed<Transposition<0,1>>(transposed2a);
    checkTransposedMatrix(testSuite, transposed2a, ttransposed2a);
    checkEqual(testSuite, ttransposed2a, tensor2);


    auto transposed3a = transposed<Transposition<0,1>>(tensor3);
    checkTransposedTensor01(testSuite, transposed3a, tensor3);

    auto transposed3b= transposed<Transposition<0,2>>(tensor3);
    checkTransposedTensor02(testSuite, transposed3b, tensor3);

    auto transposed3c = transposed<Transposition<1,2>>(tensor3);
    checkTransposedTensor12(testSuite, transposed3c, tensor3);
  }

  using Tensor3Complex = FieldTensor<std::complex<double>,2,3,4>;
  {
    Tensor3Complex tensor3;
    initComplex(tensor3);
    std::cout << "tensor3Complex = " << tensor3 << std::endl;

    auto transposed3d = transposed<Transposition<1,2>, Conjugate>(tensor3);
    checkHermiteanTensor12(testSuite, transposed3d, tensor3);
    std::cout << "hermitean3Complex = " << transposed3d << std::endl;
  }

  {

    Tensor3 tensor3;
    init(tensor3);

    auto transposed3c = transposed<Transposition<1,2>>(tensor3);
    auto permuted3c = permuted<Transposition<1,2>>(tensor3);

    checkEqual(testSuite, transposed3c, permuted3c);
  }

  return testSuite.exit();
}
