#pragma once

#include <cmath>

#include <dune/tensor/operations/operations.hh>

namespace Dune::Tensor {

/// \brief Conjugate Gradient method without preconditioning.
/**
* Solve the linear system A*x=b using a Conjugate Gradient method. Returns the number of solver
* iterations.
*
* \param[in]     A  A linear operator. Must provide matrix-vector product
* \param[in,out] x  An array like container. Must be multiplicable with matrix. Is used as
*                   initial value for the iteration.
* \param[in]     b  An array-like container. Must be compatible with `x`
**/
template <class Matrix, class VectorX, class VectorB>
int cg(Matrix const& A, VectorX& x, VectorB const& b, double tol, int iter = 500)
{
  using std::abs; using std::sqrt;
  using Vector = VectorX;
  using Scalar = typename VectorX::value_type;

  Scalar rho = 0, rho_old = 0, alpha = 0;
  Vector p{x}, q{x};

  Vector r(b - dot(A, x));
  rho = inner(r, r);

  int it = 0;
  for (; it < iter; ++it) {
    if (sqrt(abs(rho)) < tol)
      break;

    if (it == 0)
      p = r;
    else
      p.aypx(rho / rho_old, r);

    q = dot(A, p);
    alpha = rho / inner(p, q);

    x.axpy(alpha, p);
    rho_old = rho;

    r.axpy(-alpha, q);
    rho = inner(r, r);
    std::cout << it << ") " << sqrt(abs(rho)) << std::endl;
  }

  return it;
}

} // end namespace Dune::Tensor
