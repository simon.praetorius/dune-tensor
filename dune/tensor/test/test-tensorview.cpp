#include <config.h>

#include <iostream>
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/hybridutilities.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/test/testsuite.hh>

#if ENABLE_DUNE_CONCEPTS
#include <dune/concepts/common/tensor.hh>
#endif

#include <dune/tensor/ftensor.hh>
#include <dune/tensor/tensorview.hh>
#include <dune/tensor/operations/tensordot.hh>

using namespace Dune;
using namespace Dune::Tensor;

template <class Tensor>
void checkEqual(Dune::TestSuite& testSuite, Tensor const& a, Tensor const& b)
{
  Dune::TestSuite subTestSuite("checkEqual");
  if constexpr(Tensor::rank() == 0) {
    subTestSuite.check(a() == b());
  }
  else if constexpr(Tensor::rank() == 1) {
    for (int i = 0; i < a.extent(0); ++i)
      subTestSuite.check(a(i) == b(i));
  }
  else if constexpr(Tensor::rank() == 2) {
    for (int i = 0; i < a.extent(0); ++i)
      for (int j = 0; j < a.extent(1); ++j)
        subTestSuite.check(a(i,j) == b(i,j));
  }
  else if constexpr(Tensor::rank() == 3) {
    for (int i = 0; i < a.extent(0); ++i)
      for (int j = 0; j < a.extent(1); ++j)
        for (int k = 0; k < a.extent(2); ++k)
          subTestSuite.check(a(i,j,k) == b(i,j,k));
  }
  testSuite.subTest(subTestSuite);
}


template <class Tensor>
void checkEqualValue(Dune::TestSuite& testSuite, Tensor const& a, typename Tensor::value_type const& value)
{
  using index_type = typename Tensor::index_type;
  Dune::TestSuite subTestSuite("checkEqualValue");
  if constexpr(Tensor::rank() == 0) {
    subTestSuite.check(a() == value);
  }
  else if constexpr(Tensor::rank() == 1) {
    for (index_type i = 0; i < a.extent(0); ++i)
      subTestSuite.check(a(i) == value);
  }
  else if constexpr(Tensor::rank() == 2) {
    for (index_type i = 0; i < a.extent(0); ++i)
      for (index_type j = 0; j < a.extent(1); ++j)
        subTestSuite.check(a(i,j) == value);
  }
  else if constexpr(Tensor::rank() == 3) {
    for (index_type i = 0; i < a.extent(0); ++i)
      for (index_type j = 0; j < a.extent(1); ++j)
        for (index_type k = 0; k < a.extent(2); ++k)
          subTestSuite.check(a(i,j,k) == value);
  }
  testSuite.subTest(subTestSuite);
}

template <class Tensor>
void checkAccess(Dune::TestSuite& testSuite, Tensor tensor)
{
  Dune::TestSuite subTestSuite("checkAccess");

  checkEqualValue(subTestSuite, tensor, 42.0);

  if constexpr(Tensor::rank() == 0) {
    subTestSuite.check(tensor[std::array<std::size_t,0>{}] == 42.0);
    subTestSuite.check(tensor() == 42.0);
  }
  else if constexpr(Tensor::rank() == 1) {
    for (std::size_t i = 0; i < Tensor::static_extent(0); ++i) {
      subTestSuite.check(tensor[std::array{i}] == 42.0);
      subTestSuite.check(tensor(i) == 42.0);
      subTestSuite.check(tensor[i] == 42.0);
    }
  }
  else if constexpr(Tensor::rank() == 2) {
    for (std::size_t i = 0; i < Tensor::static_extent(0); ++i) {
      for (std::size_t j = 0; j < Tensor::static_extent(1); ++j) {
        subTestSuite.check(tensor[std::array{i,j}] == 42.0);
        subTestSuite.check(tensor(i,j) == 42.0);
        subTestSuite.check(tensor[i][j] == 42.0);
      }
    }
  }
  else if constexpr(Tensor::rank() == 3) {
    for (std::size_t i = 0; i < Tensor::static_extent(0); ++i) {
      for (std::size_t j = 0; j < Tensor::static_extent(1); ++j) {
        for (std::size_t k = 0; k < Tensor::static_extent(2); ++k) {
          subTestSuite.check(tensor[std::array{i,j,k}] == 42.0);
          subTestSuite.check(tensor(i,j,k) == 42.0);
          subTestSuite.check(tensor[i][j][k] == 42.0);
        }
      }
    }
  }

  testSuite.subTest(subTestSuite);
}

template <class Tensor, class View>
void checkTensorView(Dune::TestSuite& testSuite, Tensor&& tensor, View const& view)
{
  TensorView tview{tensor, view};

  checkAccess(testSuite, tview);

  [[maybe_unused]] auto dyadic = tensordot<0>(tview, tview);
  if constexpr(std::decay_t<Tensor>::rank() >= 1)
    [[maybe_unused]] auto prod1 = tensordot<1>(tview, tview);
  if constexpr(std::decay_t<Tensor>::rank() >= 2)
    [[maybe_unused]] auto prod2 = tensordot<2>(tview, tview);
  if constexpr(std::decay_t<Tensor>::rank() >= 3)
    [[maybe_unused]] auto prod3 = tensordot<3>(tview, tview);
}

int main(int argc, char** argv)
{
  MPIHelper::instance(argc, argv);

  TestSuite testSuite;

  using Tensor0 = FieldTensor<double>;
  using Tensor1 = FieldTensor<double,2>;
  using Tensor2 = FieldTensor<double,2,2>;
  using Tensor3 = FieldTensor<double,2,2,2>;

  auto view = [](auto const& value) { return 2.0*value; };

  { // mutable tensors
    Tensor0 tensor0(21.0);
    Tensor1 tensor1(21.0);
    Tensor2 tensor2(21.0);
    Tensor3 tensor3(21.0);

    checkTensorView(testSuite, tensor0, view);
    checkTensorView(testSuite, tensor1, view);
    checkTensorView(testSuite, tensor2, view);
    checkTensorView(testSuite, tensor3, view);
  }

  { // const tensors
    const Tensor0 tensor0(21.0);
    const Tensor1 tensor1(21.0);
    const Tensor2 tensor2(21.0);
    const Tensor3 tensor3(21.0);

    checkTensorView(testSuite, tensor0, view);
    checkTensorView(testSuite, tensor1, view);
    checkTensorView(testSuite, tensor2, view);
    checkTensorView(testSuite, tensor3, view);
  }

  return testSuite.exit();
}
