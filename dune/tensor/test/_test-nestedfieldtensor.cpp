#include <config.h>

#include <iostream>
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/hybridutilities.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/test/testsuite.hh>

#if ENABLE_DUNE_CONCEPTS
#include <dune/concepts/common/tensor.hh>
#endif

#include <dune/tensor/nestedfieldtensor.hh>

using namespace Dune;
using namespace Dune::Tensor;

template <class F>
void checkEqual(Dune::TestSuite& testSuite, NestedFieldTensor<F> const& a, NestedFieldTensor<F> const& b)
{
  Dune::TestSuite subTestSuite("checkEqual");
  subTestSuite.check(a() == b());
  testSuite.subTest(subTestSuite);
}

template <class F, int n>
void checkEqual(Dune::TestSuite& testSuite, NestedFieldTensor<F,n> const& a, NestedFieldTensor<F,n> const& b)
{
  Dune::TestSuite subTestSuite("checkEqual");
  for (int i = 0; i < n; ++i)
    subTestSuite.check(a(i) == b(i));
  testSuite.subTest(subTestSuite);
}

template <class F, int m, int n>
void checkEqual(Dune::TestSuite& testSuite, NestedFieldTensor<F,m,n> const& a, NestedFieldTensor<F,m,n> const& b)
{
  Dune::TestSuite subTestSuite("checkEqual");
  for (int i = 0; i < m; ++i)
    for (int j = 0; j < n; ++j)
      subTestSuite.check(a(i,j) == b(i,j));
  testSuite.subTest(subTestSuite);
}

template <class F, int n1, int n2, int n3>
void checkEqual(Dune::TestSuite& testSuite, NestedFieldTensor<F,n1,n2,n3> const& a, NestedFieldTensor<F,n1,n2,n3> const& b)
{
  Dune::TestSuite subTestSuite("checkEqual");
  for (int i = 0; i < n1; ++i)
    for (int j = 0; j < n2; ++j)
      for (int k = 0; k < n3; ++k)
        subTestSuite.check(a(i,j,k) == b(i,j,k));
  testSuite.subTest(subTestSuite);
}

template <class Tensor>
void checkEqualValue(Dune::TestSuite& testSuite, Tensor const& a, typename Tensor::value_type const& value)
{
  Dune::TestSuite subTestSuite("checkEqualValue");
  for (int i = 0; i < a.size(); ++i)
    subTestSuite.check(a.data()[i] == value, "== "+std::to_string(value));
  testSuite.subTest(subTestSuite);
}


template <class Tensor>
void checkConstructors(Dune::TestSuite& testSuite)
{
  Dune::TestSuite subTestSuite("checkConstructors");

  // default constructor
  Tensor tensor;
  checkEqualValue(subTestSuite, tensor, 0.0);

  tensor = 1.0;
  checkEqualValue(subTestSuite, tensor, 1.0);

  // constructor with a default value
  Tensor tensor1(1.0);
  checkEqual(subTestSuite, tensor, tensor1);

  // copy/move constructors
  Tensor tensor2{tensor};
  checkEqual(subTestSuite, tensor,tensor2);

  Tensor tensor3 = tensor;
  checkEqual(subTestSuite, tensor,tensor3);

  Tensor tensor4{std::move(tensor2)};
  checkEqual(subTestSuite, tensor,tensor4);

  Tensor tensor5 = std::move(tensor3);
  checkEqual(subTestSuite, tensor,tensor5);

  // copy/move assignment operators
  tensor4 = tensor;
  checkEqual(subTestSuite, tensor,tensor4);

  tensor5 = std::move(tensor4);
  checkEqual(subTestSuite, tensor,tensor5);

  testSuite.subTest(subTestSuite);
}


template <class Tensor>
void checkAccess(Dune::TestSuite& testSuite)
{
  Dune::TestSuite subTestSuite("checkAccess");

  Tensor tensor(42.0);
  checkEqualValue(subTestSuite, tensor, 42.0);

  if constexpr(Tensor::rank() == 0) {
    subTestSuite.check(tensor[std::array<std::size_t,0>{}] == 42.0);
    subTestSuite.check(tensor() == 42.0);
    subTestSuite.check(tensor == 42.0);
  }
  else if constexpr(Tensor::rank() == 1) {
    for (int i = 0; i < Tensor::static_extent(0); ++i) {
      subTestSuite.check(tensor[std::array{i}] == 42.0);
      subTestSuite.check(tensor(i) == 42.0);
      subTestSuite.check(tensor[i] == 42.0);
    }
  }
  else if constexpr(Tensor::rank() == 2) {
    for (int i = 0; i < Tensor::static_extent(0); ++i) {
      for (int j = 0; j < Tensor::static_extent(1); ++j) {
        subTestSuite.check(tensor[std::array{i,j}] == 42.0);
        subTestSuite.check(tensor(i,j) == 42.0);
        subTestSuite.check(tensor[i][j] == 42.0);
      }
    }
  }
  else if constexpr(Tensor::rank() == 3) {
    for (int i = 0; i < Tensor::static_extent(0); ++i) {
      for (int j = 0; j < Tensor::static_extent(1); ++j) {
        for (int k = 0; k < Tensor::static_extent(2); ++k) {
          subTestSuite.check(tensor[std::array{i,j,k}] == 42.0);
          subTestSuite.check(tensor(i,j,k) == 42.0);
          subTestSuite.check(tensor[i][j][k] == 42.0);
        }
      }
    }
  }

  testSuite.subTest(subTestSuite);
}


template <class Tensor>
void checkArithmetic(Dune::TestSuite& testSuite)
{
  Dune::TestSuite subTestSuite("checkArithmetic");

  Tensor tensor(1.0);
  Tensor tensor2(2.0);
  checkEqualValue(subTestSuite, tensor, 1.0);
  checkEqualValue(subTestSuite, tensor2, 2.0);

  tensor *= 2.0;
  checkEqualValue(subTestSuite, tensor, 2.0);

  tensor += tensor2;
  checkEqualValue(subTestSuite, tensor, 4.0);

  tensor.axpy(4.0, tensor2); // 12
  checkEqualValue(subTestSuite, tensor, 12.0);

  tensor.aypx(4.0, tensor2); // 50
  checkEqualValue(subTestSuite, tensor, 50.0);

  tensor -= tensor2;
  checkEqualValue(subTestSuite, tensor, 48.0);

  testSuite.subTest(subTestSuite);
}



int main(int argc, char** argv)
{
  MPIHelper::instance(argc, argv);

  TestSuite testSuite;

#if ENABLE_DUNE_CONCEPTS
  static_assert(Dune::Concept::Tensor<NestedFieldTensor<double>, 0>);
  static_assert(Dune::Concept::Tensor<NestedFieldTensor<double, 2>, 1>);
  static_assert(Dune::Concept::Tensor<NestedFieldTensor<double, 2,2>, 2>);
  static_assert(Dune::Concept::Tensor<NestedFieldTensor<double, 2,2,2>, 3>);
  static_assert(Dune::Concept::Tensor<NestedFieldTensor<double, 2,2,2,2>, 4>);
#endif

  using Tensor0 = NestedFieldTensor<double>;
  using Tensor1 = NestedFieldTensor<double,2>;
  using Tensor2 = NestedFieldTensor<double,2,2>;
  using Tensor3 = NestedFieldTensor<double,2,2,2>;

  checkConstructors<Tensor0>(testSuite);
  checkConstructors<Tensor1>(testSuite);
  checkConstructors<Tensor2>(testSuite);
  checkConstructors<Tensor3>(testSuite);

  checkAccess<Tensor0>(testSuite);
  checkAccess<Tensor1>(testSuite);
  checkAccess<Tensor2>(testSuite);
  checkAccess<Tensor3>(testSuite);

  checkArithmetic<Tensor0>(testSuite);
  checkArithmetic<Tensor1>(testSuite);
  checkArithmetic<Tensor2>(testSuite);
  checkArithmetic<Tensor3>(testSuite);

  Tensor2 matrix;
  matrix = 0.0;

  auto&& vec = matrix[0];
  testSuite.check(vec.size() == 2);

  [[maybe_unused]] auto& value = vec[0];

  return testSuite.exit();
}
