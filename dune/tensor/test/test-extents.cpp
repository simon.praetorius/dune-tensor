#include <config.h>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/std/extents.hh>
#include <dune/common/test/testsuite.hh>
#include <dune/tensor/extents/dynamic.hh>
#include <dune/tensor/extents/static.hh>

using namespace Dune;
using namespace Dune::Tensor;

int main(int argc, char** argv)
{
  Dune::MPIHelper::instance(argc, argv);
  Dune::TestSuite testSuite;

  { // static extents

    using Extents0 = StaticExtents<int>;
    using Extents1 = StaticExtents<int,7>;
    using Extents2 = StaticExtents<int,7,7>;
    using Extents3 = StaticExtents<int,7,7,7>;

    [[maybe_unused]] Extents0 extents0;
    static_assert(std::is_empty_v<Extents0>);
    testSuite.check(Extents0::rank() == 0);

    Extents1 extents1;
    static_assert(std::is_empty_v<Extents1>);
    testSuite.check(Extents1::rank() == 1);
    testSuite.check(Extents1::static_extent(0) == 7);
    testSuite.check(extents1.extent(0) == 7);

    Extents2 extents2;
    static_assert(std::is_empty_v<Extents2>);
    testSuite.check(Extents2::rank() == 2);
    testSuite.check(Extents2::static_extent(0) == 7);
    testSuite.check(Extents2::static_extent(1) == 7);
    testSuite.check(extents2.extent(0) == 7);
    testSuite.check(extents2.extent(1) == 7);

    Extents3 extents3;
    static_assert(std::is_empty_v<Extents3>);
    testSuite.check(Extents3::rank() == 3);
    testSuite.check(Extents3::static_extent(0) == 7);
    testSuite.check(Extents3::static_extent(1) == 7);
    testSuite.check(Extents3::static_extent(2) == 7);
    testSuite.check(extents3.extent(0) == 7);
    testSuite.check(extents3.extent(1) == 7);
    testSuite.check(extents3.extent(2) == 7);

    constexpr Extents0 cextents0;
    static_assert(cextents0.extent(0) == 1);

    constexpr Extents1 cextents1;
    static_assert(cextents1.extent(0) == 7);

    constexpr Extents2 cextents2;
    static_assert(cextents2.extent(0) == 7);
    static_assert(cextents2.extent(1) == 7);

    constexpr Extents3 cextents3;
    static_assert(cextents3.extent(0) == 7);
    static_assert(cextents3.extent(1) == 7);
    static_assert(cextents3.extent(2) == 7);
  }

  { // dynamic extents

    using Extents0 = DynamicExtents<int,0>;
    using Extents1 = DynamicExtents<int,1>;
    using Extents2 = DynamicExtents<int,2>;
    using Extents3 = DynamicExtents<int,3>;

    [[maybe_unused]] Extents0 extents0{};
    testSuite.check(Extents0::rank() == 0);

    Extents1 extents1{7};
    testSuite.check(Extents1::rank() == 1);
    testSuite.check(Extents1::static_extent(0) == Std::dynamic_extent);
    testSuite.check(extents1.extent(0) == 7);

    Extents2 extents2{7,7};
    testSuite.check(Extents2::rank() == 2);
    testSuite.check(Extents2::static_extent(0) == Std::dynamic_extent);
    testSuite.check(Extents2::static_extent(1) == Std::dynamic_extent);
    testSuite.check(extents2.extent(0) == 7);
    testSuite.check(extents2.extent(1) == 7);

    Extents3 extents3{7,7,7};
    testSuite.check(Extents3::rank() == 3);
    testSuite.check(Extents3::static_extent(0) == Std::dynamic_extent);
    testSuite.check(Extents3::static_extent(1) == Std::dynamic_extent);
    testSuite.check(Extents3::static_extent(2) == Std::dynamic_extent);
    testSuite.check(extents3.extent(0) == 7);
    testSuite.check(extents3.extent(1) == 7);
    testSuite.check(extents3.extent(2) == 7);

    constexpr Extents0 cextents0{};
    static_assert(cextents0.extent(0) == 1);

    constexpr Extents1 cextents1{7};
    static_assert(cextents1.extent(0) == 7);

    constexpr Extents2 cextents2{7,7};
    static_assert(cextents2.extent(0) == 7);
    static_assert(cextents2.extent(1) == 7);

    constexpr Extents3 cextents3{7,7,7};
    static_assert(cextents3.extent(0) == 7);
    static_assert(cextents3.extent(1) == 7);
    static_assert(cextents3.extent(2) == 7);
  }


  { // hybrid extents

    using Extents0 = Dune::Std::extents<int>;
    using Extents1a = Dune::Std::extents<int,2>;
    using Extents1b = Dune::Std::extents<int,Std::dynamic_extent>;
    using Extents2a = Dune::Std::extents<int,2,3>;
    using Extents2b = Dune::Std::extents<int,2,Std::dynamic_extent>;
    using Extents2c = Dune::Std::extents<int,Std::dynamic_extent,3>;
    using Extents2d = Dune::Std::extents<int,Std::dynamic_extent,Std::dynamic_extent>;
    using Extents3a = Dune::Std::extents<int,2,3,4>;
    using Extents3b = Dune::Std::extents<int,2,Std::dynamic_extent,4>;

    [[maybe_unused]] Extents0 extents0{};
    static_assert(std::is_empty_v<Extents0>);
    testSuite.check(Extents0::rank() == 0);

    Extents1a extents1a{2};
    Extents1b extents1b{2};
    static_assert(std::is_empty_v<Extents1a>);
    testSuite.check(Extents1a::rank() == 1);
    testSuite.check(Extents1b::rank() == 1);
    testSuite.check(Extents1a::static_extent(0) == 2);
    testSuite.check(Extents1b::static_extent(0) == Std::dynamic_extent);
    testSuite.check(extents1a.extent(0) == 2);
    testSuite.check(extents1b.extent(0) == 2);

    Extents2a extents2a{2,3};
    Extents2b extents2b{2,3};
    Extents2c extents2c{2,3};
    Extents2d extents2d{2,3};
    static_assert(std::is_empty_v<Extents2a>);
    testSuite.check(Extents2a::rank() == 2);
    testSuite.check(Extents2b::rank() == 2);
    testSuite.check(Extents2c::rank() == 2);
    testSuite.check(Extents2d::rank() == 2);
    testSuite.check(Extents2a::static_extent(0) == 2);
    testSuite.check(Extents2a::static_extent(1) == 3);
    testSuite.check(Extents2b::static_extent(0) == 2);
    testSuite.check(Extents2b::static_extent(1) == Std::dynamic_extent);
    testSuite.check(Extents2c::static_extent(0) == Std::dynamic_extent);
    testSuite.check(Extents2c::static_extent(1) == 3);
    testSuite.check(Extents2d::static_extent(0) == Std::dynamic_extent);
    testSuite.check(Extents2d::static_extent(1) == Std::dynamic_extent);
    testSuite.check(extents2a.extent(0) == 2);
    testSuite.check(extents2a.extent(1) == 3);
    testSuite.check(extents2b.extent(0) == 2);
    testSuite.check(extents2b.extent(1) == 3);
    testSuite.check(extents2c.extent(0) == 2);
    testSuite.check(extents2c.extent(1) == 3);
    testSuite.check(extents2d.extent(0) == 2);
    testSuite.check(extents2d.extent(1) == 3);

    Extents3a extents3a{2,3,4};
    Extents3b extents3b{2,3,4};
    static_assert(std::is_empty_v<Extents3a>);
    testSuite.check(Extents3a::rank() == 3);
    testSuite.check(Extents3b::rank() == 3);
    testSuite.check(Extents3a::static_extent(0) == 2);
    testSuite.check(Extents3a::static_extent(1) == 3);
    testSuite.check(Extents3a::static_extent(2) == 4);
    testSuite.check(Extents3b::static_extent(0) == 2);
    testSuite.check(Extents3b::static_extent(1) == Std::dynamic_extent);
    testSuite.check(Extents3b::static_extent(2) == 4);
    testSuite.check(extents3a.extent(0) == 2);
    testSuite.check(extents3a.extent(1) == 3);
    testSuite.check(extents3a.extent(2) == 4);
    testSuite.check(extents3b.extent(0) == 2);
    testSuite.check(extents3b.extent(1) == 3);
    testSuite.check(extents3b.extent(2) == 4);
  }
}
