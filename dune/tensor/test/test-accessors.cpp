#include <config.h>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/std/default_accessor.hh>

#include <dune/tensor/accessors/alignedaccessor.hh>
#include <dune/tensor/accessors/concepts.hh>
#include <dune/tensor/accessors/restrictaccessor.hh>

#if DUNE_HAVE_CXX_ATOMIC_REF
  #include <dune/tensor/accessors/atomicaccessor.hh>
#endif

using namespace Dune::Tensor;

int main(int argc, char** argv)
{
  Dune::MPIHelper::instance(argc, argv);

  static_assert(Dune::Concept::Accessor<AlignedAccessor<double,alignof(double)>>);
  static_assert(Dune::Concept::Accessor<AlignedAccessor<const double,alignof(double)>>);
  static_assert(Dune::Concept::Accessor<Dune::Std::default_accessor<double>>);
  static_assert(Dune::Concept::Accessor<Dune::Std::default_accessor<const double>>);
  static_assert(Dune::Concept::Accessor<RestrictAccessor<double>>);
  static_assert(Dune::Concept::Accessor<RestrictAccessor<const double>>);

#if DUNE_HAVE_CXX_ATOMIC_REF
  static_assert(Dune::Concept::Accessor<AtomicAccessor<double>>);
  static_assert(Dune::Concept::Accessor<AtomicAccessor<const double>>);
#endif
}
