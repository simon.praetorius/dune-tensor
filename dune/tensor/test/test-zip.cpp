#include <cassert>
#include <iostream>
#include <list>
#include <vector>

#include <dune/tensor/ranges/zippedrange.hh>

int main()
{
  std::vector v{1.0,2.0,3.0,4.0};
  std::list l{4.0,3.0,2.0,1.0};

  for (auto [v_i,l_i] : Dune::Tensor::zip(v,l))
    assert(v_i+l_i == 5.0);
}