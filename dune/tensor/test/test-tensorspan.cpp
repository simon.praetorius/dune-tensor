#include <config.h>

#include <iostream>
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/hybridutilities.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/test/testsuite.hh>

#if ENABLE_DUNE_CONCEPTS
#include <dune/concepts/common/tensor.hh>
#endif

#include <dune/tensor/ftensor.hh>
#include <dune/tensor/tensorspan.hh>
#include <dune/tensor/operations/tensordot.hh>

#include <dune/tensor/accessors/restrictaccessor.hh>
#if DUNE_HAVE_CXX_ATOMIC_REF
  #include <dune/tensor/accessors/atomicaccessor.hh>
#endif

using namespace Dune;
using namespace Dune::Tensor;

template <class T, class E, class L, class A>
void checkEqual(Dune::TestSuite& testSuite, TensorSpan<T,E,L,A> a, TensorSpan<T,E,L,A> b)
{
  Dune::TestSuite subTestSuite("checkEqual");
  if constexpr(E::rank() == 0) {
    subTestSuite.check(a() == b());
  }
  else if constexpr(E::rank() == 1) {
    for (int i = 0; i < a.extent(0); ++i)
      subTestSuite.check(a(i) == b(i));
  }
  else if constexpr(E::rank() == 2) {
    for (int i = 0; i < a.extent(0); ++i)
      for (int j = 0; j < a.extent(1); ++j)
        subTestSuite.check(a(i,j) == b(i,j));
  }
  else if constexpr(E::rank() == 3) {
    for (int i = 0; i < a.extent(0); ++i)
      for (int j = 0; j < a.extent(1); ++j)
        for (int k = 0; k < a.extent(2); ++k)
          subTestSuite.check(a(i,j,k) == b(i,j,k));
  }
  testSuite.subTest(subTestSuite);
}


template <class Tensor>
void checkEqualValue(Dune::TestSuite& testSuite, Tensor const& a, typename Tensor::value_type const& value)
{
  Dune::TestSuite subTestSuite("checkEqualValue");
  for (auto const& a_value : a.values())
    subTestSuite.check(a_value == value, "== "+std::to_string(value));
  testSuite.subTest(subTestSuite);
}

template <class Tensor>
void checkAccess(Dune::TestSuite& testSuite, Tensor tensor)
{
  Dune::TestSuite subTestSuite("checkAccess");

  checkEqualValue(subTestSuite, tensor, 42.0);

  if constexpr(Tensor::rank() == 0) {
    subTestSuite.check(tensor[std::array<std::size_t,0>{}] == 42.0);
    subTestSuite.check(tensor() == 42.0);
  }
  else if constexpr(Tensor::rank() == 1) {
    for (std::size_t i = 0; i < Tensor::static_extent(0); ++i) {
      subTestSuite.check(tensor[std::array{i}] == 42.0);
      subTestSuite.check(tensor(i) == 42.0);
      subTestSuite.check(tensor[i] == 42.0);
    }
  }
  else if constexpr(Tensor::rank() == 2) {
    for (std::size_t i = 0; i < Tensor::static_extent(0); ++i) {
      for (std::size_t j = 0; j < Tensor::static_extent(1); ++j) {
        subTestSuite.check(tensor[std::array{i,j}] == 42.0);
        subTestSuite.check(tensor(i,j) == 42.0);
        subTestSuite.check(tensor[i][j] == 42.0);
      }
    }
  }
  else if constexpr(Tensor::rank() == 3) {
    for (std::size_t i = 0; i < Tensor::static_extent(0); ++i) {
      for (std::size_t j = 0; j < Tensor::static_extent(1); ++j) {
        for (std::size_t k = 0; k < Tensor::static_extent(2); ++k) {
          subTestSuite.check(tensor[std::array{i,j,k}] == 42.0);
          subTestSuite.check(tensor(i,j,k) == 42.0);
          subTestSuite.check(tensor[i][j][k] == 42.0);
        }
      }
    }
  }

  testSuite.subTest(subTestSuite);
}

template <class Tensor>
void checkTensorSpan(Dune::TestSuite& testSuite, Tensor&& tensor)
{
  using T = std::decay_t<Tensor>;
  TensorSpan tspan1{tensor.container_data(),tensor.mapping()};
  TensorSpan tspan2{tspan1};
  TensorSpan tspan3{tspan1};

  checkAccess(testSuite, tspan3);

  [[maybe_unused]] auto dyadic = tensordot<0>(tspan1, tspan2);
  if constexpr(T::rank() >= 1)
    [[maybe_unused]] auto prod1 = tensordot<1>(tspan1, tspan2);
  if constexpr(T::rank() >= 2)
    [[maybe_unused]] auto prod2 = tensordot<2>(tspan1, tspan2);
  if constexpr(T::rank() >= 3)
    [[maybe_unused]] auto prod3 = tensordot<3>(tspan1, tspan2);

  using E = std::conditional_t<std::is_const_v<std::remove_reference_t<Tensor>>,
    std::add_const_t<typename T::value_type>,
    std::remove_const_t<typename T::value_type>>;

  RestrictAccessor<E> accessor4;
  TensorSpan tspan4{tensor.container_data(),tensor.mapping(),accessor4};
  checkAccess(testSuite, tspan4);
  tensordot<T::rank()>(tspan4, tspan4);

#if DUNE_HAVE_CXX_ATOMIC_REF
  AtomicAccessor<E> accessor5;
  TensorSpan tspan5{tensor.container_data(),tensor.mapping(),accessor5};
  checkAccess(testSuite, tspan5);
  tensordot<T::rank()>(tspan5, tspan5);
#endif
}

int main(int argc, char** argv)
{
  MPIHelper::instance(argc, argv);

  TestSuite testSuite;

  using Tensor0 = FieldTensor<double>;
  using Tensor1 = FieldTensor<double,2>;
  using Tensor2 = FieldTensor<double,2,2>;
  using Tensor3 = FieldTensor<double,2,2,2>;

  { // mutable tensors
    Tensor0 tensor0(42.0);
    Tensor1 tensor1(42.0);
    Tensor2 tensor2(42.0);
    Tensor3 tensor3(42.0);

    checkTensorSpan(testSuite, tensor0);
    checkTensorSpan(testSuite, tensor1);
    checkTensorSpan(testSuite, tensor2);
    checkTensorSpan(testSuite, tensor3);
  }

  { // const tensors
    const Tensor0 tensor0(42.0);
    const Tensor1 tensor1(42.0);
    const Tensor2 tensor2(42.0);
    const Tensor3 tensor3(42.0);

    checkTensorSpan(testSuite, tensor0);
    checkTensorSpan(testSuite, tensor1);
    checkTensorSpan(testSuite, tensor2);
    checkTensorSpan(testSuite, tensor3);
  }

  return testSuite.exit();
}
