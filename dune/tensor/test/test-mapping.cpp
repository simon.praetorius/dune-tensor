#include <config.h>

#include <set>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/test/testsuite.hh>
#include <dune/tensor/ftensor.hh>

int main(int argc, char** argv)
{
  Dune::MPIHelper::instance(argc, argv);
  Dune::TestSuite testSuite;

  using Tensor0 = Dune::Tensor::FieldTensor<double>;
  using Tensor1 = Dune::Tensor::FieldTensor<double,7>;
  using Tensor2 = Dune::Tensor::FieldTensor<double,7,7>;
  using Tensor3 = Dune::Tensor::FieldTensor<double,7,7,7>;

  using Mapping0 = typename Tensor0::mapping_type;
  using Mapping1 = typename Tensor1::mapping_type;
  using Mapping2 = typename Tensor2::mapping_type;
  using Mapping3 = typename Tensor3::mapping_type;

  Mapping0 map0;
  Mapping1 map1;
  Mapping2 map2;
  Mapping3 map3;

  testSuite.check(map0() == 0);

  for (int i = 0; i < 7; ++i)
    testSuite.check(map1(i) == i);

  for (int i = 0; i < 7; ++i)
    for (int j = 0; j < 7; ++j)
      testSuite.check(map2(i,j) == j*7 + i);

  std::set<int> indices;
  for (int i = 0; i < 7; ++i)
    for (int j = 0; j < 7; ++j)
      for (int k = 0; k < 7; ++k)
        indices.insert(map3(i,j,k));
  testSuite.check(indices.size() == 7*7*7);
}
