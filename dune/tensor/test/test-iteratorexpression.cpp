#include <array>
#include <cassert>
#include <iostream>
#include <list>
#include <vector>

#include <dune/tensor/ranges/iteratorrange.hh>
#include <dune/tensor/ranges/iteratorexpression.hh>

int main()
{
  std::vector v{1.0,1.0,1.0,1.0};
  std::list l{2.0,2.0,2.0,2.0};
  std::array a{3.0,3.0,3.0,3.0};

  auto factor_2 = [](const auto& x) { return 2.0*x; };

  auto v_begin = Dune::Tensor::IteratorExpression{factor_2, v.begin()};
  auto v_end = Dune::Tensor::IteratorExpression{factor_2, v.end()};
  for (auto v_i : Dune::Tensor::IteratorRange{v_begin,v_end})
    assert(v_i == 2.0);

  auto l_begin = Dune::Tensor::IteratorExpression{factor_2, l.begin()};
  auto l_end = Dune::Tensor::IteratorExpression{factor_2, l.end()};
  for (auto l_i : Dune::Tensor::IteratorRange{l_begin,l_end})
    assert(l_i == 4.0);

  // test constructors
  [[maybe_unused]] decltype(l_begin) l_iter1;
  [[maybe_unused]] decltype(l_begin) l_iter2 = l_iter1;
  [[maybe_unused]] decltype(l_begin) l_iter3 = std::move(l_iter1);

  auto a_begin = Dune::Tensor::IteratorExpression{factor_2, a.begin()};
  auto a_end = Dune::Tensor::IteratorExpression{factor_2, a.end()};
  for (auto a_i : Dune::Tensor::IteratorRange{a_begin,a_end})
    assert(a_i == 6.0);

  // test constructors
  [[maybe_unused]] decltype(a_begin) a_iter1;
  [[maybe_unused]] decltype(a_begin) a_iter2 = a_iter1;
  [[maybe_unused]] decltype(a_begin) a_iter3 = std::move(a_iter1);
}