#include <config.h>

#include <iostream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/std/span.hh>
#include <dune/common/test/testsuite.hh>
#include <dune/tensor/containers/capacitystorage.hh>
#include <dune/tensor/containers/concepts.hh>
#include <dune/tensor/containers/fixedsizecontainer.hh>
#include <dune/tensor/containers/dynamicsizecontainer.hh>

int main(int argc, char** argv)
{
  using namespace Dune;
  MPIHelper::instance(argc, argv);

  TestSuite testSuite;

  static_assert(Concept::Container<Tensor::FixedSizeContainer, double, 0>);
  static_assert(Concept::Container<Tensor::FixedSizeContainer, double, 1>);
  static_assert(Concept::Container<Tensor::FixedSizeContainer, double, 2>);
  static_assert(Concept::Container<Tensor::FixedSizeContainer, double, 3>);
  static_assert(Concept::Container<Tensor::FixedSizeContainer, double, 4>);

  static_assert(Concept::Container<Tensor::DynamicSizeContainer, double>);

  static_assert(Concept::Container<Tensor::CapacityContainer<10>, double, 0>);
  static_assert(Concept::Container<Tensor::CapacityContainer<10>, double, 1>);
  static_assert(Concept::Container<Tensor::CapacityContainer<10>, double, 2>);
  static_assert(Concept::Container<Tensor::CapacityContainer<10>, double, 3>);
  static_assert(Concept::Container<Tensor::CapacityContainer<10>, double, 4>);

  static_assert(Concept::Container<Tensor::CapacityContainer<Std::dynamic_extent>, double, 0>);
  static_assert(Concept::Container<Tensor::CapacityContainer<Std::dynamic_extent>, double, 1>);
  static_assert(Concept::Container<Tensor::CapacityContainer<Std::dynamic_extent>, double, 2>);
  static_assert(Concept::Container<Tensor::CapacityContainer<Std::dynamic_extent>, double, 3>);
  static_assert(Concept::Container<Tensor::CapacityContainer<Std::dynamic_extent>, double, 4>);

  return testSuite.exit();
}