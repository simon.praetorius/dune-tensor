#pragma once

#include <functional>

#include <dune/tensor/tensorexpression.hh>

namespace Dune::Tensor {

/// \brief Expression representing the sum of two tensors of the same shape
template <class Tensor1, class Tensor2,
  std::enable_if_t<(Tensor1::rank() == Tensor2::rank()), int> = 0>
TensorExpression<std::plus<>,Tensor1,Tensor2>
operator+ (const Tensor1& tensor1, const Tensor2& tensor2) noexcept
{
  return {std::plus<>{}, tensor1, tensor2};
}

/// \brief Expression representing the difference of two tensors of the same shape
template <class Tensor1, class Tensor2,
  std::enable_if_t<(Tensor1::rank() == Tensor2::rank()), int> = 0>
TensorExpression<std::minus<>,Tensor1,Tensor2>
operator- (const Tensor1& tensor1, const Tensor2& tensor2) noexcept
{
  return {std::minus<>{}, tensor1, tensor2};
}


} // end namespace Dune::Tensor
