#pragma once

#include <type_traits>

#include <dune/common/densevector.hh>
#include <dune/common/ftraits.hh>
#include <dune/tensor/operations/sparsetensordot.hh>
#include <dune/tensor/operations/tensordot.hh>

namespace Dune::Tensor {

template <class,class,class,class,class> class SparseTensor;

/// Tensor operations
/// @{

/// \brief Dot-product of two tensors.
/**
 * Sum over the last axis of `a` and the first axis of `b`.
 * NOTE: equivalent to `tensordot<1>(a,b)
 *
 * \b Examples:
 * - vector inner product: `c = a(i) * b(i)`
 * - matrix-matrix product: `c(i,j) = a(i,k) * b(k,j)`
 * - matrix-vector product: `c(i) = a(i,j) * b(j)`
 **/
template <class Tensor1, class Tensor2,
  std::enable_if_t<(Tensor1::rank() >= 1 && Tensor2::rank() >= 1), int> = 0>
auto dot (const Tensor1& a, const Tensor2& b)
{
  return tensordot<1>(a,b);
}

/// \brief Dot-product of two tensors, stored in the output tensor.
template <class Tensor1, class Tensor2, class OutTensor,
  std::enable_if_t<(Tensor1::rank() >= 1 && Tensor2::rank() >= 1), int> = 0,
  std::enable_if_t<(OutTensor::rank() == Tensor1::rank()-1+Tensor2::rank()-1), int> = 0>
void dot (const Tensor1& a, const Tensor2& b, OutTensor& out)
{
  tensordot<1>(a,b,out);
}

/// \brief Dot-product of two tensors where one is sparse
template <class F1, class E1, class L1, class C1, class T1, class Tensor2,
  std::enable_if_t<(E1::rank() >= 1 && Tensor2::rank() >= 1), int> = 0>
auto dot (const SparseTensor<F1,E1,L1,C1,T1>& a, const Tensor2& b)
{
  return sparsetensordot<1>(a,b);
}

/// \brief Dot-product of two tensors where one is sparse
template <class Tensor1, class F2, class E2, class L2, class C2, class T2,
  std::enable_if_t<(Tensor1::rank() >= 1 && E2::rank() >= 1), int> = 0>
auto dot (const Tensor1& a, const SparseTensor<F2,E2,L2,C2,T2>& b)
{
  return sparsetensordot<1>(a,b);
}

/// \brief Dot-product of two sparse tensors
template <class F1, class E1, class L1, class C1, class T1,
          class F2, class E2, class L2, class C2, class T2,
  std::enable_if_t<(E1::rank() >= 1 && E2::rank() >= 1), int> = 0>
auto dot (const SparseTensor<F1,E1,L1,C1,T1>& a, const SparseTensor<F2,E2,L2,C2,T2>& b)
{
  return sparsetensordot<1>(a,b);
}

namespace Impl {

// full contraction of indices
template <class Tensor1, class Tensor2>
auto innerDefault (const Tensor1& a, const Tensor2& b)
{
  return tensordot<Tensor1::rank(),ConjMultAdd>(a,b);
}

// iteration over the flat representation of all entries
template <class Tensor1, class Tensor2>
auto innerFast (const Tensor1& a, const Tensor2& b)
{
  using V1 = typename Tensor1::value_type;
  using V2 = typename Tensor2::value_type;
  using V = typename ConjMultAdd::template result_t<V1,V2>;

  auto const span_a = a.values();
  auto const span_b = b.values();
  auto it_a = span_a.begin();
  auto it_b = span_b.begin();
  auto end_a = span_a.end();

  V result(0);
  for (; it_a != end_a; ++it_a, ++it_b)
    ConjMultAdd{}(*it_a, *it_b, result);
  return result;
}

} // end namespace Impl

/// \brief inner-product of two tensors.
/**
 * The inner product of tensors always results in a scalar. It is thus a tensordot
 * over `N` axes, where `N` equals the rank of the two tensors. Note, the elements
 * of the second tensor are conjugated during the product.
 *
 * \b Examples:
 * - vector inner product = dot product: `c = a(i) * conj(b(i))`
 * - matrix inner product = Frobenius inner product : `c = a(i,j) * conj(b(i,j))`
 **/
template <class Tensor1, class Tensor2,
  std::enable_if_t<(Tensor1::rank() == Tensor2::rank()), int> = 0>
auto inner (const Tensor1& a, const Tensor2& b)
{
  assert(a.extents() == b.extents());

  using V1 = typename Tensor1::value_type;
  using V2 = typename Tensor2::value_type;
  using V = typename ConjMultAdd::template result_t<V1,V2>;

  if constexpr (std::is_same_v<Tensor1, Tensor2>)
    return V(Impl::innerFast(a,b));

  return V(Impl::innerDefault(a,b));
}

/// \brief outer-product of two tensors.
/**
 * The outer product of tensors results in a tensor of rank=rank(a)+rank(b). Note,
 * the elements of the second tensor are conjugated during the product.
 *
 * \b Examples:
 * - vector outer product: `c(i,j) = a(i) * conj(b(j))`
 **/
template <class Tensor1, class Tensor2,
  std::enable_if_t<(Tensor1::rank() == Tensor2::rank()), int> = 0>
auto outer (const Tensor1& a, const Tensor2& b)
{
  return tensordot<0,ConjMultAdd>(a,b);
}


/// \brief one norm (sum over absolute values of entries)
template <class Tensor,
  class real_type = typename Dune::FieldTraits<typename Tensor::value_type>::real_type>
constexpr real_type one_norm (const Tensor& tensor) noexcept
{
  using std::abs;
  auto const values = tensor.values();
  real_type result{0};
  for (auto const& value : values)
    result += abs(value);
  return result;
}

/// \brief square of two norm (sum over squared values of entries)
template <class Tensor,
  class real_type = typename Dune::FieldTraits<typename Tensor::value_type>::real_type>
constexpr real_type frobenius_norm2 (const Tensor& tensor) noexcept
{
  using std::abs;
  auto const values = tensor.values();
  real_type result{0};
  for (auto const& value : values)
    result += fvmeta::abs2(value);
  return result;
}

/// \brief square of two norm (sum over squared values of entries)
template <class Tensor,
  std::enable_if_t<(Tensor::rank() == 1), int> = 0,
  class real_type = typename Dune::FieldTraits<typename Tensor::value_type>::real_type>
constexpr real_type two_norm2 (const Tensor& tensor) noexcept
{
  return frobenius_norm2(tensor);
}

/// \brief two norm (sqrt of sum over squared values of entries)
template <class Tensor,
  class real_type = typename Dune::FieldTraits<typename Tensor::value_type>::real_type>
constexpr real_type frobenius_norm (const Tensor& tensor) noexcept
{
  return fvmeta::sqrt(frobenius_norm2(tensor));
}

/// \brief two norm (sqrt of sum over squared values of entries)
template <class Tensor,
  std::enable_if_t<(Tensor::rank() == 1), int> = 0,
  class real_type = typename Dune::FieldTraits<typename Tensor::value_type>::real_type>
constexpr real_type two_norm (const Tensor& tensor) noexcept
{
  return fvmeta::sqrt(two_norm2(tensor));
}

/// \brief infinity norm (maximum of absolute values of entries)
template <class Tensor,
  class real_type = typename Dune::FieldTraits<typename Tensor::value_type>::real_type>
constexpr real_type infinity_norm (const Tensor& tensor) noexcept
{
  using std::abs; using std::max;
  auto const values = tensor.values();
  real_type result{0};
  for (auto const& value : values)
    result = max(real_type(abs(value)), result);
  return result;
}
/// @}

} // end namespace Dune::Tensor
