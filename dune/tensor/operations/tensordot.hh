#pragma once

#include <iostream>
#include <complex>
#include <numeric>
#include <type_traits>

#include <dune/common/std/extents.hh>

#include <dune/tensor/tags.hh>
#include <dune/tensor/extents/traits.hh>
#include <dune/tensor/utility/updater.hh>
#include <dune/tensor/layouts/traits.hh>
#include <dune/tensor/utility/array.hh>
#include <dune/tensor/utility/indices.hh>

namespace Dune::Tensor {

// forward declarations
template <class V, class E, class L, class C>
class DenseTensor;

template <class V, int... exts>
class FieldTensor;

struct DynamicSizeContainer;
struct FixedSizeContainer;

namespace Impl {

// Define the result tensor type of the dot operation.
// If N indices are contracted, the first rank1-N extents of tensor1 and
// the last rank2-N extents of tensor2 are combined to build the extents
// of the output tensor. The storage type is a FixedSizeContainer if all
// extents are static, otherwise a DynamicSizeContainer.
// The index mapping (layout) is given by `L` and is determined by `LayoutTraits?.`.
template <class V, class L, std::size_t I0, std::size_t J0,
          class E1, std::size_t... I, class E2, std::size_t... J>
auto makeTensor (const E1& e1, IndexSeq<I...>, const E2& e2, IndexSeq<J...>)
{
  using S = std::common_type_t<typename E1::index_type, typename E2::index_type>;
  using E = Std::extents<S, E1::static_extent(I+I0)...,E2::static_extent(J+J0)...>;

  constexpr bool any_dynamic = (... || (E1::static_extent(I+I0) == Std::dynamic_extent))
                            || (... || (E2::static_extent(J+J0) == Std::dynamic_extent));

  if constexpr(any_dynamic) {
    using Tensor = DenseTensor<V,E,L,DynamicSizeContainer>;
    return Tensor(E{S(e1.extent(I+I0))...,S(e2.extent(J+J0))...}, V(0));
  } else {
    using Tensor = FieldTensor<V,int(E1::static_extent(I+I0))...,int(E2::static_extent(J+J0))...>;
    return Tensor(V(0));
  }
}

// Check that the extents of the two tensors are compatible in the dot product, i.e.,
// the last N extents of tensor1 must be equal to the first N extents of tensor2.
template <std::size_t I0, std::size_t J0, class E1, class E2, std::size_t... I>
bool checkExtents(const E1& e1, const E2& e2, IndexSeq<I...>)
{
  static constexpr std::size_t rank = sizeof...(I);

  using S = std::common_type_t<typename E1::index_type, typename E2::index_type>;
  return std::array<S,rank>{S(e1.extent(I+I0))...} == std::array<S,rank>{S(e2.extent(I+J0))...};
}

template <std::size_t I0, std::size_t J0, class E1, class E2>
constexpr bool checkExtents(const E1& e1, const E2& e2, IndexSeq<>)
{
  return true;
}

// Perform a recursive nested loop over all indices
template <std::size_t I, std::size_t N1,
          std::size_t J, std::size_t N2,
          std::size_t K, std::size_t N3>
struct RecursiveLoop
{
  // copy the indices [i] from a to c
  template <class U, class T1, class T2, class T3>
  static void apply (const U& update, const T1& a, const T2& b, T3& c,
    std::array<typename T1::index_type,T1::rank()> indices_a = {},
    std::array<typename T2::index_type,T2::rank()> indices_b = {},
    std::array<typename T3::index_type,T3::rank()> indices_c = {})
  {
    for (typename T1::index_type i = 0; i < a.extent(I); ++i) {
      indices_a[I] = i;
      indices_c[I] = i;
      RecursiveLoop<I+1,N1,J,N2,K,N3>::apply(update,a,b,c,indices_a,indices_b,indices_c);
    }
  }
};

template <std::size_t N1,
          std::size_t J, std::size_t N2,
          std::size_t K, std::size_t N3>
struct RecursiveLoop<N1,N1,J,N2,K,N3>
{
  // copy the indices [N3+j] of b to indices[N1+j] of c
  template <class U, class T1, class T2, class T3>
  static void apply (const U& update, const T1& a, const T2& b, T3& c,
    std::array<typename T1::index_type,T1::rank()> indices_a = {},
    std::array<typename T2::index_type,T2::rank()> indices_b = {},
    std::array<typename T3::index_type,T3::rank()> indices_c = {})
  {
    for (typename T2::index_type j = 0; j < b.extent(N3+J); ++j) {
      indices_b[N3+J] = j;
      indices_c[N1+J] = j;
      RecursiveLoop<N1,N1,J+1,N2,K,N3>::apply(update,a,b,c,indices_a,indices_b,indices_c);
    }
  }
};

template <std::size_t N1,
          std::size_t N2,
          std::size_t K, std::size_t N3>
struct RecursiveLoop<N1,N1,N2,N2,K,N3>
{
  // combine the last N3 indices of a with the first N3 indices of b
  template <class U, class T1, class T2, class T3>
  static void apply (const U& update, const T1& a, const T2& b, T3& c,
    std::array<typename T1::index_type,T1::rank()> indices_a = {},
    std::array<typename T2::index_type,T2::rank()> indices_b = {},
    std::array<typename T3::index_type,T3::rank()> indices_c = {})
  {
    for (typename T2::index_type k = 0; k < b.extent(K); ++k) {
      indices_a[N1+K] = k;
      indices_b[K] = k;
      RecursiveLoop<N1,N1,N2,N2,K+1,N3>::apply(update,a,b,c,indices_a,indices_b,indices_c);
    }
  }
};

template <std::size_t N1, std::size_t N2, std::size_t N3>
struct RecursiveLoop<N1,N1,N2,N2,N3,N3>
{
  template <class U, class T1, class T2, class T3>
  static void apply (const U& update, const T1& a, const T2& b, T3& c,
    std::array<typename T1::index_type,T1::rank()> indices_a = {},
    std::array<typename T2::index_type,T2::rank()> indices_b = {},
    std::array<typename T3::index_type,T3::rank()> indices_c = {})
  {
    update(a[indices_a], b[indices_b], c[indices_c]);
  }
};

} // end namespace Impl


/// \brief Product of two tensors, stored in the output tensor.
template <std::size_t N, class Updater = MultAdd, class Tensor1, class Tensor2, class OutTensor,
  std::enable_if_t<(Tensor1::rank() >= N && Tensor2::rank() >= N), int> = 0,
  std::enable_if_t<(OutTensor::rank() == Tensor1::rank()-N+Tensor2::rank()-N), int> = 0>
void tensordot (const Tensor1& a, const Tensor2& b, OutTensor& out, Index<N> axes = {}, Updater updater = {})
{
  // perform multiplication by recursively looping over all dimensions
  Impl::RecursiveLoop<0,Tensor1::rank()-N,0,Tensor2::rank()-N,0,N>::apply(updater,a,b,out);
}


/// \brief Product of two tensors.
/**
 * Sum over the last `N` axes of `a` and the first `N` axes of `b`
 *
 * \b Examples:
 * - outer product of 2-tensors: `c(i,j,k,l) = a(i,j) * b(k,l)` is `tensordot<0>(a,b)`
 * - matrix-matrix product: `c(i,j) = a(i,k) * b(k,j)` is `tensordot<1>(a,b)`
 * - inner product of 2-tensors: `c = a(i,j) * b(i,j)` is `tensordot<2>(a,b)`
 **/
template <std::size_t N, class Updater = MultAdd, class Tensor1, class Tensor2,
  std::enable_if_t<(Tensor1::rank() >= N && Tensor2::rank() >= N), int> = 0>
auto tensordot (const Tensor1& a, const Tensor2& b, Index<N> axes = {}, Updater updater = {})
{
  // the last N extents of a and the first N extents of b must match
  assert((Impl::checkExtents<Tensor1::rank()-N,0>(a.extents(), b.extents(), MakeIndexSeq<N>{})));

  // create result tensor by collecting the extents of a and b that are not folded
  using V1 = typename Tensor1::reference;
  using V2 = typename Tensor2::reference;
  using V = typename Updater::template result_t<V1,V2>;
  using L = typename LayoutTraits<Tensor1,Tensor2>::type;
  auto c = Impl::makeTensor<V,L,0,N>(a.extents(), MakeIndexSeq<Tensor1::rank()-N>{},
                                     b.extents(), MakeIndexSeq<Tensor2::rank()-N>{});
  tensordot(a,b,c,axes,updater);
  return c;
}


} // end namespace Dune::Tensor
