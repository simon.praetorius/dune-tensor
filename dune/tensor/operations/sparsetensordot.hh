#pragma once

#include <numeric>
#include <type_traits>

#include <dune/common/rangeutilities.hh>
#include <dune/tensor/layouts/traits.hh>
#include <dune/tensor/operations/tensordot.hh>
#include <dune/tensor/ranges/rangewrapper.hh>


namespace Dune::Tensor {
namespace Impl {

template <std::size_t I, std::size_t N1,
          std::size_t J, std::size_t N2,
          std::size_t K, std::size_t N3>
struct RecursiveSparseLoop
{
  template <bool a_dense, bool b_dense, class U, class T1, class T2, class T3>
  static void apply (const U& update, const T1& a, const T2& b, T3& c,
    std::array<typename T3::size_type,T3::rank()> indices_c = {})
  {
    for (auto&& [a_i, i] : Dune::sparseRange(a)) {
      indices_c[I] = i;
      RecursiveSparseLoop<I+1,N1,J,N2,K,N3>::template apply<a_dense,b_dense>(update,a_i,b,c,indices_c);
    }
  }
};

template <std::size_t N1,
          std::size_t J, std::size_t N2,
          std::size_t K, std::size_t N3>
struct RecursiveSparseLoop<N1,N1,J,N2,K,N3>
{
  // if tensor a is dense
  template <bool a_dense, bool b_dense, class U, class T1, class T2, class T3>
  static void impl1 (const U& update, const T1& a, const T2& b, T3& c,
    std::array<typename T3::size_type,T3::rank()> indices_c = {})
  {
    static_assert(a_dense);
    for (auto&& [b_i, i] : Dune::sparseRange(b))
      RecursiveSparseLoop<N1,N1,J,N2,K+1,N3>::template apply<a_dense,b_dense>(update,a.begin()[i],b_i,c,indices_c);
  }

  // if tensor b is dense
  template <bool a_dense, bool b_dense, class U, class T1, class T2, class T3>
  static void impl2 (const U& update, const T1& a, const T2& b, T3& c,
    std::array<typename T3::size_type,T3::rank()> indices_c = {})
  {
    static_assert(b_dense);
    for (auto&& [a_i, i] : Dune::sparseRange(a))
      RecursiveSparseLoop<N1,N1,J,N2,K+1,N3>::template apply<a_dense,b_dense>(update,a_i,b.begin()[i],c,indices_c);
  }

  // if both tensors are sparse
  template <bool a_dense, bool b_dense, class U, class T1, class T2, class T3>
  static void impl3 (const U& update, const T1& a, const T2& b, T3& c,
    std::array<typename T3::size_type,T3::rank()> indices_c = {})
  {
    for (auto&& [a_i, i] : Dune::sparseRange(a)) {
      // find overlapping indices
      auto b_it = b.begin();
      auto b_end = b.end();
      for (; b_it != b_end; ++b_it) {
        if (i == b_it.index())
          break;
      }

      if (b_it != b_end)
        RecursiveSparseLoop<N1,N1,J,N2,K+1,N3>::template apply<a_dense,b_dense>(update,a_i,*b_it,c,indices_c);
    }
  }

  // choose in which way to iterate over a nd b depending on their sparsity
  template <bool a_dense, bool b_dense, class U, class T1, class T2, class T3>
  static void apply (const U& update, const T1& a, const T2& b, T3& c,
    std::array<typename T3::size_type,T3::rank()> indices_c = {})
  {
    if constexpr(a_dense)
      impl1<a_dense,b_dense>(update,a,b,c,indices_c);
    else if constexpr(b_dense)
      impl2<a_dense,b_dense>(update,a,b,c,indices_c);
    else
      impl3<a_dense,b_dense>(update,a,b,c,indices_c);
  }
};

template <std::size_t N1,
          std::size_t J, std::size_t N2,
          std::size_t N3>
struct RecursiveSparseLoop<N1,N1,J,N2,N3,N3>
{
  template <bool a_dense, bool b_dense, class U, class T1, class T2, class T3>
  static void apply (const U& update, const T1& a, const T2& b, T3& c,
    std::array<typename T3::size_type,T3::rank()> indices_c = {})
  {
    for (auto&& [b_j, j] : Dune::sparseRange(b)) {
      indices_c[N1+J] = j;
      RecursiveSparseLoop<N1,N1,J+1,N2,N3,N3>::template apply<a_dense,b_dense>(update,a,b_j,c,indices_c);
    }
  }
};

template <std::size_t N1, std::size_t N2, std::size_t N3>
struct RecursiveSparseLoop<N1,N1,N2,N2,N3,N3>
{
  template <bool a_dense, bool b_dense, class U, class T1, class T2, class T3>
  static void apply (const U& update, const T1& a, const T2& b, T3& c,
    std::array<typename T3::size_type,T3::rank()> indices_c = {})
  {
    update(a, b, c[indices_c]);
  }
};

} // end namespace Impl


/// \brief A recursive tensor dot-product implemented using `Dune::sparseRange` over all dimension.
template <std::size_t N, class Updater = MultAdd, class Tensor1, class Tensor2, class OutTensor,
  std::enable_if_t<(Tensor1::rank() >= N && Tensor2::rank() >= N), int> = 0>
auto sparsetensordot (const Tensor1& a, const Tensor2& b, OutTensor& c,
                      Index<N> axes = {}, Updater updater = {})
{
  // perform multiplication by recursively looping over all dimensions
  Impl::RecursiveSparseLoop<0,Tensor1::rank()-N,0,Tensor2::rank()-N,0,N>
    ::template apply<Tensor1::is_always_dense(),Tensor2::is_always_dense()>(updater,left_of(a),left_of(b),c);
}


/// \brief A recursive tensor dot-product implemented using `Dune::sparseRange` over all dimension.
template <std::size_t N, class Updater = MultAdd, class Tensor1, class Tensor2,
  std::enable_if_t<(Tensor1::rank() >= N && Tensor2::rank() >= N), int> = 0>
auto sparsetensordot (const Tensor1& a, const Tensor2& b,
                      Index<N> axes = {}, Updater updater = {})
{
  // the last N extents of a and the first N extents of b must match
  assert((Impl::checkExtents<Tensor1::rank()-N,0>(a.extents(), b.extents(), MakeIndexSeq<N>{})));

  // create result tensor by collecting the extents of a and b that are not folded
  using V1 = typename Tensor1::value_type;
  using V2 = typename Tensor2::value_type;
  using V = typename Updater::template result_t<V1,V2>;
  using L = typename LayoutTraits<Tensor1,Tensor2>::type;
  auto c = Impl::makeTensor<V,L,0,N>(a.extents(), MakeIndexSeq<Tensor1::rank()-N>{},
                                     b.extents(), MakeIndexSeq<Tensor2::rank()-N>{});
  sparsetensordot(a,b,c,axes,updater);
  return c;
}

} // end namespace Dune::Tensor
