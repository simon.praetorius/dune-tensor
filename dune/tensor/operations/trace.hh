#pragma once

#include <cassert>
#include <type_traits>

#include <dune/common/std/extents.hh>
#include <dune/tensor/diagonaltensor.hh>
#include <dune/tensor/operations/tensordot.hh>

namespace Dune::Tensor {

/// \brief Compute the tensor trace of the last two indices
template <class Tensor,
  std::enable_if_t<(Tensor::rank() > 2), int> = 0>
auto trace (const Tensor& tensor)
{
  assert(tensor.extent(Tensor::rank()-2) == tensor.extent(Tensor::rank()-1));

  using E = typename Tensor::extents_type;
  using S = typename E::size_type;
  using value_type = typename Tensor::value_type;
  using extents_type = Std::extents<S, E::static_extent(Tensor::rank()-2), E::static_extent(Tensor::rank()-1)>;
  extents_type extents{tensor.extent(Tensor::rank()-2),tensor.extent(Tensor::rank()-1)};

  return tensordot<2>(tensor, DiagonalTensor{extents, value_type(1)});
}

/// \brief Compute the matrix trace
template <class Matrix,
  std::enable_if_t<(Matrix::rank() == 2), int> = 0>
auto trace (const Matrix& matrix)
{
  assert(matrix.extent(0) == matrix.extent(1));
  using index_type = typename Matrix::index_type;
  using value_type = typename Matrix::value_type;

  value_type result(0);
  for (index_type i = 0; i < matrix.extent(0); ++i)
    result += matrix(i,i);
  return result;
}

} // end namespace Dune::Tensor
