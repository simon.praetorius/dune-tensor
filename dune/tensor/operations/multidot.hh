#pragma once

#include <type_traits>
#include <dune/tensor/operations/operations.hh>

namespace Dune::Tensor {
namespace Impl {

template <std::size_t I>
struct Multidot
{
  template <class Tensor, class... Vectors>
  static auto apply (const Tensor& tensor, const Vectors&... vectors)
  {
    return Multidot<I-1>::apply(Dune::Tensor::dot(tensor,std::get<I>(std::tie(vectors...))),vectors...);
  }
};

template <>
struct Multidot<0>
{
  template <class Tensor, class... Vectors>
  static auto apply (const Tensor& tensor, const Vectors&... vectors)
  {
    return Dune::Tensor::dot(tensor,std::get<0>(std::tie(vectors...)));
  }
};

} // end namespace Impl


template <class Tensor,
  std::enable_if_t<(Tensor::rank() == 0), int> = 0>
auto multidot (const Tensor& tensor) { return tensor; }

/// \brief Multiply a rank-N tensor by N vectors
template <class Tensor, class Vector,
  std::enable_if_t<(Tensor::rank() == 1), int> = 0,
  std::enable_if_t<(Vector::rank() == 1), int> = 0>
auto multidot (const Tensor& tensor, const Vector& vector)
{
  return Dune::Tensor::dot(tensor,vector);
}

/// \brief Multiply a rank-N tensor by N vectors
template <class Tensor, class... Vectors,
  std::enable_if_t<(Tensor::rank() == sizeof...(Vectors)), int> = 0,
  std::enable_if_t<(... && (Vectors::rank() == 1)), int> = 0>
auto multidot (const Tensor& tensor, const Vectors&... vectors)
{
  return Impl::Multidot<Tensor::rank()-1>::apply(tensor,vectors...);
}

} // end namespace Dune::Tensor
