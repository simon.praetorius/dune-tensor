#pragma once

#include <dune/common/std/extents.hh>

#include <dune/tensor/sparsetensor.hh>
#include <dune/tensor/containers/capacitystorage.hh>
#include <dune/tensor/inserters/sparseinserter.hh>
#include <dune/tensor/iterators/cooiterators.hh>
#include <dune/tensor/layouts/coolayout.hh>

namespace Dune::Tensor {

struct COOTraits
{
  template <class T>
  using Inserter = SparseInserter<T>;

  using Iterators = COOIterators;
};


/**
 * \brief A sparse tensor with coordinate storage.
 * \ingroup Tensors
 * \nosubgrouping
 *
 * \tparam Value    The element type stored in the tensor.
 * \tparam Extents  The type representing the tensor extents. Must be compatible
 *                  to `Dune::Tensor::Extents`.
 * \tparam capacity  A static bound for the number of nonzeros or `Std::dynamic_extent`.
 **/
template <class Value, class Extents, std::size_t capacity = Std::dynamic_extent>
using COOTensor
  = SparseTensor<Value,Extents,COOLayout<capacity>,CapacityContainer<capacity>,COOTraits>;

/// Specialization of DerivativeTraits for SparseTensor valued maps
/// @{
template <class V, class S, std::size_t... exts, std::size_t capacity, class K, int dim>
struct DerivativeTraits<SparseTensor<V,Std::extents<S,exts...>,COOLayout<capacity>,CapacityContainer<capacity>,COOTraits>(Dune::FieldVector<K,dim>)>
{
  static constexpr std::size_t newCapacity = (capacity == Std::dynamic_extent ? Std::dynamic_extent : capacity*dim);
  using Range = SparseTensor<V,Std::extents<S,exts...,dim>,COOLayout<newCapacity>,CapacityContainer<newCapacity>,COOTraits>;
};

template <class V, class S, std::size_t... exts, std::size_t capacity, class K, int dim>
struct DerivativeTraits<SparseTensor<V,Std::extents<S,exts...>,COOLayout<capacity>,CapacityContainer<capacity>,COOTraits>(FieldTensor<K,dim>)>
{
  static constexpr std::size_t newCapacity = (capacity == Std::dynamic_extent ? Std::dynamic_extent : capacity*dim);
  using Range = SparseTensor<V,Std::extents<S,exts...,dim>,COOLayout<newCapacity>,CapacityContainer<newCapacity>,COOTraits>;
};
/// @}

} // end namespace Dune::Tensor
