// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
// SPDX-FileCopyrightInfo: Copyright © DUNE Project contributors, see file LICENSE.md in module root
// SPDX-License-Identifier: LicenseRef-GPL-2.0-only-with-DUNE-exception
#ifndef DUNE_COMMON_STD_IMPL_EXPLICIT_HH
#define DUNE_COMMON_STD_IMPL_EXPLICIT_HH

#if __has_include(<version>)
#include <version>
#endif

#ifndef DUNE_EXPLICIT
#if  __cpp_conditional_explicit >= 201806L
  #define DUNE_EXPLICIT(...) explicit(__VA_ARGS__)
#else
  #define DUNE_EXPLICIT(...)
#endif
#endif // DUNE_EXPLICIT

#endif // DUNE_COMMON_STD_IMPL_EXPLICIT_HH
