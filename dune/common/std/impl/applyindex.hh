// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
// SPDX-FileCopyrightInfo: Copyright © DUNE Project contributors, see file LICENSE.md in module root
// SPDX-License-Identifier: LicenseRef-GPL-2.0-only-with-DUNE-exception
#ifndef DUNE_COMMON_STD_IMPL_APPLY_INDEX_HH
#define DUNE_COMMON_STD_IMPL_APPLY_INDEX_HH

#include <type_traits>
#include <utility>

namespace Dune::Std {
namespace Impl {

template<class F, std::size_t... i>
inline __attribute__((always_inline))
constexpr decltype(auto) applyIndexHelper(F&& f, std::index_sequence<i...>)
{
  return f(std::integral_constant<std::size_t,i>{}...);
}

template<std::size_t N, class F>
inline __attribute__((always_inline))
constexpr decltype(auto) applyIndex(F&& f, std::integral_constant<std::size_t,N> = {})
{
  return Impl::applyIndexHelper(std::forward<F>(f),
    std::make_index_sequence<N>{});
}

} // end namespace Impl
} // end namespace Dune::Std

#endif // DUNE_COMMON_STD_IMPL_APPLY_INDEX_HH
